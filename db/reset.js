db.counters.drop();
db.users.drop();
db.groups.drop();
db.photos.drop();
db.tiles.drop();
db.sits.drop();

db.groups.insert({ name : "su",     id : 1 });
db.groups.insert({ name : "admins", id : 2 });
db.groups.insert({ name : "users",  id : 3 });

db.users.insert({ name : "root",  is_admin : "true",  password : "wfD3fc8qTvJPI", id : 1, groups : [ 1, 2 ], groups_admin : [ 1, 2 ] });
db.users.insert({ name : "admin", is_admin : "true",  password : "im6WcKWZ1MwyQ", id : 2, groups : [ 2, 3 ], groups_admin : [ 2, 3 ] });
db.users.insert({ name : "oko",   is_admin : "false", password : "cuVgNgg7TBOAc", id : 3, groups : [ 3 ], groups_admin : [ ]   });
db.users.insert({ name : "ktr",   is_admin : "false", password : "apNRkHF5AVNJ2", id : 4, groups : [ 3 ], groups_admin : [ ]   });

db.counters.insert({ name: "userId", lastId: 4 });
db.counters.insert({ name: "groupId", lastId: 3 });

