// load the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var server = mongoose.Schema({

    // server name
    name: { 
        type : String, 
        default: ''
    },
    // server url
    url: { 
        type : String, 
        default: ''
    },
    // last sync time
    utc: { 
        type : Number, 
        default: 0
    },
    // last sync time
    status: { 
        type : String, 
        default: ''
    }
});

module.exports = mongoose.model('Server', server);
