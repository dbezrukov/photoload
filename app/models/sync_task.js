// load the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var syncTask = mongoose.Schema({

    interval: { 
        type : Number, 
        default: 60
    },
    localAddress: {
    	type: String,
    	default: 'http://127.0.0.1:3010'
    }
});

module.exports = mongoose.model('SyncTask', syncTask);
