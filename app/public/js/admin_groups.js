require([
    'jquery', 
    'bootstrap',
    'bootstrap-multiselect', 
	'css!stylesheets/bootstrap-multiselect'
], function($) {

    var ui = {};

    $(function() {

    	populateGroupTable();

	    // group management
	    $('#btnAddGroup').on('click', addGroup);
	    
	    ui.dialog = $('#modalGroupEdit');
	    ui.dialog.btnSubmit = ui.dialog.find('#btnSubmit');
	    ui.dialog.form = ui.dialog.find('form');
	    ui.dialog.form.name = ui.dialog.form.find('#groupName');
	    ui.dialog.form.admins = ui.dialog.form.find('#groupAdmins');
	    ui.dialog.form.users = ui.dialog.form.find('#groupUsers');
	    ui.dialog.form.regP1Lat  = ui.dialog.form.find('#regP1Lat');
	    ui.dialog.form.regP1Lon  = ui.dialog.form.find('#regP1Lon');
	    ui.dialog.form.regP2Lat  = ui.dialog.form.find('#regP2Lat');
	    ui.dialog.form.regP2Lon  = ui.dialog.form.find('#regP2Lon');

	    ui.dialog.btnSubmit.on('click', groupSubmit);
	    
	    ui.dialog.on('keypress', function(e) {
	        if (e.keyCode == 13) {
	            groupSubmit();
	        }
	    });
	    
	    ui.dialog.form.admins.removeClass('hidden');
	    ui.dialog.form.admins.multiselect({
	        nonSelectedText: 'Не выбрано',
	        nSelectedText: 'выбрано',
	        numberDisplayed: 4
	    });

	    ui.dialog.form.users.removeClass('hidden');
	    ui.dialog.form.users.multiselect({
	        nonSelectedText: 'Не выбрано',
	        nSelectedText: 'выбрано',
	        numberDisplayed: 4
	    });

	    $('#groupList table tbody').on('click', 'td a.linkeditegroup', editGroup);
	    $('#groupList table tbody').on('click', 'td a.linkdeletegroup', deleteGroup);
    });

	function populateGroupTable() {
	    var tableContent = '';

	    $.getJSON( '/groups', function( data ) {
	        
	        $.each(data, function(index){
	            tableContent += '<tr>';
	            tableContent += '<td>' + this.id + '</td>';
	            tableContent += '<td>' + this.name + '</td>';
	            tableContent += '<td>';
	            tableContent += '<a href="#" class="linkeditegroup" style="text-decoration: none" rel="' + this.id + '">Редактировать</a> &middot ';
	            tableContent += '<a href="#" class="linkdeletegroup" style="text-decoration: none" rel="' + this.id + '">Удалить</a>';
	            tableContent += '</td>';
	            tableContent += '</tr>';
	        });

	        $('#groupList table tbody').html(tableContent);
	    });
	};

	function addGroup(event) {
	    event.preventDefault();

	    ui.dialog.form.attr('groupId', '');
	    ui.dialog.form.name.val('');

	    ui.dialog.form.regP1Lat.val('89');
	    ui.dialog.form.regP1Lon.val('-180');
	    ui.dialog.form.regP2Lat.val('-89');
	    ui.dialog.form.regP2Lon.val('180');

	    // get available groups
	    $.getJSON( '/users', function( users ) {
	        ui.dialog.form.admins.multiselect('dataprovider', multiselectData(users));
	        ui.dialog.form.users.multiselect('dataprovider', multiselectData(users));

	        ui.dialog.form.admins.multiselect('select', window.user.id, true);
	        ui.dialog.form.users.multiselect('select', window.user.id, true);    

	        ui.dialog.btnSubmit.text('Создать');
	        ui.dialog.modal('show');
	    });

	    return false;    
	};

	function editGroup(event) {
	    event.preventDefault();

	    var id = $(this).attr('rel');

	    ui.dialog.form.attr('groupId', id);

	    // get available users
	    $.getJSON( '/users', function( users ) {
	        ui.dialog.form.admins.multiselect('dataprovider', multiselectData(users));
	        ui.dialog.form.users.multiselect('dataprovider', multiselectData(users));

	        $.getJSON( '/groups/' + id, function( data ) {
	            ui.dialog.form.name.val(data.name);

	            ui.dialog.form.regP1Lat.val(data.region ? data.region.p1Lat : 89);
	            ui.dialog.form.regP1Lon.val(data.region ? data.region.p1Lon : -180);
	            ui.dialog.form.regP2Lat.val(data.region ? data.region.p2Lat : -89);
	            ui.dialog.form.regP2Lon.val(data.region ? data.region.p2Lon : 180);

	            data.admins.forEach(function(userId) {
	                ui.dialog.form.admins.multiselect('select', userId.id, true);
	            });

	            data.users.forEach(function(userId) {
	                ui.dialog.form.users.multiselect('select', userId.id, true);
	            });

	            ui.dialog.btnSubmit.text('Сохранить');
	            ui.dialog.modal('show');
	        });
	    });

	    return false;
	};

	function deleteGroup(event) {
	    event.preventDefault();

	    var confirmation = confirm('Удалить эту группу?');

	    if (confirmation !== true) {
	        return false;
	    }

	    $.ajax({
	        type: 'DELETE',
	        url: '/groups/' + $(this).attr('rel')
	    })
	    .done(function( response ) {
	        populateGroupTable();
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	};

	function groupSubmit(event) {
	    var name = ui.dialog.form.name.val();
	    var admins = ui.dialog.form.admins.val();
	    var users = ui.dialog.form.users.val();
	    var region = {
	        p1Lat: ui.dialog.form.regP1Lat.val(),
	        p1Lon: ui.dialog.form.regP1Lon.val(),
	        p2Lat: ui.dialog.form.regP2Lat.val(),
	        p2Lon: ui.dialog.form.regP2Lon.val()
	    }

	    // show whether we're editing a group or creating one
	    var groupId = ui.dialog.form.attr('groupId');

	    var groupData = {};

	    if (jQuery.inArray("", [ name ]) !== -1) {
	        alert('Пожалуйста, введите имя');
	        return false;
	    }

	    groupData.name = name;
	    groupData.admins = admins;
	    groupData.users = users;
	    groupData.region = region;

	    ui.dialog.modal('hide');

	    $.ajax({
	        type: 'POST',
	        data: groupData,
	        url: groupId === '' ? '/groups' : '/groups/' + groupId,
	        dataType: 'JSON'
	    })
	    .done(function( response ) {
	        populateGroupTable();
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	}
	
});

