$(document).ready(function () {

    $('.captcha_refresh').click(function (event) {
        refreshCapcha();
    });

    $(document).mousemove(function (event) {
        TweenLite.to($('body'), .5, { css: {
            'background-position': parseInt(event.pageX / 8) + "px " + parseInt(event.pageY / 12) + "px, " + parseInt(event.pageX / 15) + "px " + parseInt(event.pageY / 15) + "px, " + parseInt(event.pageX / 30) + "px " + parseInt(event.pageY / 30) + "px"
        }});
    });

    $('#btnLogin').click(login);

    var form = $('#loginDialog form');
    form.find('input').on('keypress', function(e) {
        if (e.keyCode == 13) {
            login();
        }
    });
});

function login(event) {
    var form = $('#loginDialog form');

    var info = { 
        name: form.find('#loginName').val(), 
        password: form.find('#loginPassword').val(), 
        captcha: form.find('#loginCaptcha').val()
    };

    var alert = form.find('.alert');

    $.ajax({ 
        type: 'POST', 
        url: '/login', 
        data: info, 
        dataType: 'json' 
    })
        .done(function( response ) {
            alert.hide();
            window.location.href = window.successRedirect || '/';
        })
        .fail(function(jqXHR, textStatus, err) {
            refreshCapcha();
            alert.text(JSON.parse(jqXHR.responseText).error);
            alert.show();
        });
};

function refreshCapcha() {
    var img = document.getElementById('captcha_img');
    if (img) {
    	img.src = '/captcha.png?' + new Date().getTime();
    }
};
