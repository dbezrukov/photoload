define([
    'underscore',
    'app',
    'marionette'
    ,'buzz'
], function(_, app, Marionette, buzz) {

    var sounds = {
        'call-in' : new buzz.sound( "/sounds/call-in", { formats: [ "ogg", "mp3", "aac" ], preload: true }),
        'call-out' : new buzz.sound( "/sounds/call-out", { formats: [ "ogg", "mp3", "aac" ], preload: true }),
    }

    var activeSound;

    var SoundNotifications = app.module("SoundNotifications", function() {
        this.startWithParent = false;
        
        this.addInitializer(function(options){
            options || (options = {});

            var self = this;
        })

        this.addFinalizer(function(){
        })
    });

    SoundNotifications.playSound = function(name) {
        if (activeSound) {
            activeSound.stop();
        }

        if (name in sounds) {
            activeSound = sounds[name];
            activeSound.setVolume(100);
            activeSound.play();

            var timer = setInterval(function() {
                if (activeSound) {
                    activeSound.play();
                } else {
                    clearInterval(timer);
                }
            }, 4000);
        } else {
            console.log('SoundNotifications: no sound named %s', name);
        }
    }

    SoundNotifications.stopSound = function() {
        if (activeSound) {
            activeSound.stop();
            activeSound = null;
        }
    }

    return SoundNotifications;
});
