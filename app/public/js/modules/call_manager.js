define([
    'underscore',
    'app',
    'marionette',
    'views/modal_incomming_call',
    'views/video_pane',
], function(_, app, Marionette, ModalIncommingCall, VideoPane) {

    var PeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var IceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.RTCIceCandidate;
    var SessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
    navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;

    var pc_config = {"iceServers": [{"url": "turn:dmitry.v.bezrukov%40gmail.com@numb.viagenie.ca:3478", "credential": "txw2p995"},
                                    {"url": "stun:stun.l.google.com:19302"}]};

    // var v = document.createElement("video");
    // var SRC_OBJECT = 'srcObject' in v ? "srcObject" :
    //              'mozSrcObject' in v ? "mozSrcObject" :
    //              'webkitSrcObject' in v ? "webkitSrcObject" : "srcObject";

    var CallManager = app.module("CallManager", function() {
        this.startWithParent = false;
        
        this.addInitializer(function(options){
            options || (options = {});

            var self = this;
            self.contacts = options.contacts;

            app.vent.on('transport:sdpMessage', self._handleSdpMessage, self);

            self.contacts.on('call:start',  self._call,       self);
            self.contacts.on('call:stop',   self._stopCall,   self);
            self.contacts.on('call:answer', self._answerCall, self);
            self.contacts.on('call:reject', self._rejectCall, self);
        })

        this.addFinalizer(function(){
        })
    });

    CallManager._call = function(contactId, canVideo) {
        var self = this;

        console.log('Call manager: Start call to contact %s', contactId);
        
        self._setPartner(self.contacts.findWhere({ id : contactId }, { isGroup: undefined }));
        
        navigator.getUserMedia({ audio: true, video: true }, 
            function(localStream) {
                if (!self.pc) {
                    self.localStream = localStream;

                    self.pc = new PeerConnection(pc_config);
                    self.pc.addStream(self.localStream);
                    self.pc.onicecandidate = _.bind(self._gotIceCandidate, self);
                    self.pc.onaddstream = _.bind(self._onRemoteStreamAdded, self);
                    self.pc.onremovestream = _.bind(self._onRemoteStreamRemoved, self);

                    app.vent.trigger('call:localStream', URL.createObjectURL(self.localStream));
                }

                self.partner.set({ "callState": "call-out" });
                
                self.pc.createOffer(
                    _.bind(self._gotLocalDescription, self), 
                    function(error) { console.log(error) }, 
                    { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
                );
            },
            function(error) { 
                switch(error.name) {
                    case "PermissionDeniedError": 
                        alert('Вы запретили доступ к микрофону.\nДля функционирования звонков разрешите доступ в настройках браузера.');
                    break;
                    default:
                        alert('Не удается получить доступ к микрофону.\nКод ошибки браузера: ' + error.name);
                }
                console.log(error.name) 
            }
        );
    }

    CallManager._stopCall = function(contactId) {
        var self = this;

        console.log('Call manager: Stop call with %s', contactId);

        self._dropCall();
        self._sendSdpMessage({
            type: 'bye',
        });
    }

    CallManager._answerCall = function(contactId, localStream, canVideo) {
        var self = this;

        console.log('Call manager: Answer call from %s', contactId);
        
        self.localStream = localStream;
        self.partner.set({ "callState": "incall" });

        self.pc.addStream(localStream);
        self.pc.createAnswer(
            _.bind(self._gotLocalDescription, self),
            function(error) { console.log(error) }, 
            { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
        );
    }

    CallManager._rejectCall = function(contactId) {
        var self = this;

        console.log('Call manager: Reject call from %s', contactId);

        var self = this;
        self._stopCall();
    }

    CallManager._dropCall = function() {
        var self = this;

        console.log('Call manager: Dropping call');

        if (!self.pc) {
            console.log('Call manager: no PeerConnection yet');
            return;
        }

        if (self.localStream) {
            console.log('Removing local stream: ' + self.localStream);
            self.pc.removeStream(self.localStream);
	    self.localStream.stop();
        }

        self.remoteStream = null;
        app.vent.trigger('call:remoteStream', null);

        self.pc.close();
        self.pc = null;

        self.partner.set({ "callState": self.partner.get('online') === true ? 'drop' : 'offline' });
    }

    CallManager._handleSdpMessage = function(message) {
        var self = this;

        console.log('Call manager: Sdp of type %s from %s to %s', message.type, message.from, message.to);

        _.defer(function(){ 
            if (message.type === 'offer') {
                self._setPartner(self.contacts.findWhere({ id : message.from, isGroup: undefined }));
                self.partner.set({ "callState": "call-in" });

                if (!self.pc) {
                    self.pc = new PeerConnection(pc_config);
                    self.pc.onicecandidate = _.bind(self._gotIceCandidate, self);
                    self.pc.onaddstream = _.bind(self._onRemoteStreamAdded, self);
                    self.pc.onremovestream = _.bind(self._onRemoteStreamRemoved, self);
                } else {
                    console.log('Call manager: Peer connection is created already');
                }

                self.pc.setRemoteDescription(new SessionDescription(message));

                // awaiting user response

                var modalIncommingCall = new ModalIncommingCall({
                    model: self.partner
                });
            
                app.getRegion('modal').show(modalIncommingCall);
            } 
            else if (message.type === 'answer') {
                self.partner.set({ "callState": "incall" });
                self.pc.setRemoteDescription(new SessionDescription(message));
            } 
            else if (message.type === 'candidate') {
                var candidate = new IceCandidate({sdpMLineIndex: message.label, candidate: message.candidate});
                self.pc.addIceCandidate(candidate);
            }
            else if (message.type === 'bye') {
                self._dropCall();
            }
            else {
                console.log('Call manager: unknown sdp message type');
            }
        });
    }

    CallManager._onRemoteStreamAdded = function(event) {
        var self = this;

        console.log('Call manager: Remote stream added');

        self.remoteStream = event.stream;

        app.vent.trigger('call:remoteStream', URL.createObjectURL(self.remoteStream));
    }

    CallManager._onRemoteStreamRemoved = function() {
        var self = this;

        console.log('Call manager: Remote stream removed');

        self.remoteStream = null;
        app.vent.trigger('call:remoteStream', null);
    }

    CallManager._gotIceCandidate = function(event) {
        var self = this;

        if (event.candidate) {
            self._sendSdpMessage({
                type: 'candidate',
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
            });
        }
    }

    CallManager._gotLocalDescription = function(description){
        var self = this;

        self.pc.setLocalDescription(description);
        self._sendSdpMessage({
            'type': description.type,
            'sdp': description.sdp,
		});
    }

    CallManager._sendSdpMessage = function(message){
        var self = this;

        console.log('Call Manager: Sending Sdp of type %s', message.type);

        message.from = window.user.id;
        message.to = self.partner.get('id');

        if (!message.to) {
            console.log('Remote contact ID is not set!');
        }

        app.vent.trigger('transport:sendMessage', 'sdpMessage', message);
    },

    CallManager._setPartner = function(partner){
        var self = this;

        if (self.partner) {
            self.partner.off('change:callState', self._onCallStateChanged);
            self.partner.off('change:online', self._onPartnerOnlineChanged);
        }

        self.partner = partner;
        
        self.partner.on('change:callState', self._onCallStateChanged, self);
        self.partner.on('change:online', self._onPartnerOnlineChanged, self);
    },

    CallManager._onCallStateChanged = function(partner, callState){
        var self = this;

        var videoRegion = app.getRegion("videoRegion");

        // sound notifications
        switch (callState) {
            case 'call-in':
            case 'call-out':
                app.module("SoundNotifications").playSound(callState);
                break;
            default:
                app.module("SoundNotifications").stopSound();
                break;
        }
                
        if (callState === 'incall') {

            if (!self.videoPane) {
                self.videoPane = new VideoPane({
                    localStream: URL.createObjectURL(self.localStream),
                    remoteStream: self.remoteStream ? URL.createObjectURL(self.remoteStream) : null
                });
                videoRegion.show(self.videoPane);
            }

            self.videoPane.setPartner(partner);
            $(videoRegion).show();
            $(videoRegion.el).addClass('incall');

        } else {
            $(videoRegion.el).removeClass('incall');
            $(videoRegion).hide();
        }
    }

    CallManager._onPartnerOnlineChanged = function(partner, online){
        var self = this;

        if (online === false) {
            console.log('Contact is going offline');
            self._dropCall();
        }
    }

    return CallManager;
});
