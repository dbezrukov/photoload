define([
    'app',
    'marionette'
], function(app, Marionette){

    var TestModule = app.module("TestModule", function() {
        this.startWithParent = false;
        
        this.addInitializer(function(options){
            options || (options = {});
        })

        this.addFinalizer(function(){
        })
    })

    TestModule.test = function() {
            
    }

    return TestModule;
});
