define(function() {

    var module_options;

    // Start with the constructor
    function OLLayerBuilder(options) {
        module_options = options;
    }

    OLLayerBuilder.prototype = {
        constructor: OLLayerBuilder
    }

    OLLayerBuilder.prototype.constructBaseLayers = function() {
        return [
            new OpenLayers.Layer.Google("Google Hybrid", {
                type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20
            }),
            new OpenLayers.Layer.Google("Google Satellite", {
                type: google.maps.MapTypeId.SATELLITE, 
                numZoomLevels: 22,
                sphericalMercator: true
            }),
            new OpenLayers.Layer.Google("Google Streets", {
                numZoomLevels: 20
            }),
            new OpenLayers.Layer.Google("Google Physical", {
                type: google.maps.MapTypeId.TERRAIN,
                sphericalMercator: true
            })
        ]
    }

    OLLayerBuilder.prototype.constructTestLayers = function() {
        return [
            new OpenLayers.Layer.Vector("Sundials", {
                projection: module_options.map_options.displayProjection,
                strategies: [new OpenLayers.Strategy.Fixed()],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "files/sundials.kml",
                    format: new OpenLayers.Format.KML({
                        extractStyles: true,
                        extractAttributes: true
                    })
                })
            }),
            new OpenLayers.Layer.Vector("Zones", {
                projection: module_options.map_options.displayProjection,
                strategies: [new OpenLayers.Strategy.Fixed()],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "files/zones.kml",
                    format: new OpenLayers.Format.KML({
                        extractStyles: true, 
                        extractAttributes: true
                    })
                })
            })
        ]
    }

    OLLayerBuilder.prototype.constructMeteoLayers = function() {
        return [
            new OpenLayers.Layer.XYZ(
                "Облачность",
                "http://${s}.tile.openweathermap.org/map/clouds/${z}/${x}/${y}.png", {
                    isBaseLayer: false,
                    opacity: 0.6,
                    sphericalMercator: true,
                    visibility: false
            }),
            new OpenLayers.Layer.XYZ(
                "Осадки",
                "http://${s}.tile.openweathermap.org/map/precipitation/${z}/${x}/${y}.png", {
                    isBaseLayer: false,
                    opacity: 0.6,
                    sphericalMercator: true,
                    visibility: false
            })
        ]
    }

    OLLayerBuilder.prototype.constructAviationLayers = function() {
        function constructKmlLayer(layerName, fileName) {
            var vpLayer = new OpenLayers.Layer.Vector(layerName, {
                strategies: [new OpenLayers.Strategy.Fixed()],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "/files/vp/" + fileName,
                    format: new OpenLayers.Format.KML({
                        extractStyles: true, 
                        extractAttributes: true,
                        maxDepth: 3
                    })
                }),
                projection: module_options.map_options.displayProjection,
                visibility: false
            });
            return vpLayer;
        };

        return [
            constructKmlLayer("Воздушные трассы", "airpath.kml"),
            constructKmlLayer("Опасные зоны", "dangerzone.kml"),
            constructKmlLayer("Запретные зоны", "forbzone.kml"),
            constructKmlLayer("NOTAM", "notam.kml"),
            constructKmlLayer("Местные воздушные линии", "regaviation.kml"),
            constructKmlLayer("Зоны ограничения полетов", "restrarea.kml"),
            constructKmlLayer("Специальные зоны", "specialzone.kml"),
            constructKmlLayer("Зоны и районы", "zonearea.kml"),
            constructKmlLayer("Аэродромы", "airdrome.kml")
        ]
    }

    OLLayerBuilder.prototype.constructNslObjectLayers = function() {
        var blaLayer = constructBlaLayer();
        var robotLayer = constructRobotLayer();

        // set filters for this user
        blaLayer.protocol.params.postId = robotLayer.protocol.params.postId = window.user.npu_list;
        blaLayer.protocol.params.p1_lat = robotLayer.protocol.params.p1_lat = parseInt(window.user.region.p1_lat);
        blaLayer.protocol.params.p1_lon = robotLayer.protocol.params.p1_lon = parseInt(window.user.region.p1_lon);
        blaLayer.protocol.params.p2_lat = robotLayer.protocol.params.p2_lat = parseInt(window.user.region.p2_lat);
        blaLayer.protocol.params.p2_lon = robotLayer.protocol.params.p2_lon = parseInt(window.user.region.p2_lon);

        blaLayer.protocol.params.low_id = robotLayer.protocol.params.low_id = module_options.robot_id_boundaries.lower_id;
        blaLayer.protocol.params.up_id  = robotLayer.protocol.params.up_id  = module_options.robot_id_boundaries.upper_id;

        return [ blaLayer, robotLayer ];
    }


    geojson_layer = new OpenLayers.Layer.Vector("GeoJSON", {
            strategies: [new OpenLayers.Strategy.Fixed()],
            protocol: new OpenLayers.Protocol.HTTP({
                url: "ml/lines.json",
                format: new OpenLayers.Format.GeoJSON()
            })
        });

    // a simple analog of _.findWhere(map.object_names, { 'id': attributes.name });
    // for using without underscore
    function objectFindByKey(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
        return null;
    }

    function constructBlaLayer() {
        var gpxSegmentStyles = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style({
                pointRadius: "24",
                labelAlign: "left",
                labelYOffset: 20,
                labelXOffset: 20,
                labelOutlineColor: "#111",
                labelOutlineWidth: 3,
                labelOutlineOpacity: 0.7,
                strokeColor: "red",
                strokeWidth: 6,
                strokeOpacity: 0.8,
                rotation: "${angle}",
                fontColor: "#ddd",
                fontSize: 16,
                fontWeight: "bold",
                fontFamily: "Arial",
                externalGraphic: "/img/plane.svg"
            }, {
                rules: [
                    new OpenLayers.Rule({
                        filter: new OpenLayers.Filter.Function({
                            evaluate: function(attributes) {
                                if (attributes.name) {
                                    var result = objectFindByKey(module_options.object_names, 'id', attributes.name);
                                    if (result !== null) {
                                        attributes.name += "\n" + result.name;
                                    }
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }),
                        symbolizer: { label: "БЛА №${name}\nВысота: ${ele} м\nVвозд: ${speed} км/ч\nVgps: ${speedGps} км/ч" }
                    }),
                    new OpenLayers.Rule({
                        filter: new OpenLayers.Filter.Comparison({
                            type: OpenLayers.Filter.Comparison.NOT_EQUAL_TO,
                            property: "color",
                            value: undefined
                        }),
                        symbolizer: { strokeColor: "#${color}" }
                    }),
                    new OpenLayers.Rule({
                        elseFilter: true,
                        symbolizer: { label: "", fontSize: 14 }
                    })
                ]
            })
        });

        var gpxLayer = new OpenLayers.Layer.Vector("БЛА", {
            strategies: [
                new OpenLayers.Strategy.Fixed(),
                new OpenLayers.Strategy.Refresh({interval: 1000})
            ],
            protocol: new OpenLayers.Protocol.HTTP({
                url: "/profiles/track/segment.gpx",
                params: { filter: "" },
                format: new OpenLayers.Format.GPX({
                    extractWaypoints: true,
                    extractRoutes: true,
                    extractAttributes: true,
                    extractTracks: true })
            }),
            styleMap: gpxSegmentStyles,
            projection: module_options.map_options.displayProjection,
            visibility: true
        });

        gpxLayer.protocol.params.is_robot = 1;
        gpxLayer.protocol.params.rand = new Date().getTime();

        return gpxLayer;
    }

    function constructRobotLayer(){
        var gpxSegmentStyles = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style({
                pointRadius: "24",
                labelAlign: "left",
                labelYOffset: 20,
                labelXOffset: 20,
                labelOutlineColor: "#111",
                labelOutlineWidth: 3,
                labelOutlineOpacity: 0.7,
                strokeColor: "red",
                strokeWidth: 6,
                strokeOpacity: 0.8,
                rotation: "${angle}",
                fontColor: "#ddd",
                fontSize: 16,
                fontWeight: "bold",
                fontFamily: "Arial",
                externalGraphic: "/img/robot.svg"
            }, {
                rules: [
                    new OpenLayers.Rule({
                        filter: new OpenLayers.Filter.Function({
                            evaluate: function(attributes) {
                                if (attributes.name) {
                                    var result = objectFindByKey(module_options.object_names, 'id', attributes.name);
                                    if (result !== null) {
                                        attributes.name += "\n" + result.name;
                                    }
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }),
                        symbolizer: {
                            rotation: 0,
                            labelYOffset: 5,
                            label: "Робот №${name}\nV: ${speedGpx} км/ч"
                        }
                    })
                ]
            })
        });

        var gpxLayer = new OpenLayers.Layer.Vector("Роботы", {
            projection: module_options.map_options.displayProjection,
            strategies: [
                new OpenLayers.Strategy.Fixed(),
                new OpenLayers.Strategy.Refresh({interval: 1000})
            ],
            protocol: new OpenLayers.Protocol.HTTP({
                url: "/profiles/track/segment.gpx",
                params: { filter: "" },
                format: new OpenLayers.Format.GPX({
                    extractWaypoints: true,
                    extractRoutes: true,
                    extractAttributes: true,
                    extractTracks: false })
            }),
            styleMap: gpxSegmentStyles,
            projection: module_options.map_options.displayProjection,
            visibility: true
        });

        gpxLayer.protocol.params.is_robot = 0;
        gpxLayer.protocol.params.rand = new Date().getTime();

        return gpxLayer;
    }

    return OLLayerBuilder;
});
