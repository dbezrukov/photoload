define([
    'underscore',
    'app',
    'marionette',
    'views/tooltip_uav_pane'
], function(_, app, Marionette, TooltipUavPane) {

    var TooltipManager = app.module("TooltipManager", function() {
        this.startWithParent = false;
        
        this.addInitializer(function(options){
            options || (options = {});

            var self = this;
            
            self.region = app.getRegion("tooltipRegion");

            app.vent.on('tooltip:show', self.handleShow, self);
            app.vent.on('tooltip:close', self.handleClose.bind(self));
        })

        this.addFinalizer(function(){
        })
    });


    TooltipManager.handleShow = function(x, y, model) {
        var self = this;

		if (model.name != 'uav') {
			console.log('TooltipManager unknown model name, returning');
			return;
		}

		self.tooltip = new TooltipUavPane({ model: model });
		
		$(self.region.el).css('left', x + 'px');
		$(self.region.el).css('top',  y + 'px')
		
		self.region.show(self.tooltip);

		$(self.region.el).on('mouseleave', self.handleClose.bind(self));
		$(self.region.el).show();
    }

    TooltipManager.handleClose = function() {
        var self = this;

		$(self.region.el).off('mouseleave');
		self.tooltip.close();
        $(self.region.el).hide();
    }

    return TooltipManager;
});
