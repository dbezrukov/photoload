define(['underscore.string'],
    function(_str) {

    // Start with the constructor
    function GMapTypeBuilder(options) {
    }

    GMapTypeBuilder.prototype = {
        constructor: GMapTypeBuilder
    }

    GMapTypeBuilder.prototype.constructOSMMap = function() {
        return new google.maps.ImageMapType({
            getTileUrl: function( coord, zoom ) {
                var w = Math.pow( 2, zoom );
                if ( coord.y < 0 || coord.y >= w ) return null;
                var x = coord.x % w;
                return "http://tile.openstreetmap.org/" + zoom + "/" + (x >= 0 ? x : (w + x)) + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OSM",
            minZoom: 0,
            maxZoom: 18
        })
    }

    // Normalizes the coords that tiles repeat across the x axis (horizontally)
	// like the standard Google map tiles.
	function getNormalizedCoord(coord, zoom) {
	  	var y = coord.y;
	  	var x = coord.x;

	  	// tile range in one direction range is dependent on zoom level
	  	// 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
	  	var tileRange = 1 << zoom;

	  	// don't repeat across y-axis (vertically)
	  	if (y < 0 || y >= tileRange) {
	    	return null;
	  	}

	  	// repeat across x-axis
	  	if (x < 0 || x >= tileRange) {
	    	x = (x % tileRange + tileRange) % tileRange;
	  	}

	  	return {
	    	x: x,
	    	y: y
	  	};
	}

    GMapTypeBuilder.prototype.constructTMSOSMMap = function() {
        return new google.maps.ImageMapType({
            getTileUrl: function( coord, zoom ) {
                return window.tms_url + "/maps/gmt/osm/z" + zoom + "/" + coord.y + "/" + coord.x + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OSM",
            minZoom: 0,
            maxZoom: 18
        })
    }

    GMapTypeBuilder.prototype.constructTMSWorldMap = function() {
        return new google.maps.ImageMapType({
            getTileUrl: function( coord, zoom ) {
                return window.tms_url + "/maps/gmt/cache/z" + zoom + "/" + coord.y + "/" + coord.x + ".jpg";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "Google Спутник",
            minZoom: 0,
            maxZoom: 18
        })
    }

    GMapTypeBuilder.prototype.constructTMSTiffMap = function() {

    	var baseUrlPattern = window.tms_url + "/gatchina/$z/$y/$x.png";

		var yFlip = true;
		var bounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(59.54945654653804, 30.11332083168049),
                new google.maps.LatLng(59.52937583952866, 30.132810013987264));

        return new google.maps.ImageMapType({

        	getTileUrl: function(coord, zoom){
	            var proj = app.map.getProjection();
	            var zfactor = Math.pow(2, zoom);
	            
	            // get Long Lat coordinates
	            var swCoord = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
	            var neCoord = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, coord.y * 256 / zfactor));
	            
	            if(bounds) {
	                var tileLatLng = new google.maps.LatLngBounds(swCoord, neCoord);
	                
	                console.log('tile bounds ne', tileLatLng.getNorthEast().lat(), tileLatLng.getNorthEast().lng());
	                console.log('tile bounds sw', tileLatLng.getSouthWest().lat(), tileLatLng.getSouthWest().lng());

	                console.log('tmts bounds ne', bounds.getNorthEast().lat(), bounds.getNorthEast().lng());
	                console.log('tmts bounds sw', bounds.getSouthWest().lat(), bounds.getSouthWest().lng());

	                // if(!tileLatLng.intersects(bounds)) {
	                // 	console.log('no intersects');
	                //     return "http://maps.gstatic.com/intl/en_us/mapfiles/transparent.png";
	                // } else {
	                // 	console.log('intersects!');
	                // }
	            }

	            return baseUrlPattern.replace("$z", zoom).replace("$y", yFlip ? coord.y : (1 << zoom) - coord.y - 1).replace("$x", coord.x);
			},


            tileSize: new google.maps.Size(256, 256),
            name: "TMSTiff",
            minZoom: 0,
            maxZoom: 20
        })
    }

    GMapTypeBuilder.prototype.constructYandexMap = function() {
        var yandexMapType = new google.maps.ImageMapType({
            getTileUrl: function(coord, zoom) {
                return "http://vec0"+((coord.x+coord.y)%5)+".maps.yandex.net/tiles?l=map&v=2.16.0&x=" + 
                    coord.x + "&y=" + coord.y + "&z=" + zoom + "";
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            alt: "Yandex",
            name: "Yandex",
            minZoom: 0,
            maxZoom: 17
        })
        yandexMapType.projection = new YandexProjection();

        return yandexMapType;
    }

    /// Utility functions

    function YandexProjection() {
        var MERCATOR_RANGE = 256;
        this.origin = new google.maps.Point( MERCATOR_RANGE / 2, MERCATOR_RANGE / 2 );
        this.pxPerDegree = MERCATOR_RANGE / 360;
        this.pxPerRadian = MERCATOR_RANGE / (2 * Math.PI);
        
        this.fromLatLngToPoint = function(latLng, point) {
            function atanh(x) {
                return 0.5 * Math.log( (1 + x) / (1 - x) );
            }

            function degreesToRadians(deg) {
                return deg * (Math.PI / 180);
            }

            function bound(value, min, max) {
                if (min != null) value = Math.max(value, min);
                if (max != null) value = Math.min(value, max);
                return value;
            }

            var result = point || new google.maps.Point( 0, 0 );
            var origin = this.origin;
            var exct = 0.0818197;
            var y = bound( Math.sin( latLng.lat() * (Math.PI / 180) ) );
            result.x = origin.x + this.pxPerDegree * latLng.lng();
            result.y = origin.y - this.pxPerRadian * ( atanh( y ) - exct * atanh( exct * y ) );
            return result;
        };

        this.fromPointToLatLng = function(point, noWrap) {
            var origin = this.origin;
            var lng = (point.x - origin.x) / this.pxPerDegree;
            var latRadians = (point.y - origin.y) / -this.pxPerRadian;
            var lat = Math.abs((2*Math.atan(Math.exp(latRadians))-Math.PI/2)*180/Math.PI);
            var Zu = lat/(180/Math.PI);
            var Zum1 = Zu+1;
            var exct = 0.0818197;
            var yy = -Math.abs(((point.y)-128));

            while (Math.abs(Zum1-Zu)>0.0000001){
                Zum1 = Zu;
                Zu = Math.asin(1-((1+Math.sin(Zum1))*Math.pow(1-exct*Math.sin(Zum1),exct))
                / (Math.exp((2*yy)/-(256/(2*Math.PI)))*Math.pow(1+exct*Math.sin(Zum1),exct)));
            }
            
            if (point.y>256/2) {
                lat=-Zu*180/Math.PI;
            } else {
                lat=Zu*180/Math.PI;
            }

            return new google.maps.LatLng(lat, lng, noWrap);
        };
        
        return this;
    }
    
    return GMapTypeBuilder;
});


