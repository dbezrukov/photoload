define([
    'app',
    'marionette'
], function(app, Marionette){

    var socket = io();

    var TransportModule = app.module("TransportModule", function() {
        this.startWithParent = false;
        
        this.addInitializer(function(options){
            console.log('initializing transport');

            options || (options = {});

            app.vent.on('transport:sendMessage', this.sendMessage, this);

            socket.on('connect', this.handleConnect, this);
            socket.on('disconnect', this.handleDisconnect, this);
            
            socket.on('loggedIn', this.handleLoggedIn, this);

            socket.on('chatMessage', this.handleChatMessage, this);
            socket.on('chatHistory', this.handleChatHistory, this);
            socket.on('sdpMessage', this.handleSdpMessage, this);

            socket.on('userOnline', this.handleUserOnline, this);
            socket.on('userOffline', this.handleUserOffline, this);
        })

        this.addFinalizer(function(){
        })
    })

    TransportModule.sendMessage = function(type, data) {
        console.log('transport send message: ' + type);

        if (!socket.connected || socket.connected === false) {
            return;
        }

        socket.emit(type, data);
    }

    TransportModule.handleLoggedIn = function(data) {
        console.log('transport handle loggedIn');

        app.vent.trigger('transport:loggedIn', data);
    },

    TransportModule.handleChatMessage = function(data) {
        console.log('transport handle chatMessage');

        app.vent.trigger('transport:chatMessage', data);
    },

    TransportModule.handleChatHistory = function(data) {
        console.log('transport handle chatHistory');

        app.vent.trigger('transport:chatHistory', data);
    },

    TransportModule.handleSdpMessage = function(data) {
        console.log('transport handle sdpMessage');

        app.vent.trigger('transport:sdpMessage', data);
    },

    TransportModule.handleUserOnline = function(data) {
        console.log('transport handle userOnline');

        app.vent.trigger('transport:userOnline', data);
    },

    TransportModule.handleUserOffline = function(data) {
        console.log('transport handle userOffline');

        app.vent.trigger('transport:userOffline', data);
    },

    TransportModule.handleConnect = function() {
        console.log('transport connected');

        // for the faster startup this module can be started before UI is initialized
        // so we need to share the current connection state accross the UI
        window.connected = true;
        app.vent.trigger('transport:connected');
    },

    // TransportModule.handleConnectError = function(error) {
    //     console.log('transport connect error');
    //     window.connected = false;
    //     app.vent.trigger('transport:connect_error');
    // },

    TransportModule.handleDisconnect = function(error) {
        console.log('transport disconnected');
        console.dir(error);

        window.connected = false;
        app.vent.trigger('transport:disconnected');
    }

    return TransportModule;
});
