define([
    'underscore.string',
    './object_layer',
    'models/uav',
    '../../views/npu_info_pane'
], function(_str, ObjectLayer, Uav, InfoPane){

    NpuLayer.prototype = new ObjectLayer();
    NpuLayer.prototype.constructor = NpuLayer;

    function NpuLayer()
    {
        this.InfoPane = InfoPane;

        this.getObjectTitle = function() 
        {
        	return 'НПУ';
        }

        this.getUrl = function()  
        {
            return '/profiles/npu?timestamp=' + Date.now();
        }

        this.getModelUrl = function()  
        {
            // todo: update npu model
            return 'files/models/orlan.gltf';
        }

        this.getMarkerStyle = function(feature)
        {
            var labelText = 'НПУ ' + feature.postId;

            return {
                draggable: false,
                raiseOnDrag: false,
                labelContent: labelText,
                labelAnchor: new google.maps.Point(50, 40),
                labelClass: "labels", // the CSS class for the label
                labelStyle: {opacity: 0.75}
            }
        }

        this.getMarkerIcon = function(feature)
        {
            var svg = 'M609.208,298.575L313.306,3.815c-3.987-3.988-10.445-3.988-14.433,0L2.961,298.575c-3.988,3.988-3.907,10.353,0,14.393 \
                c1.999,2.06,4.61,3.11,7.201,3.131v0.041h71.441v284.865c0,2.815,1.142,5.335,2.988,7.171c1.846,1.856,4.396,3.009,7.211,3.009 \
                h428.401c5.63,0,10.2-4.569,10.2-10.18V316.14h71.899c2.673,0.051,5.191-1,6.905-3.162 \
                C612.676,308.541,613.197,302.563,609.208,298.575z M408.003,417.926h-91.8v91.524c0,5.6-4.569,10.18-10.201,10.18 \
                c-5.63,0-10.2-4.57-10.2-10.18v-91.524h-91.8c-5.63,0-10.2-4.56-10.2-10.18c0-5.601,4.57-10.159,10.2-10.159h91.8v-91.505 \
                c0-5.6,4.57-10.159,10.2-10.159c5.631,0,10.201,4.56,10.201,10.159v91.505h91.8c5.631,0,10.2,4.549,10.2,10.159 \
                C418.204,413.376,413.634,417.926,408.003,417.926z';

            return {
                path: svg,
                fillColor: '#E26161',
                fillOpacity: 1,
                anchor: new google.maps.Point(32, 45),
                strokeColor: 'black',
                strokeWeight: 1,
                rotation: 0,
                scale: 0.07
            }
        }

        this.getInfoPaneModel = function(feature)
        {
        	feature.name = feature.postId;
        	feature.objectTitle = this.getObjectTitle();

        	return new Uav(feature);
        }

        ObjectLayer.apply(this, arguments);
    }

    return NpuLayer;
});
