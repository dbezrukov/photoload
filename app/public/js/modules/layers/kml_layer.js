define([
    'jquery',
    'underscore.string'
], function($, _str){

    /** @constructor */
    function KmlLayer(options) {
        this.layers_ = [];
        this.minZoom_ = options.minZoom ? options.minZoom : 1;

        var me = this;
        options.urls.forEach(function(url) {
            me.layers_.push(new google.maps.KmlLayer({
                url: url,
                preserveViewport: true
            }))
        });
    } 

    KmlLayer.prototype.hide = function() {
        google.maps.event.removeListener(this.zoomChanged);

        var me = this;
        me.layers_.forEach(function(layer) {
            layer.setMap(null);
        });
    };

    KmlLayer.prototype.show = function() {
        var me = this;

        function checkZoomAndShow(){
            var zoomLevel = app.map.getZoom();
            var val = (zoomLevel >= me.minZoom_ ? app.map : null);

            me.layers_.forEach(function(layer) {
                layer.setMap(val);
            });
        };

        checkZoomAndShow();
        this.zoomChanged = google.maps.event.addListener(app.map, 'zoom_changed', checkZoomAndShow);
    };

    return KmlLayer;
});
