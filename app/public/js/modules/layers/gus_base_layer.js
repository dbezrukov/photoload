define([
    'underscore.string',
    './object_layer',
    '../../views/gus_info_pane'
], function(_str, ObjectLayer, InfoPane){

    GusBaseLayer.prototype = new ObjectLayer();
    GusBaseLayer.prototype.constructor = GusBaseLayer;

    function GusBaseLayer()
    {
    	this.InfoPane = InfoPane;

        this.getObjectTitle = function() 
        {
        	return 'ГУС Машина';
        }

        this.getUrl = function() 
        {
            return '/data/gus?timestamp=' + Date.now();
        }

        this.getModelUrl = function()  
        {
            return 'files/models/CesiumGround/Cesium_Ground.gltf';
        }

        this.getModelOrientation = function()
		{
			return {
				heading: 180.,
				pitch: 0.,
				roll: 0.
			}
		}

		this.getModelMinSize = function()
		{
			return 128;
		}

        this.getMarkerStyle = function(feature)
        {
            var labelText = 'ГУС Машина';

            var anchor = new google.maps.Point(-17, 40);

            return {
                draggable: false,
                raiseOnDrag: false,
                labelContent: labelText,
                labelAnchor: anchor, // x: greater is to the left
                labelClass: "labels", // the CSS class for the label
                labelStyle: {opacity: 0.75}
            }
        }

        this.getMarkerIcon = function(feature)
        {
            var svgRobot = 'M512,272l-64-128h-96V80c0-17.6-14.4-32-32-32H32C14.4,48,0,62.4,0,80v256l32,32h40.583C67.134,377.416,64,388.339,64,400 \
			c0,35.346,28.654,64,64,64s64-28.654,64-64c0-11.661-3.134-22.584-8.583-32h177.167c-5.449,9.416-8.584,20.339-8.584,32 \
			c0,35.346,28.654,64,64,64s64-28.654,64-64c0-11.661-3.135-22.584-8.584-32H512V272z M352,272v-80h66.334l40,80H352z'
            
            var path = svgRobot;
            var scale = 0.1;
            var anchor = new google.maps.Point(-150, 0);

            return {
                path: path,
                fillColor: '#E26161',
                fillOpacity: 1,
                anchor: anchor,
                strokeColor: 'black',
                strokeWeight: 1,
                rotation: 0,
                scale: scale
            }
        }

        ObjectLayer.apply(this, arguments);
    }

    return GusBaseLayer;
});
