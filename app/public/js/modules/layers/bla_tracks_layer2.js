define([
    'jquery',
    'marionette',
    'underscore',
    'underscore.string',
    'vendors/label'
], function($, Marionette, _, _str, Label){

    /** @constructor */
    function BlaTracksLayer() {
    }

    BlaTracksLayer.prototype.hide = function() {
        clearInterval(this.refreshIntervalId_);

        var self = this;

		app.collections.uavs.off('change:track');

		for (var userId in self.trackedUavs) {
            if (self.trackedUavs.hasOwnProperty(userId)) {
        		self.deleteTrack(userId);
        	}
		}

    };

    BlaTracksLayer.prototype.show = function() {
        var self = this;

        self.trackedUavs = {};
        
        app.collections.uavs.on('change:track', self.trackChanged, self);

        // app.collections.uavs.forEach(function(uav) {
        // 	if (uav.track) {
        // 		self.trackChanged(uav);
        // 	}
        // })

        function fetchAllTracks() {

			for (var userId in self.trackedUavs) {
				if (self.trackedUavs.hasOwnProperty(userId)) {
	            	self.fetchTrack(userId);
	            }
			}
        }

        self.refreshIntervalId_ = setInterval(fetchAllTracks, 1000);
        fetchAllTracks();
    };

    BlaTracksLayer.prototype.trackChanged = function(user) 
    {
    	var self = this;

    	var userId = user.get('id');

    	var track = user.get('track');

		if (track.interval === 'no') { // switch off

			self.deleteTrack(userId);

		} else { // switch on

			if (userId in self.trackedUavs) {
				self.deleteTrack(userId);

			}

			// get interval in milliseconds
			var startTimestamp = new Date();
			var epsilon = 1.;

			switch(track.interval) {
				case 'hour':
					startTimestamp = new Date(startTimestamp.getTime() - 60 * 60000);
					epsilon = 0.0001;
				break;
				case 'today':
					startTimestamp.setHours(0, 0, 0);
					epsilon = 0.0002;
				break;
				case 'yesterday':
					startTimestamp.setDate(startTimestamp.getDate() - 1)
					epsilon = 0.0005;
				break;
				case 'week':
					startTimestamp.setDate(startTimestamp.getDate() - 7)
					epsilon = 0.001;
				break;
			}

			// recreate track
			self.trackedUavs[userId] = {
				polyline: undefined,
    			checkpoints: [],
				lastReceivedTimestamp: startTimestamp.getTime() - window.timedelta,
				epsilon: epsilon
			}
    		
    		self.fetchTrack(userId);
			
		}
    }

    BlaTracksLayer.prototype.deleteTrack = function(userId) 
    {
    	var self = this;

    	if (userId in self.trackedUavs) {
    		if (self.trackedUavs[userId].polyline) {
    			self.trackedUavs[userId].polyline.setMap(null);
    		}

    		self.trackedUavs[userId].checkpoints.forEach(function(checkpoint) {
				checkpoint.setMap(null);
    		})
        	
        	delete self.trackedUavs[userId];
    	}
    }

    BlaTracksLayer.prototype.fetchTrack = function(userId) 
    {
    	var self = this;

		function processTrack(trackData) {

        	trackData.track.forEach(function(position) {
            	processPosition(trackData.id, position);
        	})

        	if (self.trackedUavs[trackData.id].polyline) {
        		console.log('track length: ', self.trackedUavs[trackData.id].polyline.getPath().length);
        	}

        	function processPosition(userId, position) {

                // update the latest timestamp for optimized location requests
                if (position.utc > self.trackedUavs[userId].lastReceivedTimestamp) {
                    self.trackedUavs[userId].lastReceivedTimestamp = position.utc;
				}

				var polyline;
				if (self.trackedUavs[userId].polyline) {
					polyline = self.trackedUavs[userId].polyline;
                } else {
					polyline = new google.maps.Polyline({
	                    geodesic: true,
	                    strokeColor: self.palette.getColor(userId),
	                    strokeOpacity: 0.9,
	                    strokeWeight: 4
	                });

	                polyline.setMap(app.map);
	                self.trackedUavs[userId].polyline = polyline;
				}

				var path = polyline.getPath();
  				
  				// add position to the track polyline
  				var coordinate = new google.maps.LatLng(position.lat, position.lon)
  				path.push(coordinate);

  				// limit the track by to 50 checkpoints
				if (path.length > 50) {
					path.removeAt(0);
				}

  				/*
  				// add checkpoint marker for this position
  				var checkpoint = new google.maps.Marker(self.getMarkerStyle(position));
				checkpoint.iw1 = new google.maps.InfoWindow({ 
					content: self.getMarkerInfo(position).content
				});
                            
				google.maps.event.addListener(checkpoint, "click", function (e) { 
					checkpoint.iw1.open(app.map, this);
				});

				checkpoint.setMap(app.map);
                checkpoint.setPosition(coordinate);
                checkpoint.setIcon(self.getCheckpointIcon(userId));

                self.trackedUavs[userId].checkpoints.push(checkpoint);
                */
            }
    	}

    	$.ajax({
            type: "GET",
            url: '/board/' + userId + '/track',
            data: { 
            	since: self.trackedUavs[userId].lastReceivedTimestamp,
            	epsilon: self.trackedUavs[userId].epsilon
            },
            dataType: 'json',
            success: processTrack
    	})
    	.fail(function() {
		});
	}

	BlaTracksLayer.prototype.getCheckpointIcon = function(userId)
    {
        var self = this;
        var svgPoint = 'M 10, 10 m -7.5, 0 a 7.5,7.5 0 1,0 15,0 a 7.5,7.5 0 1,0 -15,0';

        return {
            path: svgPoint,
            fillColor: self.palette.getColor(userId),
            fillOpacity: 1,
            anchor: new google.maps.Point(10, 10),
            strokeColor: 'black',
            strokeWeight: 1,
            rotation: 0,
            scale: 0.7
        }
    }

    BlaTracksLayer.prototype.getMarkerStyle = function(position)
    {
        function prettyMonth(date) {
            var months = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
            return date.getUTCDate() + ' ' + months[date.getUTCMonth()];
        }

        function checkTime(i) {
            if (i < 10) {
                i= "0" + i;
            }
            return i;
        }

        function formatDate(date) {
            return prettyMonth(date)
                + ' ' + checkTime(date.getHours())
                + ':' + checkTime(date.getMinutes())
                + ':' + checkTime(date.getSeconds())
        }

        var time = formatDate(new Date(position.utc));

        return {
            title: _str.vsprintf('%s', [time]),
            zIndex: 1
        }
    }

	BlaTracksLayer.prototype.getMarkerInfo = function(position)
    {
        function prettyMonth(date) {
            var months = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
            return date.getUTCDate() + ' ' + months[date.getUTCMonth()];
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function formatDate(date) {
            return prettyMonth(date)
                + ' ' + checkTime(date.getHours())
                + ':' + checkTime(date.getMinutes())
                + ':' + checkTime(date.getSeconds())
        }

        var time = formatDate(new Date(position.utc));

        return {
            content: _str.vsprintf('%s', [time])
        }
    }

    return BlaTracksLayer;
});