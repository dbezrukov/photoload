define([
    'underscore.string'
], function(_str){

    NavlostLayer.prototype = new google.maps.OverlayView();

    /** @constructor */
    function NavlostLayer(options) {
        this.div_ = null;
        this.img_ = null;

        this.layerType_ = options.layerType;
        this.legendSrc_ = options.legendSrc;
        
        this.setMap(app.map);
    }

    NavlostLayer.prototype.onAdd = function() {
        var div = document.createElement('div');
        div.style.borderStyle = 'none';
        div.style.borderWidth = '0px';
        div.style.position = 'absolute';

        // Create the img element and attach it to the div.
        var img = document.createElement('img');
        img.style.width = '100%';
        img.style.height = '100%';
        img.style.position = 'absolute';
        img.style.opacity = 0.6;
        div.appendChild(img);

        this.div_ = div;
        this.img_ = img;

        var panes = this.getPanes();
        panes.overlayLayer.appendChild(div);

        var me = this;
        var tid = null;

        function startTimeout() {
            me.img_.style.visibility = 'hidden';

            return window.setTimeout(function() {
                me.draw();
            }, 500);
        }

        $(me.img_).load(function() {
            me.img_.style.visibility = 'visible';
        });

        this.listeners_ = [
            google.maps.event.addListener(app.map, 'center_changed', function(){
                tid && window.clearTimeout(tid);
                tid = startTimeout();
            })
        ]

        this.setLegendVisible(true);
    };

    NavlostLayer.prototype.draw = function() {
        var bounds = app.map.getBounds();

        var swGeo = bounds.getSouthWest();
        var neGeo = bounds.getNorthEast();

        var overlayProjection = this.getProjection();
        var sw = overlayProjection.fromLatLngToDivPixel(swGeo);
        var ne = overlayProjection.fromLatLngToDivPixel(neGeo);

        var div = this.div_;
        div.style.left = sw.x + 'px';
        div.style.top = ne.y + 'px';

        var width = Math.floor(ne.x - sw.x);
        var height = Math.floor(sw.y - ne.y);

        div.style.width = width + 'px';
        div.style.height = height + 'px';

        this.reloadImage(width, height, swGeo, neGeo);
    };

    NavlostLayer.prototype.reloadImage = function(x, y, swCoord, neCoord) {
        var url = 'http://www.navlost.eu/aero/met?z=1000&t=1&img=5&key=5468089gh9088-00-9';
                
        var type = _str.sprintf('&winf=%s;', this.layerType_);

        var size = _str.vsprintf('&x=%u&y=%u', [x, y]);

        var bounds = _str.vsprintf('&lat0=%s&lon0=%s&lat1=%s&lon1=%s', [ 
            swCoord.lat().toFixed(8), swCoord.lng().toFixed(8), 
            neCoord.lat().toFixed(8), neCoord.lng().toFixed(8) ]
        );

        url += type;
        url += size;
        url += bounds;

        var date = new Date();
        this.img_.src = url; // + '?' + date.getTime();
    };

    NavlostLayer.prototype.onRemove = function() {
        for (var i = 0, I = this.listeners_.length; i < I; ++i) {
            google.maps.event.removeListener(this.listeners_[i]);
        }
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
        this.setLegendVisible(false);
    };

    NavlostLayer.prototype.hide = function() {
        if (this.div_) {
            this.div_.style.visibility = 'hidden';
            this.setLegendVisible(false);
        }
    };

    NavlostLayer.prototype.show = function() {
        if (this.div_) {
            this.div_.style.visibility = 'visible';
            this.setLegendVisible(true);
        }
    };

    NavlostLayer.prototype.setLegendVisible = function(visible) {
        if (this.legendSrc_) {
            visible === true ? app.map.setLegend(this.legendSrc_) 
                             : app.map.setLegend(null);
        }
    }

    return NavlostLayer;
});
