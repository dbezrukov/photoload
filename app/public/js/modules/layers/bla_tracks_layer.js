define([
    'jquery',
    'marionette',
    'underscore',
    'underscore.string',
    'vendors/label'
], function($, Marionette, _, _str, Label){

    /** @constructor */
    function BlaTracksLayer() {
        this.polylines_= {};
        this.polylines3d_ = {};
    }

    BlaTracksLayer.prototype.hide = function() {
        clearInterval(this.refreshIntervalId_);

        var me = this;

        for (var key in me.polylines_) {
            if (me.polylines_.hasOwnProperty(key)) {
                me.polylines_[key].setMap(null);
                delete me.polylines_[key];

                app.map3d.entities.remove(me.polylines3d_[key]);
        		delete me.polylines3d_[key];
            }
        }
    };

    BlaTracksLayer.prototype.show = function() {
        var me = this;
        var url = this.getUrl();
        
        this.refreshIntervalId_ = setInterval(function() {
            $.get(url, function(vbsTracks) {

                var new_polylines = [];

                vbsTracks.forEach(function(feature) {
                    
                    if (parseInt(feature.id) >= 49900){
                        return;
                    }

                    new_polylines.push(feature.id);

                    var trackCoordinates = [];
                    var trackCoordinates3d = [];

                    feature.track.forEach(function(coordinate) {
                        trackCoordinates.push(new google.maps.LatLng(coordinate.lat, coordinate.lon));

                        trackCoordinates3d.push(coordinate.lon);
                        trackCoordinates3d.push(coordinate.lat);
                        trackCoordinates3d.push(coordinate.ele);
                    });

                    var polyline;
                    var polyline3d;

                    if (feature.id in me.polylines_ === true) {
                        polyline = me.polylines_[feature.id];
                        polyline3d = me.polylines3d_[feature.id];
                    } else {
                        polyline = new google.maps.Polyline({
                            geodesic: true,
                            strokeColor: me.palette.getColor(feature.id),
                            strokeOpacity: 0.9,
                            strokeWeight: 4
                        });
                        me.polylines_[feature.id] = polyline;

                        polyline3d = app.map3d.entities.add({
                        	name: feature.id,
                        	polyline: {
                        		positions: Cesium.Cartesian3.fromDegreesArrayHeights(trackCoordinates3d),
                        		width: 3,
                        		material: Cesium.Color.fromCssColorString(polyline.strokeColor)
                        	}
                        })

                        me.polylines3d_[feature.id] = polyline3d;
                    }

                    polyline.setPath(trackCoordinates);
                    polyline.setMap(app.map);

                    polyline3d.polyline.positions = Cesium.Cartesian3.fromDegreesArrayHeights(trackCoordinates3d);
                });

                // polylines cleanup
                var current_polylines = [];
                for (var key in me.polylines_) {
                    if (me.polylines_.hasOwnProperty(key)) {
                        current_polylines.push(key);
                    }
                }

                var invalid_polylines = _.difference(current_polylines, new_polylines);
                invalid_polylines.forEach(function(id) {
                    me.polylines_[id].setMap(null);
                    delete me.polylines_[id];

                    app.map3d.entities.remove(me.polylines3d_[id]);
        			delete me.polylines3d_[id];
                });
            }, 'json')
            .fail(function() {
                for (var key in me.polylines_) {
                    if (me.polylines_.hasOwnProperty(key)) {
                        me.polylines_[key].setMap(null);
                        delete me.polylines_[key];

                        app.map3d.entities.remove(me.polylines3d_[key]);
        				delete me.polylines3d_[key];
                    }
                }
            });
        }, 1000);
    };

    BlaTracksLayer.prototype.getUrl = function() 
    {
        return '/profiles/bla/tracks?timestamp=' + Date.now();
    }

    return BlaTracksLayer;
});