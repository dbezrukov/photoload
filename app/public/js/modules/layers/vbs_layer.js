define([
    'underscore.string',
    './object_layer',
    'models/uav',
    '../../views/vbs_info_pane'
], function(_str, ObjectLayer, Uav, InfoPane){

	VbsLayer.prototype = new ObjectLayer();
    VbsLayer.prototype.constructor = VbsLayer;

    function VbsLayer()
    {
        this.InfoPane = InfoPane;

        this.getObjectTitle = function() 
        {
        	return 'ВБС';
        }

        this.getUrl = function() 
        {
        	return '/profiles/vbs?timestamp=' + Date.now();
        }

        this.getModelUrl = function()  
        {
            return 'files/models/henry.gltf';
        }

        this.getModelOrientation = function()
		{
			return {
				heading: 0.,
				pitch: 0.,
				roll: -90.
			}
		}

        this.getModelMinSize = function()
		{
			return 50;
		}

        this.getMarkerStyle = function(feature)
        {
        	var labelText = 'ВБС: ' + feature.id;

	        return {
	            draggable: false,
	            raiseOnDrag: false,
	            labelContent: labelText,
	            labelAnchor: new google.maps.Point(-17, 40), // x: greater is to the left
	            labelClass: "labels", // the CSS class for the label
	            labelStyle: {opacity: 0.75},
	            zIndex: 2
	        }
        }

        this.getMarkerIcon = function(feature)
        {
            var svgVbs = 'M43.001,10.621c-1.105,0-1.999-0.896-1.999-2V6.106c0-1.105-0.896-2-2-2h-1.334c-1.105,0-2,0.895-2,2v2.516  c0,1.104-0.896,2-2,2H21c-1.105,0-2,0.895-2,2v45.046c0,1.104,0.895,2,2,2h22c1.104,0,2-0.896,2-2V12.621  C45,11.517,44.105,10.621,43.001,10.621z M23.375,49.375c0-1.104,0.895-2,2-2c1.104,0,2,0.896,2,2s-0.896,2-2,2  C24.27,51.375,23.375,50.479,23.375,49.375z M25.375,40.75c1.104,0,2,0.896,2,2s-0.896,2-2,2c-1.105,0-2-0.896-2-2  S24.27,40.75,25.375,40.75z M23.375,36.125c0-1.104,0.895-2,2-2c1.104,0,2,0.896,2,2s-0.896,2-2,2  C24.27,38.125,23.375,37.229,23.375,36.125z M36.062,56.266h-8.375c-0.553,0-1-0.447-1-1c0-0.553,0.447-1,1-1h8.375  c0.553,0,1,0.447,1,1C37.062,55.818,36.615,56.266,36.062,56.266z M31.875,40.75c1.104,0,2,0.896,2,2s-0.896,2-2,2  c-1.105,0-2-0.896-2-2S30.77,40.75,31.875,40.75z M29.875,36.125c0-1.104,0.895-2,2-2c1.104,0,2,0.896,2,2s-0.896,2-2,2  C30.77,38.125,29.875,37.229,29.875,36.125z M31.875,47.375c1.104,0,2,0.896,2,2s-0.896,2-2,2c-1.105,0-2-0.896-2-2  S30.77,47.375,31.875,47.375z M38.375,51.375c-1.105,0-2-0.896-2-2s0.895-2,2-2c1.104,0,2,0.896,2,2S39.479,51.375,38.375,51.375z   M38.375,44.75c-1.105,0-2-0.896-2-2s0.895-2,2-2c1.104,0,2,0.896,2,2S39.479,44.75,38.375,44.75z M38.375,38.125  c-1.105,0-2-0.896-2-2s0.895-2,2-2c1.104,0,2,0.896,2,2S39.479,38.125,38.375,38.125z M41.625,28.343c0,1.104-0.896,2-2,2h-15.25  c-1.105,0-2-0.896-2-2V17.315c0-1.105,0.895-2,2-2h15.25c1.104,0,2,0.895,2,2V28.343z';

	        return {
	            path: svgVbs,
	            fillColor: '#E26161',
	            fillOpacity: 1,
	            anchor: new google.maps.Point(25, 25),
	            strokeColor: 'black',
	            strokeWeight: 1,
	            rotation: 0,
	            scale: 0.9
	        }
        }

        this.getInfoPaneModel = function(feature)
        {
        	feature.name = feature.id;
        	feature.objectTitle = this.getObjectTitle();
        	
        	return new Uav(feature);
        }

        ObjectLayer.apply(this, arguments);
    }

    return VbsLayer;
});
