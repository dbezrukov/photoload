define([
    'jquery',
    'marionette',
    'underscore',
    'underscore.string',
    'vendors/label',
    'vendors/markerwithlabel'
], function($, Marionette, _, _str, Label, MarkerWithLabel){

	/* Base class for Friends and other layers */

    /** @constructor */
    function ObjectLayer() {
        var self = this;

        self.markers_ = {};
        self.photos_ = {};
    }

    ObjectLayer.prototype.hide = function() {
        clearInterval(this.refreshIntervalId_);

        var me = this;

        app.vent.off('object:selected', me.handleSelected);

        for (var id in me.markers_) {
            if (me.markers_.hasOwnProperty(id)) {
                deleteMarker(me.markers_, id);
            }
        }
    };

    ObjectLayer.prototype.show = function() {
        var me = this;
        
        var urlApi = me.getUrl();
        
        var urlModel = me.getModelUrl ? me.getModelUrl() : null;
        
        var modelOrientation = me.getModelOrientation ? me.getModelOrientation() : {
			heading: 180.,
			pitch: 0.,
			roll: 90.
		};

		var getModelMinSize = me.getModelMinSize ? me.getModelMinSize() : 64;

        me.handleSelected = function(userModel, shallFocus) {

			var id = userModel ? userModel.get('id') : null;

			/*
			// deselect 2d
			if (me.lastSelectedId && (me.lastSelectedId in me.markers_ === true)) {
				marker = me.markers_[me.lastSelectedId];
				marker.iw1.close();
				//marker.infoPane.onClosed();
			}
			*/

			// select 2d
			if (id in me.markers_ === true) {
				marker = me.markers_[id];
				marker.iw1.open(app.map, marker);
				//marker.infoPane.onOpened();
			}

			me.lastSelectedId = id;
		};

		app.vent.on('object:selected', me.handleSelected);

        this.lastReceivedTimestamp = 0;

        function processObjectMarkers(objectMarkers) {

            function processFeature(feature) {
                // update the latest timestamp for optimized location requests
                if (feature.utc > me.lastReceivedTimestamp) {
                    me.lastReceivedTimestamp = feature.utc;
                }

                // delete marker if created already
                if (feature.valid === false) {

                    if (feature.id in me.markers_ === true) {
                        deleteMarker(me.markers_, feature.id);
                    }

                } // create marker or update location
                else if (feature.lat && feature.lon) { 

                    var marker;

                    // marker if already known
                    if (feature.id in me.markers_ === true) {

                        marker = me.markers_[feature.id];

                    } else { // create one

                        marker = new MarkerWithLabel(me.getMarkerStyle(feature));

                        var uavModel = me.getInfoPaneModel(feature);
                        uavModel.set('online', true);

						app.collections.uavs.add(uavModel);

                        marker.infoPane = new me.InfoPane({
                            model: uavModel
                        });

						marker.iw1 = new google.maps.InfoWindow();
                        marker.iw1.setContent(marker.infoPane.render().el);

                        marker.infoPane.on('resizeRequest', function() {
                            marker.iw1.setContent(marker.iw1.getContent());
                        });

                        google.maps.event.addListener(marker, "click", function (e) { 
                            app.vent.trigger('object:selected', marker.infoPane.model, false);
                        });

                        google.maps.event.addListener(marker.iw1,'closeclick',function(){
                            app.vent.trigger('object:selected', null);
                        });

                        google.maps.event.addListener(app.map, 'click', function() {
                        	app.vent.trigger('object:selected', null);
                        });

                        me.markers_[feature.id] = marker;

                        marker.setMap(app.map);
                    }

                    marker.setPosition(new google.maps.LatLng(feature.lat, feature.lon));
                    marker.setIcon(me.getMarkerIcon(feature));

                    marker.infoPane.model.set('position', { lat: feature.lat, lon: feature.lon });
                    
                    /*
                    if (feature.id in me.photos_ === true) {

                        // photo url is known already
                        marker.setIcon({
                            url: me.photos_[feature.id],
                            //size: new google.maps.Size(76, 58),
                            //scaledSize: new google.maps.Size(, 50),
                            //origin: new google.maps.Point(0,0),
                            //anchor: new google.maps.Point(0, 0),
                        });
                        
                    } else {

                        $.get(me.getProfileUrl(feature), function(userInfo) {
                            var pngPhoto = userInfo.photo.replace(".jpg", ".png");
                            
                            //me.photos_[userInfo.id] = window.user.photostorage + '/w_50,h_50,r_25,c_scale' + pngPhoto;

                            me.photos_[userInfo.id] = window.user.photostorage + '/c_scale,h_50,w_50,r_7/c_scale,h_76,w_58,x_0,y_9,u_thumbnail' + pngPhoto;

                            //http://res.cloudinary.com/teamradar/image/upload/c_scale,h_50,w_50,r_7/c_scale,h_76,w_58,x_0,y_9,u_thumbnail/v1440605208/basic/bot5.png

                            if (userInfo.id in me.markers_ === true) {
                                
                                me.markers_[userInfo.id].setIcon({
                                    url: me.photos_[feature.id],
                                    //size: new google.maps.Size(50, 50),
                                    //scaledSize: new google.maps.Size(50, 50), // scaled size
                                    //origin: new google.maps.Point(0,0), // origin
                                    //anchor: new google.maps.Point(0, 0), // anchor
                                });
                            }

                        }, 'json')
                        .fail(function() {
                        });
                    }
                    */
                }

            }

            // detect whether the result is an array or object
            if (objectMarkers.forEach) {
                objectMarkers.forEach(function(feature) {
                    processFeature(feature);
                })
            } else {
                processFeature(objectMarkers);
            }
        }
        
        function fetchObjects() {
        	$.ajax({
                type: "GET",
                url: urlApi,
                data: { since: me.lastReceivedTimestamp },
                dataType: 'json',
                success: processObjectMarkers
            })
            .fail(function() {
                for (var id in me.markers_) {
                    if (me.markers_.hasOwnProperty(id)) {
                        deleteMarker(id).bind(me);
                    }
                }
            });
        }

        this.refreshIntervalId_ = setInterval(fetchObjects, 1000);
        fetchObjects();
    };

    function deleteMarker(list, id) {
        var me = this;

        var marker = list[id];

        marker.infoPane.model.set('online', false);
        marker.infoPane.model.destroy();

        marker.setMap(null);
        delete marker;
    }

    return ObjectLayer;
});
