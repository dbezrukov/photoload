define([
    'jquery',
    'marionette',
    'underscore',
    'underscore.string',
    'vendors/label'
], function($, Marionette, _, _str, Label){

    /** @constructor */
    function VbsTracksLayer() {
        this.polylines_= {};
        this.checkpoints_= {};
    }

    VbsTracksLayer.prototype.hide = function() {
        clearInterval(this.refreshIntervalId_);

        var me = this;

        for (var key in me.polylines_) {
            if (me.polylines_.hasOwnProperty(key)) {
                me.polylines_[key].setMap(null);
                delete me.polylines_[key];
            }
        }

        for (var key in me.checkpoints_) {
            if (me.checkpoints_.hasOwnProperty(key)) {
                me.checkpoints_[key].setMap(null);
                delete me.checkpoints_[key];
            }
        }
    };

    VbsTracksLayer.prototype.show = function() {
        var me = this;
        var url = this.getUrl();
        
        this.refreshIntervalId_ = setInterval(function() {
            $.get(url, function(vbsTracks) {

                var new_polylines = [];
                var new_checkpoints = [];

                vbsTracks.forEach(function(feature) {
                    
                    new_polylines.push(feature.id);

                    var trackCoordinates = [];

                    feature.track.forEach(function(coordinate) {
                        trackCoordinates.push(new google.maps.LatLng(coordinate.lat, coordinate.lon));

                        var checkpointId = feature.id + coordinate.timestamp;
                        
                        new_checkpoints.push(checkpointId);

                        var checkpoint;

                        if (checkpointId in me.checkpoints_ === true) {
                            checkpoint = me.checkpoints_[checkpointId];
                        } else {
                            checkpoint = new google.maps.Marker(me.getMarkerStyle(coordinate));

                            checkpoint.iw1 = new google.maps.InfoWindow({ 
                                content: me.getMarkerInfo(coordinate).content
                            });
                            
                            google.maps.event.addListener(checkpoint, "click", function (e) { 
                                checkpoint.iw1.open(app.map, this);
                            });

                            me.checkpoints_[checkpointId] = checkpoint;

                            checkpoint.setMap(app.map);
                        }

                        var position = new google.maps.LatLng(coordinate.lat, coordinate.lon);

                        checkpoint.setPosition(position);
                        checkpoint.setIcon(me.getMarkerIcon(feature));
                    });

                    var polyline;

                    if (feature.id in me.polylines_ === true) {
                        polyline = me.polylines_[feature.id];
                    } else {
                        polyline = new google.maps.Polyline({
                            geodesic: true,
                            strokeColor: me.palette.getColor(feature.id),
                            strokeOpacity: 0.9,
                            strokeWeight: 4
                        });
                        me.polylines_[feature.id] = polyline;
                    }

                    polyline.setPath(trackCoordinates);
                    polyline.setMap(app.map);
                });

                // polylines cleanup
                var current_polylines = [];
                for (var key in me.polylines_) {
                    if (me.polylines_.hasOwnProperty(key)) {
                        current_polylines.push(key);
                    }
                }

                var invalid_polylines = _.difference(current_polylines, new_polylines);
                invalid_polylines.forEach(function(id) {
                    me.polylines_[id].setMap(null);
                    delete me.polylines_[id];
                });


                // checkpoints cleanup
                var current_checkpoints = [];
                for (var key in me.checkpoints_) {
                    if (me.checkpoints_.hasOwnProperty(key)) {
                        current_checkpoints.push(key);
                    }
                }

                var invalid_checkpoints = _.difference(current_checkpoints, new_checkpoints);
                invalid_checkpoints.forEach(function(id) {
                    me.checkpoints_[id].setMap(null);
                    delete me.checkpoints_[id];
                });

            }, 'json')
            .fail(function() {
                for (var key in me.polylines_) {
                    if (me.polylines_.hasOwnProperty(key)) {
                        me.polylines_[key].setMap(null);
                        delete me.polylines_[key];
                    }
                }
            });
        }, 1000);
    };

    VbsTracksLayer.prototype.getMarkerStyle = function(feature)
    {
        function prettyMonth(date) {
            var months = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
            return date.getUTCDate() + ' ' + months[date.getUTCMonth()];
        }

        function checkTime(i) {
            if (i<10) {
                i="0" + i;
            }
            return i;
        }

        function formatDate(date) {
            return prettyMonth(date)
                + ' ' + checkTime(date.getHours())
                + ':' + checkTime(date.getMinutes())
                + ':' + checkTime(date.getSeconds())
        }

        var time = formatDate(new Date(feature.timestamp));

        return {
            title: _str.vsprintf('%s', [time]),
            zIndex: 1
        }
    }

    VbsTracksLayer.prototype.getMarkerInfo = function(feature)
    {
        function prettyMonth(date) {
            var months = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
            return date.getUTCDate() + ' ' + months[date.getUTCMonth()];
        }

        function checkTime(i) {
            if (i<10) {
                i="0" + i;
            }
            return i;
        }

        function formatDate(date) {
            return prettyMonth(date)
                + ' ' + checkTime(date.getHours())
                + ':' + checkTime(date.getMinutes())
                + ':' + checkTime(date.getSeconds())
        }

        var time = formatDate(new Date(feature.timestamp));

        return {
            content: _str.vsprintf('%s', [time])
        }
    }

    VbsTracksLayer.prototype.getMarkerIcon = function(feature)
    {
        var me = this;
        var svgPoint = 'M 10, 10 m -7.5, 0 a 7.5,7.5 0 1,0 15,0 a 7.5,7.5 0 1,0 -15,0';

        return {
            path: svgPoint,
            fillColor: me.palette.getColor(feature.id),
            fillOpacity: 1,
            anchor: new google.maps.Point(10, 10),
            strokeColor: 'black',
            strokeWeight: 1,
            rotation: 0,
            scale: 0.7
        }
    }

    VbsTracksLayer.prototype.getUrl = function() 
    {
        return '/profiles/vbs/tracks?timestamp=' + Date.now();
    }

    return VbsTracksLayer;
});