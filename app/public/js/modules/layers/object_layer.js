define([
    'jquery',
    'marionette',
    'underscore',
    'underscore.string',
    'vendors/label',
    'vendors/markerwithlabel'
], function($, Marionette, _, _str, Label, MarkerWithLabel){

	/* Base class for Bla, Robot and Vbs layers */

    /** @constructor */
    function ObjectLayer(/*handleObjects*/) {
        var self = this;

        self.markers_ = {};
        self.markers3d_ = {};
    }

    ObjectLayer.prototype.hide = function() {
        clearInterval(this.refreshIntervalId_);
        clearInterval(this.check3dSelectionId_);

        var me = this;

        app.vent.off('object:selected', me.handleSelected);

        for (var id in me.markers_) {
            if (me.markers_.hasOwnProperty(id)) {
                me.markers_[id].setMap(null);
                delete me.markers_[id];

                app.map3d.entities.remove(me.markers3d_[id]);
        		delete me.markers3d_[id];
            }
        }
    };

    ObjectLayer.prototype.show = function() {
        var me = this;
        
        var urlApi = me.getUrl();
        
        var urlModel = me.getModelUrl ? me.getModelUrl() : null;
        
        var modelOrientation = me.getModelOrientation ? me.getModelOrientation() : {
			heading: 180.,
			pitch: 0.,
			roll: 90.
		};

		var getModelMinSize = me.getModelMinSize ? me.getModelMinSize() : 64;

        me.handleSelected = function(userModel, shallFocus) {

			var id = userModel ? userModel.get('id') : null;

			/*
			// deselect 2d
			if (me.lastSelectedId && (me.lastSelectedId in me.markers_ === true)) {
				marker = me.markers_[me.lastSelectedId];
				marker.iw1.close();
				marker.infoPane.onClosed();
			}
			*/

            // select 2d
			if (id in me.markers_ === true) {
				marker = me.markers_[id];

                if (marker.clickable) {
				    marker.iw1.open(app.map, marker);
				    marker.infoPane.onOpened();
                }
			}

			me.lastSelectedId = id;

			// select 3d
			if (id in me.markers3d_ === true) {
				app.map3d.selectedEntity = me.markers3d_[id];

				if (shallFocus) {
					app.map3d.zoomTo(app.map3d.selectedEntity, new Cesium.HeadingPitchRange(Cesium.Math.toRadians(0.), Cesium.Math.toRadians(-30.), 3000));
				}
			} else {
				app.map3d.selectedEntity = undefined;
			}
		};


		app.vent.on('object:selected', me.handleSelected);

        this.check3dSelectionId_ = setInterval(function() {
        	if (me.selected3dEntity !== app.map3d.selectedEntity) {
        		me.selected3dEntity = app.map3d.selectedEntity;

         		if (me.selected3dEntity && me.selected3dEntity.feature) {
         			
         			app.vent.trigger('object:selected',
						me.selected3dEntity.feature.id, 
						{ 
							lat: me.selected3dEntity.feature.lat,
							lon: me.selected3dEntity.feature.lon
						}, 
						false
					);

         		} else {
         			app.vent.trigger('object:selected', null);
         		}
        	}
        }, 3000);

        this.refreshIntervalId_ = setInterval(function() {
            $.get(urlApi, function(objectMarkers) {

                var new_markers = [];
				var modified = false;
                
                objectMarkers.forEach(function(feature) {
                    
                    new_markers.push(feature.id);

                    var marker;

                    if (feature.id in me.markers_ === true) {
                        
                        marker = me.markers_[feature.id];
                        
                    } else {

                        marker = new MarkerWithLabel(me.getMarkerStyle(feature));

						var uavModel = me.getInfoPaneModel(feature);
						uavModel.set('online', true);

						app.collections.uavs.add(uavModel);

                        marker.infoPane = new me.InfoPane({
                            model: uavModel
						});

                        marker.iw1 = new google.maps.InfoWindow();
                        marker.iw1.setContent(marker.infoPane.render().el);

                        marker.infoPane.on('resizeRequest', function() {
                            marker.iw1.setContent(marker.iw1.getContent());
                        });

                        google.maps.event.addListener(marker, "click", function (e) { 
                        	app.vent.trigger('object:selected', marker.infoPane.model, false);
                        });

                        google.maps.event.addListener(marker.iw1,'closeclick',function(){
                            app.vent.trigger('object:selected', null);
                        });

                        me.markers_[feature.id] = marker;

                        marker.setMap(app.map);

                        modified = true;
                    }

                    marker.setPosition(new google.maps.LatLng(feature.lat, feature.lon));
                    marker.setIcon(me.getMarkerIcon(feature));

                    marker.infoPane.model.set('position', feature);

                    var hasPosition = (Math.abs(Math.floor(feature.lat)) > 0 && Math.abs(Math.floor(feature.lon)) > 0);

                    marker.setVisible(hasPosition);
					
					if (urlModel && feature.lon && feature.lat && feature.ele && feature.angle) {

						var position = Cesium.Cartesian3.fromDegrees(feature.lon, feature.lat, feature.ele);
				    	var heading = Cesium.Math.toRadians(feature.angle + modelOrientation.heading);
				    	var pitch = Cesium.Math.toRadians(modelOrientation.pitch);
				    	var roll = Cesium.Math.toRadians(modelOrientation.roll);
				    	var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, heading, pitch, roll);

						if (feature.id in me.markers3d_ === false) {
					    	
					    	if (urlModel === 'files/models/henry.gltf') {

								me.markers3d_[feature.id] = app.map3d.entities.add({
							        position : position,
							        point : {
							            show : true, // default
							            color : Cesium.Color.RED, // default: WHITE
							            pixelSize : 6, // default: 1
							            outlineColor : Cesium.Color.RED, // default: BLACK
							            outlineWidth : 1 // default: 0
							        }
						    	})
							} else {
						    	me.markers3d_[feature.id] = app.map3d.entities.add({
							        name : (me.getObjectTitle ? me.getObjectTitle() : '') + ' ' + feature.name,
							        feature: feature,
							        model : {
							            uri : urlModel,
							            minimumPixelSize : getModelMinSize,
							        }
						    	});
					    	}
							
					    	if (me.getObjectTitle && me.getObjectTitle() === 'ГУС Машина') {
					    		
					   			var redEllipse = app.map3d.entities.add({
								    position: Cesium.Cartesian3.fromDegrees(feature.lon, feature.lat),
								    name : 'Зона работы',
								    ellipse : {
								        semiMinorAxis : 1500.0,
								        semiMajorAxis : 1500.0,
								        material : Cesium.Color.LIGHTGREEN.withAlpha(0.3),
								        outline : true,
								        outlineColor : Cesium.Color.BLUE
								    }
								});
					    	}
						} 

						me.markers3d_[feature.id].position = position;
						me.markers3d_[feature.id].orientation = orientation;

						if (me.markers3d_[feature.id].model) {
							me.markers3d_[feature.id].model.show = hasPosition;
						}
					}
                });

                // markers cleanup
                var current_markers = [];
                for (var id in me.markers_) {
                    if (me.markers_.hasOwnProperty(id)) {
                        current_markers.push(id);
                    }
                }

                var invalid_markers = _.difference(current_markers, new_markers);
                invalid_markers.forEach(function(id) {
                    me.markers_[id].setMap(null);
                    delete me.markers_[id];

                    app.map3d.entities.remove(me.markers3d_[id]);
        			delete me.markers3d_[id];

                    modified = true;
                });

            }, 'json')
            .fail(function() {
                for (var id in me.markers_) {
                    if (me.markers_.hasOwnProperty(id)) {
                        
                        me.markers_[id].setMap(null);
                        delete me.markers_[id];

                        app.map3d.entities.remove(me.markers3d_[id]);
        				delete me.markers3d_[id];
                    }
                }
            });
        }, 1000);
    };

    return ObjectLayer;
});
