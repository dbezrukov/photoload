define([
    'underscore',
    'app',
    'marionette'
], function(_, app, Marionette) {

    var ModalManager = app.module("ModalManager", function() {
        this.startWithParent = false;
        
        this.addInitializer(function(options){
            options || (options = {});

            var self = this;
            
            app.vent.on('modal:show', self.onShowModal, self);
        })

        this.addFinalizer(function(){
        })
    });

    ModalManager.onShowModal = function(view) {
        var self = this;

        app.getRegion('modal').show(view);
    }

    return ModalManager;
});
