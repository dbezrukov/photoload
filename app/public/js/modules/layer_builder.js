define(['underscore.string', 
        './layers/bla_layer',
        './layers/bla_tracks_layer',
        './layers/bla_layer2',
        './layers/bla_tracks_layer2',
        './layers/npu_layer',
        './layers/robot_layer',
        './layers/vbs_layer',
        './layers/vbs_tracks_layer',
        './layers/vbs_calls_layer',
        './layers/gus_base_layer',
        './layers/kml_layer',
        './layers/navlost_layer'],
    function(_str, BlaLayer, BlaTracksLayer, BlaLayer2, BlaTracksLayer2, NpuLayer, RobotLayer, 
    			   VbsLayer, VbsTracksLayer, VbsCallsLayer, ayer, KmlLayer, NavlostLayer) {

    var module_options, layers = {};

    // palette for layers
    var palette = {
        colors: ['#CFDAF0',
				 '#B5DCE1',
				 '#F4E0E9',
				 '#D7E0B1',
				 '#F4EFE2',
				 '#F4D9D0',
				 '#F4E3C9'],

        getColor: function(id) {
            var self = this;
            
            if (!self.registeredColors) {
                self.registeredColors = {};
            }

            if (!self.lastColor) {
                self.lastColor = 0;
            }

            var color;

            if (id in self.registeredColors === true) {
                color = self.registeredColors[id];
            } else {
                self.lastColor++;

                if (self.lastColor === self.colors.length) {
                    self.lastColor = 0;
                }

                color = self.colors[self.lastColor];
                self.registeredColors[id] = color;
            }
            return color;
        }
    }

    // Start with the constructor
    function LayerBuilder(options) {
        module_options = options;
    }

    LayerBuilder.prototype = {
        constructor: LayerBuilder
    }

    /// Constructing layers --------------------------------------------------------------------------

    LayerBuilder.prototype.toggleLayer = function(name, visibility) {
        var layer;

        if (name in layers === true) {
            layer = layers[name];
        } else {
            switch (name) {
            case 'objects::bla':
                layer = new BlaLayer(/*module_options.handleObjects.bind({ layer: "БЛА" })*/);
                break;
            case 'objects::bla_tracks': 
                layer = new BlaTracksLayer();
                break;
            case 'objects::bla2':
                layer = new BlaLayer2();
                break;
            case 'objects::bla_tracks2':
                layer = new BlaTracksLayer2();
                break;
            case 'objects::npu':
                layer = new NpuLayer(/*module_options.handleObjects.bind({ layer: "НПУ" })*/);
                break;
            case 'objects::robots': 
                layer = new RobotLayer(/*module_options.handleObjects.bind({ layer: "Наземные средства" })*/);
                break;
            case 'objects::vbs': 
                layer = new VbsLayer(/*module_options.handleObjects.bind({ layer: "ВБС" })*/);
                break;
            case 'objects::vbs_tracks': 
                layer = new VbsTracksLayer();
                break;
            case 'gus::base':
                layer = new GusBaseLayer(/*module_options.handleObjects.bind({ layer: "ГУС Машина" })*/);
				break;
            case 'gus::vbs':

				var vbsControls = $('.navbar #vbs__toggle');
				if (vbsControls.hasClass('hidden')) {
					vbsControls.removeClass('hidden');
					vbsControls.click(function () {
    					app.vent.trigger("toggle:vbs");
					});
				}

                layer = new VbsLayer(/*module_options.handleObjects.bind({ layer: "ГУС ВБС" })*/);
                
                layer.getUrl = function() {
        			return '/data/3gpp/phone';
        		}

        		require(['../../js/views/phone_info_pane'], function (InfoPane) {
					layer.InfoPane = InfoPane;
				});

                break;
            case 'gus::calls':
                layer = new VbsCallsLayer();
                break;
            case 'vp::199':
                layer = new KmlLayer({
                    urls: ['https://oko-center.ru/files/vp/zonearea_ctr.kmz'],
                    map: app.map,
                    minZoom: 6
                });
                break;
            case 'vp::vt':
                layer = new KmlLayer({
                    urls: ['https://oko-center.ru/files/vp/vp_ap.kmz',
                           'https://oko-center.ru/files/vp/vp_rw.kmz'],
                    map: app.map,
                    minZoom: 6
                });
                break;
            case 'vp::204':
            case 'vp::203':
            case 'vp::212':
            case 'vp::notam':
            case 'vp::mvl':
            case 'vp::ad':
            case 'vp::sz':
                var aopaCode = name.split('::')[1];
                var url = _str.sprintf('http://aopa.ru/maps/%s_v02.kmz', aopaCode);
                
                layer = new KmlLayer({
                    urls: [url],
                    map: app.map,
                    minZoom: 6
                });
                break;
            case 'weather::google::temp':
                layer = new google.maps.weather.WeatherLayer({
                    temperatureUnits: google.maps.weather.TemperatureUnit.CELSIUS,
                    windSpeedUnits: google.maps.weather.WindSpeedUnit.METERS_PER_SECOND
                });
                break;
            case 'weather::navlost::wind':
                layer = new NavlostLayer({ 
                    layerType: 'wind',
                    map: app.map 
                });
                break;
             case 'weather::navlost::pressure':
                layer = new NavlostLayer({ 
                    layerType: 'prmslmsl',
                    map: app.map 
                });
                break;
            case 'weather::navlost::temp':
                layer = new NavlostLayer({ 
                    layerType: 'tmpprs_g',
                    map: app.map 
                });
                break;
            case 'weather::navlost::clouds':
                layer = new NavlostLayer({ 
                    layerType: 'presclt',
                    legendSrc: '/img/legend_clouds.png',
                    map: app.map 
                });
                break;
            default:
                break;
            }

            if (!layer) {
                return;
            }

            layers[name] = layer;
            layer.palette = palette;
        }

        if (layer.setMap) {
            layer.setMap(visibility === true ? app.map : null);
        } else { // for co
            visibility === true ? layer.show() : layer.hide();
        }
    }

    return LayerBuilder;
});


