require.config({
    waitSeconds: 30,
    baseUrl: './js/',

    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-multiselect': 'vendors/bootstrap-multiselect',
        'underscore': 'bower_components/underscore/underscore-min',
        'underscore.string': 'bower_components/underscore.string/dist/underscore.string',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'bootstrap': {
            deps: ['jquery'],
        },
		'bootstrap-multiselect': {
            deps: [ 'jquery', 'bootstrap' ],
            exports: '$.fn.multiselect'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([
    'jquery',
    'marionette',
    'models/sync',
    'collections/servers',
    'views/admin_sync_pane',
    'admin_groups', 
    'admin_users', 
    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!bower_components/Ionicons/css/ionicons.min.css',
    'css!stylesheets/bootstrap-common',
    'css!stylesheets/admin'
], function($, Marionette, Sync, Servers, SyncPane) {

	var ModalRegion = Marionette.Region.extend({
        el: "#modal__region",

        constructor: function() {
            Marionette.Region.prototype.constructor.apply(this, arguments);
     
            this.ensureEl();
            this.$el.on('hidden', {region:this}, function(event) {
                event.data.region.close();
            });
        },
     
        onShow: function(view) {
            view.on("close", this.onClose, this);
            this.$el.modal('show');
        },
     
        onClose: function() {
            this.$el.modal('hide');
        }
    });

    var app = new Marionette.Application;

    window.app = app;

    app.on("initialize:before", function() {
        app.addRegions({
        	"modal": ModalRegion,
			"syncRegion": "#sync__region"
        });
    });

    app.on("initialize:after", function() {

    	var sync = new Sync;
    	var servers = new Servers;

        var syncPane = new SyncPane({ 
        	model: sync,
        	collection: servers
        });
        app.getRegion("syncRegion").show(syncPane);
    });

    $(function() {

    	$('body').show();
    	app.start({})
    });

    // helper functions
	window.multiselectData = function(entities) {
	    var data = [];

	    entities.forEach(function(entity) {
	        data.push({ label: entity.name, value: entity.id });
	    });

	    return data;
	}

});
