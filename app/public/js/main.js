require.config({
    waitSeconds: 30,
    baseUrl: './js/',

    paths: {
    	'jquery': 'bower_components/jquery/jquery.min',
    	'jqueryui': 'bower_components/jqueryui/ui/minified/jquery-ui.min',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-multiselect': 'vendors/bootstrap-multiselect',
        'underscore': 'bower_components/underscore/underscore-min',
        'underscore.string': 'bower_components/underscore.string/dist/underscore.string',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
        'buzz': 'bower_components/buzz/dist/buzz',
        'jquery-hoverintent': 'bower_components/jquery-hoverintent/jquery.hoverIntent',
        'jquery-dragscrollable': 'vendors/dragscrollable',
        'leaflet': 'vendors/leaflet/leaflet',
        'photoswipe': 'vendors/photoswipe/photoswipe',
        'photoswipe-ui': 'vendors/photoswipe/photoswipe-ui-default',
        'bootstrap-multiselect': 'vendors/bootstrap-multiselect',
        'bootstrap-datepicker': 'vendors/bootstrap-datepicker/bootstrap-datepicker',
        'exif-js': 'bower_components/exif-js/exif',
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'bootstrap': {
            deps: ['jquery'],
        },
        'bootstrap-multiselect': {
            deps: ['jquery', 'bootstrap'],
            exports: '$.fn.multiselect'
        },
        'jquery-hoverintent': {
            deps: ['jquery'],
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        },
        'bootstrap/bootstrap-slider': { deps: ['jquery'], exports: '$.fn.slider' }, 
        'bootstrap/bootstrap-affix': { deps: ['jquery'], exports: '$.fn.affix' },
        'bootstrap/bootstrap-alert': { deps: ['jquery'], exports: '$.fn.alert' },
        'bootstrap/bootstrap-button': { deps: ['jquery'], exports: '$.fn.button' },
        'bootstrap/bootstrap-carousel': { deps: ['jquery'], exports: '$.fn.carousel' },
        'bootstrap/bootstrap-collapse': { deps: ['jquery'], exports: '$.fn.collapse' },
        'bootstrap/bootstrap-dropdown': { deps: ['jquery'], exports: '$.fn.dropdown' },
        'bootstrap/bootstrap-modal': { deps: ['jquery'], exports: '$.fn.modal' },
        'bootstrap/bootstrap-popover': { deps: ['jquery'], exports: '$.fn.popover' },
        'bootstrap/bootstrap-scrollspy': { deps: ['jquery'], exports: '$.fn.scrollspy'        },
        'bootstrap/bootstrap-tab': { deps: ['jquery'], exports: '$.fn.tab' },
        'bootstrap/bootstrap-tooltip': { deps: ['jquery'], exports: '$.fn.tooltip' },
        'bootstrap/bootstrap-transition': { deps: ['jquery'], exports: '$.support.transition' },
        'bootstrap/bootstrap-typeahead': { deps: ['jquery'], exports: '$.fn.typeahead'  },
        'bootstrap-multiselect': {
            deps: [ 'jquery', 'bootstrap' ],
            exports: '$.fn.multiselect'
        },
        'bootstrap-datepicker' : {
    		deps: ['jqueryui', 'bootstrap'],
    		exports: '$.fn.datepicker'
		},
        'jquery-dragscrollable': {
            deps: [ 'jquery' ],
            exports: '$.fn.dragscrollable'
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([
    'jquery',
    'app',
    'models/account'

], function($, app, Account) {

    window.app = app;

    app.addInitializer(function() {
    })

    $(function() {
        
        app.account = new Account();

		app.account.fetch({
			success: function(){
				app.start({})

			}
	    });
    });
});
