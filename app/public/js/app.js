define([
    'jquery',
    'underscore',
    'marionette',
    
    'vendors/photometric',

    'views/navbar_pane',
    'views/photo_pane',
    'views/messenger_pane',
    'views/chat_toggle',

    'views/photo_item',
    'views/photo_search_control',
    'views/photo_viewer',
	'views/photo_uploader',
	
	'views/file_downloader',

	'views/tile_item',
    'views/file_uploader',

    'views/sit_item',
    'views/sit_uploader',
	
    'collections/users',
    'collections/photos_bla',
    'collections/tiles',
    'collections/sits',
    
], function($, _, Marionette, PhotoMetric, 
	NavbarPane, PhotoPane, MessengerPane, ChatToggle, 
	PhotoItem, SearchControl, PhotoViewer, PhotoUploader, 
	FileDownloader, 
	TileItem, FileUploader,
	SitItem, SitUploader,
	Users, PhotosBla, Tiles, Sits) {

    var ModalRegion = Marionette.Region.extend({
        el: "#modal__region",

        constructor: function() {
            Marionette.Region.prototype.constructor.apply(this, arguments);
     
            this.ensureEl();
            this.$el.on('hidden', {region:this}, function(event) {
                event.data.region.close();
            });
        },
     
        onShow: function(view) {
            view.on("close", this.onClose, this);
            this.$el.modal('show');
        },
     
        onClose: function() {
            this.$el.modal('hide');
        }
    });

    var app = new Marionette.Application;
    app.collections = {};

    var app_options;

    app.addInitializer(function(options) {
        options || (options = {});
        app_options = options;
    });

    app.on("initialize:before", function() {
        app.addRegions({
            "modal": ModalRegion,
            "navbarRegion": "#navbar__region",
            "photoRegion": "#photo__region",
            "tilesRegion": "#tiles__region",
            "sitsRegion": "#sits__region",
            /*
            "messengerRegion": "#messenger__region",
            "videoRegion": "#video__region",
            "tooltipRegion": "#tooltip__region",
            "chatToggleRegion": "#chat__toggle",
            */
        });

        app.vent.on("toggle:chat", function(){
            $('#messenger__region').toggle();
            $('#video__region').toggle();
        });

        window.timedelta = Date.now() - window.servertime;
        if (window.is_offline && window.tms_url.length === 0) {
        	window.tms_url = location.protocol + "//" + location.hostname;
        }

        if (window.is_offline && window.tms_url.length === 0) {
    		window.tms_url = location.protocol + "//" + location.hostname;
    	}
    });

    app.on("initialize:after", function() {

    	setupCollections();

        var navbarPane = new NavbarPane({ 
        	model: app.account 
        });
        app.getRegion("navbarRegion").show(navbarPane);

        function formatDate(date) {
			return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
		}

        var photoPane = new PhotoPane({ 
        	model : app.account,
			collection : app.collections.photos_bla,
			itemView: PhotoItem,
			itemViewClassName: 'photo-item',
			searchControl: SearchControl, 
			fileViewer: PhotoViewer, 
			fileUploader: PhotoUploader, 
			fileDownloader: FileDownloader, 
			fileDownloaderUrl: '/photo/bla/archive', 
			
			getFileDownloaderFilename: function() {
				return 'Фото-' + formatDate(new Date()) + '.7z';
			}
        });
        app.getRegion("photoRegion").show(photoPane);

        var tilesPane = new PhotoPane({ 
        	model : app.account,
			collection : app.collections.tiles,
			itemView: TileItem,
			itemViewClassName: 'tile-item',
			searchControl: SearchControl, 
			fileViewer: null, 
			fileUploader: FileUploader, 
			fileDownloader: FileDownloader, 
			fileDownloaderUrl: '/tiles/archive', 

			getFileDownloaderFilename: function() {
				return 'Тайлы-' + formatDate(new Date()) + '.7z';
			}
        });
        app.getRegion("tilesRegion").show(tilesPane);

        var sitsPane = new PhotoPane({ 
        	model : app.account,
			collection : app.collections.sits,
			itemView: SitItem,
			itemViewClassName: 'sit-item',
			searchControl: SearchControl, 
			fileViewer: null, 
			fileUploader: SitUploader, 
			fileDownloader: FileDownloader, 
			fileDownloaderUrl: '/sits/archive', 

			getFileDownloaderFilename: function() {
				return 'Обстановка-' + formatDate(new Date()) + '.7z';
			}
        });
        app.getRegion("sitsRegion").show(sitsPane);

        setupChatControls();


        function setupCollections() {
        	app.collections.users = new Users();
			app.collections.users.fetch();

			app.collections.photos_bla = new PhotosBla([], { users: app.collections.users });
        	app.collections.tiles = new Tiles([], { users: app.collections.users });
        	app.collections.sits = new Sits([], { users: app.collections.users });
        }

        function setupChatControls() {
			var chatControls = $('.navbar #chat__toggle');
        	chatControls.removeClass('hidden');
                
			chatControls.click(function () {
        		app.vent.trigger("toggle:chat");
			});

			app.vent.trigger("toggle:chat");
		}

    });

    app.geo = new PhotoMetric();

    return app;
});
