require([
    'jquery', 'bootstrap'
], function($) {

    var ui = {};

    $(function() {

    	populateServerTable();

	    // object management
	    $('#btnAddServer').on('click', addServer);
	    
	    $('#serverList table tbody').on('click', 'td a.linkediteserver', editServer);
	    $('#serverList table tbody').on('click', 'td a.linkdeleteserver', deleteServer);
    });

	function populateServerTable() {
	    var tableContent = '';

	    $.getJSON( '/servers', function( data ) {
	        
	        $.each(data, function(index){
	            tableContent += '<tr>';
	            tableContent += '<td>' + this.id + '</td>';
	            tableContent += '<td>' + this.url + '</td>';
	            tableContent += '<td>' + this.name + '</td>';
	            tableContent += '<td>' + 'время' + '</td>';
	            tableContent += '<td>';
	            tableContent += '<a href="#" class="linkediteserver" style="text-decoration: none" rel="' + this.id + '">Редактировать</a> &middot ';
	            tableContent += '<a href="#" class="linkdeleteserver" style="text-decoration: none" rel="' + this.id + '">Удалить</a>';
	            tableContent += '</td>';
	            tableContent += '</tr>';
	        });

	        $('#serverList table tbody').html(tableContent);
	    });
	};

	function addServer(event) {
	    event.preventDefault();

	    $('#modalServerEdit').modal('show');
	    $('#modalServerEdit #btnSubmit').text('Создать');

	    var form = $('#modalServerEdit form');

	    form.attr('serverId', '');

	    form.find('input').val('');

	    return false;    
	};

	function editServer(event) {
	    event.preventDefault();

	    $('#modalServerEdit').modal('show');
	    $('#modalServerEdit #btnSubmit').text('Сохранить');

	    var id = $(this).attr('rel');

	    var form = $('#modalServerEdit form');
	    form.attr('serverId', id);

	    $.getJSON( '/servers/' + id, function( data ) {
	        form.find('#objectId').val(data.id);
	        form.find('#objectName').val(data.name);
	    });

	    return false;
	};

	function robotBoundariesSubmit(event) {
	    var newRoundaries = {
	        'lower_id': $('#robotIdBoundariesEdit #robotLowerId').val(),
	        'upper_id': $('#robotIdBoundariesEdit #robotUpperId').val()
	    }

	    $.ajax({
	        type: 'PUT',
	        data: newRoundaries,
	        url: '/objects/robot_id_boundaries',
	        dataType: 'JSON'
	    })
	    .done(function( response ) {
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	}

	function deleteObject(event) {
	    event.preventDefault();

	    var confirmation = confirm('Удалить описание объекта?');

	    if (confirmation !== true) {
	        return false;
	    }

	    $.ajax({
	        type: 'DELETE',
	        url: '/objects/names/' + $(this).attr('rel')
	    })
	    .done(function( response ) {
	        populateObjectTable();
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	};

	function objectSubmit(event) {
	    var form = $('#modalObjectEdit form');

	    var id = form.find('#objectId').val();
	    var name = form.find('#objectName').val();

	    var objectId = form.attr('objectId');

	    if (jQuery.inArray("", [id, name]) !== -1) {
	        alert('Пожалуйста, введите id и имя');
	        return false;
	    }

	    var newObject = {
	        'id': id,
	        'name': name,
	    }

	    $('#modalObjectEdit').modal('hide');

	    $.ajax({
	        type: 'POST',
	        data: newObject,
	        url: objectId === '' ? '/objects/names' : '/objects/names/' + objectId,
	        dataType: 'JSON'
	    })
	    .done(function( response ) {
	        populateObjectTable();
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	}
	
});
