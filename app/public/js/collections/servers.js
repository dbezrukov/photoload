define([
    'backbone',
    'models/server'
], function(Backbone, Server) {
    return Backbone.Collection.extend({
        model: Server,
        url: function() { 
        	return '/sync/servers';
        }
    });
});