define([
	"underscore",
	"underscore.string",
    'backbone',
    'models/photo'
], function(_, _str, Backbone, Photo) {
	
	var lastQuery;
	var offset = 0;
	var somePhotosLoaded = false;

    return Backbone.Collection.extend({
        model: Photo,
        url: '/photo/bla',

		initialize: function(models, options) {
    		this.users = options.users;
  		},

		doFetch: function(query){
			var self = this;

			query.offset = offset; 
			query.count = somePhotosLoaded ? 20 : 75;

			var query_string = $.param(query);

			$.ajax({
				type: "GET",
	        	url: self.url + '/query',
	      		data: query_string,
	    		dataType: 'JSON',

	    		success: function(data){
	    			somePhotosLoaded = true;
	    			offset += query.count;

	    			self.trigger(data.length < query.count ? 'nomore' : 'hasmore');

	    			data.forEach(function(photo) {
	    				
	    				photo.src = self.url + '/' + photo.album + '/' + photo.id,
	    				photo.src_preview = self.url + '/' + photo.album + '/' + photo.id + '/preview',

	    				photo.time_string = formatDate(new Date(photo.utc * 1000));
	    				
	    				var user = self.users.get(photo.userId);
	    				photo.user_name = user ? user.get('name') : 'недоступно';
	    				
	    				var coordinates = app.account.get('usingPlaneCoordinates')
			            	? _str.vsprintf('X: %s<br>Y: %s<br>', [ photo.x.toFixed(), photo.y.toFixed() ])
			            	: _str.vsprintf('Широта: %f<br>Долгота: %f<br>', [ photo.lat, photo.lon ]);

			            var info = _str.vsprintf(
			            	'Борт: %s<br>' +
			                'Загрузил: %s<br>' +
			                'Дата: %s<br><br>' +
			                '%s' +
			                'Курс: %f°<br>' +
			                'Скорость: %f км/ч<br>' +
			                'Высота: %f м', [
			                	photo.album,
			                	photo.user_name,
			                	photo.time_string,
			                	coordinates,
			                	photo.hdg,
			                	photo.speed,
			                	photo.hgt
			                ]
						)

						if (photo.roll && photo.pch && photo.yaw) {
							info += _str.vsprintf(
							'<br><br>' +
			            	'Крен: %f°<br>' +
			                'Тангаж: %f°<br>' +
			                'Рыскание: %f°', [
			            		photo.roll,
								photo.pch,
								photo.yaw
			            	])
						}

						if (photo.cameraF && photo.cameraWidth && photo.cameraHeight) {
							info += _str.vsprintf(
							'<br><br>' +
			            	'Фокусное расстояние: %f мм<br>' +
			                'Матрица: %s мм', [
			            		photo.cameraF,
			                	round(photo.cameraWidth, 1) + 'x' + round(photo.cameraHeight, 1)
			            	])
						}

						// plain text
						photo.info = info;
						
						// html-formatted text
						photo.title = photo.info.replace(/<br>/g, '\n');

	    				self.add(photo);
	    			});
	    		},
	    		error: function(xhr){
	    		}
			});

			function formatDate(date) {
				return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
			}

			function round(value, places) {
				if (!value) {
					return 0;
				}
				var newVal = Math.round(value*Math.pow(10, places)) / Math.pow(10, places);
				return parseFloat(newVal);
			}
		},

		fetch: function(query_object){
            var self = this;

            offset = 0;
            somePhotosLoaded = false;

            if (query_object) {
            	lastQuery = query_object;
            }

            self.reset();
            self.doFetch(lastQuery);
		},

		fetchMore: function(){
			var self = this;

			if (lastQuery) {
            	self.doFetch(lastQuery);
            }
		}
    });
});
