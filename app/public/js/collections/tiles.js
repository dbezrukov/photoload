define([
	"underscore",
	"underscore.string",
    'backbone',
    'models/tile'
], function(_, _str, Backbone, Tile) {

    var lastQuery;
	var offset = 0;
	var somePhotosLoaded = false;

    return Backbone.Collection.extend({
        model: Tile,
        url: '/tiles',

		initialize: function(models, options) {
    		this.users = options.users;
  		},

		doFetch: function(query){
			var self = this;

			function formatDate(date) {
				return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
			}
			
			query.offset = offset; 
			query.count = somePhotosLoaded ? 20 : 75;

			var query_string = $.param(query);

			$.ajax({
				type: "GET",
	        	url: self.url,
	      		data: query_string,
	    		dataType: 'JSON',

	    		success: function(data){
	    			somePhotosLoaded = true;
	    			offset += query.count;

	    			self.trigger(data.length < query.count ? 'nomore' : 'hasmore');

	    			data.forEach(function(tile) {
	    				
	    				tile.src = '/img/kmz64.png',
	    				tile.src_preview = '/img/kmz64.png',

	    				tile.time_string = formatDate(new Date(tile.utc * 1000));
	    				
	    				var user = self.users.get(tile.userId);
	    				tile.user_name = user ? user.get('name') : 'недоступно';
	    				
	    				tile.title = tile.name;

	    				self.add(tile);
	    			});
	    		},
	    		error: function(xhr){
	    		}
			});
		},

		fetch: function(query_object){
            var self = this;

            offset = 0;
            somePhotosLoaded = false;

            if (query_object) {
            	lastQuery = query_object;
            }

            self.reset();
            self.doFetch(lastQuery);
		},

		fetchMore: function(){
			var self = this;

			if (lastQuery) {
            	self.doFetch(lastQuery);
            }
		}
    });
});
