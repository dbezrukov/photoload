define([
	"underscore",
	"underscore.string",
    'backbone',
    'models/sit'
], function(_, _str, Backbone, Sit) {

    var lastQuery;
	var offset = 0;
	var somePhotosLoaded = false;

    return Backbone.Collection.extend({
        model: Sit,
        url: '/sits',

		initialize: function(models, options) {
    		this.users = options.users;
  		},

		doFetch: function(query){
			var self = this;

			function formatDate(date) {
				return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
			}
			
			query.offset = offset; 
			query.count = somePhotosLoaded ? 20 : 75;

			var query_string = $.param(query);

			$.ajax({
				type: "GET",
	        	url: self.url,
	      		data: query_string,
	    		dataType: 'JSON',

	    		success: function(data){
	    			somePhotosLoaded = true;
	    			offset += query.count;

	    			self.trigger(data.length < query.count ? 'nomore' : 'hasmore');

	    			data.forEach(function(sit) {
	    				
	    				sit.src = '/img/sit64.png',
	    				sit.src_preview = '/img/sit64.png',

	    				sit.time_string = formatDate(new Date(sit.utc * 1000));
	    				
	    				var user = self.users.get(sit.userId);
	    				sit.user_name = user ? user.get('name') : 'недоступно';
	    				
	    				sit.title = sit.name;

	    				self.add(sit);
	    			});
	    		},
	    		error: function(xhr){
	    		}
			});
		},

		fetch: function(query_object){
            var self = this;

            offset = 0;
            somePhotosLoaded = false;

            if (query_object) {
            	lastQuery = query_object;
            }

            self.reset();
            self.doFetch(lastQuery);
		},

		fetchMore: function(){
			var self = this;

			if (lastQuery) {
            	self.doFetch(lastQuery);
            }
		}
    });
});
