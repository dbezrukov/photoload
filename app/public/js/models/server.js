define([
    'underscore',
    'backbone',
], function(_, Backbone) {

	return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/sync/servers',

        defaults: function() {
            return {
            	name: '',
            	url: '',
            	utc: 0,
            	status: ''
            }
        },

        initialize: function(){
            var self = this;

            self.statusMonitoring = setInterval(self.fetch.bind(self), 2000);
        },

        startSync: function() {
        	var self = this;

        	$.post(self.urlRoot + '/' + self.get('_id') + '/start');

        	self.set('status', 'Запуск синхронизации..');
        }, 
    });
});