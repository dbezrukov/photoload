define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        url: '/account/profile',
        name: 'account',

        defaults: function() {
            return {
            }
        },

        initialize: function() {
            var self = this;
        }
    });
});