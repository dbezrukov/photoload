define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'name',
        name: 'contact',

        defaults: function() {
            return {
                'callState': 'drop' // drop, incall, call-in, call-out
            }
        },

        initialize: function(){
            var self = this;
        },

        call: function(canVideo) {
            this.trigger('call:start', this.get('id'), canVideo);
        },

        stopCall: function() {
            this.trigger('call:stop', this.get('id'));
        },

        answerCall: function(localStream, canVideo) {
            this.trigger('call:answer', this.get('id'), localStream, canVideo);
        },

        rejectCall: function() {
            this.trigger('call:reject', this.get('id'));
        }
    });
});
