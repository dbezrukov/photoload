define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/sync',

        defaults: function() {
            return {
            	interval: 60,
            	localAddress: 'http://127.0.0.1:3010'
            }
        },

        initialize: function(){
            var self = this;
        },
    });
});