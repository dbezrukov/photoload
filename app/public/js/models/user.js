define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',

        defaults: function() {
            return {
            }
        }
    });
});
