define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/tiles',

        defaults: function() {
            return {
            }
        }
    });
});
