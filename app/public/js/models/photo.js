define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/photo',

        defaults: function() {
            return {
            }
        },

        getBBox: function(callback) {
        	var self = this;

        	$.ajax({
                type: "GET",
                url: "/photo/bla/" + self.get('album') + '/' + self.get('id') + '/bbox',
                dataType: 'json',

                success: function(bbox){

                    console.log(bbox);

                    /*
                    var maxLat = 59.63260884395184,
  						maxLon = 29.22107126556466,
  						minLat = 59.63012565002282,
  						minLon = 29.21483423409025;

  					var sw = L.latLng(minLat, minLon),
                        ne = L.latLng(maxLat, maxLon),
                        bounds = new L.LatLngBounds(sw, ne);
                    */

                    callback(null, bbox);

                    // // calculate the edges of the image, in coordinate space
                    // var southWest = self.map.unproject([0, bbox.h], self.map.getMaxZoom() - 1);
                    // var northEast = self.map.unproject([bbox.w, 0], self.map.getMaxZoom() - 1);
                    // var bounds = new L.LatLngBounds(southWest, northEast);

                    // L.imageOverlay(path, bounds).addTo(self.map);

                    // self.map.setMaxBounds(bounds);


                },
                error: function(xhr){
                	callback('request error')
                }
            });
        }
    });
});
