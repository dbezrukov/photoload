define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/sits',

        defaults: function() {
            return {
            }
        }
    });
});
