define(['jquery'], function($) {

    function PhotoMetric() {

        /*
        var params = {
            photoWidth: 5184,
            photoHeight: 3456,
             
            cameraWidth: 22.3,
            cameraHeight: 14.9,
            cameraF: 50.0,
             
            cameraCxOffset: 0.0,
            cameraCyOffset: 0.0,
            cameraDelta_f: 0.0,
            cameraK1: 0.0,
            cameraK2: 0.0,
            cameraP1: 0.0,
            cameraP2: 0.0,
            cameraK3: 0.0,
         
            telLon: 29.2144,
            telLat: 59.6320,
            telHeight: 624.0,
            telHeading: 196.6,
            telPitch: 0.4,
            telRoll: 17.5
        };

        var bbox = JSON.parse( photometric.getBBox( JSON.stringify( params ) ) );
        if (bbox.error) {
            console.log('Error while calculating bbox: ', bbox.error);  
        } else {
            console.log('BBox is:');
            console.dir(bbox);
        }
        */
    }

    PhotoMetric.prototype = {
        constructor: PhotoMetric
    }

    // accept string, return number
    PhotoMetric.prototype.XYtoWGS = function(x, y, cb) {
        $.ajax({
            type: "GET",
            url: "/photometric/xytowgs",
            data: {
                x : x, 
                y : y
            },
            dataType: 'JSON',

            success: function(data){
                cb(data);
                return;
            },
            error: function(xhr){
                cb({
                    lat: 0,
                    lon: 0
                });
                return;
            }
        });
    },

    // accept string, return number
    PhotoMetric.prototype.WGStoXY = function(lat, lon, cb) {
        $.ajax({
            type: "GET",
            url: "/photometric/wgstoxy",
            data: {
                lat : lat, 
                lon : lon
            },
            dataType: 'JSON',

            success: function(data){
                cb(data);
                return;
            },
            error: function(xhr){
                cb({
                    x: 0,
                    y: 0
                });
                return;
            }
        });
    }

    return PhotoMetric;
});
