// HD1909
Proj4js.defs["EPSG:3819"]="+proj=longlat +ellps=bessel +towgs84=595.48,121.69,515.35,4.115,-2.9383,0.853,-3.408 +no_defs  ";
// TWD67
Proj4js.defs["EPSG:3821"]="+proj=longlat +ellps=aust_SA +no_defs  ";
// TWD97
Proj4js.defs["EPSG:3824"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// IGRS
Proj4js.defs["EPSG:3889"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// MGI 1901
Proj4js.defs["EPSG:3906"]="+proj=longlat +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +no_defs  ";
// Unknown datum based upon the Airy 1830 ellipsoid
Proj4js.defs["EPSG:4001"]="+proj=longlat +ellps=airy +no_defs  ";
// Unknown datum based upon the Airy Modified 1849 ellipsoid
Proj4js.defs["EPSG:4002"]="+proj=longlat +ellps=mod_airy +no_defs  ";
// Unknown datum based upon the Australian National Spheroid
Proj4js.defs["EPSG:4003"]="+proj=longlat +ellps=aust_SA +no_defs  ";
// Unknown datum based upon the Bessel 1841 ellipsoid
Proj4js.defs["EPSG:4004"]="+proj=longlat +ellps=bessel +no_defs  ";
// Unknown datum based upon the Bessel Modified ellipsoid
Proj4js.defs["EPSG:4005"]="+proj=longlat +a=6377492.018 +b=6356173.508712696 +no_defs  ";
// Unknown datum based upon the Bessel Namibia ellipsoid
Proj4js.defs["EPSG:4006"]="+proj=longlat +ellps=bess_nam +no_defs  ";
// Unknown datum based upon the Clarke 1858 ellipsoid
Proj4js.defs["EPSG:4007"]="+proj=longlat +a=6378293.645208759 +b=6356617.987679838 +no_defs  ";
// Unknown datum based upon the Clarke 1866 ellipsoid
Proj4js.defs["EPSG:4008"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// Unknown datum based upon the Clarke 1866 Michigan ellipsoid
Proj4js.defs["EPSG:4009"]="+proj=longlat +a=6378450.047548896 +b=6356826.621488444 +no_defs  ";
// Unknown datum based upon the Clarke 1880 (Benoit) ellipsoid
Proj4js.defs["EPSG:4010"]="+proj=longlat +a=6378300.789 +b=6356566.435 +no_defs  ";
// Unknown datum based upon the Clarke 1880 (IGN) ellipsoid
Proj4js.defs["EPSG:4011"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Unknown datum based upon the Clarke 1880 (RGS) ellipsoid
Proj4js.defs["EPSG:4012"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// Unknown datum based upon the Clarke 1880 (Arc) ellipsoid
Proj4js.defs["EPSG:4013"]="+proj=longlat +a=6378249.145 +b=6356514.966398753 +no_defs  ";
// Unknown datum based upon the Clarke 1880 (SGA 1922) ellipsoid
Proj4js.defs["EPSG:4014"]="+proj=longlat +a=6378249.2 +b=6356514.996941779 +no_defs  ";
// Unknown datum based upon the Everest 1830 (1937 Adjustment) ellipsoid
Proj4js.defs["EPSG:4015"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +no_defs  ";
// Unknown datum based upon the Everest 1830 (1967 Definition) ellipsoid
Proj4js.defs["EPSG:4016"]="+proj=longlat +ellps=evrstSS +no_defs  ";
// Unknown datum based upon the Everest 1830 Modified ellipsoid
Proj4js.defs["EPSG:4018"]="+proj=longlat +a=6377304.063 +b=6356103.038993155 +no_defs  ";
// Unknown datum based upon the GRS 1980 ellipsoid
Proj4js.defs["EPSG:4019"]="+proj=longlat +ellps=GRS80 +no_defs  ";
// Unknown datum based upon the Helmert 1906 ellipsoid
Proj4js.defs["EPSG:4020"]="+proj=longlat +ellps=helmert +no_defs  ";
// Unknown datum based upon the Indonesian National Spheroid
Proj4js.defs["EPSG:4021"]="+proj=longlat +a=6378160 +b=6356774.50408554 +no_defs  ";
// Unknown datum based upon the International 1924 ellipsoid
Proj4js.defs["EPSG:4022"]="+proj=longlat +ellps=intl +no_defs  ";
// MOLDREF99
Proj4js.defs["EPSG:4023"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Unknown datum based upon the Krassowsky 1940 ellipsoid
Proj4js.defs["EPSG:4024"]="+proj=longlat +ellps=krass +no_defs  ";
// Unknown datum based upon the NWL 9D ellipsoid
Proj4js.defs["EPSG:4025"]="+proj=longlat +ellps=WGS66 +no_defs  ";
// Unknown datum based upon the Plessis 1817 ellipsoid
Proj4js.defs["EPSG:4027"]="+proj=longlat +a=6376523 +b=6355862.933255573 +no_defs  ";
// Unknown datum based upon the Struve 1860 ellipsoid
Proj4js.defs["EPSG:4028"]="+proj=longlat +a=6378298.3 +b=6356657.142669561 +no_defs  ";
// Unknown datum based upon the War Office ellipsoid
Proj4js.defs["EPSG:4029"]="+proj=longlat +a=6378300 +b=6356751.689189189 +no_defs  ";
// Unknown datum based upon the WGS 84 ellipsoid
Proj4js.defs["EPSG:4030"]="+proj=longlat +ellps=WGS84 +no_defs  ";
// Unknown datum based upon the GEM 10C ellipsoid
Proj4js.defs["EPSG:4031"]="+proj=longlat +ellps=WGS84 +no_defs  ";
// Unknown datum based upon the OSU86F ellipsoid
Proj4js.defs["EPSG:4032"]="+proj=longlat +a=6378136.2 +b=6356751.516927429 +no_defs  ";
// Unknown datum based upon the OSU91A ellipsoid
Proj4js.defs["EPSG:4033"]="+proj=longlat +a=6378136.3 +b=6356751.616592146 +no_defs  ";
// Unknown datum based upon the Clarke 1880 ellipsoid
Proj4js.defs["EPSG:4034"]="+proj=longlat +a=6378249.144808011 +b=6356514.966204134 +no_defs  ";
// Unknown datum based upon the Authalic Sphere
Proj4js.defs["EPSG:4035"]="+proj=longlat +a=6371000 +b=6371000 +no_defs  ";
// Unknown datum based upon the GRS 1967 ellipsoid
Proj4js.defs["EPSG:4036"]="+proj=longlat +ellps=GRS67 +no_defs  ";
// Unknown datum based upon the Average Terrestrial System 1977 ellipsoid
Proj4js.defs["EPSG:4041"]="+proj=longlat +a=6378135 +b=6356750.304921594 +no_defs  ";
// Unknown datum based upon the Everest (1830 Definition) ellipsoid
Proj4js.defs["EPSG:4042"]="+proj=longlat +a=6377299.36559538 +b=6356098.359005156 +no_defs  ";
// Unknown datum based upon the WGS 72 ellipsoid
Proj4js.defs["EPSG:4043"]="+proj=longlat +ellps=WGS72 +no_defs  ";
// Unknown datum based upon the Everest 1830 (1962 Definition) ellipsoid
Proj4js.defs["EPSG:4044"]="+proj=longlat +a=6377301.243 +b=6356100.230165384 +no_defs  ";
// Unknown datum based upon the Everest 1830 (1975 Definition) ellipsoid
Proj4js.defs["EPSG:4045"]="+proj=longlat +a=6377299.151 +b=6356098.145120132 +no_defs  ";
// RGRDC 2005
Proj4js.defs["EPSG:4046"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Unspecified datum based upon the GRS 1980 Authalic Sphere
Proj4js.defs["EPSG:4047"]="+proj=longlat +a=6371007 +b=6371007 +no_defs  ";
// Unspecified datum based upon the Clarke 1866 Authalic Sphere
Proj4js.defs["EPSG:4052"]="+proj=longlat +a=6370997 +b=6370997 +no_defs  ";
// Unspecified datum based upon the International 1924 Authalic Sphere
Proj4js.defs["EPSG:4053"]="+proj=longlat +a=6371228 +b=6371228 +no_defs  ";
// Unspecified datum based upon the Hughes 1980 ellipsoid
Proj4js.defs["EPSG:4054"]="+proj=longlat +a=6378273 +b=6356889.449 +no_defs  ";
// Popular Visualisation CRS
Proj4js.defs["EPSG:4055"]="+proj=longlat +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// SREF98
Proj4js.defs["EPSG:4075"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// REGCAN95
Proj4js.defs["EPSG:4081"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Greek
Proj4js.defs["EPSG:4120"]="+proj=longlat +ellps=bessel +no_defs  ";
// GGRS87
Proj4js.defs["EPSG:4121"]="+proj=longlat +ellps=GRS80 +towgs84=-199.87,74.79,246.62,0,0,0,0 +no_defs  ";
// ATS77
Proj4js.defs["EPSG:4122"]="+proj=longlat +a=6378135 +b=6356750.304921594 +no_defs  ";
// KKJ
Proj4js.defs["EPSG:4123"]="+proj=longlat +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +no_defs  ";
// RT90
Proj4js.defs["EPSG:4124"]="+proj=longlat +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +no_defs  ";
// Samboja
Proj4js.defs["EPSG:4125"]="+proj=longlat +ellps=bessel +towgs84=-404.78,685.68,45.47,0,0,0,0 +no_defs  ";
// LKS94 (ETRS89)
Proj4js.defs["EPSG:4126"]="+proj=longlat +ellps=GRS80 +no_defs  ";
// Tete
Proj4js.defs["EPSG:4127"]="+proj=longlat +ellps=clrk66 +towgs84=219.315,168.975,-166.145,0.198,5.926,-2.356,-57.104 +no_defs  ";
// Madzansua
Proj4js.defs["EPSG:4128"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// Observatario
Proj4js.defs["EPSG:4129"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// Moznet
Proj4js.defs["EPSG:4130"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,-0,-0,-0,0 +no_defs  ";
// Indian 1960
Proj4js.defs["EPSG:4131"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=198,881,317,0,0,0,0 +no_defs  ";
// FD58
Proj4js.defs["EPSG:4132"]="+proj=longlat +ellps=clrk80 +towgs84=-239.1,-170.02,397.5,0,0,0,0 +no_defs  ";
// EST92
Proj4js.defs["EPSG:4133"]="+proj=longlat +ellps=GRS80 +towgs84=0.055,-0.541,-0.185,0.0183,-0.0003,-0.007,-0.014 +no_defs  ";
// PSD93
Proj4js.defs["EPSG:4134"]="+proj=longlat +ellps=clrk80 +towgs84=-180.624,-225.516,173.919,-0.81,-1.898,8.336,16.7101 +no_defs  ";
// Old Hawaiian
Proj4js.defs["EPSG:4135"]="+proj=longlat +ellps=clrk66 +towgs84=61,-285,-181,0,0,0,0 +no_defs  ";
// St. Lawrence Island
Proj4js.defs["EPSG:4136"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// St. Paul Island
Proj4js.defs["EPSG:4137"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// St. George Island
Proj4js.defs["EPSG:4138"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// Puerto Rico
Proj4js.defs["EPSG:4139"]="+proj=longlat +ellps=clrk66 +towgs84=11,72,-101,0,0,0,0 +no_defs  ";
// NAD83(CSRS98)
Proj4js.defs["EPSG:4140"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Israel
Proj4js.defs["EPSG:4141"]="+proj=longlat +ellps=GRS80 +towgs84=-48,55,52,0,0,0,0 +no_defs  ";
// Locodjo 1965
Proj4js.defs["EPSG:4142"]="+proj=longlat +ellps=clrk80 +towgs84=-125,53,467,0,0,0,0 +no_defs  ";
// Abidjan 1987
Proj4js.defs["EPSG:4143"]="+proj=longlat +ellps=clrk80 +towgs84=-124.76,53,466.79,0,0,0,0 +no_defs  ";
// Kalianpur 1937
Proj4js.defs["EPSG:4144"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=282,726,254,0,0,0,0 +no_defs  ";
// Kalianpur 1962
Proj4js.defs["EPSG:4145"]="+proj=longlat +a=6377301.243 +b=6356100.230165384 +towgs84=283,682,231,0,0,0,0 +no_defs  ";
// Kalianpur 1975
Proj4js.defs["EPSG:4146"]="+proj=longlat +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +no_defs  ";
// Hanoi 1972
Proj4js.defs["EPSG:4147"]="+proj=longlat +ellps=krass +towgs84=-17.51,-108.32,-62.39,0,0,0,0 +no_defs  ";
// Hartebeesthoek94
Proj4js.defs["EPSG:4148"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// CH1903
Proj4js.defs["EPSG:4149"]="+proj=longlat +ellps=bessel +towgs84=674.4,15.1,405.3,0,0,0,0 +no_defs  ";
// CH1903+
Proj4js.defs["EPSG:4150"]="+proj=longlat +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +no_defs  ";
// CHTRF95
Proj4js.defs["EPSG:4151"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// NAD83(HARN)
Proj4js.defs["EPSG:4152"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Rassadiran
Proj4js.defs["EPSG:4153"]="+proj=longlat +ellps=intl +towgs84=-133.63,-157.5,-158.62,0,0,0,0 +no_defs  ";
// ED50(ED77)
Proj4js.defs["EPSG:4154"]="+proj=longlat +ellps=intl +towgs84=-117,-132,-164,0,0,0,0 +no_defs  ";
// Dabola 1981
Proj4js.defs["EPSG:4155"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-83,37,124,0,0,0,0 +no_defs  ";
// S-JTSK
Proj4js.defs["EPSG:4156"]="+proj=longlat +ellps=bessel +towgs84=589,76,480,0,0,0,0 +no_defs  ";
// Mount Dillon
Proj4js.defs["EPSG:4157"]="+proj=longlat +a=6378293.645208759 +b=6356617.987679838 +no_defs  ";
// Naparima 1955
Proj4js.defs["EPSG:4158"]="+proj=longlat +ellps=intl +towgs84=-0.465,372.095,171.736,0,0,0,0 +no_defs  ";
// ELD79
Proj4js.defs["EPSG:4159"]="+proj=longlat +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +no_defs  ";
// Chos Malal 1914
Proj4js.defs["EPSG:4160"]="+proj=longlat +ellps=intl +no_defs  ";
// Pampa del Castillo
Proj4js.defs["EPSG:4161"]="+proj=longlat +ellps=intl +towgs84=27.5,14,186.4,0,0,0,0 +no_defs  ";
// Korean 1985
Proj4js.defs["EPSG:4162"]="+proj=longlat +ellps=bessel +no_defs  ";
// Yemen NGN96
Proj4js.defs["EPSG:4163"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// South Yemen
Proj4js.defs["EPSG:4164"]="+proj=longlat +ellps=krass +towgs84=-76,-138,67,0,0,0,0 +no_defs  ";
// Bissau
Proj4js.defs["EPSG:4165"]="+proj=longlat +ellps=intl +towgs84=-173,253,27,0,0,0,0 +no_defs  ";
// Korean 1995
Proj4js.defs["EPSG:4166"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// NZGD2000
Proj4js.defs["EPSG:4167"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Accra
Proj4js.defs["EPSG:4168"]="+proj=longlat +a=6378300 +b=6356751.689189189 +towgs84=-199,32,322,0,0,0,0 +no_defs  ";
// American Samoa 1962
Proj4js.defs["EPSG:4169"]="+proj=longlat +ellps=clrk66 +towgs84=-115,118,426,0,0,0,0 +no_defs  ";
// SIRGAS 1995
Proj4js.defs["EPSG:4170"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// RGF93
Proj4js.defs["EPSG:4171"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// POSGAR
Proj4js.defs["EPSG:4172"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// IRENET95
Proj4js.defs["EPSG:4173"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Sierra Leone 1924
Proj4js.defs["EPSG:4174"]="+proj=longlat +a=6378300 +b=6356751.689189189 +no_defs  ";
// Sierra Leone 1968
Proj4js.defs["EPSG:4175"]="+proj=longlat +ellps=clrk80 +towgs84=-88,4,101,0,0,0,0 +no_defs  ";
// Australian Antarctic
Proj4js.defs["EPSG:4176"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Pulkovo 1942(83)
Proj4js.defs["EPSG:4178"]="+proj=longlat +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +no_defs  ";
// Pulkovo 1942(58)
Proj4js.defs["EPSG:4179"]="+proj=longlat +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +no_defs  ";
// EST97
Proj4js.defs["EPSG:4180"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Luxembourg 1930
Proj4js.defs["EPSG:4181"]="+proj=longlat +ellps=intl +towgs84=-189.681,18.3463,-42.7695,-0.33746,-3.09264,2.53861,0.4598 +no_defs  ";
// Azores Occidental 1939
Proj4js.defs["EPSG:4182"]="+proj=longlat +ellps=intl +towgs84=-425,-169,81,0,0,0,0 +no_defs  ";
// Azores Central 1948
Proj4js.defs["EPSG:4183"]="+proj=longlat +ellps=intl +towgs84=-104,167,-38,0,0,0,0 +no_defs  ";
// Azores Oriental 1940
Proj4js.defs["EPSG:4184"]="+proj=longlat +ellps=intl +towgs84=-203,141,53,0,0,0,0 +no_defs  ";
// Madeira 1936
Proj4js.defs["EPSG:4185"]="+proj=longlat +ellps=intl +no_defs  ";
// OSNI 1952
Proj4js.defs["EPSG:4188"]="+proj=longlat +ellps=airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +no_defs  ";
// REGVEN
Proj4js.defs["EPSG:4189"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// POSGAR 98
Proj4js.defs["EPSG:4190"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Albanian 1987
Proj4js.defs["EPSG:4191"]="+proj=longlat +ellps=krass +no_defs  ";
// Douala 1948
Proj4js.defs["EPSG:4192"]="+proj=longlat +ellps=intl +towgs84=-206.1,-174.7,-87.7,0,0,0,0 +no_defs  ";
// Manoca 1962
Proj4js.defs["EPSG:4193"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-70.9,-151.8,-41.4,0,0,0,0 +no_defs  ";
// Qornoq 1927
Proj4js.defs["EPSG:4194"]="+proj=longlat +ellps=intl +towgs84=164,138,-189,0,0,0,0 +no_defs  ";
// Scoresbysund 1952
Proj4js.defs["EPSG:4195"]="+proj=longlat +ellps=intl +towgs84=105,326,-102.5,0,0,0.814,-0.6 +no_defs  ";
// Ammassalik 1958
Proj4js.defs["EPSG:4196"]="+proj=longlat +ellps=intl +towgs84=-45,417,-3.5,0,0,0.814,-0.6 +no_defs  ";
// Garoua
Proj4js.defs["EPSG:4197"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// Kousseri
Proj4js.defs["EPSG:4198"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// Egypt 1930
Proj4js.defs["EPSG:4199"]="+proj=longlat +ellps=intl +no_defs  ";
// Pulkovo 1995
Proj4js.defs["EPSG:4200"]="+proj=longlat +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +no_defs  ";
// Adindan
Proj4js.defs["EPSG:4201"]="+proj=longlat +ellps=clrk80 +towgs84=-166,-15,204,0,0,0,0 +no_defs  ";
// AGD66
Proj4js.defs["EPSG:4202"]="+proj=longlat +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +no_defs  ";
// AGD84
Proj4js.defs["EPSG:4203"]="+proj=longlat +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +no_defs  ";
// Ain el Abd
Proj4js.defs["EPSG:4204"]="+proj=longlat +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +no_defs  ";
// Afgooye
Proj4js.defs["EPSG:4205"]="+proj=longlat +ellps=krass +towgs84=-43,-163,45,0,0,0,0 +no_defs  ";
// Agadez
Proj4js.defs["EPSG:4206"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Lisbon
Proj4js.defs["EPSG:4207"]="+proj=longlat +ellps=intl +towgs84=-304.046,-60.576,103.64,0,0,0,0 +no_defs  ";
// Aratu
Proj4js.defs["EPSG:4208"]="+proj=longlat +ellps=intl +towgs84=-151.99,287.04,-147.45,0,0,0,0 +no_defs  ";
// Arc 1950
Proj4js.defs["EPSG:4209"]="+proj=longlat +a=6378249.145 +b=6356514.966398753 +towgs84=-143,-90,-294,0,0,0,0 +no_defs  ";
// Arc 1960
Proj4js.defs["EPSG:4210"]="+proj=longlat +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +no_defs  ";
// Batavia
Proj4js.defs["EPSG:4211"]="+proj=longlat +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +no_defs  ";
// Barbados 1938
Proj4js.defs["EPSG:4212"]="+proj=longlat +ellps=clrk80 +towgs84=31.95,300.99,419.19,0,0,0,0 +no_defs  ";
// Beduaram
Proj4js.defs["EPSG:4213"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-106,-87,188,0,0,0,0 +no_defs  ";
// Beijing 1954
Proj4js.defs["EPSG:4214"]="+proj=longlat +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +no_defs  ";
// Belge 1950
Proj4js.defs["EPSG:4215"]="+proj=longlat +ellps=intl +no_defs  ";
// Bermuda 1957
Proj4js.defs["EPSG:4216"]="+proj=longlat +ellps=clrk66 +towgs84=-73,213,296,0,0,0,0 +no_defs  ";
// Bogota 1975
Proj4js.defs["EPSG:4218"]="+proj=longlat +ellps=intl +towgs84=307,304,-318,0,0,0,0 +no_defs  ";
// Bukit Rimpah
Proj4js.defs["EPSG:4219"]="+proj=longlat +ellps=bessel +towgs84=-384,664,-48,0,0,0,0 +no_defs  ";
// Camacupa
Proj4js.defs["EPSG:4220"]="+proj=longlat +ellps=clrk80 +towgs84=-50.9,-347.6,-231,0,0,0,0 +no_defs  ";
// Campo Inchauspe
Proj4js.defs["EPSG:4221"]="+proj=longlat +ellps=intl +towgs84=-148,136,90,0,0,0,0 +no_defs  ";
// Cape
Proj4js.defs["EPSG:4222"]="+proj=longlat +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +no_defs  ";
// Carthage
Proj4js.defs["EPSG:4223"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-263,6,431,0,0,0,0 +no_defs  ";
// Chua
Proj4js.defs["EPSG:4224"]="+proj=longlat +ellps=intl +towgs84=-134,229,-29,0,0,0,0 +no_defs  ";
// Corrego Alegre 1970-72
Proj4js.defs["EPSG:4225"]="+proj=longlat +ellps=intl +towgs84=-206,172,-6,0,0,0,0 +no_defs  ";
// Cote d'Ivoire
Proj4js.defs["EPSG:4226"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Deir ez Zor
Proj4js.defs["EPSG:4227"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-190.421,8.532,238.69,0,0,0,0 +no_defs  ";
// Douala
Proj4js.defs["EPSG:4228"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Egypt 1907
Proj4js.defs["EPSG:4229"]="+proj=longlat +ellps=helmert +towgs84=-130,110,-13,0,0,0,0 +no_defs  ";
// ED50
Proj4js.defs["EPSG:4230"]="+proj=longlat +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +no_defs  ";
// ED87
Proj4js.defs["EPSG:4231"]="+proj=longlat +ellps=intl +towgs84=-83.11,-97.38,-117.22,0.00569291,-0.0446976,0.0442851,0.1218 +no_defs  ";
// Fahud
Proj4js.defs["EPSG:4232"]="+proj=longlat +ellps=clrk80 +towgs84=-346,-1,224,0,0,0,0 +no_defs  ";
// Gandajika 1970
Proj4js.defs["EPSG:4233"]="+proj=longlat +ellps=intl +towgs84=-133,-321,50,0,0,0,0 +no_defs  ";
// Garoua
Proj4js.defs["EPSG:4234"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Guyane Francaise
Proj4js.defs["EPSG:4235"]="+proj=longlat +ellps=intl +no_defs  ";
// Hu Tzu Shan 1950
Proj4js.defs["EPSG:4236"]="+proj=longlat +ellps=intl +towgs84=-637,-549,-203,0,0,0,0 +no_defs  ";
// HD72
Proj4js.defs["EPSG:4237"]="+proj=longlat +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +no_defs  ";
// ID74
Proj4js.defs["EPSG:4238"]="+proj=longlat +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +no_defs  ";
// Indian 1954
Proj4js.defs["EPSG:4239"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=217,823,299,0,0,0,0 +no_defs  ";
// Indian 1975
Proj4js.defs["EPSG:4240"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=210,814,289,0,0,0,0 +no_defs  ";
// Jamaica 1875
Proj4js.defs["EPSG:4241"]="+proj=longlat +a=6378249.144808011 +b=6356514.966204134 +no_defs  ";
// JAD69
Proj4js.defs["EPSG:4242"]="+proj=longlat +ellps=clrk66 +towgs84=70,207,389.5,0,0,0,0 +no_defs  ";
// Kalianpur 1880
Proj4js.defs["EPSG:4243"]="+proj=longlat +a=6377299.36559538 +b=6356098.359005156 +no_defs  ";
// Kandawala
Proj4js.defs["EPSG:4244"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=-97,787,86,0,0,0,0 +no_defs  ";
// Kertau 1968
Proj4js.defs["EPSG:4245"]="+proj=longlat +a=6377304.063 +b=6356103.038993155 +towgs84=-11,851,5,0,0,0,0 +no_defs  ";
// KOC
Proj4js.defs["EPSG:4246"]="+proj=longlat +ellps=clrk80 +towgs84=-294.7,-200.1,525.5,0,0,0,0 +no_defs  ";
// La Canoa
Proj4js.defs["EPSG:4247"]="+proj=longlat +ellps=intl +towgs84=-273.5,110.6,-357.9,0,0,0,0 +no_defs  ";
// PSAD56
Proj4js.defs["EPSG:4248"]="+proj=longlat +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +no_defs  ";
// Lake
Proj4js.defs["EPSG:4249"]="+proj=longlat +ellps=intl +no_defs  ";
// Leigon
Proj4js.defs["EPSG:4250"]="+proj=longlat +ellps=clrk80 +towgs84=-130,29,364,0,0,0,0 +no_defs  ";
// Liberia 1964
Proj4js.defs["EPSG:4251"]="+proj=longlat +ellps=clrk80 +towgs84=-90,40,88,0,0,0,0 +no_defs  ";
// Lome
Proj4js.defs["EPSG:4252"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Luzon 1911
Proj4js.defs["EPSG:4253"]="+proj=longlat +ellps=clrk66 +towgs84=-133,-77,-51,0,0,0,0 +no_defs  ";
// Hito XVIII 1963
Proj4js.defs["EPSG:4254"]="+proj=longlat +ellps=intl +towgs84=16,196,93,0,0,0,0 +no_defs  ";
// Herat North
Proj4js.defs["EPSG:4255"]="+proj=longlat +ellps=intl +towgs84=-333,-222,114,0,0,0,0 +no_defs  ";
// Mahe 1971
Proj4js.defs["EPSG:4256"]="+proj=longlat +ellps=clrk80 +towgs84=41,-220,-134,0,0,0,0 +no_defs  ";
// Makassar
Proj4js.defs["EPSG:4257"]="+proj=longlat +ellps=bessel +towgs84=-587.8,519.75,145.76,0,0,0,0 +no_defs  ";
// ETRS89
Proj4js.defs["EPSG:4258"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Malongo 1987
Proj4js.defs["EPSG:4259"]="+proj=longlat +ellps=intl +towgs84=-254.1,-5.36,-100.29,0,0,0,0 +no_defs  ";
// Manoca
Proj4js.defs["EPSG:4260"]="+proj=longlat +ellps=clrk80 +towgs84=-70.9,-151.8,-41.4,0,0,0,0 +no_defs  ";
// Merchich
Proj4js.defs["EPSG:4261"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=31,146,47,0,0,0,0 +no_defs  ";
// Massawa
Proj4js.defs["EPSG:4262"]="+proj=longlat +ellps=bessel +towgs84=639,405,60,0,0,0,0 +no_defs  ";
// Minna
Proj4js.defs["EPSG:4263"]="+proj=longlat +ellps=clrk80 +towgs84=-92,-93,122,0,0,0,0 +no_defs  ";
// Mhast
Proj4js.defs["EPSG:4264"]="+proj=longlat +ellps=intl +towgs84=-252.95,-4.11,-96.38,0,0,0,0 +no_defs  ";
// Monte Mario
Proj4js.defs["EPSG:4265"]="+proj=longlat +ellps=intl +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.68 +no_defs  ";
// M'poraloko
Proj4js.defs["EPSG:4266"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-74,-130,42,0,0,0,0 +no_defs  ";
// NAD27
Proj4js.defs["EPSG:4267"]="+proj=longlat +datum=NAD27 +no_defs  ";
// NAD27 Michigan
Proj4js.defs["EPSG:4268"]="+proj=longlat +a=6378450.047548896 +b=6356826.621488444 +no_defs  ";
// NAD83
Proj4js.defs["EPSG:4269"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Nahrwan 1967
Proj4js.defs["EPSG:4270"]="+proj=longlat +ellps=clrk80 +towgs84=-243,-192,477,0,0,0,0 +no_defs  ";
// Naparima 1972
Proj4js.defs["EPSG:4271"]="+proj=longlat +ellps=intl +towgs84=-10,375,165,0,0,0,0 +no_defs  ";
// NZGD49
Proj4js.defs["EPSG:4272"]="+proj=longlat +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +no_defs  ";
// NGO 1948
Proj4js.defs["EPSG:4273"]="+proj=longlat +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +no_defs  ";
// Datum 73
Proj4js.defs["EPSG:4274"]="+proj=longlat +ellps=intl +towgs84=-223.237,110.193,36.649,0,0,0,0 +no_defs  ";
// NTF
Proj4js.defs["EPSG:4275"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +no_defs  ";
// NSWC 9Z-2
Proj4js.defs["EPSG:4276"]="+proj=longlat +ellps=WGS66 +no_defs  ";
// OSGB 1936
Proj4js.defs["EPSG:4277"]="+proj=longlat +ellps=airy +towgs84=375,-111,431,0,0,0,0 +no_defs  ";
// OSGB70
Proj4js.defs["EPSG:4278"]="+proj=longlat +ellps=airy +no_defs  ";
// OS(SN)80
Proj4js.defs["EPSG:4279"]="+proj=longlat +ellps=airy +no_defs  ";
// Padang
Proj4js.defs["EPSG:4280"]="+proj=longlat +ellps=bessel +no_defs  ";
// Palestine 1923
Proj4js.defs["EPSG:4281"]="+proj=longlat +a=6378300.789 +b=6356566.435 +towgs84=-275.722,94.7824,340.894,-8.001,-4.42,-11.821,1 +no_defs  ";
// Pointe Noire
Proj4js.defs["EPSG:4282"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-148,51,-291,0,0,0,0 +no_defs  ";
// GDA94
Proj4js.defs["EPSG:4283"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Pulkovo 1942
Proj4js.defs["EPSG:4284"]="+proj=longlat +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +no_defs  ";
// Qatar 1974
Proj4js.defs["EPSG:4285"]="+proj=longlat +ellps=intl +towgs84=-128.16,-282.42,21.93,0,0,0,0 +no_defs  ";
// Qatar 1948
Proj4js.defs["EPSG:4286"]="+proj=longlat +ellps=helmert +no_defs  ";
// Qornoq
Proj4js.defs["EPSG:4287"]="+proj=longlat +ellps=intl +towgs84=164,138,-189,0,0,0,0 +no_defs  ";
// Loma Quintana
Proj4js.defs["EPSG:4288"]="+proj=longlat +ellps=intl +no_defs  ";
// Amersfoort
Proj4js.defs["EPSG:4289"]="+proj=longlat +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +no_defs  ";
// SAD69
Proj4js.defs["EPSG:4291"]="+proj=longlat +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +no_defs  ";
// Sapper Hill 1943
Proj4js.defs["EPSG:4292"]="+proj=longlat +ellps=intl +towgs84=-355,21,72,0,0,0,0 +no_defs  ";
// Schwarzeck
Proj4js.defs["EPSG:4293"]="+proj=longlat +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +no_defs  ";
// Segora
Proj4js.defs["EPSG:4294"]="+proj=longlat +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +no_defs  ";
// Serindung
Proj4js.defs["EPSG:4295"]="+proj=longlat +ellps=bessel +no_defs  ";
// Sudan
Proj4js.defs["EPSG:4296"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Tananarive
Proj4js.defs["EPSG:4297"]="+proj=longlat +ellps=intl +towgs84=-189,-242,-91,0,0,0,0 +no_defs  ";
// Timbalai 1948
Proj4js.defs["EPSG:4298"]="+proj=longlat +ellps=evrstSS +towgs84=-533.4,669.2,-52.5,0,0,4.28,9.4 +no_defs  ";
// TM65
Proj4js.defs["EPSG:4299"]="+proj=longlat +ellps=mod_airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +no_defs  ";
// TM75
Proj4js.defs["EPSG:4300"]="+proj=longlat +ellps=mod_airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +no_defs  ";
// Tokyo
Proj4js.defs["EPSG:4301"]="+proj=longlat +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +no_defs  ";
// Trinidad 1903
Proj4js.defs["EPSG:4302"]="+proj=longlat +a=6378293.645208759 +b=6356617.987679838 +towgs84=-61.702,284.488,472.052,0,0,0,0 +no_defs  ";
// TC(1948)
Proj4js.defs["EPSG:4303"]="+proj=longlat +ellps=helmert +no_defs  ";
// Voirol 1875
Proj4js.defs["EPSG:4304"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-73,-247,227,0,0,0,0 +no_defs  ";
// Bern 1938
Proj4js.defs["EPSG:4306"]="+proj=longlat +ellps=bessel +no_defs  ";
// Nord Sahara 1959
Proj4js.defs["EPSG:4307"]="+proj=longlat +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +no_defs  ";
// RT38
Proj4js.defs["EPSG:4308"]="+proj=longlat +ellps=bessel +no_defs  ";
// Yacare
Proj4js.defs["EPSG:4309"]="+proj=longlat +ellps=intl +towgs84=-155,171,37,0,0,0,0 +no_defs  ";
// Yoff
Proj4js.defs["EPSG:4310"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Zanderij
Proj4js.defs["EPSG:4311"]="+proj=longlat +ellps=intl +towgs84=-265,120,-358,0,0,0,0 +no_defs  ";
// MGI
Proj4js.defs["EPSG:4312"]="+proj=longlat +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +no_defs  ";
// Belge 1972
Proj4js.defs["EPSG:4313"]="+proj=longlat +ellps=intl +towgs84=-106.869,52.2978,-103.724,0.3366,-0.457,1.8422,-1.2747 +no_defs  ";
// DHDN
Proj4js.defs["EPSG:4314"]="+proj=longlat +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +no_defs  ";
// Conakry 1905
Proj4js.defs["EPSG:4315"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-23,259,-9,0,0,0,0 +no_defs  ";
// Dealul Piscului 1930
Proj4js.defs["EPSG:4316"]="+proj=longlat +ellps=intl +towgs84=103.25,-100.4,-307.19,0,0,0,0 +no_defs  ";
// Dealul Piscului 1970
Proj4js.defs["EPSG:4317"]="+proj=longlat +ellps=krass +towgs84=28,-121,-77,0,0,0,0 +no_defs  ";
// NGN
Proj4js.defs["EPSG:4318"]="+proj=longlat +ellps=WGS84 +towgs84=-3.2,-5.7,2.8,0,0,0,0 +no_defs  ";
// KUDAMS
Proj4js.defs["EPSG:4319"]="+proj=longlat +ellps=GRS80 +towgs84=-20.8,11.3,2.4,0,0,0,0 +no_defs  ";
// WGS 72
Proj4js.defs["EPSG:4322"]="+proj=longlat +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +no_defs  ";
// WGS 72BE
Proj4js.defs["EPSG:4324"]="+proj=longlat +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +no_defs  ";
// WGS 84
Proj4js.defs["EPSG:4326"]="+proj=longlat +datum=WGS84 +no_defs  ";
// RGSPM06
Proj4js.defs["EPSG:4463"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// RGM04
Proj4js.defs["EPSG:4470"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Cadastre 1997
Proj4js.defs["EPSG:4475"]="+proj=longlat +ellps=intl +towgs84=-381.788,-57.501,-256.673,0,0,0,0 +no_defs  ";
// Mexican Datum of 1993
Proj4js.defs["EPSG:4483"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// China Geodetic Coordinate System 2000
Proj4js.defs["EPSG:4490"]="+proj=longlat +ellps=GRS80 +no_defs  ";
// New Beijing
Proj4js.defs["EPSG:4555"]="+proj=longlat +ellps=krass +no_defs  ";
// RRAF 1991
Proj4js.defs["EPSG:4558"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Anguilla 1957
Proj4js.defs["EPSG:4600"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// Antigua 1943
Proj4js.defs["EPSG:4601"]="+proj=longlat +ellps=clrk80 +towgs84=-255,-15,71,0,0,0,0 +no_defs  ";
// Dominica 1945
Proj4js.defs["EPSG:4602"]="+proj=longlat +ellps=clrk80 +towgs84=725,685,536,0,0,0,0 +no_defs  ";
// Grenada 1953
Proj4js.defs["EPSG:4603"]="+proj=longlat +ellps=clrk80 +towgs84=72,213.7,93,0,0,0,0 +no_defs  ";
// Montserrat 1958
Proj4js.defs["EPSG:4604"]="+proj=longlat +ellps=clrk80 +towgs84=174,359,365,0,0,0,0 +no_defs  ";
// St. Kitts 1955
Proj4js.defs["EPSG:4605"]="+proj=longlat +ellps=clrk80 +towgs84=9,183,236,0,0,0,0 +no_defs  ";
// St. Lucia 1955
Proj4js.defs["EPSG:4606"]="+proj=longlat +ellps=clrk80 +towgs84=-149,128,296,0,0,0,0 +no_defs  ";
// St. Vincent 1945
Proj4js.defs["EPSG:4607"]="+proj=longlat +ellps=clrk80 +towgs84=195.671,332.517,274.607,0,0,0,0 +no_defs  ";
// NAD27(76)
Proj4js.defs["EPSG:4608"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// NAD27(CGQ77)
Proj4js.defs["EPSG:4609"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// Xian 1980
Proj4js.defs["EPSG:4610"]="+proj=longlat +a=6378140 +b=6356755.288157528 +no_defs  ";
// Hong Kong 1980
Proj4js.defs["EPSG:4611"]="+proj=longlat +ellps=intl +towgs84=-162.619,-276.959,-161.764,0.067753,-2.24365,-1.15883,-1.09425 +no_defs  ";
// JGD2000
Proj4js.defs["EPSG:4612"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Segara
Proj4js.defs["EPSG:4613"]="+proj=longlat +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +no_defs  ";
// QND95
Proj4js.defs["EPSG:4614"]="+proj=longlat +ellps=intl +towgs84=-119.425,-303.659,-11.0006,1.1643,0.174458,1.09626,3.65706 +no_defs  ";
// Porto Santo
Proj4js.defs["EPSG:4615"]="+proj=longlat +ellps=intl +towgs84=-499,-249,314,0,0,0,0 +no_defs  ";
// Selvagem Grande
Proj4js.defs["EPSG:4616"]="+proj=longlat +ellps=intl +towgs84=-289,-124,60,0,0,0,0 +no_defs  ";
// NAD83(CSRS)
Proj4js.defs["EPSG:4617"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// SAD69
Proj4js.defs["EPSG:4618"]="+proj=longlat +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +no_defs  ";
// SWEREF99
Proj4js.defs["EPSG:4619"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Point 58
Proj4js.defs["EPSG:4620"]="+proj=longlat +ellps=clrk80 +towgs84=-106,-129,165,0,0,0,0 +no_defs  ";
// Fort Marigot
Proj4js.defs["EPSG:4621"]="+proj=longlat +ellps=intl +towgs84=137,248,-430,0,0,0,0 +no_defs  ";
// Guadeloupe 1948
Proj4js.defs["EPSG:4622"]="+proj=longlat +ellps=intl +towgs84=-467,-16,-300,0,0,0,0 +no_defs  ";
// CSG67
Proj4js.defs["EPSG:4623"]="+proj=longlat +ellps=intl +towgs84=-186,230,110,0,0,0,0 +no_defs  ";
// RGFG95
Proj4js.defs["EPSG:4624"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Martinique 1938
Proj4js.defs["EPSG:4625"]="+proj=longlat +ellps=intl +towgs84=186,482,151,0,0,0,0 +no_defs  ";
// Reunion 1947
Proj4js.defs["EPSG:4626"]="+proj=longlat +ellps=intl +towgs84=94,-948,-1262,0,0,0,0 +no_defs  ";
// RGR92
Proj4js.defs["EPSG:4627"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Tahiti 52
Proj4js.defs["EPSG:4628"]="+proj=longlat +ellps=intl +towgs84=162,117,154,0,0,0,0 +no_defs  ";
// Tahaa 54
Proj4js.defs["EPSG:4629"]="+proj=longlat +ellps=intl +towgs84=72.438,345.918,79.486,1.6045,0.8823,0.5565,1.3746 +no_defs  ";
// IGN72 Nuku Hiva
Proj4js.defs["EPSG:4630"]="+proj=longlat +ellps=intl +towgs84=84,274,65,0,0,0,0 +no_defs  ";
// K0 1949
Proj4js.defs["EPSG:4631"]="+proj=longlat +ellps=intl +towgs84=145,-187,103,0,0,0,0 +no_defs  ";
// Combani 1950
Proj4js.defs["EPSG:4632"]="+proj=longlat +ellps=intl +towgs84=-382,-59,-262,0,0,0,0 +no_defs  ";
// IGN56 Lifou
Proj4js.defs["EPSG:4633"]="+proj=longlat +ellps=intl +towgs84=335.47,222.58,-230.94,0,0,0,0 +no_defs  ";
// IGN72 Grand Terre
Proj4js.defs["EPSG:4634"]="+proj=longlat +ellps=intl +towgs84=-13,-348,292,0,0,0,0 +no_defs  ";
// ST87 Ouvea
Proj4js.defs["EPSG:4635"]="+proj=longlat +ellps=intl +towgs84=-122.383,-188.696,103.344,3.5107,-4.9668,-5.7047,4.4798 +no_defs  ";
// Petrels 1972
Proj4js.defs["EPSG:4636"]="+proj=longlat +ellps=intl +towgs84=365,194,166,0,0,0,0 +no_defs  ";
// Perroud 1950
Proj4js.defs["EPSG:4637"]="+proj=longlat +ellps=intl +towgs84=325,154,172,0,0,0,0 +no_defs  ";
// Saint Pierre et Miquelon 1950
Proj4js.defs["EPSG:4638"]="+proj=longlat +ellps=clrk66 +towgs84=30,430,368,0,0,0,0 +no_defs  ";
// MOP78
Proj4js.defs["EPSG:4639"]="+proj=longlat +ellps=intl +towgs84=253,-132,-127,0,0,0,0 +no_defs  ";
// RRAF 1991
Proj4js.defs["EPSG:4640"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// IGN53 Mare
Proj4js.defs["EPSG:4641"]="+proj=longlat +ellps=intl +towgs84=287.58,177.78,-135.41,0,0,0,0 +no_defs  ";
// ST84 Ile des Pins
Proj4js.defs["EPSG:4642"]="+proj=longlat +ellps=intl +towgs84=-13,-348,292,0,0,0,0 +no_defs  ";
// ST71 Belep
Proj4js.defs["EPSG:4643"]="+proj=longlat +ellps=intl +towgs84=-480.26,-438.32,-643.429,16.3119,20.1721,-4.0349,-111.7 +no_defs  ";
// NEA74 Noumea
Proj4js.defs["EPSG:4644"]="+proj=longlat +ellps=intl +towgs84=-10.18,-350.43,291.37,0,0,0,0 +no_defs  ";
// RGNC 1991
Proj4js.defs["EPSG:4645"]="+proj=longlat +ellps=intl +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Grand Comoros
Proj4js.defs["EPSG:4646"]="+proj=longlat +ellps=intl +towgs84=-963,510,-359,0,0,0,0 +no_defs  ";
// Reykjavik 1900
Proj4js.defs["EPSG:4657"]="+proj=longlat +a=6377019.27 +b=6355762.5391 +towgs84=-28,199,5,0,0,0,0 +no_defs  ";
// Hjorsey 1955
Proj4js.defs["EPSG:4658"]="+proj=longlat +ellps=intl +towgs84=-73,46,-86,0,0,0,0 +no_defs  ";
// ISN93
Proj4js.defs["EPSG:4659"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Helle 1954
Proj4js.defs["EPSG:4660"]="+proj=longlat +ellps=intl +towgs84=982.609,552.753,-540.873,6.68163,-31.6115,-19.8482,16.805 +no_defs  ";
// LKS92
Proj4js.defs["EPSG:4661"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// IGN72 Grande Terre
Proj4js.defs["EPSG:4662"]="+proj=longlat +ellps=intl +towgs84=-11.64,-348.6,291.98,0,0,0,0 +no_defs  ";
// Porto Santo 1995
Proj4js.defs["EPSG:4663"]="+proj=longlat +ellps=intl +towgs84=-502.862,-247.438,312.724,0,0,0,0 +no_defs  ";
// Azores Oriental 1995
Proj4js.defs["EPSG:4664"]="+proj=longlat +ellps=intl +towgs84=-204.619,140.176,55.226,0,0,0,0 +no_defs  ";
// Azores Central 1995
Proj4js.defs["EPSG:4665"]="+proj=longlat +ellps=intl +towgs84=-106.226,166.366,-37.893,0,0,0,0 +no_defs  ";
// Lisbon 1890
Proj4js.defs["EPSG:4666"]="+proj=longlat +ellps=bessel +towgs84=508.088,-191.042,565.223,0,0,0,0 +no_defs  ";
// IKBD-92
Proj4js.defs["EPSG:4667"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// ED79
Proj4js.defs["EPSG:4668"]="+proj=longlat +ellps=intl +towgs84=-86,-98,-119,0,0,0,0 +no_defs  ";
// LKS94
Proj4js.defs["EPSG:4669"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// IGM95
Proj4js.defs["EPSG:4670"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Voirol 1879
Proj4js.defs["EPSG:4671"]="+proj=longlat +a=6378249.2 +b=6356515 +no_defs  ";
// Chatham Islands 1971
Proj4js.defs["EPSG:4672"]="+proj=longlat +ellps=intl +towgs84=175,-38,113,0,0,0,0 +no_defs  ";
// Chatham Islands 1979
Proj4js.defs["EPSG:4673"]="+proj=longlat +ellps=intl +towgs84=174.05,-25.49,112.57,-0,-0,0.554,0.2263 +no_defs  ";
// SIRGAS 2000
Proj4js.defs["EPSG:4674"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Guam 1963
Proj4js.defs["EPSG:4675"]="+proj=longlat +ellps=clrk66 +towgs84=-100,-248,259,0,0,0,0 +no_defs  ";
// Vientiane 1982
Proj4js.defs["EPSG:4676"]="+proj=longlat +ellps=krass +no_defs  ";
// Lao 1993
Proj4js.defs["EPSG:4677"]="+proj=longlat +ellps=krass +no_defs  ";
// Lao 1997
Proj4js.defs["EPSG:4678"]="+proj=longlat +ellps=krass +towgs84=44.585,-131.212,-39.544,0,0,0,0 +no_defs  ";
// Jouik 1961
Proj4js.defs["EPSG:4679"]="+proj=longlat +ellps=clrk80 +towgs84=-80.01,253.26,291.19,0,0,0,0 +no_defs  ";
// Nouakchott 1965
Proj4js.defs["EPSG:4680"]="+proj=longlat +ellps=clrk80 +towgs84=124.5,-63.5,-281,0,0,0,0 +no_defs  ";
// Mauritania 1999
Proj4js.defs["EPSG:4681"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// Gulshan 303
Proj4js.defs["EPSG:4682"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=283.7,735.9,261.1,0,0,0,0 +no_defs  ";
// PRS92
Proj4js.defs["EPSG:4683"]="+proj=longlat +ellps=clrk66 +towgs84=-127.62,-67.24,-47.04,-3.068,4.903,1.578,-1.06 +no_defs  ";
// Gan 1970
Proj4js.defs["EPSG:4684"]="+proj=longlat +ellps=intl +towgs84=-133,-321,50,0,0,0,0 +no_defs  ";
// Gandajika
Proj4js.defs["EPSG:4685"]="+proj=longlat +ellps=intl +no_defs  ";
// MAGNA-SIRGAS
Proj4js.defs["EPSG:4686"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// RGPF
Proj4js.defs["EPSG:4687"]="+proj=longlat +ellps=GRS80 +towgs84=0.072,-0.507,-0.245,-0.0183,0.0003,-0.007,-0.0093 +no_defs  ";
// Fatu Iva 72
Proj4js.defs["EPSG:4688"]="+proj=longlat +ellps=intl +towgs84=347.103,1078.12,2623.92,-33.8875,70.6773,-9.3943,186.074 +no_defs  ";
// IGN63 Hiva Oa
Proj4js.defs["EPSG:4689"]="+proj=longlat +ellps=intl +towgs84=410.721,55.049,80.746,2.5779,2.3514,0.6664,17.3311 +no_defs  ";
// Tahiti 79
Proj4js.defs["EPSG:4690"]="+proj=longlat +ellps=intl +towgs84=221.525,152.948,176.768,-2.3847,-1.3896,-0.877,11.4741 +no_defs  ";
// Moorea 87
Proj4js.defs["EPSG:4691"]="+proj=longlat +ellps=intl +towgs84=215.525,149.593,176.229,-3.2624,-1.692,-1.1571,10.4773 +no_defs  ";
// Maupiti 83
Proj4js.defs["EPSG:4692"]="+proj=longlat +ellps=intl +towgs84=217.037,86.959,23.956,0,0,0,0 +no_defs  ";
// Nakhl-e Ghanem
Proj4js.defs["EPSG:4693"]="+proj=longlat +ellps=WGS84 +towgs84=0,-0.15,0.68,0,0,0,0 +no_defs  ";
// POSGAR 94
Proj4js.defs["EPSG:4694"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Katanga 1955
Proj4js.defs["EPSG:4695"]="+proj=longlat +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +no_defs  ";
// Kasai 1953
Proj4js.defs["EPSG:4696"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// IGC 1962 6th Parallel South
Proj4js.defs["EPSG:4697"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// IGN 1962 Kerguelen
Proj4js.defs["EPSG:4698"]="+proj=longlat +ellps=intl +towgs84=145,-187,103,0,0,0,0 +no_defs  ";
// Le Pouce 1934
Proj4js.defs["EPSG:4699"]="+proj=longlat +ellps=clrk80 +towgs84=-770.1,158.4,-498.2,0,0,0,0 +no_defs  ";
// IGN Astro 1960
Proj4js.defs["EPSG:4700"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// IGCB 1955
Proj4js.defs["EPSG:4701"]="+proj=longlat +ellps=clrk80 +towgs84=-79.9,-158,-168.9,0,0,0,0 +no_defs  ";
// Mauritania 1999
Proj4js.defs["EPSG:4702"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Mhast 1951
Proj4js.defs["EPSG:4703"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// Mhast (onshore)
Proj4js.defs["EPSG:4704"]="+proj=longlat +ellps=intl +no_defs  ";
// Mhast (offshore)
Proj4js.defs["EPSG:4705"]="+proj=longlat +ellps=intl +no_defs  ";
// Egypt Gulf of Suez S-650 TL
Proj4js.defs["EPSG:4706"]="+proj=longlat +ellps=helmert +towgs84=-146.21,112.63,4.05,0,0,0,0 +no_defs  ";
// Tern Island 1961
Proj4js.defs["EPSG:4707"]="+proj=longlat +ellps=intl +towgs84=114,-116,-333,0,0,0,0 +no_defs  ";
// Cocos Islands 1965
Proj4js.defs["EPSG:4708"]="+proj=longlat +ellps=aust_SA +towgs84=-491,-22,435,0,0,0,0 +no_defs  ";
// Iwo Jima 1945
Proj4js.defs["EPSG:4709"]="+proj=longlat +ellps=intl +towgs84=145,75,-272,0,0,0,0 +no_defs  ";
// St. Helena 1971
Proj4js.defs["EPSG:4710"]="+proj=longlat +ellps=intl +towgs84=-320,550,-494,0,0,0,0 +no_defs  ";
// Marcus Island 1952
Proj4js.defs["EPSG:4711"]="+proj=longlat +ellps=intl +towgs84=124,-234,-25,0,0,0,0 +no_defs  ";
// Ascension Island 1958
Proj4js.defs["EPSG:4712"]="+proj=longlat +ellps=intl +towgs84=-205,107,53,0,0,0,0 +no_defs  ";
// Ayabelle Lighthouse
Proj4js.defs["EPSG:4713"]="+proj=longlat +ellps=clrk80 +towgs84=-79,-129,145,0,0,0,0 +no_defs  ";
// Bellevue
Proj4js.defs["EPSG:4714"]="+proj=longlat +ellps=intl +towgs84=-127,-769,472,0,0,0,0 +no_defs  ";
// Camp Area Astro
Proj4js.defs["EPSG:4715"]="+proj=longlat +ellps=intl +towgs84=-104,-129,239,0,0,0,0 +no_defs  ";
// Phoenix Islands 1966
Proj4js.defs["EPSG:4716"]="+proj=longlat +ellps=intl +towgs84=298,-304,-375,0,0,0,0 +no_defs  ";
// Cape Canaveral
Proj4js.defs["EPSG:4717"]="+proj=longlat +ellps=clrk66 +towgs84=-2,151,181,0,0,0,0 +no_defs  ";
// Solomon 1968
Proj4js.defs["EPSG:4718"]="+proj=longlat +ellps=intl +towgs84=252,-209,-751,0,0,0,0 +no_defs  ";
// Easter Island 1967
Proj4js.defs["EPSG:4719"]="+proj=longlat +ellps=intl +towgs84=211,147,111,0,0,0,0 +no_defs  ";
// Fiji 1986
Proj4js.defs["EPSG:4720"]="+proj=longlat +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +no_defs  ";
// Fiji 1956
Proj4js.defs["EPSG:4721"]="+proj=longlat +ellps=intl +towgs84=265.025,384.929,-194.046,0,0,0,0 +no_defs  ";
// South Georgia 1968
Proj4js.defs["EPSG:4722"]="+proj=longlat +ellps=intl +towgs84=-794,119,-298,0,0,0,0 +no_defs  ";
// Grand Cayman 1959
Proj4js.defs["EPSG:4723"]="+proj=longlat +ellps=clrk66 +towgs84=67.8,106.1,138.8,0,0,0,0 +no_defs  ";
// Diego Garcia 1969
Proj4js.defs["EPSG:4724"]="+proj=longlat +ellps=intl +towgs84=208,-435,-229,0,0,0,0 +no_defs  ";
// Johnston Island 1961
Proj4js.defs["EPSG:4725"]="+proj=longlat +ellps=intl +towgs84=189,-79,-202,0,0,0,0 +no_defs  ";
// Little Cayman 1961
Proj4js.defs["EPSG:4726"]="+proj=longlat +ellps=clrk66 +towgs84=42,124,147,0,0,0,0 +no_defs  ";
// Midway 1961
Proj4js.defs["EPSG:4727"]="+proj=longlat +ellps=intl +towgs84=403,-81,277,0,0,0,0 +no_defs  ";
// Pico de las Nieves 1984
Proj4js.defs["EPSG:4728"]="+proj=longlat +ellps=intl +towgs84=-307,-92,127,0,0,0,0 +no_defs  ";
// Pitcairn 1967
Proj4js.defs["EPSG:4729"]="+proj=longlat +ellps=intl +towgs84=185,165,42,0,0,0,0 +no_defs  ";
// Santo 1965
Proj4js.defs["EPSG:4730"]="+proj=longlat +ellps=intl +towgs84=170,42,84,0,0,0,0 +no_defs  ";
// Viti Levu 1916
Proj4js.defs["EPSG:4731"]="+proj=longlat +ellps=clrk80 +towgs84=51,391,-36,0,0,0,0 +no_defs  ";
// Marshall Islands 1960
Proj4js.defs["EPSG:4732"]="+proj=longlat +a=6378270 +b=6356794.343434343 +towgs84=102,52,-38,0,0,0,0 +no_defs  ";
// Wake Island 1952
Proj4js.defs["EPSG:4733"]="+proj=longlat +ellps=intl +towgs84=276,-57,149,0,0,0,0 +no_defs  ";
// Tristan 1968
Proj4js.defs["EPSG:4734"]="+proj=longlat +ellps=intl +towgs84=-632,438,-609,0,0,0,0 +no_defs  ";
// Kusaie 1951
Proj4js.defs["EPSG:4735"]="+proj=longlat +ellps=intl +towgs84=647,1777,-1124,0,0,0,0 +no_defs  ";
// Deception Island
Proj4js.defs["EPSG:4736"]="+proj=longlat +ellps=clrk80 +towgs84=260,12,-147,0,0,0,0 +no_defs  ";
// Korea 2000
Proj4js.defs["EPSG:4737"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Hong Kong 1963
Proj4js.defs["EPSG:4738"]="+proj=longlat +a=6378293.645208759 +b=6356617.987679838 +no_defs  ";
// Hong Kong 1963(67)
Proj4js.defs["EPSG:4739"]="+proj=longlat +ellps=intl +towgs84=-156,-271,-189,0,0,0,0 +no_defs  ";
// PZ-90
Proj4js.defs["EPSG:4740"]="+proj=longlat +a=6378136 +b=6356751.361745712 +towgs84=0,0,1.5,-0,-0,0.076,0 +no_defs  ";
// FD54
Proj4js.defs["EPSG:4741"]="+proj=longlat +ellps=intl +no_defs  ";
// GDM2000
Proj4js.defs["EPSG:4742"]="+proj=longlat +ellps=GRS80 +no_defs  ";
// Karbala 1979
Proj4js.defs["EPSG:4743"]="+proj=longlat +ellps=clrk80 +towgs84=70.995,-335.916,262.898,0,0,0,0 +no_defs  ";
// Nahrwan 1934
Proj4js.defs["EPSG:4744"]="+proj=longlat +ellps=clrk80 +no_defs  ";
// RD/83
Proj4js.defs["EPSG:4745"]="+proj=longlat +ellps=bessel +no_defs  ";
// PD/83
Proj4js.defs["EPSG:4746"]="+proj=longlat +ellps=bessel +no_defs  ";
// GR96
Proj4js.defs["EPSG:4747"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Vanua Levu 1915
Proj4js.defs["EPSG:4748"]="+proj=longlat +a=6378306.3696 +b=6356571.996 +towgs84=51,391,-36,0,0,0,0 +no_defs  ";
// RGNC91-93
Proj4js.defs["EPSG:4749"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// ST87 Ouvea
Proj4js.defs["EPSG:4750"]="+proj=longlat +ellps=WGS84 +towgs84=-56.263,16.136,-22.856,0,0,0,0 +no_defs  ";
// Kertau (RSO)
Proj4js.defs["EPSG:4751"]="+proj=longlat +a=6377295.664 +b=6356094.667915204 +no_defs  ";
// Viti Levu 1912
Proj4js.defs["EPSG:4752"]="+proj=longlat +a=6378306.3696 +b=6356571.996 +towgs84=51,391,-36,0,0,0,0 +no_defs  ";
// fk89
Proj4js.defs["EPSG:4753"]="+proj=longlat +ellps=intl +no_defs  ";
// LGD2006
Proj4js.defs["EPSG:4754"]="+proj=longlat +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +no_defs  ";
// DGN95
Proj4js.defs["EPSG:4755"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// VN-2000
Proj4js.defs["EPSG:4756"]="+proj=longlat +ellps=WGS84 +towgs84=-192.873,-39.382,-111.202,-0.00205,-0.0005,0.00335,0.0188 +no_defs  ";
// SVY21
Proj4js.defs["EPSG:4757"]="+proj=longlat +ellps=WGS84 +no_defs  ";
// JAD2001
Proj4js.defs["EPSG:4758"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// NAD83(NSRS2007)
Proj4js.defs["EPSG:4759"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// WGS 66
Proj4js.defs["EPSG:4760"]="+proj=longlat +ellps=WGS66 +no_defs  ";
// HTRS96
Proj4js.defs["EPSG:4761"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// BDA2000
Proj4js.defs["EPSG:4762"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Pitcairn 2006
Proj4js.defs["EPSG:4763"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// RSRGD2000
Proj4js.defs["EPSG:4764"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Slovenia 1996
Proj4js.defs["EPSG:4765"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Bern 1898 (Bern)
Proj4js.defs["EPSG:4801"]="+proj=longlat +ellps=bessel +towgs84=674.4,15.1,405.3,0,0,0,0 +pm=bern +no_defs  ";
// Bogota 1975 (Bogota)
Proj4js.defs["EPSG:4802"]="+proj=longlat +ellps=intl +towgs84=307,304,-318,0,0,0,0 +pm=bogota +no_defs  ";
// Lisbon (Lisbon)
Proj4js.defs["EPSG:4803"]="+proj=longlat +ellps=intl +towgs84=-304.046,-60.576,103.64,0,0,0,0 +pm=lisbon +no_defs  ";
// Makassar (Jakarta)
Proj4js.defs["EPSG:4804"]="+proj=longlat +ellps=bessel +towgs84=-587.8,519.75,145.76,0,0,0,0 +pm=jakarta +no_defs  ";
// MGI (Ferro)
Proj4js.defs["EPSG:4805"]="+proj=longlat +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +no_defs  ";
// Monte Mario (Rome)
Proj4js.defs["EPSG:4806"]="+proj=longlat +ellps=intl +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.68 +pm=rome +no_defs  ";
// NTF (Paris)
Proj4js.defs["EPSG:4807"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +no_defs  ";
// Padang (Jakarta)
Proj4js.defs["EPSG:4808"]="+proj=longlat +ellps=bessel +pm=jakarta +no_defs  ";
// Belge 1950 (Brussels)
Proj4js.defs["EPSG:4809"]="+proj=longlat +ellps=intl +pm=brussels +no_defs  ";
// Tananarive (Paris)
Proj4js.defs["EPSG:4810"]="+proj=longlat +ellps=intl +towgs84=-189,-242,-91,0,0,0,0 +pm=paris +no_defs  ";
// Voirol 1875 (Paris)
Proj4js.defs["EPSG:4811"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-73,-247,227,0,0,0,0 +pm=paris +no_defs  ";
// Batavia (Jakarta)
Proj4js.defs["EPSG:4813"]="+proj=longlat +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +pm=jakarta +no_defs  ";
// RT38 (Stockholm)
Proj4js.defs["EPSG:4814"]="+proj=longlat +ellps=bessel +pm=stockholm +no_defs  ";
// Greek (Athens)
Proj4js.defs["EPSG:4815"]="+proj=longlat +ellps=bessel +pm=athens +no_defs  ";
// Carthage (Paris)
Proj4js.defs["EPSG:4816"]="+proj=longlat +a=6378249.2 +b=6356515 +towgs84=-263,6,431,0,0,0,0 +pm=paris +no_defs  ";
// NGO 1948 (Oslo)
Proj4js.defs["EPSG:4817"]="+proj=longlat +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +no_defs  ";
// S-JTSK (Ferro)
Proj4js.defs["EPSG:4818"]="+proj=longlat +ellps=bessel +towgs84=589,76,480,0,0,0,0 +pm=ferro +no_defs  ";
// Nord Sahara 1959 (Paris)
Proj4js.defs["EPSG:4819"]="+proj=longlat +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +pm=paris +no_defs  ";
// Segara (Jakarta)
Proj4js.defs["EPSG:4820"]="+proj=longlat +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +pm=jakarta +no_defs  ";
// Voirol 1879 (Paris)
Proj4js.defs["EPSG:4821"]="+proj=longlat +a=6378249.2 +b=6356515 +pm=paris +no_defs  ";
// Sao Tome
Proj4js.defs["EPSG:4823"]="+proj=longlat +ellps=intl +no_defs  ";
// Principe
Proj4js.defs["EPSG:4824"]="+proj=longlat +ellps=intl +no_defs  ";
// ATF (Paris)
Proj4js.defs["EPSG:4901"]="+proj=longlat +a=6376523 +b=6355862.933255573 +pm=2.337208333333333 +no_defs  ";
// NDG (Paris)
Proj4js.defs["EPSG:4902"]="+proj=longlat +a=6376523 +b=6355862.933255573 +pm=paris +no_defs  ";
// Madrid 1870 (Madrid)
Proj4js.defs["EPSG:4903"]="+proj=longlat +a=6378298.3 +b=6356657.142669561 +pm=madrid +no_defs  ";
// Lisbon 1890 (Lisbon)
Proj4js.defs["EPSG:4904"]="+proj=longlat +ellps=bessel +towgs84=508.088,-191.042,565.223,0,0,0,0 +pm=lisbon +no_defs  ";
// PTRA08
Proj4js.defs["EPSG:5013"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Tokyo 1892
Proj4js.defs["EPSG:5132"]="+proj=longlat +ellps=bessel +no_defs  ";
// S-JTSK/05
Proj4js.defs["EPSG:5228"]="+proj=longlat +ellps=bessel +towgs84=572.213,85.334,461.94,4.9732,1.529,5.2484,3.5378 +no_defs  ";
// S-JTSK/05 (Ferro)
Proj4js.defs["EPSG:5229"]="+proj=longlat +ellps=bessel +towgs84=572.213,85.334,461.94,4.9732,1.529,5.2484,3.5378 +pm=ferro +no_defs  ";
// SLD99
Proj4js.defs["EPSG:5233"]="+proj=longlat +a=6377276.345 +b=6356075.41314024 +towgs84=-0.293,766.95,87.713,0.195704,1.69507,3.47302,-0.039338 +no_defs  ";
// GDBD2009
Proj4js.defs["EPSG:5246"]="+proj=longlat +ellps=GRS80 +no_defs  ";
// TUREF
Proj4js.defs["EPSG:5252"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// DRUKREF 03
Proj4js.defs["EPSG:5264"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// ISN2004
Proj4js.defs["EPSG:5324"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// POSGAR 2007
Proj4js.defs["EPSG:5340"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// MARGEN
Proj4js.defs["EPSG:5354"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// SIRGAS-Chile
Proj4js.defs["EPSG:5360"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// CR05
Proj4js.defs["EPSG:5365"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// MACARIO SOLIS
Proj4js.defs["EPSG:5371"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Peru96
Proj4js.defs["EPSG:5373"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// SIRGAS-ROU98
Proj4js.defs["EPSG:5381"]="+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// SIRGAS_ES2007.8
Proj4js.defs["EPSG:5393"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Ocotepeque 1935
Proj4js.defs["EPSG:5451"]="+proj=longlat +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +no_defs  ";
// Sibun Gorge 1922
Proj4js.defs["EPSG:5464"]="+proj=longlat +a=6378293.645208759 +b=6356617.987679838 +no_defs  ";
// Panama-Colon 1911
Proj4js.defs["EPSG:5467"]="+proj=longlat +ellps=clrk66 +no_defs  ";
// RGAF09
Proj4js.defs["EPSG:5489"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Corrego Alegre 1961
Proj4js.defs["EPSG:5524"]="+proj=longlat +ellps=intl +no_defs  ";
// SAD69(96)
Proj4js.defs["EPSG:5527"]="+proj=longlat +ellps=aust_SA +no_defs  ";
// PNG94
Proj4js.defs["EPSG:5546"]="+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs  ";
// Anguilla 1957 / British West Indies Grid
Proj4js.defs["EPSG:2000"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +units=m +no_defs  ";
// Antigua 1943 / British West Indies Grid
Proj4js.defs["EPSG:2001"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=-255,-15,71,0,0,0,0 +units=m +no_defs  ";
// Dominica 1945 / British West Indies Grid
Proj4js.defs["EPSG:2002"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=725,685,536,0,0,0,0 +units=m +no_defs  ";
// Grenada 1953 / British West Indies Grid
Proj4js.defs["EPSG:2003"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=72,213.7,93,0,0,0,0 +units=m +no_defs  ";
// Montserrat 1958 / British West Indies Grid
Proj4js.defs["EPSG:2004"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=174,359,365,0,0,0,0 +units=m +no_defs  ";
// St. Kitts 1955 / British West Indies Grid
Proj4js.defs["EPSG:2005"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=9,183,236,0,0,0,0 +units=m +no_defs  ";
// St. Lucia 1955 / British West Indies Grid
Proj4js.defs["EPSG:2006"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=-149,128,296,0,0,0,0 +units=m +no_defs  ";
// St. Vincent 45 / British West Indies Grid
Proj4js.defs["EPSG:2007"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=195.671,332.517,274.607,0,0,0,0 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 2 (deprecated)
Proj4js.defs["EPSG:2008"]="+proj=tmerc +lat_0=0 +lon_0=-55.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 3
Proj4js.defs["EPSG:2009"]="+proj=tmerc +lat_0=0 +lon_0=-58.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 4
Proj4js.defs["EPSG:2010"]="+proj=tmerc +lat_0=0 +lon_0=-61.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 5
Proj4js.defs["EPSG:2011"]="+proj=tmerc +lat_0=0 +lon_0=-64.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 6
Proj4js.defs["EPSG:2012"]="+proj=tmerc +lat_0=0 +lon_0=-67.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 7
Proj4js.defs["EPSG:2013"]="+proj=tmerc +lat_0=0 +lon_0=-70.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 8
Proj4js.defs["EPSG:2014"]="+proj=tmerc +lat_0=0 +lon_0=-73.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 9
Proj4js.defs["EPSG:2015"]="+proj=tmerc +lat_0=0 +lon_0=-76.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / SCoPQ zone 10
Proj4js.defs["EPSG:2016"]="+proj=tmerc +lat_0=0 +lon_0=-79.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 8
Proj4js.defs["EPSG:2017"]="+proj=tmerc +lat_0=0 +lon_0=-73.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 9
Proj4js.defs["EPSG:2018"]="+proj=tmerc +lat_0=0 +lon_0=-76.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 10
Proj4js.defs["EPSG:2019"]="+proj=tmerc +lat_0=0 +lon_0=-79.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 11
Proj4js.defs["EPSG:2020"]="+proj=tmerc +lat_0=0 +lon_0=-82.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 12
Proj4js.defs["EPSG:2021"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 13
Proj4js.defs["EPSG:2022"]="+proj=tmerc +lat_0=0 +lon_0=-84 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 14
Proj4js.defs["EPSG:2023"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 15
Proj4js.defs["EPSG:2024"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 16
Proj4js.defs["EPSG:2025"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / MTM zone 17
Proj4js.defs["EPSG:2026"]="+proj=tmerc +lat_0=0 +lon_0=-96 +k=0.9999 +x_0=304800 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / UTM zone 15N
Proj4js.defs["EPSG:2027"]="+proj=utm +zone=15 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / UTM zone 16N
Proj4js.defs["EPSG:2028"]="+proj=utm +zone=16 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / UTM zone 17N
Proj4js.defs["EPSG:2029"]="+proj=utm +zone=17 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(76) / UTM zone 18N
Proj4js.defs["EPSG:2030"]="+proj=utm +zone=18 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / UTM zone 17N
Proj4js.defs["EPSG:2031"]="+proj=utm +zone=17 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / UTM zone 18N
Proj4js.defs["EPSG:2032"]="+proj=utm +zone=18 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / UTM zone 19N
Proj4js.defs["EPSG:2033"]="+proj=utm +zone=19 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / UTM zone 20N
Proj4js.defs["EPSG:2034"]="+proj=utm +zone=20 +ellps=clrk66 +units=m +no_defs  ";
// NAD27(CGQ77) / UTM zone 21N
Proj4js.defs["EPSG:2035"]="+proj=utm +zone=21 +ellps=clrk66 +units=m +no_defs  ";
// NAD83(CSRS98) / New Brunswick Stereo (deprecated)
Proj4js.defs["EPSG:2036"]="+proj=sterea +lat_0=46.5 +lon_0=-66.5 +k=0.999912 +x_0=2500000 +y_0=7500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 19N (deprecated)
Proj4js.defs["EPSG:2037"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 20N (deprecated)
Proj4js.defs["EPSG:2038"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Israel / Israeli TM Grid
Proj4js.defs["EPSG:2039"]="+proj=tmerc +lat_0=31.73439361111111 +lon_0=35.20451694444445 +k=1.0000067 +x_0=219529.584 +y_0=626907.39 +ellps=GRS80 +towgs84=-48,55,52,0,0,0,0 +units=m +no_defs  ";
// Locodjo 1965 / UTM zone 30N
Proj4js.defs["EPSG:2040"]="+proj=utm +zone=30 +ellps=clrk80 +towgs84=-125,53,467,0,0,0,0 +units=m +no_defs  ";
// Abidjan 1987 / UTM zone 30N
Proj4js.defs["EPSG:2041"]="+proj=utm +zone=30 +ellps=clrk80 +towgs84=-124.76,53,466.79,0,0,0,0 +units=m +no_defs  ";
// Locodjo 1965 / UTM zone 29N
Proj4js.defs["EPSG:2042"]="+proj=utm +zone=29 +ellps=clrk80 +towgs84=-125,53,467,0,0,0,0 +units=m +no_defs  ";
// Abidjan 1987 / UTM zone 29N
Proj4js.defs["EPSG:2043"]="+proj=utm +zone=29 +ellps=clrk80 +towgs84=-124.76,53,466.79,0,0,0,0 +units=m +no_defs  ";
// Hanoi 1972 / Gauss-Kruger zone 18
Proj4js.defs["EPSG:2044"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +towgs84=-17.51,-108.32,-62.39,0,0,0,0 +units=m +no_defs  ";
// Hanoi 1972 / Gauss-Kruger zone 19
Proj4js.defs["EPSG:2045"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +towgs84=-17.51,-108.32,-62.39,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo15
Proj4js.defs["EPSG:2046"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo17
Proj4js.defs["EPSG:2047"]="+proj=tmerc +lat_0=0 +lon_0=17 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo19
Proj4js.defs["EPSG:2048"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo21
Proj4js.defs["EPSG:2049"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo23
Proj4js.defs["EPSG:2050"]="+proj=tmerc +lat_0=0 +lon_0=23 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo25
Proj4js.defs["EPSG:2051"]="+proj=tmerc +lat_0=0 +lon_0=25 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo27
Proj4js.defs["EPSG:2052"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo29
Proj4js.defs["EPSG:2053"]="+proj=tmerc +lat_0=0 +lon_0=29 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo31
Proj4js.defs["EPSG:2054"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Hartebeesthoek94 / Lo33
Proj4js.defs["EPSG:2055"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// CH1903+ / LV95
Proj4js.defs["EPSG:2056"]="+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs  ";
// Rassadiran / Nakhl e Taqi
Proj4js.defs["EPSG:2057"]="+proj=omerc +lat_0=27.51882880555555 +lonc=52.60353916666667 +alpha=0.5716611944444444 +k=0.999895934 +x_0=658377.437 +y_0=3044969.194 +gamma=0.5716611944444444 +ellps=intl +towgs84=-133.63,-157.5,-158.62,0,0,0,0 +units=m +no_defs  ";
// ED50(ED77) / UTM zone 38N
Proj4js.defs["EPSG:2058"]="+proj=utm +zone=38 +ellps=intl +towgs84=-117,-132,-164,0,0,0,0 +units=m +no_defs  ";
// ED50(ED77) / UTM zone 39N
Proj4js.defs["EPSG:2059"]="+proj=utm +zone=39 +ellps=intl +towgs84=-117,-132,-164,0,0,0,0 +units=m +no_defs  ";
// ED50(ED77) / UTM zone 40N
Proj4js.defs["EPSG:2060"]="+proj=utm +zone=40 +ellps=intl +towgs84=-117,-132,-164,0,0,0,0 +units=m +no_defs  ";
// ED50(ED77) / UTM zone 41N
Proj4js.defs["EPSG:2061"]="+proj=utm +zone=41 +ellps=intl +towgs84=-117,-132,-164,0,0,0,0 +units=m +no_defs  ";
// Madrid 1870 (Madrid) / Spain
Proj4js.defs["EPSG:2062"]="+proj=lcc +lat_1=40 +lat_0=40 +lon_0=0 +k_0=0.9988085293 +x_0=600000 +y_0=600000 +a=6378298.3 +b=6356657.142669561 +pm=madrid +units=m +no_defs  ";
// Dabola 1981 / UTM zone 28N (deprecated)
Proj4js.defs["EPSG:2063"]="+proj=utm +zone=28 +a=6378249.2 +b=6356515 +towgs84=-23,259,-9,0,0,0,0 +units=m +no_defs  ";
// Dabola 1981 / UTM zone 29N (deprecated)
Proj4js.defs["EPSG:2064"]="+proj=utm +zone=29 +a=6378249.2 +b=6356515 +towgs84=-23,259,-9,0,0,0,0 +units=m +no_defs  ";
// S-JTSK (Ferro) / Krovak
Proj4js.defs["EPSG:2065"]="+proj=krovak +lat_0=49.5 +lon_0=42.5 +alpha=0 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=589,76,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// Mount Dillon / Tobago Grid
Proj4js.defs["EPSG:2066"]="+proj=cass +lat_0=11.25217861111111 +lon_0=-60.68600888888889 +x_0=37718.66159325 +y_0=36209.91512952 +a=6378293.645208759 +b=6356617.987679838 +to_meter=0.201166195164 +no_defs  ";
// Naparima 1955 / UTM zone 20N
Proj4js.defs["EPSG:2067"]="+proj=utm +zone=20 +ellps=intl +towgs84=-0.465,372.095,171.736,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 5
Proj4js.defs["EPSG:2068"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 6
Proj4js.defs["EPSG:2069"]="+proj=tmerc +lat_0=0 +lon_0=11 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 7
Proj4js.defs["EPSG:2070"]="+proj=tmerc +lat_0=0 +lon_0=13 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 8
Proj4js.defs["EPSG:2071"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 9
Proj4js.defs["EPSG:2072"]="+proj=tmerc +lat_0=0 +lon_0=17 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 10
Proj4js.defs["EPSG:2073"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 11
Proj4js.defs["EPSG:2074"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 12
Proj4js.defs["EPSG:2075"]="+proj=tmerc +lat_0=0 +lon_0=23 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / Libya zone 13
Proj4js.defs["EPSG:2076"]="+proj=tmerc +lat_0=0 +lon_0=25 +k=0.9999 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / UTM zone 32N
Proj4js.defs["EPSG:2077"]="+proj=utm +zone=32 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / UTM zone 33N
Proj4js.defs["EPSG:2078"]="+proj=utm +zone=33 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / UTM zone 34N
Proj4js.defs["EPSG:2079"]="+proj=utm +zone=34 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// ELD79 / UTM zone 35N
Proj4js.defs["EPSG:2080"]="+proj=utm +zone=35 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// Chos Malal 1914 / Argentina 2
Proj4js.defs["EPSG:2081"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=intl +units=m +no_defs  ";
// Pampa del Castillo / Argentina 2
Proj4js.defs["EPSG:2082"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=intl +towgs84=27.5,14,186.4,0,0,0,0 +units=m +no_defs  ";
// Hito XVIII 1963 / Argentina 2
Proj4js.defs["EPSG:2083"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=intl +towgs84=16,196,93,0,0,0,0 +units=m +no_defs  ";
// Hito XVIII 1963 / UTM zone 19S
Proj4js.defs["EPSG:2084"]="+proj=utm +zone=19 +south +ellps=intl +towgs84=16,196,93,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Cuba Norte (deprecated)
Proj4js.defs["EPSG:2085"]="+proj=lcc +lat_1=22.35 +lat_0=22.35 +lon_0=-81 +k_0=0.99993602 +x_0=500000 +y_0=280296.016 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Cuba Sur (deprecated)
Proj4js.defs["EPSG:2086"]="+proj=lcc +lat_1=20.71666666666667 +lat_0=20.71666666666667 +lon_0=-76.83333333333333 +k_0=0.99994848 +x_0=500000 +y_0=229126.939 +datum=NAD27 +units=m +no_defs  ";
// ELD79 / TM 12 NE
Proj4js.defs["EPSG:2087"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.9996 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-115.854,-99.0583,-152.462,0,0,0,0 +units=m +no_defs  ";
// Carthage / TM 11 NE
Proj4js.defs["EPSG:2088"]="+proj=tmerc +lat_0=0 +lon_0=11 +k=0.9996 +x_0=500000 +y_0=0 +a=6378249.2 +b=6356515 +towgs84=-263,6,431,0,0,0,0 +units=m +no_defs  ";
// Yemen NGN96 / UTM zone 38N
Proj4js.defs["EPSG:2089"]="+proj=utm +zone=38 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Yemen NGN96 / UTM zone 39N
Proj4js.defs["EPSG:2090"]="+proj=utm +zone=39 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// South Yemen / Gauss Kruger zone 8 (deprecated)
Proj4js.defs["EPSG:2091"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=-76,-138,67,0,0,0,0 +units=m +no_defs  ";
// South Yemen / Gauss Kruger zone 9 (deprecated)
Proj4js.defs["EPSG:2092"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=-76,-138,67,0,0,0,0 +units=m +no_defs  ";
// Hanoi 1972 / GK 106 NE
Proj4js.defs["EPSG:2093"]="+proj=tmerc +lat_0=0 +lon_0=106 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=-17.51,-108.32,-62.39,0,0,0,0 +units=m +no_defs  ";
// WGS 72BE / TM 106 NE
Proj4js.defs["EPSG:2094"]="+proj=tmerc +lat_0=0 +lon_0=106 +k=0.9996 +x_0=500000 +y_0=0 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// Bissau / UTM zone 28N
Proj4js.defs["EPSG:2095"]="+proj=utm +zone=28 +ellps=intl +towgs84=-173,253,27,0,0,0,0 +units=m +no_defs  ";
// Korean 1985 / East Belt
Proj4js.defs["EPSG:2096"]="+proj=tmerc +lat_0=38 +lon_0=129 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Central Belt
Proj4js.defs["EPSG:2097"]="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / West Belt
Proj4js.defs["EPSG:2098"]="+proj=tmerc +lat_0=38 +lon_0=125 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Qatar 1948 / Qatar Grid
Proj4js.defs["EPSG:2099"]="+proj=cass +lat_0=25.38236111111111 +lon_0=50.76138888888889 +x_0=100000 +y_0=100000 +ellps=helmert +units=m +no_defs  ";
// GGRS87 / Greek Grid
Proj4js.defs["EPSG:2100"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9996 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=-199.87,74.79,246.62,0,0,0,0 +units=m +no_defs  ";
// Lake / Maracaibo Grid M1
Proj4js.defs["EPSG:2101"]="+proj=lcc +lat_1=10.16666666666667 +lat_0=10.16666666666667 +lon_0=-71.60561777777777 +k_0=1 +x_0=0 +y_0=-52684.972 +ellps=intl +units=m +no_defs  ";
// Lake / Maracaibo Grid
Proj4js.defs["EPSG:2102"]="+proj=lcc +lat_1=10.16666666666667 +lat_0=10.16666666666667 +lon_0=-71.60561777777777 +k_0=1 +x_0=200000 +y_0=147315.028 +ellps=intl +units=m +no_defs  ";
// Lake / Maracaibo Grid M3
Proj4js.defs["EPSG:2103"]="+proj=lcc +lat_1=10.16666666666667 +lat_0=10.16666666666667 +lon_0=-71.60561777777777 +k_0=1 +x_0=500000 +y_0=447315.028 +ellps=intl +units=m +no_defs  ";
// Lake / Maracaibo La Rosa Grid
Proj4js.defs["EPSG:2104"]="+proj=lcc +lat_1=10.16666666666667 +lat_0=10.16666666666667 +lon_0=-71.60561777777777 +k_0=1 +x_0=-17044 +y_0=-23139.97 +ellps=intl +units=m +no_defs  ";
// NZGD2000 / Mount Eden 2000
Proj4js.defs["EPSG:2105"]="+proj=tmerc +lat_0=-36.87972222222222 +lon_0=174.7641666666667 +k=0.9999 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Bay of Plenty 2000
Proj4js.defs["EPSG:2106"]="+proj=tmerc +lat_0=-37.76111111111111 +lon_0=176.4661111111111 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Poverty Bay 2000
Proj4js.defs["EPSG:2107"]="+proj=tmerc +lat_0=-38.62444444444444 +lon_0=177.8855555555556 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Hawkes Bay 2000
Proj4js.defs["EPSG:2108"]="+proj=tmerc +lat_0=-39.65083333333333 +lon_0=176.6736111111111 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Taranaki 2000
Proj4js.defs["EPSG:2109"]="+proj=tmerc +lat_0=-39.13555555555556 +lon_0=174.2277777777778 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Tuhirangi 2000
Proj4js.defs["EPSG:2110"]="+proj=tmerc +lat_0=-39.51222222222222 +lon_0=175.64 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Wanganui 2000
Proj4js.defs["EPSG:2111"]="+proj=tmerc +lat_0=-40.24194444444444 +lon_0=175.4880555555555 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Wairarapa 2000
Proj4js.defs["EPSG:2112"]="+proj=tmerc +lat_0=-40.92527777777777 +lon_0=175.6472222222222 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Wellington 2000
Proj4js.defs["EPSG:2113"]="+proj=tmerc +lat_0=-41.3011111111111 +lon_0=174.7763888888889 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Collingwood 2000
Proj4js.defs["EPSG:2114"]="+proj=tmerc +lat_0=-40.71472222222223 +lon_0=172.6719444444444 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Nelson 2000
Proj4js.defs["EPSG:2115"]="+proj=tmerc +lat_0=-41.27444444444444 +lon_0=173.2991666666667 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Karamea 2000
Proj4js.defs["EPSG:2116"]="+proj=tmerc +lat_0=-41.28972222222222 +lon_0=172.1088888888889 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Buller 2000
Proj4js.defs["EPSG:2117"]="+proj=tmerc +lat_0=-41.81055555555555 +lon_0=171.5811111111111 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Grey 2000
Proj4js.defs["EPSG:2118"]="+proj=tmerc +lat_0=-42.33361111111111 +lon_0=171.5497222222222 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Amuri 2000
Proj4js.defs["EPSG:2119"]="+proj=tmerc +lat_0=-42.68888888888888 +lon_0=173.01 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Marlborough 2000
Proj4js.defs["EPSG:2120"]="+proj=tmerc +lat_0=-41.54444444444444 +lon_0=173.8019444444444 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Hokitika 2000
Proj4js.defs["EPSG:2121"]="+proj=tmerc +lat_0=-42.88611111111111 +lon_0=170.9797222222222 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Okarito 2000
Proj4js.defs["EPSG:2122"]="+proj=tmerc +lat_0=-43.11 +lon_0=170.2608333333333 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Jacksons Bay 2000
Proj4js.defs["EPSG:2123"]="+proj=tmerc +lat_0=-43.97777777777778 +lon_0=168.6061111111111 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Mount Pleasant 2000
Proj4js.defs["EPSG:2124"]="+proj=tmerc +lat_0=-43.59055555555556 +lon_0=172.7269444444445 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Gawler 2000
Proj4js.defs["EPSG:2125"]="+proj=tmerc +lat_0=-43.74861111111111 +lon_0=171.3605555555555 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Timaru 2000
Proj4js.defs["EPSG:2126"]="+proj=tmerc +lat_0=-44.40194444444445 +lon_0=171.0572222222222 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Lindis Peak 2000
Proj4js.defs["EPSG:2127"]="+proj=tmerc +lat_0=-44.735 +lon_0=169.4675 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Mount Nicholas 2000
Proj4js.defs["EPSG:2128"]="+proj=tmerc +lat_0=-45.13277777777778 +lon_0=168.3986111111111 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Mount York 2000
Proj4js.defs["EPSG:2129"]="+proj=tmerc +lat_0=-45.56361111111111 +lon_0=167.7386111111111 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Observation Point 2000
Proj4js.defs["EPSG:2130"]="+proj=tmerc +lat_0=-45.81611111111111 +lon_0=170.6283333333333 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / North Taieri 2000
Proj4js.defs["EPSG:2131"]="+proj=tmerc +lat_0=-45.86138888888889 +lon_0=170.2825 +k=0.99996 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Bluff 2000
Proj4js.defs["EPSG:2132"]="+proj=tmerc +lat_0=-46.6 +lon_0=168.3427777777778 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / UTM zone 58S
Proj4js.defs["EPSG:2133"]="+proj=utm +zone=58 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / UTM zone 59S
Proj4js.defs["EPSG:2134"]="+proj=utm +zone=59 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / UTM zone 60S
Proj4js.defs["EPSG:2135"]="+proj=utm +zone=60 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Accra / Ghana National Grid
Proj4js.defs["EPSG:2136"]="+proj=tmerc +lat_0=4.666666666666667 +lon_0=-1 +k=0.99975 +x_0=274319.7391633579 +y_0=0 +a=6378300 +b=6356751.689189189 +towgs84=-199,32,322,0,0,0,0 +to_meter=0.3047997101815088 +no_defs  ";
// Accra / TM 1 NW
Proj4js.defs["EPSG:2137"]="+proj=tmerc +lat_0=0 +lon_0=-1 +k=0.9996 +x_0=500000 +y_0=0 +a=6378300 +b=6356751.689189189 +towgs84=-199,32,322,0,0,0,0 +units=m +no_defs  ";
// NAD27(CGQ77) / Quebec Lambert
Proj4js.defs["EPSG:2138"]="+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=clrk66 +units=m +no_defs  ";
// NAD83(CSRS98) / SCoPQ zone 2 (deprecated)
Proj4js.defs["EPSG:2139"]="+proj=tmerc +lat_0=0 +lon_0=-55.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 3 (deprecated)
Proj4js.defs["EPSG:2140"]="+proj=tmerc +lat_0=0 +lon_0=-58.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 4 (deprecated)
Proj4js.defs["EPSG:2141"]="+proj=tmerc +lat_0=0 +lon_0=-61.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 5 (deprecated)
Proj4js.defs["EPSG:2142"]="+proj=tmerc +lat_0=0 +lon_0=-64.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 6 (deprecated)
Proj4js.defs["EPSG:2143"]="+proj=tmerc +lat_0=0 +lon_0=-67.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 7 (deprecated)
Proj4js.defs["EPSG:2144"]="+proj=tmerc +lat_0=0 +lon_0=-70.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 8 (deprecated)
Proj4js.defs["EPSG:2145"]="+proj=tmerc +lat_0=0 +lon_0=-73.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 9 (deprecated)
Proj4js.defs["EPSG:2146"]="+proj=tmerc +lat_0=0 +lon_0=-76.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / MTM zone 10 (deprecated)
Proj4js.defs["EPSG:2147"]="+proj=tmerc +lat_0=0 +lon_0=-79.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 21N (deprecated)
Proj4js.defs["EPSG:2148"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 18N (deprecated)
Proj4js.defs["EPSG:2149"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 17N (deprecated)
Proj4js.defs["EPSG:2150"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 13N (deprecated)
Proj4js.defs["EPSG:2151"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 12N (deprecated)
Proj4js.defs["EPSG:2152"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS98) / UTM zone 11N (deprecated)
Proj4js.defs["EPSG:2153"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / Lambert-93
Proj4js.defs["EPSG:2154"]="+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// American Samoa 1962 / American Samoa Lambert (deprecated)
Proj4js.defs["EPSG:2155"]="+proj=lcc +lat_1=-14.26666666666667 +lat_0=-14.26666666666667 +lon_0=170 +k_0=1 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=-115,118,426,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / UTM zone 59S (deprecated)
Proj4js.defs["EPSG:2156"]="+proj=utm +zone=59 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IRENET95 / Irish Transverse Mercator
Proj4js.defs["EPSG:2157"]="+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=0.99982 +x_0=600000 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IRENET95 / UTM zone 29N
Proj4js.defs["EPSG:2158"]="+proj=utm +zone=29 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Sierra Leone 1924 / New Colony Grid
Proj4js.defs["EPSG:2159"]="+proj=tmerc +lat_0=6.666666666666667 +lon_0=-12 +k=1 +x_0=152399.8550907544 +y_0=0 +a=6378300 +b=6356751.689189189 +to_meter=0.3047997101815088 +no_defs  ";
// Sierra Leone 1924 / New War Office Grid
Proj4js.defs["EPSG:2160"]="+proj=tmerc +lat_0=6.666666666666667 +lon_0=-12 +k=1 +x_0=243839.7681452071 +y_0=182879.8261089053 +a=6378300 +b=6356751.689189189 +to_meter=0.3047997101815088 +no_defs  ";
// Sierra Leone 1968 / UTM zone 28N
Proj4js.defs["EPSG:2161"]="+proj=utm +zone=28 +ellps=clrk80 +towgs84=-88,4,101,0,0,0,0 +units=m +no_defs  ";
// Sierra Leone 1968 / UTM zone 29N
Proj4js.defs["EPSG:2162"]="+proj=utm +zone=29 +ellps=clrk80 +towgs84=-88,4,101,0,0,0,0 +units=m +no_defs  ";
// US National Atlas Equal Area
Proj4js.defs["EPSG:2163"]="+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs  ";
// Locodjo 1965 / TM 5 NW
Proj4js.defs["EPSG:2164"]="+proj=tmerc +lat_0=0 +lon_0=-5 +k=0.9996 +x_0=500000 +y_0=0 +ellps=clrk80 +towgs84=-125,53,467,0,0,0,0 +units=m +no_defs  ";
// Abidjan 1987 / TM 5 NW
Proj4js.defs["EPSG:2165"]="+proj=tmerc +lat_0=0 +lon_0=-5 +k=0.9996 +x_0=500000 +y_0=0 +ellps=clrk80 +towgs84=-124.76,53,466.79,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / Gauss Kruger zone 3 (deprecated)
Proj4js.defs["EPSG:2166"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / Gauss Kruger zone 4 (deprecated)
Proj4js.defs["EPSG:2167"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / Gauss Kruger zone 5 (deprecated)
Proj4js.defs["EPSG:2168"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Luxembourg 1930 / Gauss
Proj4js.defs["EPSG:2169"]="+proj=tmerc +lat_0=49.83333333333334 +lon_0=6.166666666666667 +k=1 +x_0=80000 +y_0=100000 +ellps=intl +towgs84=-189.681,18.3463,-42.7695,-0.33746,-3.09264,2.53861,0.4598 +units=m +no_defs  ";
// MGI / Slovenia Grid (deprecated)
Proj4js.defs["EPSG:2170"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// Pulkovo 1942(58) / Poland zone I (deprecated)
Proj4js.defs["EPSG:2171"]="+proj=sterea +lat_0=50.625 +lon_0=21.08333333333333 +k=0.9998 +x_0=4637000 +y_0=5647000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Poland zone II
Proj4js.defs["EPSG:2172"]="+proj=sterea +lat_0=53.00194444444445 +lon_0=21.50277777777778 +k=0.9998 +x_0=4603000 +y_0=5806000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Poland zone III
Proj4js.defs["EPSG:2173"]="+proj=sterea +lat_0=53.58333333333334 +lon_0=17.00833333333333 +k=0.9998 +x_0=3501000 +y_0=5999000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Poland zone IV
Proj4js.defs["EPSG:2174"]="+proj=sterea +lat_0=51.67083333333333 +lon_0=16.67222222222222 +k=0.9998 +x_0=3703000 +y_0=5627000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Poland zone V
Proj4js.defs["EPSG:2175"]="+proj=tmerc +lat_0=0 +lon_0=18.95833333333333 +k=0.999983 +x_0=237000 +y_0=-4700000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// ETRS89 / Poland CS2000 zone 5
Proj4js.defs["EPSG:2176"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.999923 +x_0=5500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Poland CS2000 zone 6
Proj4js.defs["EPSG:2177"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=0.999923 +x_0=6500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Poland CS2000 zone 7
Proj4js.defs["EPSG:2178"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=0.999923 +x_0=7500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Poland CS2000 zone 8
Proj4js.defs["EPSG:2179"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.999923 +x_0=8500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Poland CS92
Proj4js.defs["EPSG:2180"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=0.9993 +x_0=500000 +y_0=-5300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Azores Occidental 1939 / UTM zone 25N
Proj4js.defs["EPSG:2188"]="+proj=utm +zone=25 +ellps=intl +towgs84=-425,-169,81,0,0,0,0 +units=m +no_defs  ";
// Azores Central 1948 / UTM zone 26N
Proj4js.defs["EPSG:2189"]="+proj=utm +zone=26 +ellps=intl +towgs84=-104,167,-38,0,0,0,0 +units=m +no_defs  ";
// Azores Oriental 1940 / UTM zone 26N
Proj4js.defs["EPSG:2190"]="+proj=utm +zone=26 +ellps=intl +towgs84=-203,141,53,0,0,0,0 +units=m +no_defs  ";
// Madeira 1936 / UTM zone 28N (deprecated)
Proj4js.defs["EPSG:2191"]="+proj=utm +zone=28 +ellps=intl +units=m +no_defs  ";
// ED50 / France EuroLambert
Proj4js.defs["EPSG:2192"]="+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=2.337229166666667 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / New Zealand Transverse Mercator 2000
Proj4js.defs["EPSG:2193"]="+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// American Samoa 1962 / American Samoa Lambert (deprecated)
Proj4js.defs["EPSG:2194"]="+proj=lcc +lat_1=-14.26666666666667 +lat_0=-14.26666666666667 +lon_0=-170 +k_0=1 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=-115,118,426,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / UTM zone 2S
Proj4js.defs["EPSG:2195"]="+proj=utm +zone=2 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Kp2000 Jutland
Proj4js.defs["EPSG:2196"]="+proj=tmerc +lat_0=0 +lon_0=9.5 +k=0.99995 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Kp2000 Zealand
Proj4js.defs["EPSG:2197"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.99995 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Kp2000 Bornholm
Proj4js.defs["EPSG:2198"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Albanian 1987 / Gauss Kruger zone 4 (deprecated)
Proj4js.defs["EPSG:2199"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// ATS77 / New Brunswick Stereographic (ATS77)
Proj4js.defs["EPSG:2200"]="+proj=sterea +lat_0=46.5 +lon_0=-66.5 +k=0.999912 +x_0=300000 +y_0=800000 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// REGVEN / UTM zone 18N
Proj4js.defs["EPSG:2201"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// REGVEN / UTM zone 19N
Proj4js.defs["EPSG:2202"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// REGVEN / UTM zone 20N
Proj4js.defs["EPSG:2203"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Tennessee
Proj4js.defs["EPSG:2204"]="+proj=lcc +lat_1=35.25 +lat_2=36.41666666666666 +lat_0=34.66666666666666 +lon_0=-86 +x_0=609601.2192024384 +y_0=30480.06096012192 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD83 / Kentucky North
Proj4js.defs["EPSG:2205"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 9
Proj4js.defs["EPSG:2206"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=9500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 10
Proj4js.defs["EPSG:2207"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=10500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 11
Proj4js.defs["EPSG:2208"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=11500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 12
Proj4js.defs["EPSG:2209"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=12500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 13
Proj4js.defs["EPSG:2210"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=13500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 14
Proj4js.defs["EPSG:2211"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=14500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / 3-degree Gauss-Kruger zone 15
Proj4js.defs["EPSG:2212"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=15500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM 30 NE
Proj4js.defs["EPSG:2213"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=0.9996 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Douala 1948 / AOF west (deprecated)
Proj4js.defs["EPSG:2214"]="+proj=tmerc +lat_0=0 +lon_0=10.5 +k=0.999 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=-206.1,-174.7,-87.7,0,0,0,0 +units=m +no_defs  ";
// Manoca 1962 / UTM zone 32N
Proj4js.defs["EPSG:2215"]="+proj=utm +zone=32 +a=6378249.2 +b=6356515 +towgs84=-70.9,-151.8,-41.4,0,0,0,0 +units=m +no_defs  ";
// Qornoq 1927 / UTM zone 22N
Proj4js.defs["EPSG:2216"]="+proj=utm +zone=22 +ellps=intl +towgs84=164,138,-189,0,0,0,0 +units=m +no_defs  ";
// Qornoq 1927 / UTM zone 23N
Proj4js.defs["EPSG:2217"]="+proj=utm +zone=23 +ellps=intl +towgs84=164,138,-189,0,0,0,0 +units=m +no_defs  ";
// Scoresbysund 1952 / Greenland zone 5 east
// Unable to translate coordinate system EPSG:2218 into PROJ.4 format.
//
// ATS77 / UTM zone 19N
Proj4js.defs["EPSG:2219"]="+proj=utm +zone=19 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// ATS77 / UTM zone 20N
Proj4js.defs["EPSG:2220"]="+proj=utm +zone=20 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// Scoresbysund 1952 / Greenland zone 6 east
// Unable to translate coordinate system EPSG:2221 into PROJ.4 format.
//
// NAD83 / Arizona East (ft)
Proj4js.defs["EPSG:2222"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Arizona Central (ft)
Proj4js.defs["EPSG:2223"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Arizona West (ft)
Proj4js.defs["EPSG:2224"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / California zone 1 (ftUS)
Proj4js.defs["EPSG:2225"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / California zone 2 (ftUS)
Proj4js.defs["EPSG:2226"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / California zone 3 (ftUS)
Proj4js.defs["EPSG:2227"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / California zone 4 (ftUS)
Proj4js.defs["EPSG:2228"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / California zone 5 (ftUS)
Proj4js.defs["EPSG:2229"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / California zone 6 (ftUS)
Proj4js.defs["EPSG:2230"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Colorado North (ftUS)
Proj4js.defs["EPSG:2231"]="+proj=lcc +lat_1=40.78333333333333 +lat_2=39.71666666666667 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Colorado Central (ftUS)
Proj4js.defs["EPSG:2232"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Colorado South (ftUS)
Proj4js.defs["EPSG:2233"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Connecticut (ftUS)
Proj4js.defs["EPSG:2234"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096012192 +y_0=152400.3048006096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Delaware (ftUS)
Proj4js.defs["EPSG:2235"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Florida East (ftUS)
Proj4js.defs["EPSG:2236"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Florida West (ftUS)
Proj4js.defs["EPSG:2237"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Florida North (ftUS)
Proj4js.defs["EPSG:2238"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Georgia East (ftUS)
Proj4js.defs["EPSG:2239"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Georgia West (ftUS)
Proj4js.defs["EPSG:2240"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Idaho East (ftUS)
Proj4js.defs["EPSG:2241"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Idaho Central (ftUS)
Proj4js.defs["EPSG:2242"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Idaho West (ftUS)
Proj4js.defs["EPSG:2243"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=800000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Indiana East (ftUS) (deprecated)
Proj4js.defs["EPSG:2244"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=99999.99989839978 +y_0=249364.9987299975 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Indiana West (ftUS) (deprecated)
Proj4js.defs["EPSG:2245"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=249364.9987299975 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Kentucky North (ftUS)
Proj4js.defs["EPSG:2246"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Kentucky South (ftUS)
Proj4js.defs["EPSG:2247"]="+proj=lcc +lat_1=37.93333333333333 +lat_2=36.73333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=500000.0001016001 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Maryland (ftUS)
Proj4js.defs["EPSG:2248"]="+proj=lcc +lat_1=39.45 +lat_2=38.3 +lat_0=37.66666666666666 +lon_0=-77 +x_0=399999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Massachusetts Mainland (ftUS)
Proj4js.defs["EPSG:2249"]="+proj=lcc +lat_1=42.68333333333333 +lat_2=41.71666666666667 +lat_0=41 +lon_0=-71.5 +x_0=200000.0001016002 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Massachusetts Island (ftUS)
Proj4js.defs["EPSG:2250"]="+proj=lcc +lat_1=41.48333333333333 +lat_2=41.28333333333333 +lat_0=41 +lon_0=-70.5 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Michigan North (ft)
Proj4js.defs["EPSG:2251"]="+proj=lcc +lat_1=47.08333333333334 +lat_2=45.48333333333333 +lat_0=44.78333333333333 +lon_0=-87 +x_0=7999999.999968001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Michigan Central (ft)
Proj4js.defs["EPSG:2252"]="+proj=lcc +lat_1=45.7 +lat_2=44.18333333333333 +lat_0=43.31666666666667 +lon_0=-84.36666666666666 +x_0=5999999.999976001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Michigan South (ft)
Proj4js.defs["EPSG:2253"]="+proj=lcc +lat_1=43.66666666666666 +lat_2=42.1 +lat_0=41.5 +lon_0=-84.36666666666666 +x_0=3999999.999984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Mississippi East (ftUS)
Proj4js.defs["EPSG:2254"]="+proj=tmerc +lat_0=29.5 +lon_0=-88.83333333333333 +k=0.99995 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Mississippi West (ftUS)
Proj4js.defs["EPSG:2255"]="+proj=tmerc +lat_0=29.5 +lon_0=-90.33333333333333 +k=0.99995 +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Montana (ft)
Proj4js.defs["EPSG:2256"]="+proj=lcc +lat_1=49 +lat_2=45 +lat_0=44.25 +lon_0=-109.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / New Mexico East (ftUS)
Proj4js.defs["EPSG:2257"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=165000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New Mexico Central (ftUS)
Proj4js.defs["EPSG:2258"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New Mexico West (ftUS)
Proj4js.defs["EPSG:2259"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=830000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New York East (ftUS)
Proj4js.defs["EPSG:2260"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New York Central (ftUS)
Proj4js.defs["EPSG:2261"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=249999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New York West (ftUS)
Proj4js.defs["EPSG:2262"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=350000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New York Long Island (ftUS)
Proj4js.defs["EPSG:2263"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / North Carolina (ftUS)
Proj4js.defs["EPSG:2264"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.2192024384 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / North Dakota North (ft)
Proj4js.defs["EPSG:2265"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.43333333333333 +lat_0=47 +lon_0=-100.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / North Dakota South (ft)
Proj4js.defs["EPSG:2266"]="+proj=lcc +lat_1=47.48333333333333 +lat_2=46.18333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Oklahoma North (ftUS)
Proj4js.defs["EPSG:2267"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=35.56666666666667 +lat_0=35 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Oklahoma South (ftUS)
Proj4js.defs["EPSG:2268"]="+proj=lcc +lat_1=35.23333333333333 +lat_2=33.93333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Oregon North (ft)
Proj4js.defs["EPSG:2269"]="+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000.0001424 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Oregon South (ft)
Proj4js.defs["EPSG:2270"]="+proj=lcc +lat_1=44 +lat_2=42.33333333333334 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=1500000.0001464 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Pennsylvania North (ftUS)
Proj4js.defs["EPSG:2271"]="+proj=lcc +lat_1=41.95 +lat_2=40.88333333333333 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Pennsylvania South (ftUS)
Proj4js.defs["EPSG:2272"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / South Carolina (ft)
Proj4js.defs["EPSG:2273"]="+proj=lcc +lat_1=34.83333333333334 +lat_2=32.5 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609600 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Tennessee (ftUS)
Proj4js.defs["EPSG:2274"]="+proj=lcc +lat_1=36.41666666666666 +lat_2=35.25 +lat_0=34.33333333333334 +lon_0=-86 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Texas North (ftUS)
Proj4js.defs["EPSG:2275"]="+proj=lcc +lat_1=36.18333333333333 +lat_2=34.65 +lat_0=34 +lon_0=-101.5 +x_0=200000.0001016002 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Texas North Central (ftUS)
Proj4js.defs["EPSG:2276"]="+proj=lcc +lat_1=33.96666666666667 +lat_2=32.13333333333333 +lat_0=31.66666666666667 +lon_0=-98.5 +x_0=600000 +y_0=2000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Texas Central (ftUS)
Proj4js.defs["EPSG:2277"]="+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=699999.9998983998 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Texas South Central (ftUS)
Proj4js.defs["EPSG:2278"]="+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=3999999.9998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Texas South (ftUS)
Proj4js.defs["EPSG:2279"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=300000.0000000001 +y_0=5000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Utah North (ft)
Proj4js.defs["EPSG:2280"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000.0001504 +y_0=999999.9999960001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Utah Central (ft)
Proj4js.defs["EPSG:2281"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000.0001504 +y_0=1999999.999992 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Utah South (ft)
Proj4js.defs["EPSG:2282"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000.0001504 +y_0=2999999.999988 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83 / Virginia North (ftUS)
Proj4js.defs["EPSG:2283"]="+proj=lcc +lat_1=39.2 +lat_2=38.03333333333333 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=2000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Virginia South (ftUS)
Proj4js.defs["EPSG:2284"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Washington North (ftUS)
Proj4js.defs["EPSG:2285"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.5 +lat_0=47 +lon_0=-120.8333333333333 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Washington South (ftUS)
Proj4js.defs["EPSG:2286"]="+proj=lcc +lat_1=47.33333333333334 +lat_2=45.83333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wisconsin North (ftUS)
Proj4js.defs["EPSG:2287"]="+proj=lcc +lat_1=46.76666666666667 +lat_2=45.56666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wisconsin Central (ftUS)
Proj4js.defs["EPSG:2288"]="+proj=lcc +lat_1=45.5 +lat_2=44.25 +lat_0=43.83333333333334 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wisconsin South (ftUS)
Proj4js.defs["EPSG:2289"]="+proj=lcc +lat_1=44.06666666666667 +lat_2=42.73333333333333 +lat_0=42 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// ATS77 / Prince Edward Isl. Stereographic (ATS77)
Proj4js.defs["EPSG:2290"]="+proj=sterea +lat_0=47.25 +lon_0=-63 +k=0.999912 +x_0=700000 +y_0=400000 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// NAD83(CSRS98) / Prince Edward Isl. Stereographic (NAD83) (deprecated)
Proj4js.defs["EPSG:2291"]="+proj=sterea +lat_0=47.25 +lon_0=-63 +k=0.999912 +x_0=400000 +y_0=800000 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// NAD83(CSRS98) / Prince Edward Isl. Stereographic (NAD83) (deprecated)
Proj4js.defs["EPSG:2292"]="+proj=sterea +lat_0=47.25 +lon_0=-63 +k=0.999912 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ATS77 / MTM Nova Scotia zone 4
Proj4js.defs["EPSG:2294"]="+proj=tmerc +lat_0=0 +lon_0=-61.5 +k=0.9999 +x_0=4500000 +y_0=0 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// ATS77 / MTM Nova Scotia zone 5
Proj4js.defs["EPSG:2295"]="+proj=tmerc +lat_0=0 +lon_0=-64.5 +k=0.9999 +x_0=5500000 +y_0=0 +a=6378135 +b=6356750.304921594 +units=m +no_defs  ";
// Ammassalik 1958 / Greenland zone 7 east
// Unable to translate coordinate system EPSG:2296 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 1 east (deprecated)
// Unable to translate coordinate system EPSG:2297 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 2 east (deprecated)
// Unable to translate coordinate system EPSG:2298 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 2 west
// Unable to translate coordinate system EPSG:2299 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 3 east (deprecated)
// Unable to translate coordinate system EPSG:2300 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 3 west
// Unable to translate coordinate system EPSG:2301 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 4 east (deprecated)
// Unable to translate coordinate system EPSG:2302 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 4 west
// Unable to translate coordinate system EPSG:2303 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 5 west
// Unable to translate coordinate system EPSG:2304 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 6 west
// Unable to translate coordinate system EPSG:2305 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 7 west
// Unable to translate coordinate system EPSG:2306 into PROJ.4 format.
//
// Qornoq 1927 / Greenland zone 8 east
// Unable to translate coordinate system EPSG:2307 into PROJ.4 format.
//
// Batavia / TM 109 SE
Proj4js.defs["EPSG:2308"]="+proj=tmerc +lat_0=0 +lon_0=109 +k=0.9996 +x_0=500000 +y_0=10000000 +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / TM 116 SE
Proj4js.defs["EPSG:2309"]="+proj=tmerc +lat_0=0 +lon_0=116 +k=0.9996 +x_0=500000 +y_0=10000000 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / TM 132 SE
Proj4js.defs["EPSG:2310"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=0.9996 +x_0=500000 +y_0=10000000 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / TM 6 NE
Proj4js.defs["EPSG:2311"]="+proj=tmerc +lat_0=0 +lon_0=6 +k=0.9996 +x_0=500000 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// Garoua / UTM zone 33N
Proj4js.defs["EPSG:2312"]="+proj=utm +zone=33 +ellps=clrk80 +units=m +no_defs  ";
// Kousseri / UTM zone 33N
Proj4js.defs["EPSG:2313"]="+proj=utm +zone=33 +ellps=clrk80 +units=m +no_defs  ";
// Trinidad 1903 / Trinidad Grid (ftCla)
Proj4js.defs["EPSG:2314"]="+proj=cass +lat_0=10.44166666666667 +lon_0=-61.33333333333334 +x_0=86501.46392052001 +y_0=65379.0134283 +a=6378293.645208759 +b=6356617.987679838 +towgs84=-61.702,284.488,472.052,0,0,0,0 +to_meter=0.3047972654 +no_defs  ";
// Campo Inchauspe / UTM zone 19S
Proj4js.defs["EPSG:2315"]="+proj=utm +zone=19 +south +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / UTM zone 20S
Proj4js.defs["EPSG:2316"]="+proj=utm +zone=20 +south +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / ICN Regional
Proj4js.defs["EPSG:2317"]="+proj=lcc +lat_1=9 +lat_2=3 +lat_0=6 +lon_0=-66 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / Aramco Lambert
Proj4js.defs["EPSG:2318"]="+proj=lcc +lat_1=17 +lat_2=33 +lat_0=25.08951 +lon_0=48 +x_0=0 +y_0=0 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM27
Proj4js.defs["EPSG:2319"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM30
Proj4js.defs["EPSG:2320"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM33
Proj4js.defs["EPSG:2321"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM36
Proj4js.defs["EPSG:2322"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM39
Proj4js.defs["EPSG:2323"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM42
Proj4js.defs["EPSG:2324"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM45
Proj4js.defs["EPSG:2325"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// Hong Kong 1980 Grid System
Proj4js.defs["EPSG:2326"]="+proj=tmerc +lat_0=22.31213333333334 +lon_0=114.1785555555556 +k=1 +x_0=836694.05 +y_0=819069.8 +ellps=intl +towgs84=-162.619,-276.959,-161.764,0.067753,-2.24365,-1.15883,-1.09425 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 13
Proj4js.defs["EPSG:2327"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=13500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 14
Proj4js.defs["EPSG:2328"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=14500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 15
Proj4js.defs["EPSG:2329"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=15500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 16
Proj4js.defs["EPSG:2330"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=16500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 17
Proj4js.defs["EPSG:2331"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=17500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 18
Proj4js.defs["EPSG:2332"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 19
Proj4js.defs["EPSG:2333"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 20
Proj4js.defs["EPSG:2334"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 21
Proj4js.defs["EPSG:2335"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=21500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 22
Proj4js.defs["EPSG:2336"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=22500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger zone 23
Proj4js.defs["EPSG:2337"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=23500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2338"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2339"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2340"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2341"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2342"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2343"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2344"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2345"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2346"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2347"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2348"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 25
Proj4js.defs["EPSG:2349"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=25500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 26
Proj4js.defs["EPSG:2350"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=26500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 27
Proj4js.defs["EPSG:2351"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=27500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 28
Proj4js.defs["EPSG:2352"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=28500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 29
Proj4js.defs["EPSG:2353"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=29500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 30
Proj4js.defs["EPSG:2354"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=30500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 31
Proj4js.defs["EPSG:2355"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=31500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 32
Proj4js.defs["EPSG:2356"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=32500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 33
Proj4js.defs["EPSG:2357"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=33500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 34
Proj4js.defs["EPSG:2358"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=34500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 35
Proj4js.defs["EPSG:2359"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=35500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 36
Proj4js.defs["EPSG:2360"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=36500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 37
Proj4js.defs["EPSG:2361"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=37500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 38
Proj4js.defs["EPSG:2362"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=38500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 39
Proj4js.defs["EPSG:2363"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=39500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 40
Proj4js.defs["EPSG:2364"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=40500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 41
Proj4js.defs["EPSG:2365"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=41500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 42
Proj4js.defs["EPSG:2366"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=42500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 43
Proj4js.defs["EPSG:2367"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=43500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 44
Proj4js.defs["EPSG:2368"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=44500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger zone 45
Proj4js.defs["EPSG:2369"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=45500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2370"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 78E
Proj4js.defs["EPSG:2371"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2372"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 84E
Proj4js.defs["EPSG:2373"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2374"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 90E
Proj4js.defs["EPSG:2375"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2376"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 96E
Proj4js.defs["EPSG:2377"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2378"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 102E
Proj4js.defs["EPSG:2379"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2380"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 108E
Proj4js.defs["EPSG:2381"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2382"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 114E
Proj4js.defs["EPSG:2383"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2384"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 120E
Proj4js.defs["EPSG:2385"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2386"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 126E
Proj4js.defs["EPSG:2387"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2388"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 132E
Proj4js.defs["EPSG:2389"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// Xian 1980 / 3-degree Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2390"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +a=6378140 +b=6356755.288157528 +units=m +no_defs  ";
// KKJ / Finland zone 1
Proj4js.defs["EPSG:2391"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=1500000 +y_0=0 +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +units=m +no_defs  ";
// KKJ / Finland zone 2
Proj4js.defs["EPSG:2392"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=2500000 +y_0=0 +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +units=m +no_defs  ";
// KKJ / Finland Uniform Coordinate System
Proj4js.defs["EPSG:2393"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=3500000 +y_0=0 +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +units=m +no_defs  ";
// KKJ / Finland zone 4
Proj4js.defs["EPSG:2394"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=4500000 +y_0=0 +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +units=m +no_defs  ";
// South Yemen / Gauss-Kruger zone 8
Proj4js.defs["EPSG:2395"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=-76,-138,67,0,0,0,0 +units=m +no_defs  ";
// South Yemen / Gauss-Kruger zone 9
Proj4js.defs["EPSG:2396"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=-76,-138,67,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 3
Proj4js.defs["EPSG:2397"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 4
Proj4js.defs["EPSG:2398"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 5
Proj4js.defs["EPSG:2399"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// RT90 2.5 gon W (deprecated)
Proj4js.defs["EPSG:2400"]="+proj=tmerc +lat_0=0 +lon_0=15.80827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 25
Proj4js.defs["EPSG:2401"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=25500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 26
Proj4js.defs["EPSG:2402"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=26500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 27
Proj4js.defs["EPSG:2403"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=27500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 28
Proj4js.defs["EPSG:2404"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=28500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 29
Proj4js.defs["EPSG:2405"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=29500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 30
Proj4js.defs["EPSG:2406"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=30500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 31
Proj4js.defs["EPSG:2407"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=31500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 32
Proj4js.defs["EPSG:2408"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=32500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 33
Proj4js.defs["EPSG:2409"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=33500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 34
Proj4js.defs["EPSG:2410"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=34500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 35
Proj4js.defs["EPSG:2411"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=35500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 36
Proj4js.defs["EPSG:2412"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=36500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 37
Proj4js.defs["EPSG:2413"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=37500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 38
Proj4js.defs["EPSG:2414"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=38500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 39
Proj4js.defs["EPSG:2415"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=39500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 40
Proj4js.defs["EPSG:2416"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=40500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 41
Proj4js.defs["EPSG:2417"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=41500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 42
Proj4js.defs["EPSG:2418"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=42500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 43
Proj4js.defs["EPSG:2419"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=43500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 44
Proj4js.defs["EPSG:2420"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=44500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger zone 45
Proj4js.defs["EPSG:2421"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=45500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2422"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 78E
Proj4js.defs["EPSG:2423"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2424"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 84E
Proj4js.defs["EPSG:2425"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2426"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 90E
Proj4js.defs["EPSG:2427"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2428"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 96E
Proj4js.defs["EPSG:2429"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2430"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 102E
Proj4js.defs["EPSG:2431"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2432"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 108E
Proj4js.defs["EPSG:2433"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2434"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 114E
Proj4js.defs["EPSG:2435"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2436"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 120E
Proj4js.defs["EPSG:2437"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2438"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 126E
Proj4js.defs["EPSG:2439"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2440"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 132E
Proj4js.defs["EPSG:2441"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / 3-degree Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2442"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS I
Proj4js.defs["EPSG:2443"]="+proj=tmerc +lat_0=33 +lon_0=129.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS II
Proj4js.defs["EPSG:2444"]="+proj=tmerc +lat_0=33 +lon_0=131 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS III
Proj4js.defs["EPSG:2445"]="+proj=tmerc +lat_0=36 +lon_0=132.1666666666667 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS IV
Proj4js.defs["EPSG:2446"]="+proj=tmerc +lat_0=33 +lon_0=133.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS V
Proj4js.defs["EPSG:2447"]="+proj=tmerc +lat_0=36 +lon_0=134.3333333333333 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS VI
Proj4js.defs["EPSG:2448"]="+proj=tmerc +lat_0=36 +lon_0=136 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS VII
Proj4js.defs["EPSG:2449"]="+proj=tmerc +lat_0=36 +lon_0=137.1666666666667 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS VIII
Proj4js.defs["EPSG:2450"]="+proj=tmerc +lat_0=36 +lon_0=138.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS IX
Proj4js.defs["EPSG:2451"]="+proj=tmerc +lat_0=36 +lon_0=139.8333333333333 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS X
Proj4js.defs["EPSG:2452"]="+proj=tmerc +lat_0=40 +lon_0=140.8333333333333 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XI
Proj4js.defs["EPSG:2453"]="+proj=tmerc +lat_0=44 +lon_0=140.25 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XII
Proj4js.defs["EPSG:2454"]="+proj=tmerc +lat_0=44 +lon_0=142.25 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XIII
Proj4js.defs["EPSG:2455"]="+proj=tmerc +lat_0=44 +lon_0=144.25 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XIV
Proj4js.defs["EPSG:2456"]="+proj=tmerc +lat_0=26 +lon_0=142 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XV
Proj4js.defs["EPSG:2457"]="+proj=tmerc +lat_0=26 +lon_0=127.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XVI
Proj4js.defs["EPSG:2458"]="+proj=tmerc +lat_0=26 +lon_0=124 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XVII
Proj4js.defs["EPSG:2459"]="+proj=tmerc +lat_0=26 +lon_0=131 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XVIII
Proj4js.defs["EPSG:2460"]="+proj=tmerc +lat_0=20 +lon_0=136 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / Japan Plane Rectangular CS XIX
Proj4js.defs["EPSG:2461"]="+proj=tmerc +lat_0=26 +lon_0=154 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Albanian 1987 / Gauss-Kruger zone 4
Proj4js.defs["EPSG:2462"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 21E
Proj4js.defs["EPSG:2463"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 27E
Proj4js.defs["EPSG:2464"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 33E
Proj4js.defs["EPSG:2465"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 39E
Proj4js.defs["EPSG:2466"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 45E
Proj4js.defs["EPSG:2467"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 51E
Proj4js.defs["EPSG:2468"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 57E
Proj4js.defs["EPSG:2469"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 63E
Proj4js.defs["EPSG:2470"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 69E
Proj4js.defs["EPSG:2471"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2472"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2473"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2474"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2475"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2476"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2477"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2478"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2479"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2480"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2481"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2482"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 141E
Proj4js.defs["EPSG:2483"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 147E
Proj4js.defs["EPSG:2484"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 153E
Proj4js.defs["EPSG:2485"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 159E
Proj4js.defs["EPSG:2486"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 165E
Proj4js.defs["EPSG:2487"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 171E
Proj4js.defs["EPSG:2488"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 177E
Proj4js.defs["EPSG:2489"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 177W
Proj4js.defs["EPSG:2490"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger CM 171W
Proj4js.defs["EPSG:2491"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 9E (deprecated)
Proj4js.defs["EPSG:2492"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 15E (deprecated)
Proj4js.defs["EPSG:2493"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 21E
Proj4js.defs["EPSG:2494"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 27E
Proj4js.defs["EPSG:2495"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 33E
Proj4js.defs["EPSG:2496"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 39E
Proj4js.defs["EPSG:2497"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 45E
Proj4js.defs["EPSG:2498"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 51E
Proj4js.defs["EPSG:2499"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 57E
Proj4js.defs["EPSG:2500"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 63E
Proj4js.defs["EPSG:2501"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 69E
Proj4js.defs["EPSG:2502"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2503"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2504"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2505"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2506"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2507"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2508"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2509"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2510"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2511"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2512"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2513"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 141E
Proj4js.defs["EPSG:2514"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 147E
Proj4js.defs["EPSG:2515"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 153E
Proj4js.defs["EPSG:2516"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 159E
Proj4js.defs["EPSG:2517"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 165E
Proj4js.defs["EPSG:2518"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 171E
Proj4js.defs["EPSG:2519"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 177E
Proj4js.defs["EPSG:2520"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 177W
Proj4js.defs["EPSG:2521"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger CM 171W
Proj4js.defs["EPSG:2522"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 7
Proj4js.defs["EPSG:2523"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 8
Proj4js.defs["EPSG:2524"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 9
Proj4js.defs["EPSG:2525"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 10
Proj4js.defs["EPSG:2526"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=10500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 11
Proj4js.defs["EPSG:2527"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=11500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 12
Proj4js.defs["EPSG:2528"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=12500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 13
Proj4js.defs["EPSG:2529"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=13500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 14
Proj4js.defs["EPSG:2530"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=14500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 15
Proj4js.defs["EPSG:2531"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=15500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 16
Proj4js.defs["EPSG:2532"]="+proj=tmerc +lat_0=0 +lon_0=48 +k=1 +x_0=16500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 17
Proj4js.defs["EPSG:2533"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=17500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 18
Proj4js.defs["EPSG:2534"]="+proj=tmerc +lat_0=0 +lon_0=54 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 19
Proj4js.defs["EPSG:2535"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 20
Proj4js.defs["EPSG:2536"]="+proj=tmerc +lat_0=0 +lon_0=60 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 21
Proj4js.defs["EPSG:2537"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=21500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 22
Proj4js.defs["EPSG:2538"]="+proj=tmerc +lat_0=0 +lon_0=66 +k=1 +x_0=22500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 23
Proj4js.defs["EPSG:2539"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=23500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 24
Proj4js.defs["EPSG:2540"]="+proj=tmerc +lat_0=0 +lon_0=72 +k=1 +x_0=24500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 25
Proj4js.defs["EPSG:2541"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=25500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 26
Proj4js.defs["EPSG:2542"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=26500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 27
Proj4js.defs["EPSG:2543"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=27500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 28
Proj4js.defs["EPSG:2544"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=28500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 29
Proj4js.defs["EPSG:2545"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=29500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 30
Proj4js.defs["EPSG:2546"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=30500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 31
Proj4js.defs["EPSG:2547"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=31500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 32
Proj4js.defs["EPSG:2548"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=32500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 33
Proj4js.defs["EPSG:2549"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=33500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Samboja / UTM zone 50S (deprecated)
Proj4js.defs["EPSG:2550"]="+proj=utm +zone=50 +south +ellps=bessel +towgs84=-404.78,685.68,45.47,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 34
Proj4js.defs["EPSG:2551"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=34500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 35
Proj4js.defs["EPSG:2552"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=35500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 36
Proj4js.defs["EPSG:2553"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=36500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 37
Proj4js.defs["EPSG:2554"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=37500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 38
Proj4js.defs["EPSG:2555"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=38500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 39
Proj4js.defs["EPSG:2556"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=39500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 40
Proj4js.defs["EPSG:2557"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=40500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 41
Proj4js.defs["EPSG:2558"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=41500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 42
Proj4js.defs["EPSG:2559"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=42500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 43
Proj4js.defs["EPSG:2560"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=43500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 44
Proj4js.defs["EPSG:2561"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=44500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 45
Proj4js.defs["EPSG:2562"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=45500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 46
Proj4js.defs["EPSG:2563"]="+proj=tmerc +lat_0=0 +lon_0=138 +k=1 +x_0=46500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 47
Proj4js.defs["EPSG:2564"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=47500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 48
Proj4js.defs["EPSG:2565"]="+proj=tmerc +lat_0=0 +lon_0=144 +k=1 +x_0=48500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 49
Proj4js.defs["EPSG:2566"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=49500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 50
Proj4js.defs["EPSG:2567"]="+proj=tmerc +lat_0=0 +lon_0=150 +k=1 +x_0=50500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 51
Proj4js.defs["EPSG:2568"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=51500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 52
Proj4js.defs["EPSG:2569"]="+proj=tmerc +lat_0=0 +lon_0=156 +k=1 +x_0=52500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 53
Proj4js.defs["EPSG:2570"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=53500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 54
Proj4js.defs["EPSG:2571"]="+proj=tmerc +lat_0=0 +lon_0=162 +k=1 +x_0=54500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 55
Proj4js.defs["EPSG:2572"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=55500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 56
Proj4js.defs["EPSG:2573"]="+proj=tmerc +lat_0=0 +lon_0=168 +k=1 +x_0=56500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 57
Proj4js.defs["EPSG:2574"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=57500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 58
Proj4js.defs["EPSG:2575"]="+proj=tmerc +lat_0=0 +lon_0=174 +k=1 +x_0=58500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 59
Proj4js.defs["EPSG:2576"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=59500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 60 (deprecated)
Proj4js.defs["EPSG:2577"]="+proj=tmerc +lat_0=0 +lon_0=180 +k=1 +x_0=60000000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 61
Proj4js.defs["EPSG:2578"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=61500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 62
Proj4js.defs["EPSG:2579"]="+proj=tmerc +lat_0=0 +lon_0=-174 +k=1 +x_0=62500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 63
Proj4js.defs["EPSG:2580"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=63500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 64
Proj4js.defs["EPSG:2581"]="+proj=tmerc +lat_0=0 +lon_0=-168 +k=1 +x_0=64500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 21E
Proj4js.defs["EPSG:2582"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 24E
Proj4js.defs["EPSG:2583"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 27E
Proj4js.defs["EPSG:2584"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 30E
Proj4js.defs["EPSG:2585"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 33E
Proj4js.defs["EPSG:2586"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 36E
Proj4js.defs["EPSG:2587"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 39E
Proj4js.defs["EPSG:2588"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 42E
Proj4js.defs["EPSG:2589"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 45E
Proj4js.defs["EPSG:2590"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 48E
Proj4js.defs["EPSG:2591"]="+proj=tmerc +lat_0=0 +lon_0=48 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 51E
Proj4js.defs["EPSG:2592"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 54E
Proj4js.defs["EPSG:2593"]="+proj=tmerc +lat_0=0 +lon_0=54 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 57E
Proj4js.defs["EPSG:2594"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 60E
Proj4js.defs["EPSG:2595"]="+proj=tmerc +lat_0=0 +lon_0=60 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 63E
Proj4js.defs["EPSG:2596"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 66E
Proj4js.defs["EPSG:2597"]="+proj=tmerc +lat_0=0 +lon_0=66 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 69E
Proj4js.defs["EPSG:2598"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 72E
Proj4js.defs["EPSG:2599"]="+proj=tmerc +lat_0=0 +lon_0=72 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Lietuvos Koordinoei Sistema 1994 (deprecated)
Proj4js.defs["EPSG:2600"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2601"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 78E
Proj4js.defs["EPSG:2602"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2603"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 84E
Proj4js.defs["EPSG:2604"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2605"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 90E
Proj4js.defs["EPSG:2606"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2607"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 96E
Proj4js.defs["EPSG:2608"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2609"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 102E
Proj4js.defs["EPSG:2610"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2611"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 108E
Proj4js.defs["EPSG:2612"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2613"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 114E
Proj4js.defs["EPSG:2614"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2615"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 120E
Proj4js.defs["EPSG:2616"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2617"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 126E
Proj4js.defs["EPSG:2618"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2619"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 132E
Proj4js.defs["EPSG:2620"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2621"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 138E
Proj4js.defs["EPSG:2622"]="+proj=tmerc +lat_0=0 +lon_0=138 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 141E
Proj4js.defs["EPSG:2623"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 144E
Proj4js.defs["EPSG:2624"]="+proj=tmerc +lat_0=0 +lon_0=144 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 147E
Proj4js.defs["EPSG:2625"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 150E
Proj4js.defs["EPSG:2626"]="+proj=tmerc +lat_0=0 +lon_0=150 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 153E
Proj4js.defs["EPSG:2627"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 156E
Proj4js.defs["EPSG:2628"]="+proj=tmerc +lat_0=0 +lon_0=156 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 159E
Proj4js.defs["EPSG:2629"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 162E
Proj4js.defs["EPSG:2630"]="+proj=tmerc +lat_0=0 +lon_0=162 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 165E
Proj4js.defs["EPSG:2631"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 168E
Proj4js.defs["EPSG:2632"]="+proj=tmerc +lat_0=0 +lon_0=168 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 171E
Proj4js.defs["EPSG:2633"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 174E
Proj4js.defs["EPSG:2634"]="+proj=tmerc +lat_0=0 +lon_0=174 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 177E
Proj4js.defs["EPSG:2635"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 180E
Proj4js.defs["EPSG:2636"]="+proj=tmerc +lat_0=0 +lon_0=180 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 177W
Proj4js.defs["EPSG:2637"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 174W
Proj4js.defs["EPSG:2638"]="+proj=tmerc +lat_0=0 +lon_0=-174 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 171W
Proj4js.defs["EPSG:2639"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 168W
Proj4js.defs["EPSG:2640"]="+proj=tmerc +lat_0=0 +lon_0=-168 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 7
Proj4js.defs["EPSG:2641"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 8
Proj4js.defs["EPSG:2642"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 9
Proj4js.defs["EPSG:2643"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 10
Proj4js.defs["EPSG:2644"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=10500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 11
Proj4js.defs["EPSG:2645"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=11500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 12
Proj4js.defs["EPSG:2646"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=12500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 13
Proj4js.defs["EPSG:2647"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=13500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 14
Proj4js.defs["EPSG:2648"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=14500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 15
Proj4js.defs["EPSG:2649"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=15500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 16
Proj4js.defs["EPSG:2650"]="+proj=tmerc +lat_0=0 +lon_0=48 +k=1 +x_0=16500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 17
Proj4js.defs["EPSG:2651"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=17500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 18
Proj4js.defs["EPSG:2652"]="+proj=tmerc +lat_0=0 +lon_0=54 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 19
Proj4js.defs["EPSG:2653"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 20
Proj4js.defs["EPSG:2654"]="+proj=tmerc +lat_0=0 +lon_0=60 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 21
Proj4js.defs["EPSG:2655"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=21500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 22
Proj4js.defs["EPSG:2656"]="+proj=tmerc +lat_0=0 +lon_0=66 +k=1 +x_0=22500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 23
Proj4js.defs["EPSG:2657"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=23500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 24
Proj4js.defs["EPSG:2658"]="+proj=tmerc +lat_0=0 +lon_0=72 +k=1 +x_0=24500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 25
Proj4js.defs["EPSG:2659"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=25500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 26
Proj4js.defs["EPSG:2660"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=26500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 27
Proj4js.defs["EPSG:2661"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=27500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 28
Proj4js.defs["EPSG:2662"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=28500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 29
Proj4js.defs["EPSG:2663"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=29500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 30
Proj4js.defs["EPSG:2664"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=30500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 31
Proj4js.defs["EPSG:2665"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=31500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 32
Proj4js.defs["EPSG:2666"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=32500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 33
Proj4js.defs["EPSG:2667"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=33500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 34
Proj4js.defs["EPSG:2668"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=34500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 35
Proj4js.defs["EPSG:2669"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=35500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 36
Proj4js.defs["EPSG:2670"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=36500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 37
Proj4js.defs["EPSG:2671"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=37500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 38
Proj4js.defs["EPSG:2672"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=38500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 39
Proj4js.defs["EPSG:2673"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=39500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 40
Proj4js.defs["EPSG:2674"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=40500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 41
Proj4js.defs["EPSG:2675"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=41500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 42
Proj4js.defs["EPSG:2676"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=42500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 43
Proj4js.defs["EPSG:2677"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=43500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 44
Proj4js.defs["EPSG:2678"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=44500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 45
Proj4js.defs["EPSG:2679"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=45500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 46
Proj4js.defs["EPSG:2680"]="+proj=tmerc +lat_0=0 +lon_0=138 +k=1 +x_0=46500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 47
Proj4js.defs["EPSG:2681"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=47500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 48
Proj4js.defs["EPSG:2682"]="+proj=tmerc +lat_0=0 +lon_0=144 +k=1 +x_0=48500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 49
Proj4js.defs["EPSG:2683"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=49500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 50
Proj4js.defs["EPSG:2684"]="+proj=tmerc +lat_0=0 +lon_0=150 +k=1 +x_0=50500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 51
Proj4js.defs["EPSG:2685"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=51500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 52
Proj4js.defs["EPSG:2686"]="+proj=tmerc +lat_0=0 +lon_0=156 +k=1 +x_0=52500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 53
Proj4js.defs["EPSG:2687"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=53500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 54
Proj4js.defs["EPSG:2688"]="+proj=tmerc +lat_0=0 +lon_0=162 +k=1 +x_0=54500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 55
Proj4js.defs["EPSG:2689"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=55500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 56
Proj4js.defs["EPSG:2690"]="+proj=tmerc +lat_0=0 +lon_0=168 +k=1 +x_0=56500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 57
Proj4js.defs["EPSG:2691"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=57500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 58
Proj4js.defs["EPSG:2692"]="+proj=tmerc +lat_0=0 +lon_0=174 +k=1 +x_0=58500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 59
Proj4js.defs["EPSG:2693"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=59500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 60 (deprecated)
Proj4js.defs["EPSG:2694"]="+proj=tmerc +lat_0=0 +lon_0=180 +k=1 +x_0=60000000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 61
Proj4js.defs["EPSG:2695"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=61500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 62
Proj4js.defs["EPSG:2696"]="+proj=tmerc +lat_0=0 +lon_0=-174 +k=1 +x_0=62500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 63
Proj4js.defs["EPSG:2697"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=63500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 64
Proj4js.defs["EPSG:2698"]="+proj=tmerc +lat_0=0 +lon_0=-168 +k=1 +x_0=64500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 21E
Proj4js.defs["EPSG:2699"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 24E
Proj4js.defs["EPSG:2700"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 27E
Proj4js.defs["EPSG:2701"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 30E
Proj4js.defs["EPSG:2702"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 33E
Proj4js.defs["EPSG:2703"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 36E
Proj4js.defs["EPSG:2704"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 39E
Proj4js.defs["EPSG:2705"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 42E
Proj4js.defs["EPSG:2706"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 45E
Proj4js.defs["EPSG:2707"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 48E
Proj4js.defs["EPSG:2708"]="+proj=tmerc +lat_0=0 +lon_0=48 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 51E
Proj4js.defs["EPSG:2709"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 54E
Proj4js.defs["EPSG:2710"]="+proj=tmerc +lat_0=0 +lon_0=54 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 57E
Proj4js.defs["EPSG:2711"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 60E
Proj4js.defs["EPSG:2712"]="+proj=tmerc +lat_0=0 +lon_0=60 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 63E
Proj4js.defs["EPSG:2713"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 66E
Proj4js.defs["EPSG:2714"]="+proj=tmerc +lat_0=0 +lon_0=66 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 69E
Proj4js.defs["EPSG:2715"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 72E
Proj4js.defs["EPSG:2716"]="+proj=tmerc +lat_0=0 +lon_0=72 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 75E
Proj4js.defs["EPSG:2717"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 78E
Proj4js.defs["EPSG:2718"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 81E
Proj4js.defs["EPSG:2719"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 84E
Proj4js.defs["EPSG:2720"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 87E
Proj4js.defs["EPSG:2721"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 90E
Proj4js.defs["EPSG:2722"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 93E
Proj4js.defs["EPSG:2723"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 96E
Proj4js.defs["EPSG:2724"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 99E
Proj4js.defs["EPSG:2725"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 102E
Proj4js.defs["EPSG:2726"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 105E
Proj4js.defs["EPSG:2727"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 108E
Proj4js.defs["EPSG:2728"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 111E
Proj4js.defs["EPSG:2729"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 114E
Proj4js.defs["EPSG:2730"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 117E
Proj4js.defs["EPSG:2731"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 120E
Proj4js.defs["EPSG:2732"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 123E
Proj4js.defs["EPSG:2733"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 126E
Proj4js.defs["EPSG:2734"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 129E
Proj4js.defs["EPSG:2735"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Tete / UTM zone 36S
Proj4js.defs["EPSG:2736"]="+proj=utm +zone=36 +south +ellps=clrk66 +towgs84=219.315,168.975,-166.145,0.198,5.926,-2.356,-57.104 +units=m +no_defs  ";
// Tete / UTM zone 37S
Proj4js.defs["EPSG:2737"]="+proj=utm +zone=37 +south +ellps=clrk66 +towgs84=219.315,168.975,-166.145,0.198,5.926,-2.356,-57.104 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 132E
Proj4js.defs["EPSG:2738"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 135E
Proj4js.defs["EPSG:2739"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 138E
Proj4js.defs["EPSG:2740"]="+proj=tmerc +lat_0=0 +lon_0=138 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 141E
Proj4js.defs["EPSG:2741"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 144E
Proj4js.defs["EPSG:2742"]="+proj=tmerc +lat_0=0 +lon_0=144 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 147E
Proj4js.defs["EPSG:2743"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 150E
Proj4js.defs["EPSG:2744"]="+proj=tmerc +lat_0=0 +lon_0=150 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 153E
Proj4js.defs["EPSG:2745"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 156E
Proj4js.defs["EPSG:2746"]="+proj=tmerc +lat_0=0 +lon_0=156 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 159E
Proj4js.defs["EPSG:2747"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 162E
Proj4js.defs["EPSG:2748"]="+proj=tmerc +lat_0=0 +lon_0=162 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 165E
Proj4js.defs["EPSG:2749"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 168E
Proj4js.defs["EPSG:2750"]="+proj=tmerc +lat_0=0 +lon_0=168 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 171E
Proj4js.defs["EPSG:2751"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 174E
Proj4js.defs["EPSG:2752"]="+proj=tmerc +lat_0=0 +lon_0=174 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 177E
Proj4js.defs["EPSG:2753"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 180E
Proj4js.defs["EPSG:2754"]="+proj=tmerc +lat_0=0 +lon_0=180 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 177W
Proj4js.defs["EPSG:2755"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 174W
Proj4js.defs["EPSG:2756"]="+proj=tmerc +lat_0=0 +lon_0=-174 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 171W
Proj4js.defs["EPSG:2757"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 168W
Proj4js.defs["EPSG:2758"]="+proj=tmerc +lat_0=0 +lon_0=-168 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// NAD83(HARN) / Alabama East
Proj4js.defs["EPSG:2759"]="+proj=tmerc +lat_0=30.5 +lon_0=-85.83333333333333 +k=0.99996 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Alabama West
Proj4js.defs["EPSG:2760"]="+proj=tmerc +lat_0=30 +lon_0=-87.5 +k=0.999933333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Arizona East
Proj4js.defs["EPSG:2761"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Arizona Central
Proj4js.defs["EPSG:2762"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Arizona West
Proj4js.defs["EPSG:2763"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Arkansas North
Proj4js.defs["EPSG:2764"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Arkansas South
Proj4js.defs["EPSG:2765"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California zone 1
Proj4js.defs["EPSG:2766"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California zone 2
Proj4js.defs["EPSG:2767"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California zone 3
Proj4js.defs["EPSG:2768"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California zone 4
Proj4js.defs["EPSG:2769"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California zone 5
Proj4js.defs["EPSG:2770"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California zone 6
Proj4js.defs["EPSG:2771"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Colorado North
Proj4js.defs["EPSG:2772"]="+proj=lcc +lat_1=40.78333333333333 +lat_2=39.71666666666667 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Colorado Central
Proj4js.defs["EPSG:2773"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Colorado South
Proj4js.defs["EPSG:2774"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Connecticut
Proj4js.defs["EPSG:2775"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096 +y_0=152400.3048 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Delaware
Proj4js.defs["EPSG:2776"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Florida East
Proj4js.defs["EPSG:2777"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Florida West
Proj4js.defs["EPSG:2778"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Florida North
Proj4js.defs["EPSG:2779"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Georgia East
Proj4js.defs["EPSG:2780"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Georgia West
Proj4js.defs["EPSG:2781"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Hawaii zone 1
Proj4js.defs["EPSG:2782"]="+proj=tmerc +lat_0=18.83333333333333 +lon_0=-155.5 +k=0.999966667 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Hawaii zone 2
Proj4js.defs["EPSG:2783"]="+proj=tmerc +lat_0=20.33333333333333 +lon_0=-156.6666666666667 +k=0.999966667 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Hawaii zone 3
Proj4js.defs["EPSG:2784"]="+proj=tmerc +lat_0=21.16666666666667 +lon_0=-158 +k=0.99999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Hawaii zone 4
Proj4js.defs["EPSG:2785"]="+proj=tmerc +lat_0=21.83333333333333 +lon_0=-159.5 +k=0.99999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Hawaii zone 5
Proj4js.defs["EPSG:2786"]="+proj=tmerc +lat_0=21.66666666666667 +lon_0=-160.1666666666667 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Idaho East
Proj4js.defs["EPSG:2787"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Idaho Central
Proj4js.defs["EPSG:2788"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Idaho West
Proj4js.defs["EPSG:2789"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=800000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Illinois East
Proj4js.defs["EPSG:2790"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Illinois West
Proj4js.defs["EPSG:2791"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Indiana East
Proj4js.defs["EPSG:2792"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=100000 +y_0=250000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Indiana West
Proj4js.defs["EPSG:2793"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=250000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Iowa North
Proj4js.defs["EPSG:2794"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=1500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Iowa South
Proj4js.defs["EPSG:2795"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Kansas North
Proj4js.defs["EPSG:2796"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Kansas South
Proj4js.defs["EPSG:2797"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Kentucky North
Proj4js.defs["EPSG:2798"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Kentucky South
Proj4js.defs["EPSG:2799"]="+proj=lcc +lat_1=37.93333333333333 +lat_2=36.73333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=500000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Louisiana North
Proj4js.defs["EPSG:2800"]="+proj=lcc +lat_1=32.66666666666666 +lat_2=31.16666666666667 +lat_0=30.5 +lon_0=-92.5 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Louisiana South
Proj4js.defs["EPSG:2801"]="+proj=lcc +lat_1=30.7 +lat_2=29.3 +lat_0=28.5 +lon_0=-91.33333333333333 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine East
Proj4js.defs["EPSG:2802"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine West
Proj4js.defs["EPSG:2803"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maryland
Proj4js.defs["EPSG:2804"]="+proj=lcc +lat_1=39.45 +lat_2=38.3 +lat_0=37.66666666666666 +lon_0=-77 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Massachusetts Mainland
Proj4js.defs["EPSG:2805"]="+proj=lcc +lat_1=42.68333333333333 +lat_2=41.71666666666667 +lat_0=41 +lon_0=-71.5 +x_0=200000 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Massachusetts Island
Proj4js.defs["EPSG:2806"]="+proj=lcc +lat_1=41.48333333333333 +lat_2=41.28333333333333 +lat_0=41 +lon_0=-70.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Michigan North
Proj4js.defs["EPSG:2807"]="+proj=lcc +lat_1=47.08333333333334 +lat_2=45.48333333333333 +lat_0=44.78333333333333 +lon_0=-87 +x_0=8000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Michigan Central
Proj4js.defs["EPSG:2808"]="+proj=lcc +lat_1=45.7 +lat_2=44.18333333333333 +lat_0=43.31666666666667 +lon_0=-84.36666666666666 +x_0=6000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Michigan South
Proj4js.defs["EPSG:2809"]="+proj=lcc +lat_1=43.66666666666666 +lat_2=42.1 +lat_0=41.5 +lon_0=-84.36666666666666 +x_0=4000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Minnesota North
Proj4js.defs["EPSG:2810"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Minnesota Central
Proj4js.defs["EPSG:2811"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Minnesota South
Proj4js.defs["EPSG:2812"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Mississippi East
Proj4js.defs["EPSG:2813"]="+proj=tmerc +lat_0=29.5 +lon_0=-88.83333333333333 +k=0.99995 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Mississippi West
Proj4js.defs["EPSG:2814"]="+proj=tmerc +lat_0=29.5 +lon_0=-90.33333333333333 +k=0.99995 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Missouri East
Proj4js.defs["EPSG:2815"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-90.5 +k=0.999933333 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Missouri Central
Proj4js.defs["EPSG:2816"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-92.5 +k=0.999933333 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Missouri West
Proj4js.defs["EPSG:2817"]="+proj=tmerc +lat_0=36.16666666666666 +lon_0=-94.5 +k=0.999941177 +x_0=850000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Montana
Proj4js.defs["EPSG:2818"]="+proj=lcc +lat_1=49 +lat_2=45 +lat_0=44.25 +lon_0=-109.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Nebraska
Proj4js.defs["EPSG:2819"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Nevada East
Proj4js.defs["EPSG:2820"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=200000 +y_0=8000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Nevada Central
Proj4js.defs["EPSG:2821"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=500000 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Nevada West
Proj4js.defs["EPSG:2822"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=800000 +y_0=4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New Hampshire
Proj4js.defs["EPSG:2823"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New Jersey
Proj4js.defs["EPSG:2824"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New Mexico East
Proj4js.defs["EPSG:2825"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=165000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New Mexico Central
Proj4js.defs["EPSG:2826"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New Mexico West
Proj4js.defs["EPSG:2827"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=830000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New York East
Proj4js.defs["EPSG:2828"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New York Central
Proj4js.defs["EPSG:2829"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New York West
Proj4js.defs["EPSG:2830"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=350000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / New York Long Island
Proj4js.defs["EPSG:2831"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / North Dakota North
Proj4js.defs["EPSG:2832"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.43333333333333 +lat_0=47 +lon_0=-100.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / North Dakota South
Proj4js.defs["EPSG:2833"]="+proj=lcc +lat_1=47.48333333333333 +lat_2=46.18333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Ohio North
Proj4js.defs["EPSG:2834"]="+proj=lcc +lat_1=41.7 +lat_2=40.43333333333333 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Ohio South
Proj4js.defs["EPSG:2835"]="+proj=lcc +lat_1=40.03333333333333 +lat_2=38.73333333333333 +lat_0=38 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Oklahoma North
Proj4js.defs["EPSG:2836"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=35.56666666666667 +lat_0=35 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Oklahoma South
Proj4js.defs["EPSG:2837"]="+proj=lcc +lat_1=35.23333333333333 +lat_2=33.93333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Oregon North
Proj4js.defs["EPSG:2838"]="+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Oregon South
Proj4js.defs["EPSG:2839"]="+proj=lcc +lat_1=44 +lat_2=42.33333333333334 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=1500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Rhode Island
Proj4js.defs["EPSG:2840"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.99999375 +x_0=100000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / South Dakota North
Proj4js.defs["EPSG:2841"]="+proj=lcc +lat_1=45.68333333333333 +lat_2=44.41666666666666 +lat_0=43.83333333333334 +lon_0=-100 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / South Dakota South
Proj4js.defs["EPSG:2842"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Tennessee
Proj4js.defs["EPSG:2843"]="+proj=lcc +lat_1=36.41666666666666 +lat_2=35.25 +lat_0=34.33333333333334 +lon_0=-86 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas North
Proj4js.defs["EPSG:2844"]="+proj=lcc +lat_1=36.18333333333333 +lat_2=34.65 +lat_0=34 +lon_0=-101.5 +x_0=200000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas North Central
Proj4js.defs["EPSG:2845"]="+proj=lcc +lat_1=33.96666666666667 +lat_2=32.13333333333333 +lat_0=31.66666666666667 +lon_0=-98.5 +x_0=600000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas Central
Proj4js.defs["EPSG:2846"]="+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=700000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas South Central
Proj4js.defs["EPSG:2847"]="+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas South
Proj4js.defs["EPSG:2848"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=300000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Utah North
Proj4js.defs["EPSG:2849"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Utah Central
Proj4js.defs["EPSG:2850"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Utah South
Proj4js.defs["EPSG:2851"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Vermont
Proj4js.defs["EPSG:2852"]="+proj=tmerc +lat_0=42.5 +lon_0=-72.5 +k=0.999964286 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Virginia North
Proj4js.defs["EPSG:2853"]="+proj=lcc +lat_1=39.2 +lat_2=38.03333333333333 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=3500000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Virginia South
Proj4js.defs["EPSG:2854"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Washington North
Proj4js.defs["EPSG:2855"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.5 +lat_0=47 +lon_0=-120.8333333333333 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Washington South
Proj4js.defs["EPSG:2856"]="+proj=lcc +lat_1=47.33333333333334 +lat_2=45.83333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / West Virginia North
Proj4js.defs["EPSG:2857"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / West Virginia South
Proj4js.defs["EPSG:2858"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wisconsin North
Proj4js.defs["EPSG:2859"]="+proj=lcc +lat_1=46.76666666666667 +lat_2=45.56666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wisconsin Central
Proj4js.defs["EPSG:2860"]="+proj=lcc +lat_1=45.5 +lat_2=44.25 +lat_0=43.83333333333334 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wisconsin South
Proj4js.defs["EPSG:2861"]="+proj=lcc +lat_1=44.06666666666667 +lat_2=42.73333333333333 +lat_0=42 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wyoming East
Proj4js.defs["EPSG:2862"]="+proj=tmerc +lat_0=40.5 +lon_0=-105.1666666666667 +k=0.9999375 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wyoming East Central
Proj4js.defs["EPSG:2863"]="+proj=tmerc +lat_0=40.5 +lon_0=-107.3333333333333 +k=0.9999375 +x_0=400000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wyoming West Central
Proj4js.defs["EPSG:2864"]="+proj=tmerc +lat_0=40.5 +lon_0=-108.75 +k=0.9999375 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wyoming West
Proj4js.defs["EPSG:2865"]="+proj=tmerc +lat_0=40.5 +lon_0=-110.0833333333333 +k=0.9999375 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Puerto Rico and Virgin Is.
Proj4js.defs["EPSG:2866"]="+proj=lcc +lat_1=18.43333333333333 +lat_2=18.03333333333333 +lat_0=17.83333333333333 +lon_0=-66.43333333333334 +x_0=200000 +y_0=200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Arizona East (ft)
Proj4js.defs["EPSG:2867"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Arizona Central (ft)
Proj4js.defs["EPSG:2868"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Arizona West (ft)
Proj4js.defs["EPSG:2869"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / California zone 1 (ftUS)
Proj4js.defs["EPSG:2870"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / California zone 2 (ftUS)
Proj4js.defs["EPSG:2871"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / California zone 3 (ftUS)
Proj4js.defs["EPSG:2872"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / California zone 4 (ftUS)
Proj4js.defs["EPSG:2873"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / California zone 5 (ftUS)
Proj4js.defs["EPSG:2874"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / California zone 6 (ftUS)
Proj4js.defs["EPSG:2875"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Colorado North (ftUS)
Proj4js.defs["EPSG:2876"]="+proj=lcc +lat_1=40.78333333333333 +lat_2=39.71666666666667 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Colorado Central (ftUS)
Proj4js.defs["EPSG:2877"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Colorado South (ftUS)
Proj4js.defs["EPSG:2878"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Connecticut (ftUS)
Proj4js.defs["EPSG:2879"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096012192 +y_0=152400.3048006096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Delaware (ftUS)
Proj4js.defs["EPSG:2880"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Florida East (ftUS)
Proj4js.defs["EPSG:2881"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Florida West (ftUS)
Proj4js.defs["EPSG:2882"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Florida North (ftUS)
Proj4js.defs["EPSG:2883"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Georgia East (ftUS)
Proj4js.defs["EPSG:2884"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Georgia West (ftUS)
Proj4js.defs["EPSG:2885"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Idaho East (ftUS)
Proj4js.defs["EPSG:2886"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Idaho Central (ftUS)
Proj4js.defs["EPSG:2887"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Idaho West (ftUS)
Proj4js.defs["EPSG:2888"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=800000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Indiana East (ftUS) (deprecated)
Proj4js.defs["EPSG:2889"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=99999.99989839978 +y_0=249364.9987299975 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Indiana West (ftUS) (deprecated)
Proj4js.defs["EPSG:2890"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=249364.9987299975 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Kentucky North (ftUS)
Proj4js.defs["EPSG:2891"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Kentucky South (ftUS)
Proj4js.defs["EPSG:2892"]="+proj=lcc +lat_1=37.93333333333333 +lat_2=36.73333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=500000.0001016001 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Maryland (ftUS)
Proj4js.defs["EPSG:2893"]="+proj=lcc +lat_1=39.45 +lat_2=38.3 +lat_0=37.66666666666666 +lon_0=-77 +x_0=399999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Massachusetts Mainland (ftUS)
Proj4js.defs["EPSG:2894"]="+proj=lcc +lat_1=42.68333333333333 +lat_2=41.71666666666667 +lat_0=41 +lon_0=-71.5 +x_0=200000.0001016002 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Massachusetts Island (ftUS)
Proj4js.defs["EPSG:2895"]="+proj=lcc +lat_1=41.48333333333333 +lat_2=41.28333333333333 +lat_0=41 +lon_0=-70.5 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Michigan North (ft)
Proj4js.defs["EPSG:2896"]="+proj=lcc +lat_1=47.08333333333334 +lat_2=45.48333333333333 +lat_0=44.78333333333333 +lon_0=-87 +x_0=7999999.999968001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Michigan Central (ft)
Proj4js.defs["EPSG:2897"]="+proj=lcc +lat_1=45.7 +lat_2=44.18333333333333 +lat_0=43.31666666666667 +lon_0=-84.36666666666666 +x_0=5999999.999976001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Michigan South (ft)
Proj4js.defs["EPSG:2898"]="+proj=lcc +lat_1=43.66666666666666 +lat_2=42.1 +lat_0=41.5 +lon_0=-84.36666666666666 +x_0=3999999.999984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Mississippi East (ftUS)
Proj4js.defs["EPSG:2899"]="+proj=tmerc +lat_0=29.5 +lon_0=-88.83333333333333 +k=0.99995 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Mississippi West (ftUS)
Proj4js.defs["EPSG:2900"]="+proj=tmerc +lat_0=29.5 +lon_0=-90.33333333333333 +k=0.99995 +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Montana (ft)
Proj4js.defs["EPSG:2901"]="+proj=lcc +lat_1=49 +lat_2=45 +lat_0=44.25 +lon_0=-109.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / New Mexico East (ftUS)
Proj4js.defs["EPSG:2902"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=165000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New Mexico Central (ftUS)
Proj4js.defs["EPSG:2903"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New Mexico West (ftUS)
Proj4js.defs["EPSG:2904"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=830000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New York East (ftUS)
Proj4js.defs["EPSG:2905"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New York Central (ftUS)
Proj4js.defs["EPSG:2906"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=249999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New York West (ftUS)
Proj4js.defs["EPSG:2907"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=350000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New York Long Island (ftUS)
Proj4js.defs["EPSG:2908"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / North Dakota North (ft)
Proj4js.defs["EPSG:2909"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.43333333333333 +lat_0=47 +lon_0=-100.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / North Dakota South (ft)
Proj4js.defs["EPSG:2910"]="+proj=lcc +lat_1=47.48333333333333 +lat_2=46.18333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Oklahoma North (ftUS)
Proj4js.defs["EPSG:2911"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=35.56666666666667 +lat_0=35 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Oklahoma South (ftUS)
Proj4js.defs["EPSG:2912"]="+proj=lcc +lat_1=35.23333333333333 +lat_2=33.93333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Oregon North (ft)
Proj4js.defs["EPSG:2913"]="+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000.0001424 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Oregon South (ft)
Proj4js.defs["EPSG:2914"]="+proj=lcc +lat_1=44 +lat_2=42.33333333333334 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=1500000.0001464 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Tennessee (ftUS)
Proj4js.defs["EPSG:2915"]="+proj=lcc +lat_1=36.41666666666666 +lat_2=35.25 +lat_0=34.33333333333334 +lon_0=-86 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Texas North (ftUS)
Proj4js.defs["EPSG:2916"]="+proj=lcc +lat_1=36.18333333333333 +lat_2=34.65 +lat_0=34 +lon_0=-101.5 +x_0=200000.0001016002 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Texas North Central (ftUS)
Proj4js.defs["EPSG:2917"]="+proj=lcc +lat_1=33.96666666666667 +lat_2=32.13333333333333 +lat_0=31.66666666666667 +lon_0=-98.5 +x_0=600000 +y_0=2000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Texas Central (ftUS)
Proj4js.defs["EPSG:2918"]="+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=699999.9998983998 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Texas South Central (ftUS)
Proj4js.defs["EPSG:2919"]="+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=3999999.9998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Texas South (ftUS)
Proj4js.defs["EPSG:2920"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=300000.0000000001 +y_0=5000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Utah North (ft)
Proj4js.defs["EPSG:2921"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000.0001504 +y_0=999999.9999960001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Utah Central (ft)
Proj4js.defs["EPSG:2922"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000.0001504 +y_0=1999999.999992 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Utah South (ft)
Proj4js.defs["EPSG:2923"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000.0001504 +y_0=2999999.999988 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Virginia North (ftUS)
Proj4js.defs["EPSG:2924"]="+proj=lcc +lat_1=39.2 +lat_2=38.03333333333333 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=2000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Virginia South (ftUS)
Proj4js.defs["EPSG:2925"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Washington North (ftUS)
Proj4js.defs["EPSG:2926"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.5 +lat_0=47 +lon_0=-120.8333333333333 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Washington South (ftUS)
Proj4js.defs["EPSG:2927"]="+proj=lcc +lat_1=47.33333333333334 +lat_2=45.83333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wisconsin North (ftUS)
Proj4js.defs["EPSG:2928"]="+proj=lcc +lat_1=46.76666666666667 +lat_2=45.56666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wisconsin Central (ftUS)
Proj4js.defs["EPSG:2929"]="+proj=lcc +lat_1=45.5 +lat_2=44.25 +lat_0=43.83333333333334 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wisconsin South (ftUS)
Proj4js.defs["EPSG:2930"]="+proj=lcc +lat_1=44.06666666666667 +lat_2=42.73333333333333 +lat_0=42 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Beduaram / TM 13 NE
Proj4js.defs["EPSG:2931"]="+proj=tmerc +lat_0=0 +lon_0=13 +k=0.9996 +x_0=500000 +y_0=0 +a=6378249.2 +b=6356515 +towgs84=-106,-87,188,0,0,0,0 +units=m +no_defs  ";
// QND95 / Qatar National Grid
Proj4js.defs["EPSG:2932"]="+proj=tmerc +lat_0=24.45 +lon_0=51.21666666666667 +k=0.99999 +x_0=200000 +y_0=300000 +ellps=intl +towgs84=-119.425,-303.659,-11.0006,1.1643,0.174458,1.09626,3.65706 +units=m +no_defs  ";
// Segara / UTM zone 50S
Proj4js.defs["EPSG:2933"]="+proj=utm +zone=50 +south +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +units=m +no_defs  ";
// Segara (Jakarta) / NEIEZ (deprecated)
Proj4js.defs["EPSG:2934"]="+proj=merc +lon_0=110 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +pm=jakarta +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone A1
Proj4js.defs["EPSG:2935"]="+proj=tmerc +lat_0=0.1166666666666667 +lon_0=41.53333333333333 +k=1 +x_0=1300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone A2
Proj4js.defs["EPSG:2936"]="+proj=tmerc +lat_0=0.1166666666666667 +lon_0=44.53333333333333 +k=1 +x_0=2300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone A3
Proj4js.defs["EPSG:2937"]="+proj=tmerc +lat_0=0.1166666666666667 +lon_0=47.53333333333333 +k=1 +x_0=3300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone A4
Proj4js.defs["EPSG:2938"]="+proj=tmerc +lat_0=0.1166666666666667 +lon_0=50.53333333333333 +k=1 +x_0=4300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone K2
Proj4js.defs["EPSG:2939"]="+proj=tmerc +lat_0=0.1333333333333333 +lon_0=50.76666666666667 +k=1 +x_0=2300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone K3
Proj4js.defs["EPSG:2940"]="+proj=tmerc +lat_0=0.1333333333333333 +lon_0=53.76666666666667 +k=1 +x_0=3300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone K4
Proj4js.defs["EPSG:2941"]="+proj=tmerc +lat_0=0.1333333333333333 +lon_0=56.76666666666667 +k=1 +x_0=4300000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Porto Santo / UTM zone 28N
Proj4js.defs["EPSG:2942"]="+proj=utm +zone=28 +ellps=intl +towgs84=-499,-249,314,0,0,0,0 +units=m +no_defs  ";
// Selvagem Grande / UTM zone 28N
Proj4js.defs["EPSG:2943"]="+proj=utm +zone=28 +ellps=intl +towgs84=-289,-124,60,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / SCoPQ zone 2 (deprecated)
Proj4js.defs["EPSG:2944"]="+proj=tmerc +lat_0=0 +lon_0=-55.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 3
Proj4js.defs["EPSG:2945"]="+proj=tmerc +lat_0=0 +lon_0=-58.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 4
Proj4js.defs["EPSG:2946"]="+proj=tmerc +lat_0=0 +lon_0=-61.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 5
Proj4js.defs["EPSG:2947"]="+proj=tmerc +lat_0=0 +lon_0=-64.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 6
Proj4js.defs["EPSG:2948"]="+proj=tmerc +lat_0=0 +lon_0=-67.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 7
Proj4js.defs["EPSG:2949"]="+proj=tmerc +lat_0=0 +lon_0=-70.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 8
Proj4js.defs["EPSG:2950"]="+proj=tmerc +lat_0=0 +lon_0=-73.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 9
Proj4js.defs["EPSG:2951"]="+proj=tmerc +lat_0=0 +lon_0=-76.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 10
Proj4js.defs["EPSG:2952"]="+proj=tmerc +lat_0=0 +lon_0=-79.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / New Brunswick Stereographic
Proj4js.defs["EPSG:2953"]="+proj=sterea +lat_0=46.5 +lon_0=-66.5 +k=0.999912 +x_0=2500000 +y_0=7500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Prince Edward Isl. Stereographic (NAD83)
Proj4js.defs["EPSG:2954"]="+proj=sterea +lat_0=47.25 +lon_0=-63 +k=0.999912 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 11N
Proj4js.defs["EPSG:2955"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 12N
Proj4js.defs["EPSG:2956"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 13N
Proj4js.defs["EPSG:2957"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 17N
Proj4js.defs["EPSG:2958"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 18N
Proj4js.defs["EPSG:2959"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 19N
Proj4js.defs["EPSG:2960"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 20N
Proj4js.defs["EPSG:2961"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 21N
Proj4js.defs["EPSG:2962"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Lisbon 1890 (Lisbon) / Portugal Bonne
// Unable to translate coordinate system EPSG:2963 into PROJ.4 format.
//
// NAD27 / Alaska Albers
Proj4js.defs["EPSG:2964"]="+proj=aea +lat_1=55 +lat_2=65 +lat_0=50 +lon_0=-154 +x_0=0 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD83 / Indiana East (ftUS)
Proj4js.defs["EPSG:2965"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=99999.99989839978 +y_0=249999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Indiana West (ftUS)
Proj4js.defs["EPSG:2966"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=249999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Indiana East (ftUS)
Proj4js.defs["EPSG:2967"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=99999.99989839978 +y_0=249999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Indiana West (ftUS)
Proj4js.defs["EPSG:2968"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=249999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Fort Marigot / UTM zone 20N
Proj4js.defs["EPSG:2969"]="+proj=utm +zone=20 +ellps=intl +towgs84=137,248,-430,0,0,0,0 +units=m +no_defs  ";
// Guadeloupe 1948 / UTM zone 20N
Proj4js.defs["EPSG:2970"]="+proj=utm +zone=20 +ellps=intl +towgs84=-467,-16,-300,0,0,0,0 +units=m +no_defs  ";
// CSG67 / UTM zone 22N
Proj4js.defs["EPSG:2971"]="+proj=utm +zone=22 +ellps=intl +towgs84=-186,230,110,0,0,0,0 +units=m +no_defs  ";
// RGFG95 / UTM zone 22N
Proj4js.defs["EPSG:2972"]="+proj=utm +zone=22 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Martinique 1938 / UTM zone 20N
Proj4js.defs["EPSG:2973"]="+proj=utm +zone=20 +ellps=intl +towgs84=186,482,151,0,0,0,0 +units=m +no_defs  ";
// RGR92 / UTM zone 40S
Proj4js.defs["EPSG:2975"]="+proj=utm +zone=40 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Tahiti 52 / UTM zone 6S
Proj4js.defs["EPSG:2976"]="+proj=utm +zone=6 +south +ellps=intl +towgs84=162,117,154,0,0,0,0 +units=m +no_defs  ";
// Tahaa 54 / UTM zone 5S
Proj4js.defs["EPSG:2977"]="+proj=utm +zone=5 +south +ellps=intl +towgs84=72.438,345.918,79.486,1.6045,0.8823,0.5565,1.3746 +units=m +no_defs  ";
// IGN72 Nuku Hiva / UTM zone 7S
Proj4js.defs["EPSG:2978"]="+proj=utm +zone=7 +south +ellps=intl +towgs84=84,274,65,0,0,0,0 +units=m +no_defs  ";
// K0 1949 / UTM zone 42S (deprecated)
Proj4js.defs["EPSG:2979"]="+proj=utm +zone=42 +south +ellps=intl +towgs84=145,-187,103,0,0,0,0 +units=m +no_defs  ";
// Combani 1950 / UTM zone 38S
Proj4js.defs["EPSG:2980"]="+proj=utm +zone=38 +south +ellps=intl +towgs84=-382,-59,-262,0,0,0,0 +units=m +no_defs  ";
// IGN56 Lifou / UTM zone 58S
Proj4js.defs["EPSG:2981"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=335.47,222.58,-230.94,0,0,0,0 +units=m +no_defs  ";
// IGN72 Grand Terre / UTM zone 58S (deprecated)
Proj4js.defs["EPSG:2982"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=-13,-348,292,0,0,0,0 +units=m +no_defs  ";
// ST87 Ouvea / UTM zone 58S (deprecated)
Proj4js.defs["EPSG:2983"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=-122.383,-188.696,103.344,3.5107,-4.9668,-5.7047,4.4798 +units=m +no_defs  ";
// RGNC 1991 / Lambert New Caledonia (deprecated)
Proj4js.defs["EPSG:2984"]="+proj=lcc +lat_1=-20.66666666666667 +lat_2=-22.33333333333333 +lat_0=-21.5 +lon_0=166 +x_0=400000 +y_0=300000 +ellps=intl +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Petrels 1972 / Terre Adelie Polar Stereographic
// Unable to translate coordinate system EPSG:2985 into PROJ.4 format.
//
// Perroud 1950 / Terre Adelie Polar Stereographic
// Unable to translate coordinate system EPSG:2986 into PROJ.4 format.
//
// Saint Pierre et Miquelon 1950 / UTM zone 21N
Proj4js.defs["EPSG:2987"]="+proj=utm +zone=21 +ellps=clrk66 +towgs84=30,430,368,0,0,0,0 +units=m +no_defs  ";
// MOP78 / UTM zone 1S
Proj4js.defs["EPSG:2988"]="+proj=utm +zone=1 +south +ellps=intl +towgs84=253,-132,-127,0,0,0,0 +units=m +no_defs  ";
// RRAF 1991 / UTM zone 20N (deprecated)
Proj4js.defs["EPSG:2989"]="+proj=utm +zone=20 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Reunion 1947 / TM Reunion (deprecated)
Proj4js.defs["EPSG:2990"]="+proj=tmerc +lat_0=-21.11666666666667 +lon_0=55.53333333333333 +k=1 +x_0=50000 +y_0=160000 +ellps=intl +towgs84=94,-948,-1262,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Oregon Lambert
Proj4js.defs["EPSG:2991"]="+proj=lcc +lat_1=43 +lat_2=45.5 +lat_0=41.75 +lon_0=-120.5 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Oregon Lambert (ft)
Proj4js.defs["EPSG:2992"]="+proj=lcc +lat_1=43 +lat_2=45.5 +lat_0=41.75 +lon_0=-120.5 +x_0=399999.9999984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Oregon Lambert
Proj4js.defs["EPSG:2993"]="+proj=lcc +lat_1=43 +lat_2=45.5 +lat_0=41.75 +lon_0=-120.5 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Oregon Lambert (ft)
Proj4js.defs["EPSG:2994"]="+proj=lcc +lat_1=43 +lat_2=45.5 +lat_0=41.75 +lon_0=-120.5 +x_0=399999.9999984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// IGN53 Mare / UTM zone 58S
Proj4js.defs["EPSG:2995"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=287.58,177.78,-135.41,0,0,0,0 +units=m +no_defs  ";
// ST84 Ile des Pins / UTM zone 58S
Proj4js.defs["EPSG:2996"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=-13,-348,292,0,0,0,0 +units=m +no_defs  ";
// ST71 Belep / UTM zone 58S
Proj4js.defs["EPSG:2997"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=-480.26,-438.32,-643.429,16.3119,20.1721,-4.0349,-111.7 +units=m +no_defs  ";
// NEA74 Noumea / UTM zone 58S
Proj4js.defs["EPSG:2998"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=-10.18,-350.43,291.37,0,0,0,0 +units=m +no_defs  ";
// Grand Comoros / UTM zone 38S
Proj4js.defs["EPSG:2999"]="+proj=utm +zone=38 +south +ellps=intl +towgs84=-963,510,-359,0,0,0,0 +units=m +no_defs  ";
// Segara / NEIEZ
Proj4js.defs["EPSG:3000"]="+proj=merc +lon_0=110 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +units=m +no_defs  ";
// Batavia / NEIEZ
Proj4js.defs["EPSG:3001"]="+proj=merc +lon_0=110 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +units=m +no_defs  ";
// Makassar / NEIEZ
Proj4js.defs["EPSG:3002"]="+proj=merc +lon_0=110 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-587.8,519.75,145.76,0,0,0,0 +units=m +no_defs  ";
// Monte Mario / Italy zone 1
Proj4js.defs["EPSG:3003"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=1500000 +y_0=0 +ellps=intl +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.68 +units=m +no_defs  ";
// Monte Mario / Italy zone 2
Proj4js.defs["EPSG:3004"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9996 +x_0=2520000 +y_0=0 +ellps=intl +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.68 +units=m +no_defs  ";
// NAD83 / BC Albers
Proj4js.defs["EPSG:3005"]="+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 TM
Proj4js.defs["EPSG:3006"]="+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 12 00
Proj4js.defs["EPSG:3007"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 13 30
Proj4js.defs["EPSG:3008"]="+proj=tmerc +lat_0=0 +lon_0=13.5 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 15 00
Proj4js.defs["EPSG:3009"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 16 30
Proj4js.defs["EPSG:3010"]="+proj=tmerc +lat_0=0 +lon_0=16.5 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 18 00
Proj4js.defs["EPSG:3011"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 14 15
Proj4js.defs["EPSG:3012"]="+proj=tmerc +lat_0=0 +lon_0=14.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 15 45
Proj4js.defs["EPSG:3013"]="+proj=tmerc +lat_0=0 +lon_0=15.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 17 15
Proj4js.defs["EPSG:3014"]="+proj=tmerc +lat_0=0 +lon_0=17.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 18 45
Proj4js.defs["EPSG:3015"]="+proj=tmerc +lat_0=0 +lon_0=18.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 20 15
Proj4js.defs["EPSG:3016"]="+proj=tmerc +lat_0=0 +lon_0=20.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 21 45
Proj4js.defs["EPSG:3017"]="+proj=tmerc +lat_0=0 +lon_0=21.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 23 15
Proj4js.defs["EPSG:3018"]="+proj=tmerc +lat_0=0 +lon_0=23.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RT90 7.5 gon V
Proj4js.defs["EPSG:3019"]="+proj=tmerc +lat_0=0 +lon_0=11.30827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// RT90 5 gon V
Proj4js.defs["EPSG:3020"]="+proj=tmerc +lat_0=0 +lon_0=13.55827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// RT90 2.5 gon V
Proj4js.defs["EPSG:3021"]="+proj=tmerc +lat_0=0 +lon_0=15.80827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// RT90 0 gon
Proj4js.defs["EPSG:3022"]="+proj=tmerc +lat_0=0 +lon_0=18.05827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// RT90 2.5 gon O
Proj4js.defs["EPSG:3023"]="+proj=tmerc +lat_0=0 +lon_0=20.30827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// RT90 5 gon O
Proj4js.defs["EPSG:3024"]="+proj=tmerc +lat_0=0 +lon_0=22.55827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs  ";
// RT38 7.5 gon V
Proj4js.defs["EPSG:3025"]="+proj=tmerc +lat_0=0 +lon_0=11.30827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RT38 5 gon V
Proj4js.defs["EPSG:3026"]="+proj=tmerc +lat_0=0 +lon_0=13.55827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RT38 2.5 gon V
Proj4js.defs["EPSG:3027"]="+proj=tmerc +lat_0=0 +lon_0=15.80827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RT38 0 gon
Proj4js.defs["EPSG:3028"]="+proj=tmerc +lat_0=0 +lon_0=18.05827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RT38 2.5 gon O
Proj4js.defs["EPSG:3029"]="+proj=tmerc +lat_0=0 +lon_0=20.30827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RT38 5 gon O
Proj4js.defs["EPSG:3030"]="+proj=tmerc +lat_0=0 +lon_0=22.55827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// WGS 84 / Antarctic Polar Stereographic
Proj4js.defs["EPSG:3031"]="+proj=stere +lat_0=-90 +lat_ts=-71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / Australian Antarctic Polar Stereographic
Proj4js.defs["EPSG:3032"]="+proj=stere +lat_0=-90 +lat_ts=-71 +lon_0=70 +k=1 +x_0=6000000 +y_0=6000000 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / Australian Antarctic Lambert
Proj4js.defs["EPSG:3033"]="+proj=lcc +lat_1=-68.5 +lat_2=-74.5 +lat_0=-50 +lon_0=70 +x_0=6000000 +y_0=6000000 +datum=WGS84 +units=m +no_defs  ";
// ETRS89 / LCC Europe
Proj4js.defs["EPSG:3034"]="+proj=lcc +lat_1=35 +lat_2=65 +lat_0=52 +lon_0=10 +x_0=4000000 +y_0=2800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / LAEA Europe
Proj4js.defs["EPSG:3035"]="+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Moznet / UTM zone 36S
Proj4js.defs["EPSG:3036"]="+proj=utm +zone=36 +south +ellps=WGS84 +towgs84=0,0,0,-0,-0,-0,0 +units=m +no_defs  ";
// Moznet / UTM zone 37S
Proj4js.defs["EPSG:3037"]="+proj=utm +zone=37 +south +ellps=WGS84 +towgs84=0,0,0,-0,-0,-0,0 +units=m +no_defs  ";
// ETRS89 / TM26
Proj4js.defs["EPSG:3038"]="+proj=utm +zone=26 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM27
Proj4js.defs["EPSG:3039"]="+proj=utm +zone=27 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM28
Proj4js.defs["EPSG:3040"]="+proj=utm +zone=28 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM29
Proj4js.defs["EPSG:3041"]="+proj=utm +zone=29 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM30
Proj4js.defs["EPSG:3042"]="+proj=utm +zone=30 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM31
Proj4js.defs["EPSG:3043"]="+proj=utm +zone=31 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM32
Proj4js.defs["EPSG:3044"]="+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM33
Proj4js.defs["EPSG:3045"]="+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM34
Proj4js.defs["EPSG:3046"]="+proj=utm +zone=34 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM35
Proj4js.defs["EPSG:3047"]="+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM36
Proj4js.defs["EPSG:3048"]="+proj=utm +zone=36 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM37
Proj4js.defs["EPSG:3049"]="+proj=utm +zone=37 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM38
Proj4js.defs["EPSG:3050"]="+proj=utm +zone=38 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM39
Proj4js.defs["EPSG:3051"]="+proj=utm +zone=39 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Reykjavik 1900 / Lambert 1900
// Unable to translate coordinate system EPSG:3052 into PROJ.4 format.
//
// Hjorsey 1955 / Lambert 1955
// Unable to translate coordinate system EPSG:3053 into PROJ.4 format.
//
// Hjorsey 1955 / UTM zone 26N
Proj4js.defs["EPSG:3054"]="+proj=utm +zone=26 +ellps=intl +towgs84=-73,46,-86,0,0,0,0 +units=m +no_defs  ";
// Hjorsey 1955 / UTM zone 27N
Proj4js.defs["EPSG:3055"]="+proj=utm +zone=27 +ellps=intl +towgs84=-73,46,-86,0,0,0,0 +units=m +no_defs  ";
// Hjorsey 1955 / UTM zone 28N
Proj4js.defs["EPSG:3056"]="+proj=utm +zone=28 +ellps=intl +towgs84=-73,46,-86,0,0,0,0 +units=m +no_defs  ";
// ISN93 / Lambert 1993
Proj4js.defs["EPSG:3057"]="+proj=lcc +lat_1=64.25 +lat_2=65.75 +lat_0=65 +lon_0=-19 +x_0=500000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Helle 1954 / Jan Mayen Grid
Proj4js.defs["EPSG:3058"]="+proj=tmerc +lat_0=0 +lon_0=-8.5 +k=1 +x_0=50000 +y_0=-7800000 +ellps=intl +towgs84=982.609,552.753,-540.873,6.68163,-31.6115,-19.8482,16.805 +units=m +no_defs  ";
// LKS92 / Latvia TM
Proj4js.defs["EPSG:3059"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9996 +x_0=500000 +y_0=-6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGN72 Grande Terre / UTM zone 58S
Proj4js.defs["EPSG:3060"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=-11.64,-348.6,291.98,0,0,0,0 +units=m +no_defs  ";
// Porto Santo 1995 / UTM zone 28N
Proj4js.defs["EPSG:3061"]="+proj=utm +zone=28 +ellps=intl +towgs84=-502.862,-247.438,312.724,0,0,0,0 +units=m +no_defs  ";
// Azores Oriental 1995 / UTM zone 26N
Proj4js.defs["EPSG:3062"]="+proj=utm +zone=26 +ellps=intl +towgs84=-204.619,140.176,55.226,0,0,0,0 +units=m +no_defs  ";
// Azores Central 1995 / UTM zone 26N
Proj4js.defs["EPSG:3063"]="+proj=utm +zone=26 +ellps=intl +towgs84=-106.226,166.366,-37.893,0,0,0,0 +units=m +no_defs  ";
// IGM95 / UTM zone 32N
Proj4js.defs["EPSG:3064"]="+proj=utm +zone=32 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGM95 / UTM zone 33N
Proj4js.defs["EPSG:3065"]="+proj=utm +zone=33 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ED50 / Jordan TM
Proj4js.defs["EPSG:3066"]="+proj=tmerc +lat_0=0 +lon_0=37 +k=0.9998 +x_0=500000 +y_0=-3000000 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM35FIN(E,N)
Proj4js.defs["EPSG:3067"]="+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DHDN / Soldner Berlin
Proj4js.defs["EPSG:3068"]="+proj=cass +lat_0=52.41864827777778 +lon_0=13.62720366666667 +x_0=40000 +y_0=10000 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// NAD27 / Wisconsin Transverse Mercator
Proj4js.defs["EPSG:3069"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9996 +x_0=500000 +y_0=-4500000 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / Wisconsin Transverse Mercator
Proj4js.defs["EPSG:3070"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9996 +x_0=520000 +y_0=-4480000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Wisconsin Transverse Mercator
Proj4js.defs["EPSG:3071"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9996 +x_0=520000 +y_0=-4480000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine CS2000 East
Proj4js.defs["EPSG:3072"]="+proj=tmerc +lat_0=43.83333333333334 +lon_0=-67.875 +k=0.99998 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine CS2000 Central (deprecated)
Proj4js.defs["EPSG:3073"]="+proj=tmerc +lat_0=43 +lon_0=-69.125 +k=0.99998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine CS2000 West
Proj4js.defs["EPSG:3074"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.375 +k=0.99998 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine CS2000 East
Proj4js.defs["EPSG:3075"]="+proj=tmerc +lat_0=43.83333333333334 +lon_0=-67.875 +k=0.99998 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine CS2000 Central (deprecated)
Proj4js.defs["EPSG:3076"]="+proj=tmerc +lat_0=43 +lon_0=-69.125 +k=0.99998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine CS2000 West
Proj4js.defs["EPSG:3077"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.375 +k=0.99998 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Michigan Oblique Mercator
Proj4js.defs["EPSG:3078"]="+proj=omerc +lat_0=45.30916666666666 +lonc=-86 +alpha=337.25556 +k=0.9996 +x_0=2546731.496 +y_0=-4354009.816 +gamma=337.25556 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Michigan Oblique Mercator
Proj4js.defs["EPSG:3079"]="+proj=omerc +lat_0=45.30916666666666 +lonc=-86 +alpha=337.25556 +k=0.9996 +x_0=2546731.496 +y_0=-4354009.816 +gamma=337.25556 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Shackleford
Proj4js.defs["EPSG:3080"]="+proj=lcc +lat_1=27.41666666666667 +lat_2=34.91666666666666 +lat_0=31.16666666666667 +lon_0=-100 +x_0=914400 +y_0=914400 +datum=NAD27 +units=ft +no_defs  ";
// NAD83 / Texas State Mapping System
Proj4js.defs["EPSG:3081"]="+proj=lcc +lat_1=27.41666666666667 +lat_2=34.91666666666666 +lat_0=31.16666666666667 +lon_0=-100 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas Centric Lambert Conformal
Proj4js.defs["EPSG:3082"]="+proj=lcc +lat_1=27.5 +lat_2=35 +lat_0=18 +lon_0=-100 +x_0=1500000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas Centric Albers Equal Area
Proj4js.defs["EPSG:3083"]="+proj=aea +lat_1=27.5 +lat_2=35 +lat_0=18 +lon_0=-100 +x_0=1500000 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas Centric Lambert Conformal
Proj4js.defs["EPSG:3084"]="+proj=lcc +lat_1=27.5 +lat_2=35 +lat_0=18 +lon_0=-100 +x_0=1500000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Texas Centric Albers Equal Area
Proj4js.defs["EPSG:3085"]="+proj=aea +lat_1=27.5 +lat_2=35 +lat_0=18 +lon_0=-100 +x_0=1500000 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Florida GDL Albers
Proj4js.defs["EPSG:3086"]="+proj=aea +lat_1=24 +lat_2=31.5 +lat_0=24 +lon_0=-84 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Florida GDL Albers
Proj4js.defs["EPSG:3087"]="+proj=aea +lat_1=24 +lat_2=31.5 +lat_0=24 +lon_0=-84 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Kentucky Single Zone
Proj4js.defs["EPSG:3088"]="+proj=lcc +lat_1=37.08333333333334 +lat_2=38.66666666666666 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=1500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Kentucky Single Zone (ftUS)
Proj4js.defs["EPSG:3089"]="+proj=lcc +lat_1=37.08333333333334 +lat_2=38.66666666666666 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=1500000 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Kentucky Single Zone
Proj4js.defs["EPSG:3090"]="+proj=lcc +lat_1=37.08333333333334 +lat_2=38.66666666666666 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=1500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Kentucky Single Zone (ftUS)
Proj4js.defs["EPSG:3091"]="+proj=lcc +lat_1=37.08333333333334 +lat_2=38.66666666666666 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=1500000 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Tokyo / UTM zone 51N
Proj4js.defs["EPSG:3092"]="+proj=utm +zone=51 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / UTM zone 52N
Proj4js.defs["EPSG:3093"]="+proj=utm +zone=52 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / UTM zone 53N
Proj4js.defs["EPSG:3094"]="+proj=utm +zone=53 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / UTM zone 54N
Proj4js.defs["EPSG:3095"]="+proj=utm +zone=54 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / UTM zone 55N
Proj4js.defs["EPSG:3096"]="+proj=utm +zone=55 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / UTM zone 51N
Proj4js.defs["EPSG:3097"]="+proj=utm +zone=51 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / UTM zone 52N
Proj4js.defs["EPSG:3098"]="+proj=utm +zone=52 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / UTM zone 53N
Proj4js.defs["EPSG:3099"]="+proj=utm +zone=53 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / UTM zone 54N
Proj4js.defs["EPSG:3100"]="+proj=utm +zone=54 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JGD2000 / UTM zone 55N
Proj4js.defs["EPSG:3101"]="+proj=utm +zone=55 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// American Samoa 1962 / American Samoa Lambert
Proj4js.defs["EPSG:3102"]="+proj=lcc +lat_1=-14.26666666666667 +lat_0=-14.26666666666667 +lon_0=-170 +k_0=1 +x_0=152400.3048006096 +y_0=95169.31165862332 +ellps=clrk66 +towgs84=-115,118,426,0,0,0,0 +units=us-ft +no_defs  ";
// Mauritania 1999 / UTM zone 28N (deprecated)
Proj4js.defs["EPSG:3103"]="+proj=utm +zone=28 +ellps=clrk80 +units=m +no_defs  ";
// Mauritania 1999 / UTM zone 29N (deprecated)
Proj4js.defs["EPSG:3104"]="+proj=utm +zone=29 +ellps=clrk80 +units=m +no_defs  ";
// Mauritania 1999 / UTM zone 30N (deprecated)
Proj4js.defs["EPSG:3105"]="+proj=utm +zone=30 +ellps=clrk80 +units=m +no_defs  ";
// Gulshan 303 / Bangladesh Transverse Mercator
Proj4js.defs["EPSG:3106"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=0.9996 +x_0=500000 +y_0=0 +a=6377276.345 +b=6356075.41314024 +towgs84=283.7,735.9,261.1,0,0,0,0 +units=m +no_defs  ";
// GDA94 / SA Lambert
Proj4js.defs["EPSG:3107"]="+proj=lcc +lat_1=-28 +lat_2=-36 +lat_0=-32 +lon_0=135 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Guernsey Grid
Proj4js.defs["EPSG:3108"]="+proj=tmerc +lat_0=49.5 +lon_0=-2.416666666666667 +k=0.999997 +x_0=47000 +y_0=50000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Jersey Transverse Mercator
Proj4js.defs["EPSG:3109"]="+proj=tmerc +lat_0=49.225 +lon_0=-2.135 +k=0.9999999000000001 +x_0=40000 +y_0=70000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// AGD66 / Vicgrid66
Proj4js.defs["EPSG:3110"]="+proj=lcc +lat_1=-36 +lat_2=-38 +lat_0=-37 +lon_0=145 +x_0=2500000 +y_0=4500000 +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// GDA94 / Vicgrid94
Proj4js.defs["EPSG:3111"]="+proj=lcc +lat_1=-36 +lat_2=-38 +lat_0=-37 +lon_0=145 +x_0=2500000 +y_0=2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / Geoscience Australia Lambert
Proj4js.defs["EPSG:3112"]="+proj=lcc +lat_1=-18 +lat_2=-36 +lat_0=0 +lon_0=134 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / BCSG02
Proj4js.defs["EPSG:3113"]="+proj=tmerc +lat_0=-28 +lon_0=153 +k=0.99999 +x_0=50000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MAGNA-SIRGAS / Colombia Far West zone
Proj4js.defs["EPSG:3114"]="+proj=tmerc +lat_0=4.596200416666666 +lon_0=-80.07750791666666 +k=1 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MAGNA-SIRGAS / Colombia West zone
Proj4js.defs["EPSG:3115"]="+proj=tmerc +lat_0=4.596200416666666 +lon_0=-77.07750791666666 +k=1 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MAGNA-SIRGAS / Colombia Bogota zone
Proj4js.defs["EPSG:3116"]="+proj=tmerc +lat_0=4.596200416666666 +lon_0=-74.07750791666666 +k=1 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MAGNA-SIRGAS / Colombia East Central zone
Proj4js.defs["EPSG:3117"]="+proj=tmerc +lat_0=4.596200416666666 +lon_0=-71.07750791666666 +k=1 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MAGNA-SIRGAS / Colombia East zone
Proj4js.defs["EPSG:3118"]="+proj=tmerc +lat_0=4.596200416666666 +lon_0=-68.07750791666666 +k=1 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Douala 1948 / AEF west
Proj4js.defs["EPSG:3119"]="+proj=tmerc +lat_0=0 +lon_0=10.5 +k=0.999 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=-206.1,-174.7,-87.7,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(58) / Poland zone I
Proj4js.defs["EPSG:3120"]="+proj=sterea +lat_0=50.625 +lon_0=21.08333333333333 +k=0.9998 +x_0=4637000 +y_0=5467000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// PRS92 / Philippines zone 1
Proj4js.defs["EPSG:3121"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-127.62,-67.24,-47.04,-3.068,4.903,1.578,-1.06 +units=m +no_defs  ";
// PRS92 / Philippines zone 2
Proj4js.defs["EPSG:3122"]="+proj=tmerc +lat_0=0 +lon_0=119 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-127.62,-67.24,-47.04,-3.068,4.903,1.578,-1.06 +units=m +no_defs  ";
// PRS92 / Philippines zone 3
Proj4js.defs["EPSG:3123"]="+proj=tmerc +lat_0=0 +lon_0=121 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-127.62,-67.24,-47.04,-3.068,4.903,1.578,-1.06 +units=m +no_defs  ";
// PRS92 / Philippines zone 4
Proj4js.defs["EPSG:3124"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-127.62,-67.24,-47.04,-3.068,4.903,1.578,-1.06 +units=m +no_defs  ";
// PRS92 / Philippines zone 5
Proj4js.defs["EPSG:3125"]="+proj=tmerc +lat_0=0 +lon_0=125 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-127.62,-67.24,-47.04,-3.068,4.903,1.578,-1.06 +units=m +no_defs  ";
// ETRS89 / ETRS-GK19FIN
Proj4js.defs["EPSG:3126"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK20FIN
Proj4js.defs["EPSG:3127"]="+proj=tmerc +lat_0=0 +lon_0=20 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK21FIN
Proj4js.defs["EPSG:3128"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK22FIN
Proj4js.defs["EPSG:3129"]="+proj=tmerc +lat_0=0 +lon_0=22 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK23FIN
Proj4js.defs["EPSG:3130"]="+proj=tmerc +lat_0=0 +lon_0=23 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK24FIN
Proj4js.defs["EPSG:3131"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK25FIN
Proj4js.defs["EPSG:3132"]="+proj=tmerc +lat_0=0 +lon_0=25 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK26FIN
Proj4js.defs["EPSG:3133"]="+proj=tmerc +lat_0=0 +lon_0=26 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK27FIN
Proj4js.defs["EPSG:3134"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK28FIN
Proj4js.defs["EPSG:3135"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK29FIN
Proj4js.defs["EPSG:3136"]="+proj=tmerc +lat_0=0 +lon_0=29 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK30FIN
Proj4js.defs["EPSG:3137"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / ETRS-GK31FIN
Proj4js.defs["EPSG:3138"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Vanua Levu 1915 / Vanua Levu Grid
// Unable to translate coordinate system EPSG:3139 into PROJ.4 format.
//
// Viti Levu 1912 / Viti Levu Grid
Proj4js.defs["EPSG:3140"]="+proj=cass +lat_0=-18 +lon_0=178 +x_0=109435.392 +y_0=141622.272 +a=6378306.3696 +b=6356571.996 +towgs84=51,391,-36,0,0,0,0 +to_meter=0.201168 +no_defs  ";
// Fiji 1956 / UTM zone 60S
Proj4js.defs["EPSG:3141"]="+proj=utm +zone=60 +south +ellps=intl +towgs84=265.025,384.929,-194.046,0,0,0,0 +units=m +no_defs  ";
// Fiji 1956 / UTM zone 1S
Proj4js.defs["EPSG:3142"]="+proj=utm +zone=1 +south +ellps=intl +towgs84=265.025,384.929,-194.046,0,0,0,0 +units=m +no_defs  ";
// Fiji 1986 / Fiji Map Grid (deprecated)
Proj4js.defs["EPSG:3143"]="+proj=tmerc +lat_0=-17 +lon_0=178.75 +k=0.99985 +x_0=2000000 +y_0=4000000 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// FD54 / Faroe Lambert
// Unable to translate coordinate system EPSG:3144 into PROJ.4 format.
//
// ETRS89 / Faroe Lambert
// Unable to translate coordinate system EPSG:3145 into PROJ.4 format.
//
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 6
Proj4js.defs["EPSG:3146"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger CM 18E
Proj4js.defs["EPSG:3147"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Indian 1960 / UTM zone 48N
Proj4js.defs["EPSG:3148"]="+proj=utm +zone=48 +a=6377276.345 +b=6356075.41314024 +towgs84=198,881,317,0,0,0,0 +units=m +no_defs  ";
// Indian 1960 / UTM zone 49N
Proj4js.defs["EPSG:3149"]="+proj=utm +zone=49 +a=6377276.345 +b=6356075.41314024 +towgs84=198,881,317,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 6
Proj4js.defs["EPSG:3150"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger CM 18E
Proj4js.defs["EPSG:3151"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// ST74
Proj4js.defs["EPSG:3152"]="+proj=tmerc +lat_0=0 +lon_0=18.05779 +k=0.99999425 +x_0=100178.1808 +y_0=-6500614.7836 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / BC Albers
Proj4js.defs["EPSG:3153"]="+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 7N
Proj4js.defs["EPSG:3154"]="+proj=utm +zone=7 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 8N
Proj4js.defs["EPSG:3155"]="+proj=utm +zone=8 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 9N
Proj4js.defs["EPSG:3156"]="+proj=utm +zone=9 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 10N
Proj4js.defs["EPSG:3157"]="+proj=utm +zone=10 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 14N
Proj4js.defs["EPSG:3158"]="+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 15N
Proj4js.defs["EPSG:3159"]="+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / UTM zone 16N
Proj4js.defs["EPSG:3160"]="+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Ontario MNR Lambert
Proj4js.defs["EPSG:3161"]="+proj=lcc +lat_1=44.5 +lat_2=53.5 +lat_0=0 +lon_0=-85 +x_0=930000 +y_0=6430000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Ontario MNR Lambert
Proj4js.defs["EPSG:3162"]="+proj=lcc +lat_1=44.5 +lat_2=53.5 +lat_0=0 +lon_0=-85 +x_0=930000 +y_0=6430000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGNC91-93 / Lambert New Caledonia
Proj4js.defs["EPSG:3163"]="+proj=lcc +lat_1=-20.66666666666667 +lat_2=-22.33333333333333 +lat_0=-21.5 +lon_0=166 +x_0=400000 +y_0=300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ST87 Ouvea / UTM zone 58S
Proj4js.defs["EPSG:3164"]="+proj=utm +zone=58 +south +ellps=WGS84 +towgs84=-56.263,16.136,-22.856,0,0,0,0 +units=m +no_defs  ";
// NEA74 Noumea / Noumea Lambert
Proj4js.defs["EPSG:3165"]="+proj=lcc +lat_1=-22.24469175 +lat_2=-22.29469175 +lat_0=-22.26969175 +lon_0=166.44242575 +x_0=0.66 +y_0=1.02 +ellps=intl +towgs84=-10.18,-350.43,291.37,0,0,0,0 +units=m +no_defs  ";
// NEA74 Noumea / Noumea Lambert 2
Proj4js.defs["EPSG:3166"]="+proj=lcc +lat_1=-22.24472222222222 +lat_2=-22.29472222222222 +lat_0=-22.26972222222222 +lon_0=166.4425 +x_0=8.313000000000001 +y_0=-2.354 +ellps=intl +towgs84=-10.18,-350.43,291.37,0,0,0,0 +units=m +no_defs  ";
// Kertau (RSO) / RSO Malaya (ch)
Proj4js.defs["EPSG:3167"]="+proj=omerc +lat_0=4 +lonc=102.25 +alpha=323.0257905 +k=0.99984 +x_0=40000 +y_0=0 +gamma=323.1301023611111 +a=6377295.664 +b=6356094.667915204 +to_meter=20.116756 +no_defs  ";
// Kertau (RSO) / RSO Malaya (m)
Proj4js.defs["EPSG:3168"]="+proj=omerc +lat_0=4 +lonc=102.25 +alpha=323.0257905 +k=0.99984 +x_0=804670.24 +y_0=0 +gamma=323.1301023611111 +a=6377295.664 +b=6356094.667915204 +units=m +no_defs  ";
// RGNC91-93 / UTM zone 57S
Proj4js.defs["EPSG:3169"]="+proj=utm +zone=57 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGNC91-93 / UTM zone 58S
Proj4js.defs["EPSG:3170"]="+proj=utm +zone=58 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGNC91-93 / UTM zone 59S
Proj4js.defs["EPSG:3171"]="+proj=utm +zone=59 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGN53 Mare / UTM zone 59S
Proj4js.defs["EPSG:3172"]="+proj=utm +zone=59 +south +ellps=intl +towgs84=287.58,177.78,-135.41,0,0,0,0 +units=m +no_defs  ";
// fk89 / Faroe Lambert FK89
// Unable to translate coordinate system EPSG:3173 into PROJ.4 format.
//
// NAD83 / Great Lakes Albers
Proj4js.defs["EPSG:3174"]="+proj=aea +lat_1=42.122774 +lat_2=49.01518 +lat_0=45.568977 +lon_0=-84.455955 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Great Lakes and St Lawrence Albers
Proj4js.defs["EPSG:3175"]="+proj=aea +lat_1=42.122774 +lat_2=49.01518 +lat_0=45.568977 +lon_0=-83.248627 +x_0=1000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Indian 1960 / TM 106 NE
Proj4js.defs["EPSG:3176"]="+proj=tmerc +lat_0=0 +lon_0=106 +k=0.9996 +x_0=500000 +y_0=0 +a=6377276.345 +b=6356075.41314024 +towgs84=198,881,317,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM
Proj4js.defs["EPSG:3177"]="+proj=tmerc +lat_0=0 +lon_0=17 +k=0.9965000000000001 +x_0=1000000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 18N
Proj4js.defs["EPSG:3178"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 19N
Proj4js.defs["EPSG:3179"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 20N
Proj4js.defs["EPSG:3180"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 21N
Proj4js.defs["EPSG:3181"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 22N
Proj4js.defs["EPSG:3182"]="+proj=utm +zone=22 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 23N
Proj4js.defs["EPSG:3183"]="+proj=utm +zone=23 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 24N
Proj4js.defs["EPSG:3184"]="+proj=utm +zone=24 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 25N
Proj4js.defs["EPSG:3185"]="+proj=utm +zone=25 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 26N
Proj4js.defs["EPSG:3186"]="+proj=utm +zone=26 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 27N
Proj4js.defs["EPSG:3187"]="+proj=utm +zone=27 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 28N
Proj4js.defs["EPSG:3188"]="+proj=utm +zone=28 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GR96 / UTM zone 29N
Proj4js.defs["EPSG:3189"]="+proj=utm +zone=29 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 5
Proj4js.defs["EPSG:3190"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 6
Proj4js.defs["EPSG:3191"]="+proj=tmerc +lat_0=0 +lon_0=11 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 7
Proj4js.defs["EPSG:3192"]="+proj=tmerc +lat_0=0 +lon_0=13 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 8
Proj4js.defs["EPSG:3193"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 9
Proj4js.defs["EPSG:3194"]="+proj=tmerc +lat_0=0 +lon_0=17 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 10
Proj4js.defs["EPSG:3195"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 11
Proj4js.defs["EPSG:3196"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 12
Proj4js.defs["EPSG:3197"]="+proj=tmerc +lat_0=0 +lon_0=23 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / Libya TM zone 13
Proj4js.defs["EPSG:3198"]="+proj=tmerc +lat_0=0 +lon_0=25 +k=0.99995 +x_0=200000 +y_0=0 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / UTM zone 32N
Proj4js.defs["EPSG:3199"]="+proj=utm +zone=32 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// FD58 / Iraq zone
Proj4js.defs["EPSG:3200"]="+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=45 +k_0=0.9987864078000001 +x_0=1500000 +y_0=1166200 +ellps=clrk80 +towgs84=-239.1,-170.02,397.5,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / UTM zone 33N
Proj4js.defs["EPSG:3201"]="+proj=utm +zone=33 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / UTM zone 34N
Proj4js.defs["EPSG:3202"]="+proj=utm +zone=34 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// LGD2006 / UTM zone 35N
Proj4js.defs["EPSG:3203"]="+proj=utm +zone=35 +ellps=intl +towgs84=-208.406,-109.878,-2.5764,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SP19-20
Proj4js.defs["EPSG:3204"]="+proj=lcc +lat_1=-60.66666666666666 +lat_2=-63.33333333333334 +lat_0=-90 +lon_0=-66 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SP21-22
Proj4js.defs["EPSG:3205"]="+proj=lcc +lat_1=-60.66666666666666 +lat_2=-63.33333333333334 +lat_0=-90 +lon_0=-54 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SP23-24
Proj4js.defs["EPSG:3206"]="+proj=lcc +lat_1=-60.66666666666666 +lat_2=-63.33333333333334 +lat_0=-90 +lon_0=-42 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ01-02
Proj4js.defs["EPSG:3207"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=-174 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ19-20
Proj4js.defs["EPSG:3208"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=-66 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ21-22
Proj4js.defs["EPSG:3209"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=-54 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ37-38
Proj4js.defs["EPSG:3210"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=42 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ39-40
Proj4js.defs["EPSG:3211"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=54 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ41-42
Proj4js.defs["EPSG:3212"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=66 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ43-44
Proj4js.defs["EPSG:3213"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=78 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ45-46
Proj4js.defs["EPSG:3214"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=90 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ47-48
Proj4js.defs["EPSG:3215"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=102 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ49-50
Proj4js.defs["EPSG:3216"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=114 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ51-52
Proj4js.defs["EPSG:3217"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=126 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ53-54
Proj4js.defs["EPSG:3218"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=138 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ55-56
Proj4js.defs["EPSG:3219"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=150 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SQ57-58
Proj4js.defs["EPSG:3220"]="+proj=lcc +lat_1=-64.66666666666667 +lat_2=-67.33333333333333 +lat_0=-90 +lon_0=162 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR13-14
Proj4js.defs["EPSG:3221"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=-102 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR15-16
Proj4js.defs["EPSG:3222"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=-90 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR17-18
Proj4js.defs["EPSG:3223"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=-78 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR19-20
Proj4js.defs["EPSG:3224"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=-66 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR27-28
Proj4js.defs["EPSG:3225"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=-18 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR29-30
Proj4js.defs["EPSG:3226"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=-6 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR31-32
Proj4js.defs["EPSG:3227"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=6 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR33-34
Proj4js.defs["EPSG:3228"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=18 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR35-36
Proj4js.defs["EPSG:3229"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=30 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR37-38
Proj4js.defs["EPSG:3230"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=42 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR39-40
Proj4js.defs["EPSG:3231"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=54 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR41-42
Proj4js.defs["EPSG:3232"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=66 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR43-44
Proj4js.defs["EPSG:3233"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=78 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR45-46
Proj4js.defs["EPSG:3234"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=90 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR47-48
Proj4js.defs["EPSG:3235"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=102 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR49-50
Proj4js.defs["EPSG:3236"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=114 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR51-52
Proj4js.defs["EPSG:3237"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=126 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR53-54
Proj4js.defs["EPSG:3238"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=138 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR55-56
Proj4js.defs["EPSG:3239"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=150 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR57-58
Proj4js.defs["EPSG:3240"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=162 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SR59-60
Proj4js.defs["EPSG:3241"]="+proj=lcc +lat_1=-68.66666666666667 +lat_2=-71.33333333333333 +lat_0=-90 +lon_0=174 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS04-06
Proj4js.defs["EPSG:3242"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-153 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS07-09
Proj4js.defs["EPSG:3243"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-135 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS10-12
Proj4js.defs["EPSG:3244"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-117 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS13-15
Proj4js.defs["EPSG:3245"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-99 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS16-18
Proj4js.defs["EPSG:3246"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-81 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS19-21
Proj4js.defs["EPSG:3247"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-63 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS25-27
Proj4js.defs["EPSG:3248"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-27 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS28-30
Proj4js.defs["EPSG:3249"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=-9 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS31-33
Proj4js.defs["EPSG:3250"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=9 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS34-36
Proj4js.defs["EPSG:3251"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=27 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS37-39
Proj4js.defs["EPSG:3252"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=45 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS40-42
Proj4js.defs["EPSG:3253"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=63 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS43-45
Proj4js.defs["EPSG:3254"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=81 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS46-48
Proj4js.defs["EPSG:3255"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=99 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS49-51
Proj4js.defs["EPSG:3256"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=117 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS52-54
Proj4js.defs["EPSG:3257"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=135 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS55-57
Proj4js.defs["EPSG:3258"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=153 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SS58-60
Proj4js.defs["EPSG:3259"]="+proj=lcc +lat_1=-72.66666666666667 +lat_2=-75.33333333333333 +lat_0=-90 +lon_0=171 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST01-04
Proj4js.defs["EPSG:3260"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-168 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST05-08
Proj4js.defs["EPSG:3261"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-144 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST09-12
Proj4js.defs["EPSG:3262"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-120 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST13-16
Proj4js.defs["EPSG:3263"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-96 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST17-20
Proj4js.defs["EPSG:3264"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-72 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST21-24
Proj4js.defs["EPSG:3265"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-48 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST25-28
Proj4js.defs["EPSG:3266"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=-24 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST29-32
Proj4js.defs["EPSG:3267"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST33-36
Proj4js.defs["EPSG:3268"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=24 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST37-40
Proj4js.defs["EPSG:3269"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=48 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST41-44
Proj4js.defs["EPSG:3270"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=72 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST45-48
Proj4js.defs["EPSG:3271"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=96 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST49-52
Proj4js.defs["EPSG:3272"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=120 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST53-56
Proj4js.defs["EPSG:3273"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=144 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW ST57-60
Proj4js.defs["EPSG:3274"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=168 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU01-05
Proj4js.defs["EPSG:3275"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-165 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU06-10
Proj4js.defs["EPSG:3276"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-135 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU11-15
Proj4js.defs["EPSG:3277"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-105 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU16-20
Proj4js.defs["EPSG:3278"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-75 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU21-25
Proj4js.defs["EPSG:3279"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU26-30
Proj4js.defs["EPSG:3280"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-15 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU31-35
Proj4js.defs["EPSG:3281"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=15 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU36-40
Proj4js.defs["EPSG:3282"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=45 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU41-45
Proj4js.defs["EPSG:3283"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=75 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU46-50
Proj4js.defs["EPSG:3284"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=105 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU51-55
Proj4js.defs["EPSG:3285"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=135 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SU56-60
Proj4js.defs["EPSG:3286"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=165 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SV01-10
Proj4js.defs["EPSG:3287"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SV11-20
Proj4js.defs["EPSG:3288"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-90 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SV21-30
Proj4js.defs["EPSG:3289"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=-30 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SV31-40
Proj4js.defs["EPSG:3290"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=30 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SV41-50
Proj4js.defs["EPSG:3291"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=90 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SV51-60
Proj4js.defs["EPSG:3292"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / SCAR IMW SW01-60
Proj4js.defs["EPSG:3293"]="+proj=stere +lat_0=-90 +lat_ts=-80.23861111111111 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / USGS Transantarctic Mountains
Proj4js.defs["EPSG:3294"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-78 +lon_0=162 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// Guam 1963 / Yap Islands
// Unable to translate coordinate system EPSG:3295 into PROJ.4 format.
//
// RGPF / UTM zone 5S
Proj4js.defs["EPSG:3296"]="+proj=utm +zone=5 +south +ellps=GRS80 +towgs84=0.072,-0.507,-0.245,-0.0183,0.0003,-0.007,-0.0093 +units=m +no_defs  ";
// RGPF / UTM zone 6S
Proj4js.defs["EPSG:3297"]="+proj=utm +zone=6 +south +ellps=GRS80 +towgs84=0.072,-0.507,-0.245,-0.0183,0.0003,-0.007,-0.0093 +units=m +no_defs  ";
// RGPF / UTM zone 7S
Proj4js.defs["EPSG:3298"]="+proj=utm +zone=7 +south +ellps=GRS80 +towgs84=0.072,-0.507,-0.245,-0.0183,0.0003,-0.007,-0.0093 +units=m +no_defs  ";
// RGPF / UTM zone 8S
Proj4js.defs["EPSG:3299"]="+proj=utm +zone=8 +south +ellps=GRS80 +towgs84=0.072,-0.507,-0.245,-0.0183,0.0003,-0.007,-0.0093 +units=m +no_defs  ";
// Estonian Coordinate System of 1992
Proj4js.defs["EPSG:3300"]="+proj=lcc +lat_1=59.33333333333334 +lat_2=58 +lat_0=57.51755393055556 +lon_0=24 +x_0=500000 +y_0=6375000 +ellps=GRS80 +towgs84=0.055,-0.541,-0.185,0.0183,-0.0003,-0.007,-0.014 +units=m +no_defs  ";
// Estonian Coordinate System of 1997
Proj4js.defs["EPSG:3301"]="+proj=lcc +lat_1=59.33333333333334 +lat_2=58 +lat_0=57.51755393055556 +lon_0=24 +x_0=500000 +y_0=6375000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGN63 Hiva Oa / UTM zone 7S
Proj4js.defs["EPSG:3302"]="+proj=utm +zone=7 +south +ellps=intl +towgs84=410.721,55.049,80.746,2.5779,2.3514,0.6664,17.3311 +units=m +no_defs  ";
// Fatu Iva 72 / UTM zone 7S
Proj4js.defs["EPSG:3303"]="+proj=utm +zone=7 +south +ellps=intl +towgs84=347.103,1078.12,2623.92,-33.8875,70.6773,-9.3943,186.074 +units=m +no_defs  ";
// Tahiti 79 / UTM zone 6S
Proj4js.defs["EPSG:3304"]="+proj=utm +zone=6 +south +ellps=intl +towgs84=221.525,152.948,176.768,-2.3847,-1.3896,-0.877,11.4741 +units=m +no_defs  ";
// Moorea 87 / UTM zone 6S
Proj4js.defs["EPSG:3305"]="+proj=utm +zone=6 +south +ellps=intl +towgs84=215.525,149.593,176.229,-3.2624,-1.692,-1.1571,10.4773 +units=m +no_defs  ";
// Maupiti 83 / UTM zone 5S
Proj4js.defs["EPSG:3306"]="+proj=utm +zone=5 +south +ellps=intl +towgs84=217.037,86.959,23.956,0,0,0,0 +units=m +no_defs  ";
// Nakhl-e Ghanem / UTM zone 39N
Proj4js.defs["EPSG:3307"]="+proj=utm +zone=39 +ellps=WGS84 +towgs84=0,-0.15,0.68,0,0,0,0 +units=m +no_defs  ";
// GDA94 / NSW Lambert
Proj4js.defs["EPSG:3308"]="+proj=lcc +lat_1=-30.75 +lat_2=-35.75 +lat_0=-33.25 +lon_0=147 +x_0=9300000 +y_0=4500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / California Albers
Proj4js.defs["EPSG:3309"]="+proj=aea +lat_1=34 +lat_2=40.5 +lat_0=0 +lon_0=-120 +x_0=0 +y_0=-4000000 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / California Albers
Proj4js.defs["EPSG:3310"]="+proj=aea +lat_1=34 +lat_2=40.5 +lat_0=0 +lon_0=-120 +x_0=0 +y_0=-4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / California Albers
Proj4js.defs["EPSG:3311"]="+proj=aea +lat_1=34 +lat_2=40.5 +lat_0=0 +lon_0=-120 +x_0=0 +y_0=-4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// CSG67 / UTM zone 21N
Proj4js.defs["EPSG:3312"]="+proj=utm +zone=21 +ellps=intl +towgs84=-186,230,110,0,0,0,0 +units=m +no_defs  ";
// RGFG95 / UTM zone 21N
Proj4js.defs["EPSG:3313"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Lambert (deprecated)
Proj4js.defs["EPSG:3314"]="+proj=lcc +lat_1=-6.5 +lat_2=-11.5 +lat_0=0 +lon_0=26 +x_0=0 +y_0=0 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga TM (deprecated)
Proj4js.defs["EPSG:3315"]="+proj=tmerc +lat_0=-9 +lon_0=26 +k=0.9998 +x_0=0 +y_0=0 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Kasai 1953 / Congo TM zone 22
Proj4js.defs["EPSG:3316"]="+proj=tmerc +lat_0=0 +lon_0=22 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// Kasai 1953 / Congo TM zone 24
Proj4js.defs["EPSG:3317"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 12
Proj4js.defs["EPSG:3318"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 14
Proj4js.defs["EPSG:3319"]="+proj=tmerc +lat_0=0 +lon_0=14 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 16
Proj4js.defs["EPSG:3320"]="+proj=tmerc +lat_0=0 +lon_0=16 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 18
Proj4js.defs["EPSG:3321"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 20
Proj4js.defs["EPSG:3322"]="+proj=tmerc +lat_0=0 +lon_0=20 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 22
Proj4js.defs["EPSG:3323"]="+proj=tmerc +lat_0=0 +lon_0=22 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 24
Proj4js.defs["EPSG:3324"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 26
Proj4js.defs["EPSG:3325"]="+proj=tmerc +lat_0=0 +lon_0=26 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 28
Proj4js.defs["EPSG:3326"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// IGC 1962 / Congo TM zone 30
Proj4js.defs["EPSG:3327"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +units=m +no_defs  ";
// Pulkovo 1942(58) / GUGiK-80
Proj4js.defs["EPSG:3328"]="+proj=sterea +lat_0=52.16666666666666 +lon_0=19.16666666666667 +k=0.999714 +x_0=500000 +y_0=500000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 5
Proj4js.defs["EPSG:3329"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 6
Proj4js.defs["EPSG:3330"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 7
Proj4js.defs["EPSG:3331"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 8
Proj4js.defs["EPSG:3332"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Gauss-Kruger zone 3
Proj4js.defs["EPSG:3333"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=3500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Gauss-Kruger zone 4
Proj4js.defs["EPSG:3334"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Gauss-Kruger zone 5
Proj4js.defs["EPSG:3335"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=5500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// IGN 1962 Kerguelen / UTM zone 42S
Proj4js.defs["EPSG:3336"]="+proj=utm +zone=42 +south +ellps=intl +towgs84=145,-187,103,0,0,0,0 +units=m +no_defs  ";
// Le Pouce 1934 / Mauritius Grid
Proj4js.defs["EPSG:3337"]="+proj=lcc +lat_1=-20.19506944444445 +lat_0=-20.19506944444445 +lon_0=57.52182777777778 +k_0=1 +x_0=1000000 +y_0=1000000 +ellps=clrk80 +towgs84=-770.1,158.4,-498.2,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska Albers
Proj4js.defs["EPSG:3338"]="+proj=aea +lat_1=55 +lat_2=65 +lat_0=50 +lon_0=-154 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGCB 1955 / Congo TM zone 12
Proj4js.defs["EPSG:3339"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +towgs84=-79.9,-158,-168.9,0,0,0,0 +units=m +no_defs  ";
// IGCB 1955 / Congo TM zone 14
Proj4js.defs["EPSG:3340"]="+proj=tmerc +lat_0=0 +lon_0=14 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +towgs84=-79.9,-158,-168.9,0,0,0,0 +units=m +no_defs  ";
// IGCB 1955 / Congo TM zone 16
Proj4js.defs["EPSG:3341"]="+proj=tmerc +lat_0=0 +lon_0=16 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=clrk80 +towgs84=-79.9,-158,-168.9,0,0,0,0 +units=m +no_defs  ";
// IGCB 1955 / UTM zone 33S
Proj4js.defs["EPSG:3342"]="+proj=utm +zone=33 +south +ellps=clrk80 +towgs84=-79.9,-158,-168.9,0,0,0,0 +units=m +no_defs  ";
// Mauritania 1999 / UTM zone 28N
Proj4js.defs["EPSG:3343"]="+proj=utm +zone=28 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mauritania 1999 / UTM zone 29N
Proj4js.defs["EPSG:3344"]="+proj=utm +zone=29 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mauritania 1999 / UTM zone 30N
Proj4js.defs["EPSG:3345"]="+proj=utm +zone=30 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// LKS94 / Lithuania TM
Proj4js.defs["EPSG:3346"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Statistics Canada Lambert
Proj4js.defs["EPSG:3347"]="+proj=lcc +lat_1=49 +lat_2=77 +lat_0=63.390675 +lon_0=-91.86666666666666 +x_0=6200000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Statistics Canada Lambert
Proj4js.defs["EPSG:3348"]="+proj=lcc +lat_1=49 +lat_2=77 +lat_0=63.390675 +lon_0=-91.86666666666666 +x_0=6200000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / PDC Mercator (deprecated)
Proj4js.defs["EPSG:3349"]="+proj=merc +lon_0=-150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone C0
Proj4js.defs["EPSG:3350"]="+proj=tmerc +lat_0=0.1 +lon_0=21.95 +k=1 +x_0=250000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone C1
Proj4js.defs["EPSG:3351"]="+proj=tmerc +lat_0=0.1 +lon_0=24.95 +k=1 +x_0=1250000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / CS63 zone C2
Proj4js.defs["EPSG:3352"]="+proj=tmerc +lat_0=0.1 +lon_0=27.95 +k=1 +x_0=2250000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Mhast (onshore) / UTM zone 32S
Proj4js.defs["EPSG:3353"]="+proj=utm +zone=32 +south +ellps=intl +units=m +no_defs  ";
// Mhast (offshore) / UTM zone 32S
Proj4js.defs["EPSG:3354"]="+proj=utm +zone=32 +south +ellps=intl +units=m +no_defs  ";
// Egypt Gulf of Suez S-650 TL / Red Belt
Proj4js.defs["EPSG:3355"]="+proj=tmerc +lat_0=30 +lon_0=31 +k=1 +x_0=615000 +y_0=810000 +ellps=helmert +towgs84=-146.21,112.63,4.05,0,0,0,0 +units=m +no_defs  ";
// Grand Cayman 1959 / UTM zone 17N
Proj4js.defs["EPSG:3356"]="+proj=utm +zone=17 +ellps=clrk66 +towgs84=67.8,106.1,138.8,0,0,0,0 +units=m +no_defs  ";
// Little Cayman 1961 / UTM zone 17N
Proj4js.defs["EPSG:3357"]="+proj=utm +zone=17 +ellps=clrk66 +towgs84=42,124,147,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / North Carolina
Proj4js.defs["EPSG:3358"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.22 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / North Carolina (ftUS) (deprecated)
Proj4js.defs["EPSG:3359"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.2192024385 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / South Carolina
Proj4js.defs["EPSG:3360"]="+proj=lcc +lat_1=34.83333333333334 +lat_2=32.5 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609600 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / South Carolina (ft)
Proj4js.defs["EPSG:3361"]="+proj=lcc +lat_1=34.83333333333334 +lat_2=32.5 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609600 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(HARN) / Pennsylvania North
Proj4js.defs["EPSG:3362"]="+proj=lcc +lat_1=41.95 +lat_2=40.88333333333333 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Pennsylvania North (ftUS)
Proj4js.defs["EPSG:3363"]="+proj=lcc +lat_1=41.95 +lat_2=40.88333333333333 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Pennsylvania South
Proj4js.defs["EPSG:3364"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Pennsylvania South (ftUS)
Proj4js.defs["EPSG:3365"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Hong Kong 1963 Grid System (deprecated)
Proj4js.defs["EPSG:3366"]="+proj=cass +lat_0=22.31213333333334 +lon_0=114.1785555555556 +x_0=40243.57775604237 +y_0=19069.93351512578 +a=6378293.645208759 +b=6356617.987679838 +units=m +no_defs  ";
// IGN Astro 1960 / UTM zone 28N
Proj4js.defs["EPSG:3367"]="+proj=utm +zone=28 +ellps=clrk80 +units=m +no_defs  ";
// IGN Astro 1960 / UTM zone 29N
Proj4js.defs["EPSG:3368"]="+proj=utm +zone=29 +ellps=clrk80 +units=m +no_defs  ";
// IGN Astro 1960 / UTM zone 30N
Proj4js.defs["EPSG:3369"]="+proj=utm +zone=30 +ellps=clrk80 +units=m +no_defs  ";
// NAD27 / UTM zone 59N
Proj4js.defs["EPSG:3370"]="+proj=utm +zone=59 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 60N
Proj4js.defs["EPSG:3371"]="+proj=utm +zone=60 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / UTM zone 59N
Proj4js.defs["EPSG:3372"]="+proj=utm +zone=59 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 60N
Proj4js.defs["EPSG:3373"]="+proj=utm +zone=60 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// FD54 / UTM zone 29N
Proj4js.defs["EPSG:3374"]="+proj=utm +zone=29 +ellps=intl +units=m +no_defs  ";
// GDM2000 / Peninsula RSO
Proj4js.defs["EPSG:3375"]="+proj=omerc +lat_0=4 +lonc=102.25 +alpha=323.0257964666666 +k=0.99984 +x_0=804671 +y_0=0 +gamma=323.1301023611111 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / East Malaysia BRSO
Proj4js.defs["EPSG:3376"]="+proj=omerc +lat_0=4 +lonc=115 +alpha=53.31580995 +k=0.99984 +x_0=0 +y_0=0 +gamma=53.13010236111111 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Johor Grid
Proj4js.defs["EPSG:3377"]="+proj=cass +lat_0=2.121679744444445 +lon_0=103.4279362361111 +x_0=-14810.562 +y_0=8758.32 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Sembilan and Melaka Grid
Proj4js.defs["EPSG:3378"]="+proj=cass +lat_0=2.682347636111111 +lon_0=101.9749050416667 +x_0=3673.785 +y_0=-4240.573 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / PahangGrid
Proj4js.defs["EPSG:3379"]="+proj=cass +lat_0=3.769388088888889 +lon_0=102.3682989833333 +x_0=-7368.228 +y_0=6485.858 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Selangor Grid
Proj4js.defs["EPSG:3380"]="+proj=cass +lat_0=3.68464905 +lon_0=101.3891079138889 +x_0=-34836.161 +y_0=56464.049 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Terengganu Grid
Proj4js.defs["EPSG:3381"]="+proj=cass +lat_0=4.9762852 +lon_0=103.070275625 +x_0=19594.245 +y_0=3371.895 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Pinang Grid
Proj4js.defs["EPSG:3382"]="+proj=cass +lat_0=5.421517541666667 +lon_0=100.3443769638889 +x_0=-23.414 +y_0=62.283 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Kedah and Perlis Grid
Proj4js.defs["EPSG:3383"]="+proj=cass +lat_0=5.964672713888889 +lon_0=100.6363711111111 +x_0=0 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Perak Grid
Proj4js.defs["EPSG:3384"]="+proj=cass +lat_0=4.859063022222222 +lon_0=100.8154105861111 +x_0=-1.769 +y_0=133454.779 +ellps=GRS80 +units=m +no_defs  ";
// GDM2000 / Kelantan Grid
Proj4js.defs["EPSG:3385"]="+proj=cass +lat_0=5.972543658333334 +lon_0=102.2952416694444 +x_0=13227.851 +y_0=8739.894 +ellps=GRS80 +units=m +no_defs  ";
// KKJ / Finland zone 0
Proj4js.defs["EPSG:3386"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +units=m +no_defs  ";
// KKJ / Finland zone 5
Proj4js.defs["EPSG:3387"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=5500000 +y_0=0 +ellps=intl +towgs84=-96.062,-82.428,-121.753,4.801,0.345,-1.376,1.496 +units=m +no_defs  ";
// Pulkovo 1942 / Caspian Sea Mercator
Proj4js.defs["EPSG:3388"]="+proj=merc +lon_0=51 +lat_ts=42 +x_0=0 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / 3-degree Gauss-Kruger zone 60
Proj4js.defs["EPSG:3389"]="+proj=tmerc +lat_0=0 +lon_0=180 +k=1 +x_0=60500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1995 / 3-degree Gauss-Kruger zone 60
Proj4js.defs["EPSG:3390"]="+proj=tmerc +lat_0=0 +lon_0=180 +k=1 +x_0=60500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Karbala 1979 / UTM zone 37N
Proj4js.defs["EPSG:3391"]="+proj=utm +zone=37 +ellps=clrk80 +towgs84=70.995,-335.916,262.898,0,0,0,0 +units=m +no_defs  ";
// Karbala 1979 / UTM zone 38N
Proj4js.defs["EPSG:3392"]="+proj=utm +zone=38 +ellps=clrk80 +towgs84=70.995,-335.916,262.898,0,0,0,0 +units=m +no_defs  ";
// Karbala 1979 / UTM zone 39N
Proj4js.defs["EPSG:3393"]="+proj=utm +zone=39 +ellps=clrk80 +towgs84=70.995,-335.916,262.898,0,0,0,0 +units=m +no_defs  ";
// Nahrwan 1934 / Iraq zone
Proj4js.defs["EPSG:3394"]="+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=45 +k_0=0.9987864078000001 +x_0=1500000 +y_0=1166200 +ellps=clrk80 +units=m +no_defs  ";
// WGS 84 / World Mercator
Proj4js.defs["EPSG:3395"]="+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// PD/83 / 3-degree Gauss-Kruger zone 3
Proj4js.defs["EPSG:3396"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// PD/83 / 3-degree Gauss-Kruger zone 4
Proj4js.defs["EPSG:3397"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RD/83 / 3-degree Gauss-Kruger zone 4
Proj4js.defs["EPSG:3398"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// RD/83 / 3-degree Gauss-Kruger zone 5
Proj4js.defs["EPSG:3399"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// NAD83 / Alberta 10-TM (Forest)
Proj4js.defs["EPSG:3400"]="+proj=tmerc +lat_0=0 +lon_0=-115 +k=0.9992 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alberta 10-TM (Resource)
Proj4js.defs["EPSG:3401"]="+proj=tmerc +lat_0=0 +lon_0=-115 +k=0.9992 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 10-TM (Forest)
Proj4js.defs["EPSG:3402"]="+proj=tmerc +lat_0=0 +lon_0=-115 +k=0.9992 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 10-TM (Resource)
Proj4js.defs["EPSG:3403"]="+proj=tmerc +lat_0=0 +lon_0=-115 +k=0.9992 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / North Carolina (ftUS)
Proj4js.defs["EPSG:3404"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.2192024384 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// VN-2000 / UTM zone 48N
Proj4js.defs["EPSG:3405"]="+proj=utm +zone=48 +ellps=WGS84 +towgs84=-192.873,-39.382,-111.202,-0.00205,-0.0005,0.00335,0.0188 +units=m +no_defs  ";
// VN-2000 / UTM zone 49N
Proj4js.defs["EPSG:3406"]="+proj=utm +zone=49 +ellps=WGS84 +towgs84=-192.873,-39.382,-111.202,-0.00205,-0.0005,0.00335,0.0188 +units=m +no_defs  ";
// Hong Kong 1963 Grid System
Proj4js.defs["EPSG:3407"]="+proj=cass +lat_0=22.31213333333334 +lon_0=114.1785555555556 +x_0=40243.57775604237 +y_0=19069.93351512578 +a=6378293.645208759 +b=6356617.987679838 +to_meter=0.3047972654 +no_defs  ";
// NSIDC EASE-Grid North
Proj4js.defs["EPSG:3408"]="+proj=laea +lat_0=90 +lon_0=0 +x_0=0 +y_0=0 +a=6371228 +b=6371228 +units=m +no_defs  ";
// NSIDC EASE-Grid South
Proj4js.defs["EPSG:3409"]="+proj=laea +lat_0=-90 +lon_0=0 +x_0=0 +y_0=0 +a=6371228 +b=6371228 +units=m +no_defs  ";
// NSIDC EASE-Grid Global
Proj4js.defs["EPSG:3410"]="+proj=cea +lon_0=0 +lat_ts=30 +x_0=0 +y_0=0 +a=6371228 +b=6371228 +units=m +no_defs  ";
// NSIDC Sea Ice Polar Stereographic North
Proj4js.defs["EPSG:3411"]="+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +a=6378273 +b=6356889.449 +units=m +no_defs  ";
// NSIDC Sea Ice Polar Stereographic South
Proj4js.defs["EPSG:3412"]="+proj=stere +lat_0=-90 +lat_ts=-70 +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378273 +b=6356889.449 +units=m +no_defs  ";
// WGS 84 / NSIDC Sea Ice Polar Stereographic North
Proj4js.defs["EPSG:3413"]="+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// SVY21 / Singapore TM
Proj4js.defs["EPSG:3414"]="+proj=tmerc +lat_0=1.366666666666667 +lon_0=103.8333333333333 +k=1 +x_0=28001.642 +y_0=38744.572 +ellps=WGS84 +units=m +no_defs  ";
// WGS 72BE / South China Sea Lambert
Proj4js.defs["EPSG:3415"]="+proj=lcc +lat_1=18 +lat_2=24 +lat_0=21 +lon_0=114 +x_0=500000 +y_0=500000 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// ETRS89 / Austria Lambert
Proj4js.defs["EPSG:3416"]="+proj=lcc +lat_1=49 +lat_2=46 +lat_0=47.5 +lon_0=13.33333333333333 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Iowa North (ft US)
Proj4js.defs["EPSG:3417"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=1500000 +y_0=999999.9999898402 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Iowa South (ft US)
Proj4js.defs["EPSG:3418"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Kansas North (ft US)
Proj4js.defs["EPSG:3419"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=399999.99998984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Kansas South (ft US)
Proj4js.defs["EPSG:3420"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=399999.99998984 +y_0=399999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Nevada East (ft US)
Proj4js.defs["EPSG:3421"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=200000.00001016 +y_0=8000000.000010163 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Nevada Central (ft US)
Proj4js.defs["EPSG:3422"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=500000.00001016 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Nevada West (ft US)
Proj4js.defs["EPSG:3423"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=800000.0000101599 +y_0=3999999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New Jersey (ft US)
Proj4js.defs["EPSG:3424"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Iowa North (ft US)
Proj4js.defs["EPSG:3425"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=1500000 +y_0=999999.9999898402 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Iowa South (ft US)
Proj4js.defs["EPSG:3426"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Kansas North (ft US)
Proj4js.defs["EPSG:3427"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=399999.99998984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Kansas South (ft US)
Proj4js.defs["EPSG:3428"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=399999.99998984 +y_0=399999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Nevada East (ft US)
Proj4js.defs["EPSG:3429"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=200000.00001016 +y_0=8000000.000010163 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Nevada Central (ft US)
Proj4js.defs["EPSG:3430"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=500000.00001016 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Nevada West (ft US)
Proj4js.defs["EPSG:3431"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=800000.0000101599 +y_0=3999999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New Jersey (ft US)
Proj4js.defs["EPSG:3432"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Arkansas North (ftUS)
Proj4js.defs["EPSG:3433"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=399999.99998984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Arkansas South (ftUS)
Proj4js.defs["EPSG:3434"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=399999.99998984 +y_0=399999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Illinois East (ftUS)
Proj4js.defs["EPSG:3435"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Illinois West (ftUS)
Proj4js.defs["EPSG:3436"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=699999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / New Hampshire (ftUS)
Proj4js.defs["EPSG:3437"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Rhode Island (ftUS)
Proj4js.defs["EPSG:3438"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.99999375 +x_0=99999.99998983997 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// PSD93 / UTM zone 39N
Proj4js.defs["EPSG:3439"]="+proj=utm +zone=39 +ellps=clrk80 +towgs84=-180.624,-225.516,173.919,-0.81,-1.898,8.336,16.7101 +units=m +no_defs  ";
// PSD93 / UTM zone 40N
Proj4js.defs["EPSG:3440"]="+proj=utm +zone=40 +ellps=clrk80 +towgs84=-180.624,-225.516,173.919,-0.81,-1.898,8.336,16.7101 +units=m +no_defs  ";
// NAD83(HARN) / Arkansas North (ftUS)
Proj4js.defs["EPSG:3441"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=399999.99998984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Arkansas South (ftUS)
Proj4js.defs["EPSG:3442"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=399999.99998984 +y_0=399999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Illinois East (ftUS)
Proj4js.defs["EPSG:3443"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Illinois West (ftUS)
Proj4js.defs["EPSG:3444"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=699999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / New Hampshire (ftUS)
Proj4js.defs["EPSG:3445"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Rhode Island (ftUS)
Proj4js.defs["EPSG:3446"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.99999375 +x_0=99999.99998983997 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// ETRS89 / Belgian Lambert 2005
Proj4js.defs["EPSG:3447"]="+proj=lcc +lat_1=49.83333333333334 +lat_2=51.16666666666666 +lat_0=50.797815 +lon_0=4.359215833333333 +x_0=150328 +y_0=166262 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JAD2001 / Jamaica Metric Grid
Proj4js.defs["EPSG:3448"]="+proj=lcc +lat_1=18 +lat_0=18 +lon_0=-77 +k_0=1 +x_0=750000 +y_0=650000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JAD2001 / UTM zone 17N
Proj4js.defs["EPSG:3449"]="+proj=utm +zone=17 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// JAD2001 / UTM zone 18N
Proj4js.defs["EPSG:3450"]="+proj=utm +zone=18 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Louisiana North (ftUS)
Proj4js.defs["EPSG:3451"]="+proj=lcc +lat_1=32.66666666666666 +lat_2=31.16666666666667 +lat_0=30.5 +lon_0=-92.5 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Louisiana South (ftUS)
Proj4js.defs["EPSG:3452"]="+proj=lcc +lat_1=30.7 +lat_2=29.3 +lat_0=28.5 +lon_0=-91.33333333333333 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Louisiana Offshore (ftUS)
Proj4js.defs["EPSG:3453"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.5 +lon_0=-91.33333333333333 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / South Dakota North (ftUS) (deprecated)
Proj4js.defs["EPSG:3454"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / South Dakota South (ftUS)
Proj4js.defs["EPSG:3455"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Louisiana North (ftUS)
Proj4js.defs["EPSG:3456"]="+proj=lcc +lat_1=32.66666666666666 +lat_2=31.16666666666667 +lat_0=30.5 +lon_0=-92.5 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Louisiana South (ftUS)
Proj4js.defs["EPSG:3457"]="+proj=lcc +lat_1=30.7 +lat_2=29.3 +lat_0=28.5 +lon_0=-91.33333333333333 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / South Dakota North (ftUS)
Proj4js.defs["EPSG:3458"]="+proj=lcc +lat_1=45.68333333333333 +lat_2=44.41666666666666 +lat_0=43.83333333333334 +lon_0=-100 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / South Dakota South (ftUS)
Proj4js.defs["EPSG:3459"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Fiji 1986 / Fiji Map Grid
Proj4js.defs["EPSG:3460"]="+proj=tmerc +lat_0=-17 +lon_0=178.75 +k=0.99985 +x_0=2000000 +y_0=4000000 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// Dabola 1981 / UTM zone 28N
Proj4js.defs["EPSG:3461"]="+proj=utm +zone=28 +a=6378249.2 +b=6356515 +towgs84=-83,37,124,0,0,0,0 +units=m +no_defs  ";
// Dabola 1981 / UTM zone 29N
Proj4js.defs["EPSG:3462"]="+proj=utm +zone=29 +a=6378249.2 +b=6356515 +towgs84=-83,37,124,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine CS2000 Central
Proj4js.defs["EPSG:3463"]="+proj=tmerc +lat_0=43.5 +lon_0=-69.125 +k=0.99998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine CS2000 Central
Proj4js.defs["EPSG:3464"]="+proj=tmerc +lat_0=43.5 +lon_0=-69.125 +k=0.99998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alabama East
Proj4js.defs["EPSG:3465"]="+proj=tmerc +lat_0=30.5 +lon_0=-85.83333333333333 +k=0.99996 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alabama West
Proj4js.defs["EPSG:3466"]="+proj=tmerc +lat_0=30 +lon_0=-87.5 +k=0.999933333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska Albers
Proj4js.defs["EPSG:3467"]="+proj=aea +lat_1=55 +lat_2=65 +lat_0=50 +lon_0=-154 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 1
Proj4js.defs["EPSG:3468"]="+proj=omerc +lat_0=57 +lonc=-133.6666666666667 +alpha=323.1301023611111 +k=0.9999 +x_0=5000000 +y_0=-5000000 +gamma=323.1301023611111 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 2
Proj4js.defs["EPSG:3469"]="+proj=tmerc +lat_0=54 +lon_0=-142 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 3
Proj4js.defs["EPSG:3470"]="+proj=tmerc +lat_0=54 +lon_0=-146 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 4
Proj4js.defs["EPSG:3471"]="+proj=tmerc +lat_0=54 +lon_0=-150 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 5
Proj4js.defs["EPSG:3472"]="+proj=tmerc +lat_0=54 +lon_0=-154 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 6
Proj4js.defs["EPSG:3473"]="+proj=tmerc +lat_0=54 +lon_0=-158 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 7
Proj4js.defs["EPSG:3474"]="+proj=tmerc +lat_0=54 +lon_0=-162 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 8
Proj4js.defs["EPSG:3475"]="+proj=tmerc +lat_0=54 +lon_0=-166 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 9
Proj4js.defs["EPSG:3476"]="+proj=tmerc +lat_0=54 +lon_0=-170 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Alaska zone 10
Proj4js.defs["EPSG:3477"]="+proj=lcc +lat_1=53.83333333333334 +lat_2=51.83333333333334 +lat_0=51 +lon_0=-176 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Arizona Central
Proj4js.defs["EPSG:3478"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Arizona Central (ft)
Proj4js.defs["EPSG:3479"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Arizona East
Proj4js.defs["EPSG:3480"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Arizona East (ft)
Proj4js.defs["EPSG:3481"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Arizona West
Proj4js.defs["EPSG:3482"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Arizona West (ft)
Proj4js.defs["EPSG:3483"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Arkansas North
Proj4js.defs["EPSG:3484"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Arkansas North (ftUS)
Proj4js.defs["EPSG:3485"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=399999.99998984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Arkansas South
Proj4js.defs["EPSG:3486"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Arkansas South (ftUS)
Proj4js.defs["EPSG:3487"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=399999.99998984 +y_0=399999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / California Albers
Proj4js.defs["EPSG:3488"]="+proj=aea +lat_1=34 +lat_2=40.5 +lat_0=0 +lon_0=-120 +x_0=0 +y_0=-4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 1
Proj4js.defs["EPSG:3489"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 1 (ftUS)
Proj4js.defs["EPSG:3490"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / California zone 2
Proj4js.defs["EPSG:3491"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 2 (ftUS)
Proj4js.defs["EPSG:3492"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / California zone 3
Proj4js.defs["EPSG:3493"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 3 (ftUS)
Proj4js.defs["EPSG:3494"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / California zone 4
Proj4js.defs["EPSG:3495"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 4 (ftUS)
Proj4js.defs["EPSG:3496"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / California zone 5
Proj4js.defs["EPSG:3497"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 5 (ftUS)
Proj4js.defs["EPSG:3498"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / California zone 6
Proj4js.defs["EPSG:3499"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / California zone 6 (ftUS)
Proj4js.defs["EPSG:3500"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=2000000.0001016 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Colorado Central
Proj4js.defs["EPSG:3501"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Colorado Central (ftUS)
Proj4js.defs["EPSG:3502"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Colorado North
Proj4js.defs["EPSG:3503"]="+proj=lcc +lat_1=40.78333333333333 +lat_2=39.71666666666667 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Colorado North (ftUS)
Proj4js.defs["EPSG:3504"]="+proj=lcc +lat_1=40.78333333333333 +lat_2=39.71666666666667 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Colorado South
Proj4js.defs["EPSG:3505"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Colorado South (ftUS)
Proj4js.defs["EPSG:3506"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=914401.8288036576 +y_0=304800.6096012192 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Connecticut
Proj4js.defs["EPSG:3507"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096 +y_0=152400.3048 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Connecticut (ftUS)
Proj4js.defs["EPSG:3508"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096012192 +y_0=152400.3048006096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Delaware
Proj4js.defs["EPSG:3509"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Delaware (ftUS)
Proj4js.defs["EPSG:3510"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Florida East
Proj4js.defs["EPSG:3511"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Florida East (ftUS)
Proj4js.defs["EPSG:3512"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Florida GDL Albers
Proj4js.defs["EPSG:3513"]="+proj=aea +lat_1=24 +lat_2=31.5 +lat_0=24 +lon_0=-84 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Florida North
Proj4js.defs["EPSG:3514"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Florida North (ftUS)
Proj4js.defs["EPSG:3515"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Florida West
Proj4js.defs["EPSG:3516"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Florida West (ftUS)
Proj4js.defs["EPSG:3517"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Georgia East
Proj4js.defs["EPSG:3518"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Georgia East (ftUS)
Proj4js.defs["EPSG:3519"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Georgia West
Proj4js.defs["EPSG:3520"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Georgia West (ftUS)
Proj4js.defs["EPSG:3521"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Idaho Central
Proj4js.defs["EPSG:3522"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Idaho Central (ftUS)
Proj4js.defs["EPSG:3523"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Idaho East
Proj4js.defs["EPSG:3524"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Idaho East (ftUS)
Proj4js.defs["EPSG:3525"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=200000.0001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Idaho West
Proj4js.defs["EPSG:3526"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=800000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Idaho West (ftUS)
Proj4js.defs["EPSG:3527"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=800000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Illinois East
Proj4js.defs["EPSG:3528"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Illinois East (ftUS)
Proj4js.defs["EPSG:3529"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Illinois West
Proj4js.defs["EPSG:3530"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Illinois West (ftUS)
Proj4js.defs["EPSG:3531"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=699999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Indiana East
Proj4js.defs["EPSG:3532"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=100000 +y_0=250000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Indiana East (ftUS)
Proj4js.defs["EPSG:3533"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=99999.99989839978 +y_0=249999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Indiana West
Proj4js.defs["EPSG:3534"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=250000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Indiana West (ftUS)
Proj4js.defs["EPSG:3535"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=249999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Iowa North
Proj4js.defs["EPSG:3536"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=1500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Iowa North (ft US)
Proj4js.defs["EPSG:3537"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=1500000 +y_0=999999.9999898402 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Iowa South
Proj4js.defs["EPSG:3538"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Iowa South (ft US)
Proj4js.defs["EPSG:3539"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Kansas North
Proj4js.defs["EPSG:3540"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Kansas North (ft US)
Proj4js.defs["EPSG:3541"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=399999.99998984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Kansas South
Proj4js.defs["EPSG:3542"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Kansas South (ft US)
Proj4js.defs["EPSG:3543"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=399999.99998984 +y_0=399999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Kentucky North
Proj4js.defs["EPSG:3544"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Kentucky North (ftUS)
Proj4js.defs["EPSG:3545"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Kentucky Single Zone
Proj4js.defs["EPSG:3546"]="+proj=lcc +lat_1=37.08333333333334 +lat_2=38.66666666666666 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=1500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Kentucky Single Zone (ftUS)
Proj4js.defs["EPSG:3547"]="+proj=lcc +lat_1=37.08333333333334 +lat_2=38.66666666666666 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=1500000 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Kentucky South
Proj4js.defs["EPSG:3548"]="+proj=lcc +lat_1=37.93333333333333 +lat_2=36.73333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=500000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Kentucky South (ftUS)
Proj4js.defs["EPSG:3549"]="+proj=lcc +lat_1=37.93333333333333 +lat_2=36.73333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=500000.0001016001 +y_0=500000.0001016001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Louisiana North
Proj4js.defs["EPSG:3550"]="+proj=lcc +lat_1=32.66666666666666 +lat_2=31.16666666666667 +lat_0=30.5 +lon_0=-92.5 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Louisiana North (ftUS)
Proj4js.defs["EPSG:3551"]="+proj=lcc +lat_1=32.66666666666666 +lat_2=31.16666666666667 +lat_0=30.5 +lon_0=-92.5 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Louisiana South
Proj4js.defs["EPSG:3552"]="+proj=lcc +lat_1=30.7 +lat_2=29.3 +lat_0=28.5 +lon_0=-91.33333333333333 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Louisiana South (ftUS)
Proj4js.defs["EPSG:3553"]="+proj=lcc +lat_1=30.7 +lat_2=29.3 +lat_0=28.5 +lon_0=-91.33333333333333 +x_0=999999.9999898402 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Maine CS2000 Central
Proj4js.defs["EPSG:3554"]="+proj=tmerc +lat_0=43.5 +lon_0=-69.125 +k=0.99998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maine CS2000 East
Proj4js.defs["EPSG:3555"]="+proj=tmerc +lat_0=43.83333333333334 +lon_0=-67.875 +k=0.99998 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maine CS2000 West
Proj4js.defs["EPSG:3556"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.375 +k=0.99998 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maine East
Proj4js.defs["EPSG:3557"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maine West
Proj4js.defs["EPSG:3558"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maryland
Proj4js.defs["EPSG:3559"]="+proj=lcc +lat_1=39.45 +lat_2=38.3 +lat_0=37.66666666666666 +lon_0=-77 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Utah North (ftUS)
Proj4js.defs["EPSG:3560"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000.00001016 +y_0=999999.9999898402 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Old Hawaiian / Hawaii zone 1
Proj4js.defs["EPSG:3561"]="+proj=tmerc +lat_0=18.83333333333333 +lon_0=-155.5 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=61,-285,-181,0,0,0,0 +units=us-ft +no_defs  ";
// Old Hawaiian / Hawaii zone 2
Proj4js.defs["EPSG:3562"]="+proj=tmerc +lat_0=20.33333333333333 +lon_0=-156.6666666666667 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=61,-285,-181,0,0,0,0 +units=us-ft +no_defs  ";
// Old Hawaiian / Hawaii zone 3
Proj4js.defs["EPSG:3563"]="+proj=tmerc +lat_0=21.16666666666667 +lon_0=-158 +k=0.99999 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=61,-285,-181,0,0,0,0 +units=us-ft +no_defs  ";
// Old Hawaiian / Hawaii zone 4
Proj4js.defs["EPSG:3564"]="+proj=tmerc +lat_0=21.83333333333333 +lon_0=-159.5 +k=0.99999 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=61,-285,-181,0,0,0,0 +units=us-ft +no_defs  ";
// Old Hawaiian / Hawaii zone 5
Proj4js.defs["EPSG:3565"]="+proj=tmerc +lat_0=21.66666666666667 +lon_0=-160.1666666666667 +k=1 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=61,-285,-181,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Utah Central (ftUS)
Proj4js.defs["EPSG:3566"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000.00001016 +y_0=2000000.00001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Utah South (ftUS)
Proj4js.defs["EPSG:3567"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000.00001016 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Utah North (ftUS)
Proj4js.defs["EPSG:3568"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000.00001016 +y_0=999999.9999898402 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Utah Central (ftUS)
Proj4js.defs["EPSG:3569"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000.00001016 +y_0=2000000.00001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Utah South (ftUS)
Proj4js.defs["EPSG:3570"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000.00001016 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// WGS 84 / North Pole LAEA Bering Sea
Proj4js.defs["EPSG:3571"]="+proj=laea +lat_0=90 +lon_0=180 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / North Pole LAEA Alaska
Proj4js.defs["EPSG:3572"]="+proj=laea +lat_0=90 +lon_0=-150 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / North Pole LAEA Canada
Proj4js.defs["EPSG:3573"]="+proj=laea +lat_0=90 +lon_0=-100 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / North Pole LAEA Atlantic
Proj4js.defs["EPSG:3574"]="+proj=laea +lat_0=90 +lon_0=-40 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / North Pole LAEA Europe
Proj4js.defs["EPSG:3575"]="+proj=laea +lat_0=90 +lon_0=10 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / North Pole LAEA Russia
Proj4js.defs["EPSG:3576"]="+proj=laea +lat_0=90 +lon_0=90 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// GDA94 / Australian Albers
Proj4js.defs["EPSG:3577"]="+proj=aea +lat_1=-18 +lat_2=-36 +lat_0=0 +lon_0=132 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Yukon Albers
Proj4js.defs["EPSG:3578"]="+proj=aea +lat_1=61.66666666666666 +lat_2=68 +lat_0=59 +lon_0=-132.5 +x_0=500000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Yukon Albers
Proj4js.defs["EPSG:3579"]="+proj=aea +lat_1=61.66666666666666 +lat_2=68 +lat_0=59 +lon_0=-132.5 +x_0=500000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / NWT Lambert
Proj4js.defs["EPSG:3580"]="+proj=lcc +lat_1=62 +lat_2=70 +lat_0=0 +lon_0=-112 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / NWT Lambert
Proj4js.defs["EPSG:3581"]="+proj=lcc +lat_1=62 +lat_2=70 +lat_0=0 +lon_0=-112 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maryland (ftUS)
Proj4js.defs["EPSG:3582"]="+proj=lcc +lat_1=39.45 +lat_2=38.3 +lat_0=37.66666666666666 +lon_0=-77 +x_0=399999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Massachusetts Island
Proj4js.defs["EPSG:3583"]="+proj=lcc +lat_1=41.48333333333333 +lat_2=41.28333333333333 +lat_0=41 +lon_0=-70.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Massachusetts Island (ftUS)
Proj4js.defs["EPSG:3584"]="+proj=lcc +lat_1=41.48333333333333 +lat_2=41.28333333333333 +lat_0=41 +lon_0=-70.5 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Massachusetts Mainland
Proj4js.defs["EPSG:3585"]="+proj=lcc +lat_1=42.68333333333333 +lat_2=41.71666666666667 +lat_0=41 +lon_0=-71.5 +x_0=200000 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Massachusetts Mainland (ftUS)
Proj4js.defs["EPSG:3586"]="+proj=lcc +lat_1=42.68333333333333 +lat_2=41.71666666666667 +lat_0=41 +lon_0=-71.5 +x_0=200000.0001016002 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Michigan Central
Proj4js.defs["EPSG:3587"]="+proj=lcc +lat_1=45.7 +lat_2=44.18333333333333 +lat_0=43.31666666666667 +lon_0=-84.36666666666666 +x_0=6000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Michigan Central (ft)
Proj4js.defs["EPSG:3588"]="+proj=lcc +lat_1=45.7 +lat_2=44.18333333333333 +lat_0=43.31666666666667 +lon_0=-84.36666666666666 +x_0=5999999.999976001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Michigan North
Proj4js.defs["EPSG:3589"]="+proj=lcc +lat_1=47.08333333333334 +lat_2=45.48333333333333 +lat_0=44.78333333333333 +lon_0=-87 +x_0=8000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Michigan North (ft)
Proj4js.defs["EPSG:3590"]="+proj=lcc +lat_1=47.08333333333334 +lat_2=45.48333333333333 +lat_0=44.78333333333333 +lon_0=-87 +x_0=7999999.999968001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Michigan Oblique Mercator
Proj4js.defs["EPSG:3591"]="+proj=omerc +lat_0=45.30916666666666 +lonc=-86 +alpha=337.25556 +k=0.9996 +x_0=2546731.496 +y_0=-4354009.816 +gamma=337.25556 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Michigan South
Proj4js.defs["EPSG:3592"]="+proj=lcc +lat_1=43.66666666666666 +lat_2=42.1 +lat_0=41.5 +lon_0=-84.36666666666666 +x_0=4000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Michigan South (ft)
Proj4js.defs["EPSG:3593"]="+proj=lcc +lat_1=43.66666666666666 +lat_2=42.1 +lat_0=41.5 +lon_0=-84.36666666666666 +x_0=3999999.999984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Minnesota Central
Proj4js.defs["EPSG:3594"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Minnesota North
Proj4js.defs["EPSG:3595"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Minnesota South
Proj4js.defs["EPSG:3596"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Mississippi East
Proj4js.defs["EPSG:3597"]="+proj=tmerc +lat_0=29.5 +lon_0=-88.83333333333333 +k=0.99995 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Mississippi East (ftUS)
Proj4js.defs["EPSG:3598"]="+proj=tmerc +lat_0=29.5 +lon_0=-88.83333333333333 +k=0.99995 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Mississippi West
Proj4js.defs["EPSG:3599"]="+proj=tmerc +lat_0=29.5 +lon_0=-90.33333333333333 +k=0.99995 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Mississippi West (ftUS)
Proj4js.defs["EPSG:3600"]="+proj=tmerc +lat_0=29.5 +lon_0=-90.33333333333333 +k=0.99995 +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Missouri Central
Proj4js.defs["EPSG:3601"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-92.5 +k=0.999933333 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Missouri East
Proj4js.defs["EPSG:3602"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-90.5 +k=0.999933333 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Missouri West
Proj4js.defs["EPSG:3603"]="+proj=tmerc +lat_0=36.16666666666666 +lon_0=-94.5 +k=0.999941177 +x_0=850000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Montana
Proj4js.defs["EPSG:3604"]="+proj=lcc +lat_1=49 +lat_2=45 +lat_0=44.25 +lon_0=-109.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Montana (ft)
Proj4js.defs["EPSG:3605"]="+proj=lcc +lat_1=49 +lat_2=45 +lat_0=44.25 +lon_0=-109.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Nebraska
Proj4js.defs["EPSG:3606"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Nevada Central
Proj4js.defs["EPSG:3607"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=500000 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Nevada Central (ft US)
Proj4js.defs["EPSG:3608"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=500000.00001016 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Nevada East
Proj4js.defs["EPSG:3609"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=200000 +y_0=8000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Nevada East (ft US)
Proj4js.defs["EPSG:3610"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=200000.00001016 +y_0=8000000.000010163 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Nevada West
Proj4js.defs["EPSG:3611"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=800000 +y_0=4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Nevada West (ft US)
Proj4js.defs["EPSG:3612"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=800000.0000101599 +y_0=3999999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New Hampshire
Proj4js.defs["EPSG:3613"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New Hampshire (ftUS)
Proj4js.defs["EPSG:3614"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New Jersey
Proj4js.defs["EPSG:3615"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New Jersey (ft US)
Proj4js.defs["EPSG:3616"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New Mexico Central
Proj4js.defs["EPSG:3617"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New Mexico Central (ftUS)
Proj4js.defs["EPSG:3618"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New Mexico East
Proj4js.defs["EPSG:3619"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=165000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New Mexico East (ftUS)
Proj4js.defs["EPSG:3620"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=165000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New Mexico West
Proj4js.defs["EPSG:3621"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=830000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New Mexico West (ftUS)
Proj4js.defs["EPSG:3622"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=830000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New York Central
Proj4js.defs["EPSG:3623"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New York Central (ftUS)
Proj4js.defs["EPSG:3624"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=249999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New York East
Proj4js.defs["EPSG:3625"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New York East (ftUS)
Proj4js.defs["EPSG:3626"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New York Long Island
Proj4js.defs["EPSG:3627"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New York Long Island (ftUS)
Proj4js.defs["EPSG:3628"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / New York West
Proj4js.defs["EPSG:3629"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=350000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / New York West (ftUS)
Proj4js.defs["EPSG:3630"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=350000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / North Carolina
Proj4js.defs["EPSG:3631"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.22 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / North Carolina (ftUS)
Proj4js.defs["EPSG:3632"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.2192024384 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / North Dakota North
Proj4js.defs["EPSG:3633"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.43333333333333 +lat_0=47 +lon_0=-100.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / North Dakota North (ft)
Proj4js.defs["EPSG:3634"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.43333333333333 +lat_0=47 +lon_0=-100.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / North Dakota South
Proj4js.defs["EPSG:3635"]="+proj=lcc +lat_1=47.48333333333333 +lat_2=46.18333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / North Dakota South (ft)
Proj4js.defs["EPSG:3636"]="+proj=lcc +lat_1=47.48333333333333 +lat_2=46.18333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=599999.9999976 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Ohio North
Proj4js.defs["EPSG:3637"]="+proj=lcc +lat_1=41.7 +lat_2=40.43333333333333 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Ohio South
Proj4js.defs["EPSG:3638"]="+proj=lcc +lat_1=40.03333333333333 +lat_2=38.73333333333333 +lat_0=38 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Oklahoma North
Proj4js.defs["EPSG:3639"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=35.56666666666667 +lat_0=35 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Oklahoma North (ftUS)
Proj4js.defs["EPSG:3640"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=35.56666666666667 +lat_0=35 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Oklahoma South
Proj4js.defs["EPSG:3641"]="+proj=lcc +lat_1=35.23333333333333 +lat_2=33.93333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Oklahoma South (ftUS)
Proj4js.defs["EPSG:3642"]="+proj=lcc +lat_1=35.23333333333333 +lat_2=33.93333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Oregon Lambert
Proj4js.defs["EPSG:3643"]="+proj=lcc +lat_1=43 +lat_2=45.5 +lat_0=41.75 +lon_0=-120.5 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Oregon Lambert (ft)
Proj4js.defs["EPSG:3644"]="+proj=lcc +lat_1=43 +lat_2=45.5 +lat_0=41.75 +lon_0=-120.5 +x_0=399999.9999984 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Oregon North
Proj4js.defs["EPSG:3645"]="+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Oregon North (ft)
Proj4js.defs["EPSG:3646"]="+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000.0001424 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Oregon South
Proj4js.defs["EPSG:3647"]="+proj=lcc +lat_1=44 +lat_2=42.33333333333334 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=1500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Oregon South (ft)
Proj4js.defs["EPSG:3648"]="+proj=lcc +lat_1=44 +lat_2=42.33333333333334 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=1500000.0001464 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Pennsylvania North
Proj4js.defs["EPSG:3649"]="+proj=lcc +lat_1=41.95 +lat_2=40.88333333333333 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Pennsylvania North (ftUS)
Proj4js.defs["EPSG:3650"]="+proj=lcc +lat_1=41.95 +lat_2=40.88333333333333 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Pennsylvania South
Proj4js.defs["EPSG:3651"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Pennsylvania South (ftUS)
Proj4js.defs["EPSG:3652"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Rhode Island
Proj4js.defs["EPSG:3653"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.99999375 +x_0=100000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Rhode Island (ftUS)
Proj4js.defs["EPSG:3654"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.99999375 +x_0=99999.99998983997 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / South Carolina
Proj4js.defs["EPSG:3655"]="+proj=lcc +lat_1=34.83333333333334 +lat_2=32.5 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609600 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / South Carolina (ft)
Proj4js.defs["EPSG:3656"]="+proj=lcc +lat_1=34.83333333333334 +lat_2=32.5 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609600 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / South Dakota North
Proj4js.defs["EPSG:3657"]="+proj=lcc +lat_1=45.68333333333333 +lat_2=44.41666666666666 +lat_0=43.83333333333334 +lon_0=-100 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / South Dakota North (ftUS)
Proj4js.defs["EPSG:3658"]="+proj=lcc +lat_1=45.68333333333333 +lat_2=44.41666666666666 +lat_0=43.83333333333334 +lon_0=-100 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / South Dakota South
Proj4js.defs["EPSG:3659"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / South Dakota South (ftUS)
Proj4js.defs["EPSG:3660"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Tennessee
Proj4js.defs["EPSG:3661"]="+proj=lcc +lat_1=36.41666666666666 +lat_2=35.25 +lat_0=34.33333333333334 +lon_0=-86 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Tennessee (ftUS)
Proj4js.defs["EPSG:3662"]="+proj=lcc +lat_1=36.41666666666666 +lat_2=35.25 +lat_0=34.33333333333334 +lon_0=-86 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Texas Central
Proj4js.defs["EPSG:3663"]="+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=700000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas Central (ftUS)
Proj4js.defs["EPSG:3664"]="+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=699999.9998983998 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Texas Centric Albers Equal Area
Proj4js.defs["EPSG:3665"]="+proj=aea +lat_1=27.5 +lat_2=35 +lat_0=18 +lon_0=-100 +x_0=1500000 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas Centric Lambert Conformal
Proj4js.defs["EPSG:3666"]="+proj=lcc +lat_1=27.5 +lat_2=35 +lat_0=18 +lon_0=-100 +x_0=1500000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas North
Proj4js.defs["EPSG:3667"]="+proj=lcc +lat_1=36.18333333333333 +lat_2=34.65 +lat_0=34 +lon_0=-101.5 +x_0=200000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas North (ftUS)
Proj4js.defs["EPSG:3668"]="+proj=lcc +lat_1=36.18333333333333 +lat_2=34.65 +lat_0=34 +lon_0=-101.5 +x_0=200000.0001016002 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Texas North Central
Proj4js.defs["EPSG:3669"]="+proj=lcc +lat_1=33.96666666666667 +lat_2=32.13333333333333 +lat_0=31.66666666666667 +lon_0=-98.5 +x_0=600000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas North Central (ftUS)
Proj4js.defs["EPSG:3670"]="+proj=lcc +lat_1=33.96666666666667 +lat_2=32.13333333333333 +lat_0=31.66666666666667 +lon_0=-98.5 +x_0=600000 +y_0=2000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Texas South
Proj4js.defs["EPSG:3671"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=300000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas South (ftUS)
Proj4js.defs["EPSG:3672"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=300000.0000000001 +y_0=5000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Texas South Central
Proj4js.defs["EPSG:3673"]="+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Texas South Central (ftUS)
Proj4js.defs["EPSG:3674"]="+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=3999999.9998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Utah Central
Proj4js.defs["EPSG:3675"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Utah Central (ft)
Proj4js.defs["EPSG:3676"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000.0001504 +y_0=1999999.999992 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Utah Central (ftUS)
Proj4js.defs["EPSG:3677"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000.00001016 +y_0=2000000.00001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Utah North
Proj4js.defs["EPSG:3678"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Utah North (ft)
Proj4js.defs["EPSG:3679"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000.0001504 +y_0=999999.9999960001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Utah North (ftUS)
Proj4js.defs["EPSG:3680"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000.00001016 +y_0=999999.9999898402 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Utah South
Proj4js.defs["EPSG:3681"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Utah South (ft)
Proj4js.defs["EPSG:3682"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000.0001504 +y_0=2999999.999988 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs  ";
// NAD83(NSRS2007) / Utah South (ftUS)
Proj4js.defs["EPSG:3683"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000.00001016 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Vermont
Proj4js.defs["EPSG:3684"]="+proj=tmerc +lat_0=42.5 +lon_0=-72.5 +k=0.999964286 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Virginia North
Proj4js.defs["EPSG:3685"]="+proj=lcc +lat_1=39.2 +lat_2=38.03333333333333 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=3500000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Virginia North (ftUS)
Proj4js.defs["EPSG:3686"]="+proj=lcc +lat_1=39.2 +lat_2=38.03333333333333 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=2000000.0001016 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Virginia South
Proj4js.defs["EPSG:3687"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Virginia South (ftUS)
Proj4js.defs["EPSG:3688"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=999999.9998983998 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Washington North
Proj4js.defs["EPSG:3689"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.5 +lat_0=47 +lon_0=-120.8333333333333 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Washington North (ftUS)
Proj4js.defs["EPSG:3690"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.5 +lat_0=47 +lon_0=-120.8333333333333 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Washington South
Proj4js.defs["EPSG:3691"]="+proj=lcc +lat_1=47.33333333333334 +lat_2=45.83333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Washington South (ftUS)
Proj4js.defs["EPSG:3692"]="+proj=lcc +lat_1=47.33333333333334 +lat_2=45.83333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=500000.0001016001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / West Virginia North
Proj4js.defs["EPSG:3693"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / West Virginia South
Proj4js.defs["EPSG:3694"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wisconsin Central
Proj4js.defs["EPSG:3695"]="+proj=lcc +lat_1=45.5 +lat_2=44.25 +lat_0=43.83333333333334 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wisconsin Central (ftUS)
Proj4js.defs["EPSG:3696"]="+proj=lcc +lat_1=45.5 +lat_2=44.25 +lat_0=43.83333333333334 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wisconsin North
Proj4js.defs["EPSG:3697"]="+proj=lcc +lat_1=46.76666666666667 +lat_2=45.56666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wisconsin North (ftUS)
Proj4js.defs["EPSG:3698"]="+proj=lcc +lat_1=46.76666666666667 +lat_2=45.56666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wisconsin South
Proj4js.defs["EPSG:3699"]="+proj=lcc +lat_1=44.06666666666667 +lat_2=42.73333333333333 +lat_0=42 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wisconsin South (ftUS)
Proj4js.defs["EPSG:3700"]="+proj=lcc +lat_1=44.06666666666667 +lat_2=42.73333333333333 +lat_0=42 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wisconsin Transverse Mercator
Proj4js.defs["EPSG:3701"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9996 +x_0=520000 +y_0=-4480000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wyoming East
Proj4js.defs["EPSG:3702"]="+proj=tmerc +lat_0=40.5 +lon_0=-105.1666666666667 +k=0.9999375 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wyoming East Central
Proj4js.defs["EPSG:3703"]="+proj=tmerc +lat_0=40.5 +lon_0=-107.3333333333333 +k=0.9999375 +x_0=400000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wyoming West Central
Proj4js.defs["EPSG:3704"]="+proj=tmerc +lat_0=40.5 +lon_0=-108.75 +k=0.9999375 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Wyoming West
Proj4js.defs["EPSG:3705"]="+proj=tmerc +lat_0=40.5 +lon_0=-110.0833333333333 +k=0.9999375 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 59N
Proj4js.defs["EPSG:3706"]="+proj=utm +zone=59 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 60N
Proj4js.defs["EPSG:3707"]="+proj=utm +zone=60 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 1N
Proj4js.defs["EPSG:3708"]="+proj=utm +zone=1 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 2N
Proj4js.defs["EPSG:3709"]="+proj=utm +zone=2 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 3N
Proj4js.defs["EPSG:3710"]="+proj=utm +zone=3 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 4N
Proj4js.defs["EPSG:3711"]="+proj=utm +zone=4 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 5N
Proj4js.defs["EPSG:3712"]="+proj=utm +zone=5 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 6N
Proj4js.defs["EPSG:3713"]="+proj=utm +zone=6 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 7N
Proj4js.defs["EPSG:3714"]="+proj=utm +zone=7 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 8N
Proj4js.defs["EPSG:3715"]="+proj=utm +zone=8 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 9N
Proj4js.defs["EPSG:3716"]="+proj=utm +zone=9 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 10N
Proj4js.defs["EPSG:3717"]="+proj=utm +zone=10 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 11N
Proj4js.defs["EPSG:3718"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 12N
Proj4js.defs["EPSG:3719"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 13N
Proj4js.defs["EPSG:3720"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 14N
Proj4js.defs["EPSG:3721"]="+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 15N
Proj4js.defs["EPSG:3722"]="+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 16N
Proj4js.defs["EPSG:3723"]="+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 17N
Proj4js.defs["EPSG:3724"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 18N
Proj4js.defs["EPSG:3725"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / UTM zone 19N
Proj4js.defs["EPSG:3726"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Reunion 1947 / TM Reunion
Proj4js.defs["EPSG:3727"]="+proj=tmerc +lat_0=-21.11666666666667 +lon_0=55.53333333333333 +k=1 +x_0=160000 +y_0=50000 +ellps=intl +towgs84=94,-948,-1262,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Ohio North (ftUS)
Proj4js.defs["EPSG:3728"]="+proj=lcc +lat_1=41.7 +lat_2=40.43333333333333 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Ohio South (ftUS)
Proj4js.defs["EPSG:3729"]="+proj=lcc +lat_1=40.03333333333333 +lat_2=38.73333333333333 +lat_0=38 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wyoming East (ftUS)
Proj4js.defs["EPSG:3730"]="+proj=tmerc +lat_0=40.5 +lon_0=-105.1666666666667 +k=0.9999375 +x_0=200000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wyoming East Central (ftUS)
Proj4js.defs["EPSG:3731"]="+proj=tmerc +lat_0=40.5 +lon_0=-107.3333333333333 +k=0.9999375 +x_0=399999.99998984 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wyoming West Central (ftUS)
Proj4js.defs["EPSG:3732"]="+proj=tmerc +lat_0=40.5 +lon_0=-108.75 +k=0.9999375 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Wyoming West (ftUS)
Proj4js.defs["EPSG:3733"]="+proj=tmerc +lat_0=40.5 +lon_0=-110.0833333333333 +k=0.9999375 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Ohio North (ftUS)
Proj4js.defs["EPSG:3734"]="+proj=lcc +lat_1=41.7 +lat_2=40.43333333333333 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Ohio South (ftUS)
Proj4js.defs["EPSG:3735"]="+proj=lcc +lat_1=40.03333333333333 +lat_2=38.73333333333333 +lat_0=38 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wyoming East (ftUS)
Proj4js.defs["EPSG:3736"]="+proj=tmerc +lat_0=40.5 +lon_0=-105.1666666666667 +k=0.9999375 +x_0=200000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wyoming East Central (ftUS)
Proj4js.defs["EPSG:3737"]="+proj=tmerc +lat_0=40.5 +lon_0=-107.3333333333333 +k=0.9999375 +x_0=399999.99998984 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wyoming West Central (ftUS)
Proj4js.defs["EPSG:3738"]="+proj=tmerc +lat_0=40.5 +lon_0=-108.75 +k=0.9999375 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Wyoming West (ftUS)
Proj4js.defs["EPSG:3739"]="+proj=tmerc +lat_0=40.5 +lon_0=-110.0833333333333 +k=0.9999375 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / UTM zone 10N
Proj4js.defs["EPSG:3740"]="+proj=utm +zone=10 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 11N
Proj4js.defs["EPSG:3741"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 12N
Proj4js.defs["EPSG:3742"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 13N
Proj4js.defs["EPSG:3743"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 14N
Proj4js.defs["EPSG:3744"]="+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 15N
Proj4js.defs["EPSG:3745"]="+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 16N
Proj4js.defs["EPSG:3746"]="+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 17N
Proj4js.defs["EPSG:3747"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 18N
Proj4js.defs["EPSG:3748"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 19N
Proj4js.defs["EPSG:3749"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 4N
Proj4js.defs["EPSG:3750"]="+proj=utm +zone=4 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / UTM zone 5N
Proj4js.defs["EPSG:3751"]="+proj=utm +zone=5 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / Mercator 41 (deprecated)
Proj4js.defs["EPSG:3752"]="+proj=merc +lon_0=100 +lat_ts=-41 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// NAD83(HARN) / Ohio North (ftUS)
Proj4js.defs["EPSG:3753"]="+proj=lcc +lat_1=41.7 +lat_2=40.43333333333333 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Ohio South (ftUS)
Proj4js.defs["EPSG:3754"]="+proj=lcc +lat_1=40.03333333333333 +lat_2=38.73333333333333 +lat_0=38 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wyoming East (ftUS)
Proj4js.defs["EPSG:3755"]="+proj=tmerc +lat_0=40.5 +lon_0=-105.1666666666667 +k=0.9999375 +x_0=200000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wyoming East Central (ftUS)
Proj4js.defs["EPSG:3756"]="+proj=tmerc +lat_0=40.5 +lon_0=-107.3333333333333 +k=0.9999375 +x_0=399999.99998984 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wyoming West Central (ftUS)
Proj4js.defs["EPSG:3757"]="+proj=tmerc +lat_0=40.5 +lon_0=-108.75 +k=0.9999375 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Wyoming West (ftUS)
Proj4js.defs["EPSG:3758"]="+proj=tmerc +lat_0=40.5 +lon_0=-110.0833333333333 +k=0.9999375 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Hawaii zone 3 (ftUS)
Proj4js.defs["EPSG:3759"]="+proj=tmerc +lat_0=21.16666666666667 +lon_0=-158 +k=0.99999 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Hawaii zone 3 (ftUS)
Proj4js.defs["EPSG:3760"]="+proj=tmerc +lat_0=21.16666666666667 +lon_0=-158 +k=0.99999 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(CSRS) / UTM zone 22N
Proj4js.defs["EPSG:3761"]="+proj=utm +zone=22 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / South Georgia Lambert
Proj4js.defs["EPSG:3762"]="+proj=lcc +lat_1=-54 +lat_2=-54.75 +lat_0=-55 +lon_0=-37 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// ETRS89 / Portugal TM06
Proj4js.defs["EPSG:3763"]="+proj=tmerc +lat_0=39.66825833333333 +lon_0=-8.133108333333334 +k=1 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Chatham Island Circuit 2000
Proj4js.defs["EPSG:3764"]="+proj=tmerc +lat_0=-44 +lon_0=-176.5 +k=1 +x_0=400000 +y_0=800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// HTRS96 / Croatia TM
Proj4js.defs["EPSG:3765"]="+proj=tmerc +lat_0=0 +lon_0=16.5 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// HTRS96 / Croatia LCC
Proj4js.defs["EPSG:3766"]="+proj=lcc +lat_1=45.91666666666666 +lat_2=43.08333333333334 +lat_0=0 +lon_0=16.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// HTRS96 / UTM zone 33N
Proj4js.defs["EPSG:3767"]="+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// HTRS96 / UTM zone 34N
Proj4js.defs["EPSG:3768"]="+proj=utm +zone=34 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Bermuda 1957 / UTM zone 20N
Proj4js.defs["EPSG:3769"]="+proj=utm +zone=20 +ellps=clrk66 +towgs84=-73,213,296,0,0,0,0 +units=m +no_defs  ";
// BDA2000 / Bermuda 2000 National Grid
Proj4js.defs["EPSG:3770"]="+proj=tmerc +lat_0=32 +lon_0=-64.75 +k=1 +x_0=550000 +y_0=100000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Alberta 3TM ref merid 111 W
Proj4js.defs["EPSG:3771"]="+proj=tmerc +lat_0=0 +lon_0=-111 +k=0.9999 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Alberta 3TM ref merid 114 W
Proj4js.defs["EPSG:3772"]="+proj=tmerc +lat_0=0 +lon_0=-114 +k=0.9999 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Alberta 3TM ref merid 117 W
Proj4js.defs["EPSG:3773"]="+proj=tmerc +lat_0=0 +lon_0=-117 +k=0.9999 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Alberta 3TM ref merid 120 W (deprecated)
Proj4js.defs["EPSG:3774"]="+proj=tmerc +lat_0=0 +lon_0=-120 +k=0.9999 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / Alberta 3TM ref merid 111 W
Proj4js.defs["EPSG:3775"]="+proj=tmerc +lat_0=0 +lon_0=-111 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alberta 3TM ref merid 114 W
Proj4js.defs["EPSG:3776"]="+proj=tmerc +lat_0=0 +lon_0=-114 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alberta 3TM ref merid 117 W
Proj4js.defs["EPSG:3777"]="+proj=tmerc +lat_0=0 +lon_0=-117 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alberta 3TM ref merid 120 W (deprecated)
Proj4js.defs["EPSG:3778"]="+proj=tmerc +lat_0=0 +lon_0=-120 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 3TM ref merid 111 W
Proj4js.defs["EPSG:3779"]="+proj=tmerc +lat_0=0 +lon_0=-111 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 3TM ref merid 114 W
Proj4js.defs["EPSG:3780"]="+proj=tmerc +lat_0=0 +lon_0=-114 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 3TM ref merid 117 W
Proj4js.defs["EPSG:3781"]="+proj=tmerc +lat_0=0 +lon_0=-117 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 3TM ref merid 120 W (deprecated)
Proj4js.defs["EPSG:3782"]="+proj=tmerc +lat_0=0 +lon_0=-120 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Pitcairn 2006 / Pitcairn TM 2006
Proj4js.defs["EPSG:3783"]="+proj=tmerc +lat_0=-25.06855261111111 +lon_0=-130.1129671111111 +k=1 +x_0=14200 +y_0=15500 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Pitcairn 1967 / UTM zone 9S
Proj4js.defs["EPSG:3784"]="+proj=utm +zone=9 +south +ellps=intl +towgs84=185,165,42,0,0,0,0 +units=m +no_defs  ";
// Popular Visualisation CRS / Mercator (deprecated)
Proj4js.defs["EPSG:3785"]="+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs ";
// World Equidistant Cylindrical (Sphere) (deprecated)
Proj4js.defs["EPSG:3786"]="+proj=eqc +lat_ts=0 +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +a=6371007 +b=6371007 +units=m +no_defs  ";
// MGI / Slovene National Grid (deprecated)
Proj4js.defs["EPSG:3787"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// NZGD2000 / Auckland Islands TM 2000
Proj4js.defs["EPSG:3788"]="+proj=tmerc +lat_0=0 +lon_0=166 +k=1 +x_0=3500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Campbell Island TM 2000
Proj4js.defs["EPSG:3789"]="+proj=tmerc +lat_0=0 +lon_0=169 +k=1 +x_0=3500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Antipodes Islands TM 2000
Proj4js.defs["EPSG:3790"]="+proj=tmerc +lat_0=0 +lon_0=179 +k=1 +x_0=3500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Raoul Island TM 2000
Proj4js.defs["EPSG:3791"]="+proj=tmerc +lat_0=0 +lon_0=-178 +k=1 +x_0=3500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / Chatham Islands TM 2000
Proj4js.defs["EPSG:3793"]="+proj=tmerc +lat_0=0 +lon_0=-176.5 +k=1 +x_0=3500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Slovenia 1996 / Slovene National Grid
Proj4js.defs["EPSG:3794"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Cuba Norte
Proj4js.defs["EPSG:3795"]="+proj=lcc +lat_1=23 +lat_2=21.7 +lat_0=22.35 +lon_0=-81 +x_0=500000 +y_0=280296.016 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Cuba Sur
Proj4js.defs["EPSG:3796"]="+proj=lcc +lat_1=21.3 +lat_2=20.13333333333333 +lat_0=20.71666666666667 +lon_0=-76.83333333333333 +x_0=500000 +y_0=229126.939 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / MTQ Lambert
Proj4js.defs["EPSG:3797"]="+proj=lcc +lat_1=50 +lat_2=46 +lat_0=44 +lon_0=-70 +x_0=800000 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / MTQ Lambert
Proj4js.defs["EPSG:3798"]="+proj=lcc +lat_1=50 +lat_2=46 +lat_0=44 +lon_0=-70 +x_0=800000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTQ Lambert
Proj4js.defs["EPSG:3799"]="+proj=lcc +lat_1=50 +lat_2=46 +lat_0=44 +lon_0=-70 +x_0=800000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Alberta 3TM ref merid 120 W
Proj4js.defs["EPSG:3800"]="+proj=tmerc +lat_0=0 +lon_0=-120 +k=0.9999 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / Alberta 3TM ref merid 120 W
Proj4js.defs["EPSG:3801"]="+proj=tmerc +lat_0=0 +lon_0=-120 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Alberta 3TM ref merid 120 W
Proj4js.defs["EPSG:3802"]="+proj=tmerc +lat_0=0 +lon_0=-120 +k=0.9999 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Belgian Lambert 2008
Proj4js.defs["EPSG:3812"]="+proj=lcc +lat_1=49.83333333333334 +lat_2=51.16666666666666 +lat_0=50.797815 +lon_0=4.359215833333333 +x_0=649328 +y_0=665262 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Mississippi TM
Proj4js.defs["EPSG:3814"]="+proj=tmerc +lat_0=32.5 +lon_0=-89.75 +k=0.9998335 +x_0=500000 +y_0=1300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Mississippi TM
Proj4js.defs["EPSG:3815"]="+proj=tmerc +lat_0=32.5 +lon_0=-89.75 +k=0.9998335 +x_0=500000 +y_0=1300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Mississippi TM
Proj4js.defs["EPSG:3816"]="+proj=tmerc +lat_0=32.5 +lon_0=-89.75 +k=0.9998335 +x_0=500000 +y_0=1300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TWD97 / TM2 zone 119
Proj4js.defs["EPSG:3825"]="+proj=tmerc +lat_0=0 +lon_0=119 +k=0.9999 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TWD97 / TM2 zone 121
Proj4js.defs["EPSG:3826"]="+proj=tmerc +lat_0=0 +lon_0=121 +k=0.9999 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TWD67 / TM2 zone 119
Proj4js.defs["EPSG:3827"]="+proj=tmerc +lat_0=0 +lon_0=119 +k=0.9999 +x_0=250000 +y_0=0 +ellps=aust_SA +units=m +no_defs  ";
// TWD67 / TM2 zone 121
Proj4js.defs["EPSG:3828"]="+proj=tmerc +lat_0=0 +lon_0=121 +k=0.9999 +x_0=250000 +y_0=0 +ellps=aust_SA +units=m +no_defs  ";
// Hu Tzu Shan 1950 / UTM zone 51N
Proj4js.defs["EPSG:3829"]="+proj=utm +zone=51 +ellps=intl +towgs84=-637,-549,-203,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / PDC Mercator
Proj4js.defs["EPSG:3832"]="+proj=merc +lon_0=150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// Pulkovo 1942(58) / Gauss-Kruger zone 2
Proj4js.defs["EPSG:3833"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=2500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(83) / Gauss-Kruger zone 2
Proj4js.defs["EPSG:3834"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=2500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / Gauss-Kruger zone 3
Proj4js.defs["EPSG:3835"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=3500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / Gauss-Kruger zone 4
Proj4js.defs["EPSG:3836"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 3
Proj4js.defs["EPSG:3837"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 4
Proj4js.defs["EPSG:3838"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 9
Proj4js.defs["EPSG:3839"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(58) / 3-degree Gauss-Kruger zone 10
Proj4js.defs["EPSG:3840"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=10500000 +y_0=0 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 6
Proj4js.defs["EPSG:3841"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 7 (deprecated)
Proj4js.defs["EPSG:3842"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 8 (deprecated)
Proj4js.defs["EPSG:3843"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(58) / Stereo70
Proj4js.defs["EPSG:3844"]="+proj=sterea +lat_0=46 +lon_0=25 +k=0.99975 +x_0=500000 +y_0=500000 +ellps=krass +towgs84=33.4,-146.6,-76.3,-0.359,-0.053,0.844,-0.84 +units=m +no_defs  ";
// SWEREF99 / RT90 7.5 gon V emulation
Proj4js.defs["EPSG:3845"]="+proj=tmerc +lat_0=0 +lon_0=11.30625 +k=1.000006 +x_0=1500025.141 +y_0=-667.282 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 / RT90 5 gon V emulation
Proj4js.defs["EPSG:3846"]="+proj=tmerc +lat_0=0 +lon_0=13.55626666666667 +k=1.0000058 +x_0=1500044.695 +y_0=-667.13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 / RT90 2.5 gon V emulation
Proj4js.defs["EPSG:3847"]="+proj=tmerc +lat_0=0 +lon_0=15.80628452944445 +k=1.00000561024 +x_0=1500064.274 +y_0=-667.711 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 / RT90 0 gon emulation
Proj4js.defs["EPSG:3848"]="+proj=tmerc +lat_0=0 +lon_0=18.0563 +k=1.0000054 +x_0=1500083.521 +y_0=-668.8440000000001 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 / RT90 2.5 gon O emulation
Proj4js.defs["EPSG:3849"]="+proj=tmerc +lat_0=0 +lon_0=20.30631666666667 +k=1.0000052 +x_0=1500102.765 +y_0=-670.706 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SWEREF99 / RT90 5 gon O emulation
Proj4js.defs["EPSG:3850"]="+proj=tmerc +lat_0=0 +lon_0=22.55633333333333 +k=1.0000049 +x_0=1500121.846 +y_0=-672.557 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NZGD2000 / NZCS2000
Proj4js.defs["EPSG:3851"]="+proj=lcc +lat_1=-37.5 +lat_2=-44.5 +lat_0=-41 +lon_0=173 +x_0=3000000 +y_0=7000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RSRGD2000 / DGLC2000
Proj4js.defs["EPSG:3852"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-90 +lon_0=157 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// County ST74
Proj4js.defs["EPSG:3854"]="+proj=tmerc +lat_0=0 +lon_0=18.05787 +k=0.99999506 +x_0=100182.7406 +y_0=-6500620.1207 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / Pseudo-Mercator
Proj4js.defs["EPSG:3857"]="+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs ";
// ETRS89 / GK19FIN
Proj4js.defs["EPSG:3873"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=1 +x_0=19500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK20FIN
Proj4js.defs["EPSG:3874"]="+proj=tmerc +lat_0=0 +lon_0=20 +k=1 +x_0=20500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK21FIN
Proj4js.defs["EPSG:3875"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=21500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK22FIN
Proj4js.defs["EPSG:3876"]="+proj=tmerc +lat_0=0 +lon_0=22 +k=1 +x_0=22500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK23FIN
Proj4js.defs["EPSG:3877"]="+proj=tmerc +lat_0=0 +lon_0=23 +k=1 +x_0=23500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK24FIN
Proj4js.defs["EPSG:3878"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=24500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK25FIN
Proj4js.defs["EPSG:3879"]="+proj=tmerc +lat_0=0 +lon_0=25 +k=1 +x_0=25500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK26FIN
Proj4js.defs["EPSG:3880"]="+proj=tmerc +lat_0=0 +lon_0=26 +k=1 +x_0=26500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK27FIN
Proj4js.defs["EPSG:3881"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=27500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK28FIN
Proj4js.defs["EPSG:3882"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=1 +x_0=28500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK29FIN
Proj4js.defs["EPSG:3883"]="+proj=tmerc +lat_0=0 +lon_0=29 +k=1 +x_0=29500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK30FIN
Proj4js.defs["EPSG:3884"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=30500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / GK31FIN
Proj4js.defs["EPSG:3885"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=31500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGRS / UTM zone 37N
Proj4js.defs["EPSG:3890"]="+proj=utm +zone=37 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGRS / UTM zone 38N
Proj4js.defs["EPSG:3891"]="+proj=utm +zone=38 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// IGRS / UTM zone 39N
Proj4js.defs["EPSG:3892"]="+proj=utm +zone=39 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ED50 / Iraq National Grid
Proj4js.defs["EPSG:3893"]="+proj=tmerc +lat_0=29.02626833333333 +lon_0=46.5 +k=0.9994 +x_0=800000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// MGI 1901 / Balkans zone 5
Proj4js.defs["EPSG:3907"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs  ";
// MGI 1901 / Balkans zone 6
Proj4js.defs["EPSG:3908"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs  ";
// MGI 1901 / Balkans zone 7
Proj4js.defs["EPSG:3909"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=0.9999 +x_0=7500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs  ";
// MGI 1901 / Balkans zone 8
Proj4js.defs["EPSG:3910"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9999 +x_0=8500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs  ";
// MGI 1901 / Slovenia Grid
Proj4js.defs["EPSG:3911"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs  ";
// MGI 1901 / Slovene National Grid
Proj4js.defs["EPSG:3912"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs  ";
// Puerto Rico / UTM zone 20N
Proj4js.defs["EPSG:3920"]="+proj=utm +zone=20 +ellps=clrk66 +towgs84=11,72,-101,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC42
Proj4js.defs["EPSG:3942"]="+proj=lcc +lat_1=41.25 +lat_2=42.75 +lat_0=42 +lon_0=3 +x_0=1700000 +y_0=1200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC43
Proj4js.defs["EPSG:3943"]="+proj=lcc +lat_1=42.25 +lat_2=43.75 +lat_0=43 +lon_0=3 +x_0=1700000 +y_0=2200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC44
Proj4js.defs["EPSG:3944"]="+proj=lcc +lat_1=43.25 +lat_2=44.75 +lat_0=44 +lon_0=3 +x_0=1700000 +y_0=3200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC45
Proj4js.defs["EPSG:3945"]="+proj=lcc +lat_1=44.25 +lat_2=45.75 +lat_0=45 +lon_0=3 +x_0=1700000 +y_0=4200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC46
Proj4js.defs["EPSG:3946"]="+proj=lcc +lat_1=45.25 +lat_2=46.75 +lat_0=46 +lon_0=3 +x_0=1700000 +y_0=5200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC47
Proj4js.defs["EPSG:3947"]="+proj=lcc +lat_1=46.25 +lat_2=47.75 +lat_0=47 +lon_0=3 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC48
Proj4js.defs["EPSG:3948"]="+proj=lcc +lat_1=47.25 +lat_2=48.75 +lat_0=48 +lon_0=3 +x_0=1700000 +y_0=7200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC49
Proj4js.defs["EPSG:3949"]="+proj=lcc +lat_1=48.25 +lat_2=49.75 +lat_0=49 +lon_0=3 +x_0=1700000 +y_0=8200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGF93 / CC50
Proj4js.defs["EPSG:3950"]="+proj=lcc +lat_1=49.25 +lat_2=50.75 +lat_0=50 +lon_0=3 +x_0=1700000 +y_0=9200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Virginia Lambert
Proj4js.defs["EPSG:3968"]="+proj=lcc +lat_1=37 +lat_2=39.5 +lat_0=36 +lon_0=-79.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Virginia Lambert
Proj4js.defs["EPSG:3969"]="+proj=lcc +lat_1=37 +lat_2=39.5 +lat_0=36 +lon_0=-79.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Virginia Lambert
Proj4js.defs["EPSG:3970"]="+proj=lcc +lat_1=37 +lat_2=39.5 +lat_0=36 +lon_0=-79.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / NSIDC EASE-Grid North
Proj4js.defs["EPSG:3973"]="+proj=laea +lat_0=90 +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / NSIDC EASE-Grid South
Proj4js.defs["EPSG:3974"]="+proj=laea +lat_0=-90 +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / NSIDC EASE-Grid Global
Proj4js.defs["EPSG:3975"]="+proj=cea +lon_0=0 +lat_ts=30 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / NSIDC Sea Ice Polar Stereographic South
Proj4js.defs["EPSG:3976"]="+proj=stere +lat_0=-90 +lat_ts=-70 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// NAD83 / Canada Atlas Lambert
Proj4js.defs["EPSG:3978"]="+proj=lcc +lat_1=49 +lat_2=77 +lat_0=49 +lon_0=-95 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Canada Atlas Lambert
Proj4js.defs["EPSG:3979"]="+proj=lcc +lat_1=49 +lat_2=77 +lat_0=49 +lon_0=-95 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Lambert (deprecated)
Proj4js.defs["EPSG:3985"]="+proj=lcc +lat_1=-6.5 +lat_2=-11.5 +lat_0=9 +lon_0=26 +x_0=500000 +y_0=500000 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Gauss zone A
Proj4js.defs["EPSG:3986"]="+proj=tmerc +lat_0=-9 +lon_0=30 +k=1 +x_0=200000 +y_0=500000 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Gauss zone B
Proj4js.defs["EPSG:3987"]="+proj=tmerc +lat_0=-9 +lon_0=28 +k=1 +x_0=200000 +y_0=500000 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Gauss zone C
Proj4js.defs["EPSG:3988"]="+proj=tmerc +lat_0=-9 +lon_0=26 +k=1 +x_0=200000 +y_0=500000 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Gauss zone D
Proj4js.defs["EPSG:3989"]="+proj=tmerc +lat_0=-9 +lon_0=24 +k=1 +x_0=200000 +y_0=500000 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Puerto Rico State Plane CS of 1927
Proj4js.defs["EPSG:3991"]="+proj=lcc +lat_1=18.43333333333333 +lat_2=18.03333333333333 +lat_0=17.83333333333333 +lon_0=-66.43333333333334 +x_0=152400.3048006096 +y_0=0 +ellps=clrk66 +towgs84=11,72,-101,0,0,0,0 +units=us-ft +no_defs  ";
// Puerto Rico / St. Croix
Proj4js.defs["EPSG:3992"]="+proj=lcc +lat_1=18.43333333333333 +lat_2=18.03333333333333 +lat_0=17.83333333333333 +lon_0=-66.43333333333334 +x_0=152400.3048006096 +y_0=30480.06096012192 +ellps=clrk66 +towgs84=11,72,-101,0,0,0,0 +units=us-ft +no_defs  ";
// Guam 1963 / Guam SPCS
// Unable to translate coordinate system EPSG:3993 into PROJ.4 format.
//
// WGS 84 / Mercator 41
Proj4js.defs["EPSG:3994"]="+proj=merc +lon_0=100 +lat_ts=-41 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / Arctic Polar Stereographic
Proj4js.defs["EPSG:3995"]="+proj=stere +lat_0=90 +lat_ts=71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / IBCAO Polar Stereographic
Proj4js.defs["EPSG:3996"]="+proj=stere +lat_0=90 +lat_ts=75 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / Dubai Local TM
Proj4js.defs["EPSG:3997"]="+proj=tmerc +lat_0=0 +lon_0=55.33333333333334 +k=1 +x_0=500000 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// MOLDREF99 / Moldova TM
Proj4js.defs["EPSG:4026"]="+proj=tmerc +lat_0=0 +lon_0=28.4 +k=0.9999400000000001 +x_0=200000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / TMzn35N
Proj4js.defs["EPSG:4037"]="+proj=utm +zone=35 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / TMzn36N
Proj4js.defs["EPSG:4038"]="+proj=utm +zone=36 +datum=WGS84 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 12
Proj4js.defs["EPSG:4048"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 14
Proj4js.defs["EPSG:4049"]="+proj=tmerc +lat_0=0 +lon_0=14 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 16
Proj4js.defs["EPSG:4050"]="+proj=tmerc +lat_0=0 +lon_0=16 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 18
Proj4js.defs["EPSG:4051"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 20
Proj4js.defs["EPSG:4056"]="+proj=tmerc +lat_0=0 +lon_0=20 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 22
Proj4js.defs["EPSG:4057"]="+proj=tmerc +lat_0=0 +lon_0=22 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 24
Proj4js.defs["EPSG:4058"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 26
Proj4js.defs["EPSG:4059"]="+proj=tmerc +lat_0=0 +lon_0=26 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / Congo TM zone 28
Proj4js.defs["EPSG:4060"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=0.9999 +x_0=500000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / UTM zone 33S
Proj4js.defs["EPSG:4061"]="+proj=utm +zone=33 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / UTM zone 34S
Proj4js.defs["EPSG:4062"]="+proj=utm +zone=34 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGRDC 2005 / UTM zone 35S
Proj4js.defs["EPSG:4063"]="+proj=utm +zone=35 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Chua / UTM zone 23S
Proj4js.defs["EPSG:4071"]="+proj=utm +zone=23 +south +ellps=intl +towgs84=-134,229,-29,0,0,0,0 +units=m +no_defs  ";
// REGCAN95 / UTM zone 27N
Proj4js.defs["EPSG:4082"]="+proj=utm +zone=27 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// REGCAN95 / UTM zone 28N
Proj4js.defs["EPSG:4083"]="+proj=utm +zone=28 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / World Equidistant Cylindrical
// Unable to translate coordinate system EPSG:4087 into PROJ.4 format.
//
// World Equidistant Cylindrical (Sphere)
// Unable to translate coordinate system EPSG:4088 into PROJ.4 format.
//
// ETRS89 / DKTM1
Proj4js.defs["EPSG:4093"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=0.99998 +x_0=200000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / DKTM2
Proj4js.defs["EPSG:4094"]="+proj=tmerc +lat_0=0 +lon_0=10 +k=0.99998 +x_0=400000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / DKTM3
Proj4js.defs["EPSG:4095"]="+proj=tmerc +lat_0=0 +lon_0=11.75 +k=0.99998 +x_0=600000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / DKTM4
Proj4js.defs["EPSG:4096"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=800000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / BLM 59N (ftUS)
Proj4js.defs["EPSG:4217"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD27 / BLM 59N (ftUS)
Proj4js.defs["EPSG:4399"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 60N (ftUS)
Proj4js.defs["EPSG:4400"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 1N (ftUS)
Proj4js.defs["EPSG:4401"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 2N (ftUS)
Proj4js.defs["EPSG:4402"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 3N (ftUS)
Proj4js.defs["EPSG:4403"]="+proj=tmerc +lat_0=0 +lon_0=-165 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 4N (ftUS)
Proj4js.defs["EPSG:4404"]="+proj=tmerc +lat_0=0 +lon_0=-159 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 5N (ftUS)
Proj4js.defs["EPSG:4405"]="+proj=tmerc +lat_0=0 +lon_0=-153 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 6N (ftUS)
Proj4js.defs["EPSG:4406"]="+proj=tmerc +lat_0=0 +lon_0=-147 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 7N (ftUS)
Proj4js.defs["EPSG:4407"]="+proj=tmerc +lat_0=0 +lon_0=-141 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 8N (ftUS)
Proj4js.defs["EPSG:4408"]="+proj=tmerc +lat_0=0 +lon_0=-135 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 9N (ftUS)
Proj4js.defs["EPSG:4409"]="+proj=tmerc +lat_0=0 +lon_0=-129 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 10N (ftUS)
Proj4js.defs["EPSG:4410"]="+proj=tmerc +lat_0=0 +lon_0=-123 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 11N (ftUS)
Proj4js.defs["EPSG:4411"]="+proj=tmerc +lat_0=0 +lon_0=-117 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 12N (ftUS)
Proj4js.defs["EPSG:4412"]="+proj=tmerc +lat_0=0 +lon_0=-111 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 13N (ftUS)
Proj4js.defs["EPSG:4413"]="+proj=tmerc +lat_0=0 +lon_0=-105 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD83(HARN) / Guam Map Grid
Proj4js.defs["EPSG:4414"]="+proj=tmerc +lat_0=13.5 +lon_0=144.75 +k=1 +x_0=100000 +y_0=200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Katanga 1955 / Katanga Lambert
Proj4js.defs["EPSG:4415"]="+proj=lcc +lat_1=-6.5 +lat_2=-11.5 +lat_0=-9 +lon_0=26 +x_0=500000 +y_0=500000 +ellps=clrk66 +towgs84=-103.746,-9.614,-255.95,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 7
Proj4js.defs["EPSG:4417"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// NAD27 / BLM 18N (ftUS)
Proj4js.defs["EPSG:4418"]="+proj=tmerc +lat_0=0 +lon_0=-75 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 19N (ftUS)
Proj4js.defs["EPSG:4419"]="+proj=tmerc +lat_0=0 +lon_0=-69 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD83 / BLM 60N (ftUS)
Proj4js.defs["EPSG:4420"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 1N (ftUS)
Proj4js.defs["EPSG:4421"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 2N (ftUS)
Proj4js.defs["EPSG:4422"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 3N (ftUS)
Proj4js.defs["EPSG:4423"]="+proj=tmerc +lat_0=0 +lon_0=-165 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 4N (ftUS)
Proj4js.defs["EPSG:4424"]="+proj=tmerc +lat_0=0 +lon_0=-159 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 5N (ftUS)
Proj4js.defs["EPSG:4425"]="+proj=tmerc +lat_0=0 +lon_0=-153 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 6N (ftUS)
Proj4js.defs["EPSG:4426"]="+proj=tmerc +lat_0=0 +lon_0=-147 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 7N (ftUS)
Proj4js.defs["EPSG:4427"]="+proj=tmerc +lat_0=0 +lon_0=-141 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 8N (ftUS)
Proj4js.defs["EPSG:4428"]="+proj=tmerc +lat_0=0 +lon_0=-135 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 9N (ftUS)
Proj4js.defs["EPSG:4429"]="+proj=tmerc +lat_0=0 +lon_0=-129 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 10N (ftUS)
Proj4js.defs["EPSG:4430"]="+proj=tmerc +lat_0=0 +lon_0=-123 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 11N (ftUS)
Proj4js.defs["EPSG:4431"]="+proj=tmerc +lat_0=0 +lon_0=-117 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 12N (ftUS)
Proj4js.defs["EPSG:4432"]="+proj=tmerc +lat_0=0 +lon_0=-111 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 13N (ftUS)
Proj4js.defs["EPSG:4433"]="+proj=tmerc +lat_0=0 +lon_0=-105 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// Pulkovo 1942(83) / 3-degree Gauss-Kruger zone 8
Proj4js.defs["EPSG:4434"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=26,-121,-78,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Puerto Rico and Virgin Is.
Proj4js.defs["EPSG:4437"]="+proj=lcc +lat_1=18.43333333333333 +lat_2=18.03333333333333 +lat_0=17.83333333333333 +lon_0=-66.43333333333334 +x_0=200000 +y_0=200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / BLM 18N (ftUS)
Proj4js.defs["EPSG:4438"]="+proj=tmerc +lat_0=0 +lon_0=-75 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 19N (ftUS)
Proj4js.defs["EPSG:4439"]="+proj=tmerc +lat_0=0 +lon_0=-69 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD27 / Pennsylvania South
Proj4js.defs["EPSG:4455"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New York Long Island
Proj4js.defs["EPSG:4456"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.5 +lon_0=-74 +x_0=609601.2192024384 +y_0=30480.06096012192 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD83 / South Dakota North (ftUS)
Proj4js.defs["EPSG:4457"]="+proj=lcc +lat_1=45.68333333333333 +lat_2=44.41666666666666 +lat_0=43.83333333333334 +lon_0=-100 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// WGS 84 / Australian Centre for Remote Sensing Lambert
Proj4js.defs["EPSG:4462"]="+proj=lcc +lat_1=-18 +lat_2=-36 +lat_0=-27 +lon_0=132 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// RGSPM06 / UTM zone 21N
Proj4js.defs["EPSG:4467"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGM04 / UTM zone 38S
Proj4js.defs["EPSG:4471"]="+proj=utm +zone=38 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Cadastre 1997 / UTM zone 38S
Proj4js.defs["EPSG:4474"]="+proj=utm +zone=38 +south +ellps=intl +towgs84=-382,-59,-262,0,0,0,0 +units=m +no_defs  ";
// Mexican Datum of 1993 / UTM zone 11N
Proj4js.defs["EPSG:4484"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mexican Datum of 1993 / UTM zone 12N
Proj4js.defs["EPSG:4485"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mexican Datum of 1993 / UTM zone 13N
Proj4js.defs["EPSG:4486"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mexican Datum of 1993 / UTM zone 14N
Proj4js.defs["EPSG:4487"]="+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mexican Datum of 1993 / UTM zone 15N
Proj4js.defs["EPSG:4488"]="+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Mexican Datum of 1993 / UTM zone 16N
Proj4js.defs["EPSG:4489"]="+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 13
Proj4js.defs["EPSG:4491"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=13500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 14
Proj4js.defs["EPSG:4492"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=14500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 15
Proj4js.defs["EPSG:4493"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=15500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 16
Proj4js.defs["EPSG:4494"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=16500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 17
Proj4js.defs["EPSG:4495"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=17500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 18
Proj4js.defs["EPSG:4496"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 19
Proj4js.defs["EPSG:4497"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 20
Proj4js.defs["EPSG:4498"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 21
Proj4js.defs["EPSG:4499"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=21500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 22
Proj4js.defs["EPSG:4500"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=22500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger zone 23
Proj4js.defs["EPSG:4501"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=23500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 75E
Proj4js.defs["EPSG:4502"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 81E
Proj4js.defs["EPSG:4503"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 87E
Proj4js.defs["EPSG:4504"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 93E
Proj4js.defs["EPSG:4505"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 99E
Proj4js.defs["EPSG:4506"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 105E
Proj4js.defs["EPSG:4507"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 111E
Proj4js.defs["EPSG:4508"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 117E
Proj4js.defs["EPSG:4509"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 123E
Proj4js.defs["EPSG:4510"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 129E
Proj4js.defs["EPSG:4511"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / Gauss-Kruger CM 135E
Proj4js.defs["EPSG:4512"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 25
Proj4js.defs["EPSG:4513"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=25500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 26
Proj4js.defs["EPSG:4514"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=26500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 27
Proj4js.defs["EPSG:4515"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=27500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 28
Proj4js.defs["EPSG:4516"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=28500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 29
Proj4js.defs["EPSG:4517"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=29500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 30
Proj4js.defs["EPSG:4518"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=30500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 31
Proj4js.defs["EPSG:4519"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=31500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 32
Proj4js.defs["EPSG:4520"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=32500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 33
Proj4js.defs["EPSG:4521"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=33500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 34
Proj4js.defs["EPSG:4522"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=34500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 35
Proj4js.defs["EPSG:4523"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=35500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 36
Proj4js.defs["EPSG:4524"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=36500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 37
Proj4js.defs["EPSG:4525"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=37500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 38
Proj4js.defs["EPSG:4526"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=38500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 39
Proj4js.defs["EPSG:4527"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=39500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 40
Proj4js.defs["EPSG:4528"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=40500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 41
Proj4js.defs["EPSG:4529"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=41500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 42
Proj4js.defs["EPSG:4530"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=42500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 43
Proj4js.defs["EPSG:4531"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=43500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 44
Proj4js.defs["EPSG:4532"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=44500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger zone 45
Proj4js.defs["EPSG:4533"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=45500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 75E
Proj4js.defs["EPSG:4534"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 78E
Proj4js.defs["EPSG:4535"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 81E
Proj4js.defs["EPSG:4536"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 84E
Proj4js.defs["EPSG:4537"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 87E
Proj4js.defs["EPSG:4538"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 90E
Proj4js.defs["EPSG:4539"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 93E
Proj4js.defs["EPSG:4540"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 96E
Proj4js.defs["EPSG:4541"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 99E
Proj4js.defs["EPSG:4542"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 102E
Proj4js.defs["EPSG:4543"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 105E
Proj4js.defs["EPSG:4544"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 108E
Proj4js.defs["EPSG:4545"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 111E
Proj4js.defs["EPSG:4546"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 114E
Proj4js.defs["EPSG:4547"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 117E
Proj4js.defs["EPSG:4548"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 120E
Proj4js.defs["EPSG:4549"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 123E
Proj4js.defs["EPSG:4550"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 126E
Proj4js.defs["EPSG:4551"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 129E
Proj4js.defs["EPSG:4552"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 132E
Proj4js.defs["EPSG:4553"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// CGCS2000 / 3-degree Gauss-Kruger CM 135E
Proj4js.defs["EPSG:4554"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs  ";
// RRAF 1991 / UTM zone 20N
Proj4js.defs["EPSG:4559"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 13
Proj4js.defs["EPSG:4568"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=13500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 14
Proj4js.defs["EPSG:4569"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=14500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 15
Proj4js.defs["EPSG:4570"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=15500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 16
Proj4js.defs["EPSG:4571"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=16500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 17
Proj4js.defs["EPSG:4572"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=17500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 18
Proj4js.defs["EPSG:4573"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 19
Proj4js.defs["EPSG:4574"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 20
Proj4js.defs["EPSG:4575"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 21
Proj4js.defs["EPSG:4576"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=21500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 22
Proj4js.defs["EPSG:4577"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=22500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger zone 23
Proj4js.defs["EPSG:4578"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=23500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 75E
Proj4js.defs["EPSG:4579"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 81E
Proj4js.defs["EPSG:4580"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 87E
Proj4js.defs["EPSG:4581"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 93E
Proj4js.defs["EPSG:4582"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 99E
Proj4js.defs["EPSG:4583"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 105E
Proj4js.defs["EPSG:4584"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 111E
Proj4js.defs["EPSG:4585"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 117E
Proj4js.defs["EPSG:4586"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 123E
Proj4js.defs["EPSG:4587"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 129E
Proj4js.defs["EPSG:4588"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / Gauss-Kruger CM 135E
Proj4js.defs["EPSG:4589"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// ETRS89 / UTM zone N32
Proj4js.defs["EPSG:4647"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=32500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 25
Proj4js.defs["EPSG:4652"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=25500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 26
Proj4js.defs["EPSG:4653"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=26500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 27
Proj4js.defs["EPSG:4654"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=27500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 28
Proj4js.defs["EPSG:4655"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=28500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 29
Proj4js.defs["EPSG:4656"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=29500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 30
Proj4js.defs["EPSG:4766"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=30500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 31
Proj4js.defs["EPSG:4767"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=31500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 32
Proj4js.defs["EPSG:4768"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=32500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 33
Proj4js.defs["EPSG:4769"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=33500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 34
Proj4js.defs["EPSG:4770"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=34500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 35
Proj4js.defs["EPSG:4771"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=35500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 36
Proj4js.defs["EPSG:4772"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=36500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 37
Proj4js.defs["EPSG:4773"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=37500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 38
Proj4js.defs["EPSG:4774"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=38500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 39
Proj4js.defs["EPSG:4775"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=39500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 40
Proj4js.defs["EPSG:4776"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=40500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 41
Proj4js.defs["EPSG:4777"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=41500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 42
Proj4js.defs["EPSG:4778"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=42500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 43
Proj4js.defs["EPSG:4779"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=43500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 44
Proj4js.defs["EPSG:4780"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=44500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger zone 45
Proj4js.defs["EPSG:4781"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=45500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 75E
Proj4js.defs["EPSG:4782"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 78E
Proj4js.defs["EPSG:4783"]="+proj=tmerc +lat_0=0 +lon_0=78 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 81E
Proj4js.defs["EPSG:4784"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 84E
Proj4js.defs["EPSG:4785"]="+proj=tmerc +lat_0=0 +lon_0=84 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 87E
Proj4js.defs["EPSG:4786"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 90E
Proj4js.defs["EPSG:4787"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 93E
Proj4js.defs["EPSG:4788"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 96E
Proj4js.defs["EPSG:4789"]="+proj=tmerc +lat_0=0 +lon_0=96 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 99E
Proj4js.defs["EPSG:4790"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 102E
Proj4js.defs["EPSG:4791"]="+proj=tmerc +lat_0=0 +lon_0=102 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 105E
Proj4js.defs["EPSG:4792"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 108E
Proj4js.defs["EPSG:4793"]="+proj=tmerc +lat_0=0 +lon_0=108 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 111E
Proj4js.defs["EPSG:4794"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 114E
Proj4js.defs["EPSG:4795"]="+proj=tmerc +lat_0=0 +lon_0=114 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 117E
Proj4js.defs["EPSG:4796"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 120E
Proj4js.defs["EPSG:4797"]="+proj=tmerc +lat_0=0 +lon_0=120 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 123E
Proj4js.defs["EPSG:4798"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 126E
Proj4js.defs["EPSG:4799"]="+proj=tmerc +lat_0=0 +lon_0=126 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 129E
Proj4js.defs["EPSG:4800"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 132E
Proj4js.defs["EPSG:4812"]="+proj=tmerc +lat_0=0 +lon_0=132 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// New Beijing / 3-degree Gauss-Kruger CM 135E
Proj4js.defs["EPSG:4822"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +units=m +no_defs  ";
// WGS 84 / Cape Verde National
Proj4js.defs["EPSG:4826"]="+proj=lcc +lat_1=15 +lat_2=16.66666666666667 +lat_0=15.83333333333333 +lon_0=-24 +x_0=161587.83 +y_0=128511.202 +datum=WGS84 +units=m +no_defs  ";
// ETRS89 / LCC Germany (N-E)
Proj4js.defs["EPSG:4839"]="+proj=lcc +lat_1=48.66666666666666 +lat_2=53.66666666666666 +lat_0=51 +lon_0=10.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 5 (deprecated)
Proj4js.defs["EPSG:4855"]="+proj=tmerc +lat_0=0 +lon_0=5.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 6 (deprecated)
Proj4js.defs["EPSG:4856"]="+proj=tmerc +lat_0=0 +lon_0=6.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 7 (deprecated)
Proj4js.defs["EPSG:4857"]="+proj=tmerc +lat_0=0 +lon_0=7.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 8 (deprecated)
Proj4js.defs["EPSG:4858"]="+proj=tmerc +lat_0=0 +lon_0=8.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 9 (deprecated)
Proj4js.defs["EPSG:4859"]="+proj=tmerc +lat_0=0 +lon_0=9.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 10 (deprecated)
Proj4js.defs["EPSG:4860"]="+proj=tmerc +lat_0=0 +lon_0=10.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 11 (deprecated)
Proj4js.defs["EPSG:4861"]="+proj=tmerc +lat_0=0 +lon_0=11.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 12 (deprecated)
Proj4js.defs["EPSG:4862"]="+proj=tmerc +lat_0=0 +lon_0=12.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 13 (deprecated)
Proj4js.defs["EPSG:4863"]="+proj=tmerc +lat_0=0 +lon_0=13.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 14 (deprecated)
Proj4js.defs["EPSG:4864"]="+proj=tmerc +lat_0=0 +lon_0=14.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 15 (deprecated)
Proj4js.defs["EPSG:4865"]="+proj=tmerc +lat_0=0 +lon_0=15.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 16 (deprecated)
Proj4js.defs["EPSG:4866"]="+proj=tmerc +lat_0=0 +lon_0=16.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 17 (deprecated)
Proj4js.defs["EPSG:4867"]="+proj=tmerc +lat_0=0 +lon_0=17.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 18 (deprecated)
Proj4js.defs["EPSG:4868"]="+proj=tmerc +lat_0=0 +lon_0=18.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 19 (deprecated)
Proj4js.defs["EPSG:4869"]="+proj=tmerc +lat_0=0 +lon_0=19.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 20 (deprecated)
Proj4js.defs["EPSG:4870"]="+proj=tmerc +lat_0=0 +lon_0=20.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 21 (deprecated)
Proj4js.defs["EPSG:4871"]="+proj=tmerc +lat_0=0 +lon_0=21.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 22 (deprecated)
Proj4js.defs["EPSG:4872"]="+proj=tmerc +lat_0=0 +lon_0=22.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 23 (deprecated)
Proj4js.defs["EPSG:4873"]="+proj=tmerc +lat_0=0 +lon_0=23.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 24 (deprecated)
Proj4js.defs["EPSG:4874"]="+proj=tmerc +lat_0=0 +lon_0=24.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 25 (deprecated)
Proj4js.defs["EPSG:4875"]="+proj=tmerc +lat_0=0 +lon_0=25.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 26 (deprecated)
Proj4js.defs["EPSG:4876"]="+proj=tmerc +lat_0=0 +lon_0=26.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 27 (deprecated)
Proj4js.defs["EPSG:4877"]="+proj=tmerc +lat_0=0 +lon_0=27.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 28 (deprecated)
Proj4js.defs["EPSG:4878"]="+proj=tmerc +lat_0=0 +lon_0=28.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 29 (deprecated)
Proj4js.defs["EPSG:4879"]="+proj=tmerc +lat_0=0 +lon_0=29.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 30 (deprecated)
Proj4js.defs["EPSG:4880"]="+proj=tmerc +lat_0=0 +lon_0=30.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// PTRA08 / UTM zone 25N
Proj4js.defs["EPSG:5014"]="+proj=utm +zone=25 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// PTRA08 / UTM zone 26N
Proj4js.defs["EPSG:5015"]="+proj=utm +zone=26 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// PTRA08 / UTM zone 28N
Proj4js.defs["EPSG:5016"]="+proj=utm +zone=28 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Lisbon 1890 / Portugal Bonne New
// Unable to translate coordinate system EPSG:5017 into PROJ.4 format.
//
// Lisbon / Portuguese Grid New
Proj4js.defs["EPSG:5018"]="+proj=tmerc +lat_0=39.66666666666666 +lon_0=-8.131906111111112 +k=1 +x_0=0 +y_0=0 +ellps=intl +towgs84=-304.046,-60.576,103.64,0,0,0,0 +units=m +no_defs  ";
// WGS 84 / UPS North (E,N)
Proj4js.defs["EPSG:5041"]="+proj=stere +lat_0=90 +lat_ts=90 +lon_0=0 +k=0.994 +x_0=2000000 +y_0=2000000 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UPS South (E,N)
Proj4js.defs["EPSG:5042"]="+proj=stere +lat_0=-90 +lat_ts=-90 +lon_0=0 +k=0.994 +x_0=2000000 +y_0=2000000 +datum=WGS84 +units=m +no_defs  ";
// ETRS89 / TM35FIN(N,E)
Proj4js.defs["EPSG:5048"]="+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Conus Albers
Proj4js.defs["EPSG:5069"]="+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-96 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD83 / Conus Albers
Proj4js.defs["EPSG:5070"]="+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-96 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Conus Albers
Proj4js.defs["EPSG:5071"]="+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-96 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Conus Albers
Proj4js.defs["EPSG:5072"]="+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-96 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 5
Proj4js.defs["EPSG:5105"]="+proj=tmerc +lat_0=58 +lon_0=5.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 6
Proj4js.defs["EPSG:5106"]="+proj=tmerc +lat_0=58 +lon_0=6.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 7
Proj4js.defs["EPSG:5107"]="+proj=tmerc +lat_0=58 +lon_0=7.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 8
Proj4js.defs["EPSG:5108"]="+proj=tmerc +lat_0=58 +lon_0=8.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 9
Proj4js.defs["EPSG:5109"]="+proj=tmerc +lat_0=58 +lon_0=9.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 10
Proj4js.defs["EPSG:5110"]="+proj=tmerc +lat_0=58 +lon_0=10.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 11
Proj4js.defs["EPSG:5111"]="+proj=tmerc +lat_0=58 +lon_0=11.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 12
Proj4js.defs["EPSG:5112"]="+proj=tmerc +lat_0=58 +lon_0=12.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 13
Proj4js.defs["EPSG:5113"]="+proj=tmerc +lat_0=58 +lon_0=13.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 14
Proj4js.defs["EPSG:5114"]="+proj=tmerc +lat_0=58 +lon_0=14.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 15
Proj4js.defs["EPSG:5115"]="+proj=tmerc +lat_0=58 +lon_0=15.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 16
Proj4js.defs["EPSG:5116"]="+proj=tmerc +lat_0=58 +lon_0=16.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 17
Proj4js.defs["EPSG:5117"]="+proj=tmerc +lat_0=58 +lon_0=17.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 18
Proj4js.defs["EPSG:5118"]="+proj=tmerc +lat_0=58 +lon_0=18.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 19
Proj4js.defs["EPSG:5119"]="+proj=tmerc +lat_0=58 +lon_0=19.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 20
Proj4js.defs["EPSG:5120"]="+proj=tmerc +lat_0=58 +lon_0=20.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 21
Proj4js.defs["EPSG:5121"]="+proj=tmerc +lat_0=58 +lon_0=21.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 22
Proj4js.defs["EPSG:5122"]="+proj=tmerc +lat_0=58 +lon_0=22.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 23
Proj4js.defs["EPSG:5123"]="+proj=tmerc +lat_0=58 +lon_0=23.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 24
Proj4js.defs["EPSG:5124"]="+proj=tmerc +lat_0=58 +lon_0=24.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 25
Proj4js.defs["EPSG:5125"]="+proj=tmerc +lat_0=58 +lon_0=25.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 26
Proj4js.defs["EPSG:5126"]="+proj=tmerc +lat_0=58 +lon_0=26.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 27
Proj4js.defs["EPSG:5127"]="+proj=tmerc +lat_0=58 +lon_0=27.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 28
Proj4js.defs["EPSG:5128"]="+proj=tmerc +lat_0=58 +lon_0=28.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 29
Proj4js.defs["EPSG:5129"]="+proj=tmerc +lat_0=58 +lon_0=29.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / NTM zone 30
Proj4js.defs["EPSG:5130"]="+proj=tmerc +lat_0=58 +lon_0=30.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korean 1985 / East Sea Belt
Proj4js.defs["EPSG:5167"]="+proj=tmerc +lat_0=38 +lon_0=131 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Central Belt Jeju
Proj4js.defs["EPSG:5168"]="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=550000 +ellps=bessel +units=m +no_defs  ";
// Tokyo 1892 / Korea West Belt
Proj4js.defs["EPSG:5169"]="+proj=tmerc +lat_0=38 +lon_0=125 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Tokyo 1892 / Korea Central Belt
Proj4js.defs["EPSG:5170"]="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Tokyo 1892 / Korea East Belt
Proj4js.defs["EPSG:5171"]="+proj=tmerc +lat_0=38 +lon_0=129 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Tokyo 1892 / Korea East Sea Belt
Proj4js.defs["EPSG:5172"]="+proj=tmerc +lat_0=38 +lon_0=131 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Modified West Belt
Proj4js.defs["EPSG:5173"]="+proj=tmerc +lat_0=38 +lon_0=125.0028902777778 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Modified Central Belt
Proj4js.defs["EPSG:5174"]="+proj=tmerc +lat_0=38 +lon_0=127.0028902777778 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Modified Central Belt Jeju
Proj4js.defs["EPSG:5175"]="+proj=tmerc +lat_0=38 +lon_0=127.0028902777778 +k=1 +x_0=200000 +y_0=550000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Modified East Belt
Proj4js.defs["EPSG:5176"]="+proj=tmerc +lat_0=38 +lon_0=129.0028902777778 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Modified East Sea Belt
Proj4js.defs["EPSG:5177"]="+proj=tmerc +lat_0=38 +lon_0=131.0028902777778 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs  ";
// Korean 1985 / Unified CS
Proj4js.defs["EPSG:5178"]="+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=bessel +units=m +no_defs  ";
// Korea 2000 / Unified CS
Proj4js.defs["EPSG:5179"]="+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / West Belt
Proj4js.defs["EPSG:5180"]="+proj=tmerc +lat_0=38 +lon_0=125 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / Central Belt
Proj4js.defs["EPSG:5181"]="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / Central Belt Jeju
Proj4js.defs["EPSG:5182"]="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=550000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / East Belt
Proj4js.defs["EPSG:5183"]="+proj=tmerc +lat_0=38 +lon_0=129 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / East Sea Belt
Proj4js.defs["EPSG:5184"]="+proj=tmerc +lat_0=38 +lon_0=131 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / West Belt 2010
Proj4js.defs["EPSG:5185"]="+proj=tmerc +lat_0=38 +lon_0=125 +k=1 +x_0=200000 +y_0=600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / Central Belt 2010
Proj4js.defs["EPSG:5186"]="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / East Belt 2010
Proj4js.defs["EPSG:5187"]="+proj=tmerc +lat_0=38 +lon_0=129 +k=1 +x_0=200000 +y_0=600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Korea 2000 / East Sea Belt 2010
Proj4js.defs["EPSG:5188"]="+proj=tmerc +lat_0=38 +lon_0=131 +k=1 +x_0=200000 +y_0=600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// S-JTSK (Ferro) / Krovak East North
// Unable to translate coordinate system EPSG:5221 into PROJ.4 format.
//
// WGS 84 / Gabon TM
Proj4js.defs["EPSG:5223"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.9996 +x_0=500000 +y_0=500000 +datum=WGS84 +units=m +no_defs  ";
// S-JTSK/05 (Ferro) / Modified Krovak
// Unable to translate coordinate system EPSG:5224 into PROJ.4 format.
//
// S-JTSK/05 (Ferro) / Modified Krovak East North
// Unable to translate coordinate system EPSG:5225 into PROJ.4 format.
//
// Kandawala / Sri Lanka Grid
Proj4js.defs["EPSG:5234"]="+proj=tmerc +lat_0=7.000480277777778 +lon_0=80.77171111111112 +k=0.9999238418 +x_0=200000 +y_0=200000 +a=6377276.345 +b=6356075.41314024 +towgs84=-97,787,86,0,0,0,0 +units=m +no_defs  ";
// SLD99 / Sri Lanka Grid 1999
Proj4js.defs["EPSG:5235"]="+proj=tmerc +lat_0=7.000471527777778 +lon_0=80.77171308333334 +k=0.9999238418 +x_0=500000 +y_0=500000 +a=6377276.345 +b=6356075.41314024 +towgs84=-0.293,766.95,87.713,0.195704,1.69507,3.47302,-0.039338 +units=m +no_defs  ";
// ETRS89 / LCC Germany (E-N)
Proj4js.defs["EPSG:5243"]="+proj=lcc +lat_1=48.66666666666666 +lat_2=53.66666666666666 +lat_0=51 +lon_0=10.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDBD2009 / Brunei BRSO
Proj4js.defs["EPSG:5247"]="+proj=omerc +lat_0=4 +lonc=115 +alpha=53.31580995 +k=0.99984 +x_0=0 +y_0=0 +gamma=53.13010236111111 +ellps=GRS80 +units=m +no_defs  ";
// TUREF / TM27
Proj4js.defs["EPSG:5253"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / TM30
Proj4js.defs["EPSG:5254"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / TM33
Proj4js.defs["EPSG:5255"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / TM36
Proj4js.defs["EPSG:5256"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / TM39
Proj4js.defs["EPSG:5257"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / TM42
Proj4js.defs["EPSG:5258"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / TM45
Proj4js.defs["EPSG:5259"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Bhutan National Grid
Proj4js.defs["EPSG:5266"]="+proj=tmerc +lat_0=0 +lon_0=90 +k=1 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 9
Proj4js.defs["EPSG:5269"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=9500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 10
Proj4js.defs["EPSG:5270"]="+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=10500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 11
Proj4js.defs["EPSG:5271"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=11500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 12
Proj4js.defs["EPSG:5272"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=12500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 13
Proj4js.defs["EPSG:5273"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=13500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 14
Proj4js.defs["EPSG:5274"]="+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=14500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// TUREF / 3-degree Gauss-Kruger zone 15
Proj4js.defs["EPSG:5275"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=15500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Bumthang TM
Proj4js.defs["EPSG:5292"]="+proj=tmerc +lat_0=0 +lon_0=90.73333333333333 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Chhukha TM
Proj4js.defs["EPSG:5293"]="+proj=tmerc +lat_0=0 +lon_0=89.55 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Dagana TM
Proj4js.defs["EPSG:5294"]="+proj=tmerc +lat_0=0 +lon_0=89.84999999999999 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Gasa TM
Proj4js.defs["EPSG:5295"]="+proj=tmerc +lat_0=0 +lon_0=90.03333333333333 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Ha TM
Proj4js.defs["EPSG:5296"]="+proj=tmerc +lat_0=0 +lon_0=90.15000000000001 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Lhuentse TM
Proj4js.defs["EPSG:5297"]="+proj=tmerc +lat_0=0 +lon_0=91.13333333333334 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Mongar TM
Proj4js.defs["EPSG:5298"]="+proj=tmerc +lat_0=0 +lon_0=91.23333333333333 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Paro TM
Proj4js.defs["EPSG:5299"]="+proj=tmerc +lat_0=0 +lon_0=89.34999999999999 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Pemagatshel TM
Proj4js.defs["EPSG:5300"]="+proj=tmerc +lat_0=0 +lon_0=91.34999999999999 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Punakha TM
Proj4js.defs["EPSG:5301"]="+proj=tmerc +lat_0=0 +lon_0=89.84999999999999 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Samdrup Jongkhar TM
Proj4js.defs["EPSG:5302"]="+proj=tmerc +lat_0=0 +lon_0=91.56666666666666 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Samtse TM
Proj4js.defs["EPSG:5303"]="+proj=tmerc +lat_0=0 +lon_0=89.06666666666666 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Sarpang TM
Proj4js.defs["EPSG:5304"]="+proj=tmerc +lat_0=0 +lon_0=90.26666666666667 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Thimphu TM
Proj4js.defs["EPSG:5305"]="+proj=tmerc +lat_0=0 +lon_0=89.55 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Trashigang TM
Proj4js.defs["EPSG:5306"]="+proj=tmerc +lat_0=0 +lon_0=91.75 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Trongsa TM
Proj4js.defs["EPSG:5307"]="+proj=tmerc +lat_0=0 +lon_0=90.5 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Tsirang TM
Proj4js.defs["EPSG:5308"]="+proj=tmerc +lat_0=0 +lon_0=90.16666666666667 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Wangdue Phodrang TM
Proj4js.defs["EPSG:5309"]="+proj=tmerc +lat_0=0 +lon_0=90.11666666666666 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Yangtse TM
Proj4js.defs["EPSG:5310"]="+proj=tmerc +lat_0=0 +lon_0=91.56666666666666 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DRUKREF 03 / Zhemgang TM
Proj4js.defs["EPSG:5311"]="+proj=tmerc +lat_0=0 +lon_0=90.86666666666666 +k=1 +x_0=250000 +y_0=-2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / Faroe TM
Proj4js.defs["EPSG:5316"]="+proj=tmerc +lat_0=0 +lon_0=-7 +k=0.999997 +x_0=200000 +y_0=-6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Teranet Ontario Lambert
Proj4js.defs["EPSG:5320"]="+proj=lcc +lat_1=44.5 +lat_2=54.5 +lat_0=0 +lon_0=-84 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / Teranet Ontario Lambert
Proj4js.defs["EPSG:5321"]="+proj=lcc +lat_1=44.5 +lat_2=54.5 +lat_0=0 +lon_0=-84 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ISN2004 / Lambert 2004
Proj4js.defs["EPSG:5325"]="+proj=lcc +lat_1=64.25 +lat_2=65.75 +lat_0=65 +lon_0=-19 +x_0=1700000 +y_0=300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Segara (Jakarta) / NEIEZ
Proj4js.defs["EPSG:5329"]="+proj=merc +lon_0=3.192280555555556 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-403,684,41,0,0,0,0 +pm=jakarta +units=m +no_defs  ";
// Batavia (Jakarta) / NEIEZ
Proj4js.defs["EPSG:5330"]="+proj=merc +lon_0=3.192280555555556 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +pm=jakarta +units=m +no_defs  ";
// Makassar (Jakarta) / NEIEZ
Proj4js.defs["EPSG:5331"]="+proj=merc +lon_0=3.192280555555556 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-587.8,519.75,145.76,0,0,0,0 +pm=jakarta +units=m +no_defs  ";
// Aratu / UTM zone 25S
Proj4js.defs["EPSG:5337"]="+proj=utm +zone=25 +south +ellps=intl +towgs84=-151.99,287.04,-147.45,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 1
Proj4js.defs["EPSG:5343"]="+proj=tmerc +lat_0=-90 +lon_0=-72 +k=1 +x_0=1500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 2
Proj4js.defs["EPSG:5344"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 3
Proj4js.defs["EPSG:5345"]="+proj=tmerc +lat_0=-90 +lon_0=-66 +k=1 +x_0=3500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 4
Proj4js.defs["EPSG:5346"]="+proj=tmerc +lat_0=-90 +lon_0=-63 +k=1 +x_0=4500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 5
Proj4js.defs["EPSG:5347"]="+proj=tmerc +lat_0=-90 +lon_0=-60 +k=1 +x_0=5500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 6
Proj4js.defs["EPSG:5348"]="+proj=tmerc +lat_0=-90 +lon_0=-57 +k=1 +x_0=6500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 2007 / Argentina 7
Proj4js.defs["EPSG:5349"]="+proj=tmerc +lat_0=-90 +lon_0=-54 +k=1 +x_0=7500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MARGEN / UTM zone 20S
Proj4js.defs["EPSG:5355"]="+proj=utm +zone=20 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MARGEN / UTM zone 19S
Proj4js.defs["EPSG:5356"]="+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// MARGEN / UTM zone 21S
Proj4js.defs["EPSG:5357"]="+proj=utm +zone=21 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS-Chile / UTM zone 19S
Proj4js.defs["EPSG:5361"]="+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS-Chile / UTM zone 18S
Proj4js.defs["EPSG:5362"]="+proj=utm +zone=18 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// CR05 / CRTM05
Proj4js.defs["EPSG:5367"]="+proj=tmerc +lat_0=0 +lon_0=-84 +k=0.9999 +x_0=500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS-ROU98 / UTM zone 21S
Proj4js.defs["EPSG:5382"]="+proj=utm +zone=21 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS-ROU98 / UTM zone 22S
Proj4js.defs["EPSG:5383"]="+proj=utm +zone=22 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Peru96 / UTM zone 18S
Proj4js.defs["EPSG:5387"]="+proj=utm +zone=18 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Peru96 / UTM zone 17S
Proj4js.defs["EPSG:5388"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Peru96 / UTM zone 19S
Proj4js.defs["EPSG:5389"]="+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 26S
Proj4js.defs["EPSG:5396"]="+proj=utm +zone=26 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / Costa Rica Norte
Proj4js.defs["EPSG:5456"]="+proj=lcc +lat_1=10.46666666666667 +lat_0=10.46666666666667 +lon_0=-84.33333333333333 +k_0=0.99995696 +x_0=500000 +y_0=271820.522 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / Costa Rica Sur
Proj4js.defs["EPSG:5457"]="+proj=lcc +lat_1=9 +lat_0=9 +lon_0=-83.66666666666667 +k_0=0.99995696 +x_0=500000 +y_0=327987.436 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / Guatemala Norte (deprecated)
Proj4js.defs["EPSG:5458"]="+proj=lcc +lat_1=16.81666666666667 +lat_0=16.81666666666667 +lon_0=-90.33333333333333 +k_0=0.99992226 +x_0=500000 +y_0=292209.579 +datum=NAD27 +units=m +no_defs  ";
// Ocotepeque 1935 / Guatemala Sur
Proj4js.defs["EPSG:5459"]="+proj=lcc +lat_1=14.9 +lat_0=14.9 +lon_0=-90.33333333333333 +k_0=0.99989906 +x_0=500000 +y_0=325992.681 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / El Salvador Lambert
Proj4js.defs["EPSG:5460"]="+proj=lcc +lat_1=13.78333333333333 +lat_0=13.78333333333333 +lon_0=-89 +k_0=0.99996704 +x_0=500000 +y_0=295809.184 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / Nicaragua Norte
Proj4js.defs["EPSG:5461"]="+proj=lcc +lat_1=13.86666666666667 +lat_0=13.86666666666667 +lon_0=-85.5 +k_0=0.99990314 +x_0=500000 +y_0=359891.816 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / Nicaragua Sur
Proj4js.defs["EPSG:5462"]="+proj=lcc +lat_1=11.73333333333333 +lat_0=11.73333333333333 +lon_0=-85.5 +k_0=0.9999222800000001 +x_0=500000 +y_0=288876.327 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 17N
Proj4js.defs["EPSG:5463"]="+proj=utm +zone=17 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// Sibun Gorge 1922 / Colony Grid
Proj4js.defs["EPSG:5466"]="+proj=tmerc +lat_0=17.06124194444444 +lon_0=-88.6318575 +k=1 +x_0=66220.02833082761 +y_0=135779.5099885299 +a=6378293.645208759 +b=6356617.987679838 +units=m +no_defs  ";
// Panama-Colon 1911 / Panama Lambert
Proj4js.defs["EPSG:5469"]="+proj=lcc +lat_1=8.416666666666666 +lat_0=8.416666666666666 +lon_0=-80 +k_0=0.99989909 +x_0=500000 +y_0=294865.303 +ellps=clrk66 +units=m +no_defs  ";
// Panama-Colon 1911 / Panama Polyconic
Proj4js.defs["EPSG:5472"]="+proj=poly +lat_0=8.25 +lon_0=-81 +x_0=914391.7962 +y_0=999404.7217154861 +ellps=clrk66 +to_meter=0.9143917962 +no_defs  ";
// RSRGD2000 / MSLC2000
Proj4js.defs["EPSG:5479"]="+proj=lcc +lat_1=-76.66666666666667 +lat_2=-79.33333333333333 +lat_0=-78 +lon_0=163 +x_0=7000000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RSRGD2000 / BCLC2000
Proj4js.defs["EPSG:5480"]="+proj=lcc +lat_1=-73.66666666666667 +lat_2=-75.33333333333333 +lat_0=-74.5 +lon_0=165 +x_0=5000000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RSRGD2000 / PCLC2000
Proj4js.defs["EPSG:5481"]="+proj=lcc +lat_1=-70.66666666666667 +lat_2=-72.33333333333333 +lat_0=-71.5 +lon_0=166 +x_0=3000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RSRGD2000 / RSPS2000
Proj4js.defs["EPSG:5482"]="+proj=stere +lat_0=-90 +lat_ts=-90 +lon_0=180 +k=0.994 +x_0=5000000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// RGAF09 / UTM zone 20N
Proj4js.defs["EPSG:5490"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// S-JTSK / Krovak
Proj4js.defs["EPSG:5513"]="+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=0 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=589,76,480,0,0,0,0 +units=m +no_defs  ";
// S-JTSK / Krovak East North
// Unable to translate coordinate system EPSG:5514 into PROJ.4 format.
//
// S-JTSK/05 / Modified Krovak
// Unable to translate coordinate system EPSG:5515 into PROJ.4 format.
//
// S-JTSK/05 / Modified Krovak East North
// Unable to translate coordinate system EPSG:5516 into PROJ.4 format.
//
// CI1971 / Chatham Islands Map Grid
Proj4js.defs["EPSG:5518"]="+proj=tmerc +lat_0=-44 +lon_0=-176.5 +k=1 +x_0=350000 +y_0=650000 +ellps=intl +towgs84=175,-38,113,0,0,0,0 +units=m +no_defs  ";
// CI1979 / Chatham Islands Map Grid
Proj4js.defs["EPSG:5519"]="+proj=tmerc +lat_0=-44 +lon_0=-176.5 +k=1 +x_0=350000 +y_0=650000 +ellps=intl +towgs84=174.05,-25.49,112.57,-0,-0,0.554,0.2263 +units=m +no_defs  ";
// DHDN / 3-degree Gauss-Kruger zone 1
Proj4js.defs["EPSG:5520"]="+proj=tmerc +lat_0=0 +lon_0=3 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// WGS 84 / Gabon TM 2011
Proj4js.defs["EPSG:5523"]="+proj=tmerc +lat_0=0 +lon_0=11.5 +k=0.9996 +x_0=1500000 +y_0=5500000 +datum=WGS84 +units=m +no_defs  ";
// SAD69(96) / Brazil Polyconic
Proj4js.defs["EPSG:5530"]="+proj=poly +lat_0=0 +lon_0=-54 +x_0=5000000 +y_0=10000000 +ellps=aust_SA +units=m +no_defs  ";
// SAD69(96) / UTM zone 21S
Proj4js.defs["EPSG:5531"]="+proj=utm +zone=21 +south +ellps=aust_SA +units=m +no_defs  ";
// SAD69(96) / UTM zone 22S
Proj4js.defs["EPSG:5532"]="+proj=utm +zone=22 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69(96) / UTM zone 23S
Proj4js.defs["EPSG:5533"]="+proj=utm +zone=23 +south +ellps=aust_SA +units=m +no_defs  ";
// SAD69(96) / UTM zone 24S
Proj4js.defs["EPSG:5534"]="+proj=utm +zone=24 +south +ellps=aust_SA +units=m +no_defs  ";
// SAD69(96) / UTM zone 25S
Proj4js.defs["EPSG:5535"]="+proj=utm +zone=25 +south +ellps=aust_SA +units=m +no_defs  ";
// Corrego Alegre 1961 / UTM zone 21S
Proj4js.defs["EPSG:5536"]="+proj=utm +zone=21 +south +ellps=intl +units=m +no_defs  ";
// Corrego Alegre 1961 / UTM zone 22S
Proj4js.defs["EPSG:5537"]="+proj=utm +zone=22 +south +ellps=intl +units=m +no_defs  ";
// Corrego Alegre 1961 / UTM zone 23S
Proj4js.defs["EPSG:5538"]="+proj=utm +zone=23 +south +ellps=intl +units=m +no_defs  ";
// Corrego Alegre 1961 / UTM zone 24S
Proj4js.defs["EPSG:5539"]="+proj=utm +zone=24 +south +ellps=intl +units=m +no_defs  ";
// PNG94 / PNGMG94 zone 54
Proj4js.defs["EPSG:5550"]="+proj=utm +zone=54 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// PNG94 / PNGMG94 zone 55
Proj4js.defs["EPSG:5551"]="+proj=utm +zone=55 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// PNG94 / PNGMG94 zone 56
Proj4js.defs["EPSG:5552"]="+proj=utm +zone=56 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Ocotepeque 1935 / Guatemala Norte
Proj4js.defs["EPSG:5559"]="+proj=lcc +lat_1=16.81666666666667 +lat_0=16.81666666666667 +lon_0=-90.33333333333333 +k_0=0.99992226 +x_0=500000 +y_0=292209.579 +ellps=clrk66 +towgs84=213.11,9.37,-74.95,0,0,0,0 +units=m +no_defs  ";
// EPSG topocentric example A
// Unable to translate coordinate system EPSG:5819 into PROJ.4 format.
//
// EPSG topocentric example B
// Unable to translate coordinate system EPSG:5820 into PROJ.4 format.
//
// EPSG vertical perspective example
// Unable to translate coordinate system EPSG:5821 into PROJ.4 format.
//
// Pulkovo 1995 / Gauss-Kruger zone 4
Proj4js.defs["EPSG:20004"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 5
Proj4js.defs["EPSG:20005"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=5500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 6
Proj4js.defs["EPSG:20006"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 7
Proj4js.defs["EPSG:20007"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 8
Proj4js.defs["EPSG:20008"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 9
Proj4js.defs["EPSG:20009"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 10
Proj4js.defs["EPSG:20010"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=10500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 11
Proj4js.defs["EPSG:20011"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=11500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 12
Proj4js.defs["EPSG:20012"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=12500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 13
Proj4js.defs["EPSG:20013"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=13500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 14
Proj4js.defs["EPSG:20014"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=14500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 15
Proj4js.defs["EPSG:20015"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=15500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 16
Proj4js.defs["EPSG:20016"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=16500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 17
Proj4js.defs["EPSG:20017"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=17500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 18
Proj4js.defs["EPSG:20018"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 19
Proj4js.defs["EPSG:20019"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 20
Proj4js.defs["EPSG:20020"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 21
Proj4js.defs["EPSG:20021"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=21500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 22
Proj4js.defs["EPSG:20022"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=22500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 23
Proj4js.defs["EPSG:20023"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=23500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 24
Proj4js.defs["EPSG:20024"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=24500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 25
Proj4js.defs["EPSG:20025"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=25500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 26
Proj4js.defs["EPSG:20026"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=26500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 27
Proj4js.defs["EPSG:20027"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=27500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 28
Proj4js.defs["EPSG:20028"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=28500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 29
Proj4js.defs["EPSG:20029"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=29500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 30
Proj4js.defs["EPSG:20030"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=30500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 31
Proj4js.defs["EPSG:20031"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=31500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger zone 32
Proj4js.defs["EPSG:20032"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=32500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 4N (deprecated)
Proj4js.defs["EPSG:20064"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 5N (deprecated)
Proj4js.defs["EPSG:20065"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 6N (deprecated)
Proj4js.defs["EPSG:20066"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 7N (deprecated)
Proj4js.defs["EPSG:20067"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 8N (deprecated)
Proj4js.defs["EPSG:20068"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 9N (deprecated)
Proj4js.defs["EPSG:20069"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 10N (deprecated)
Proj4js.defs["EPSG:20070"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 11N (deprecated)
Proj4js.defs["EPSG:20071"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 12N (deprecated)
Proj4js.defs["EPSG:20072"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 13N (deprecated)
Proj4js.defs["EPSG:20073"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 14N (deprecated)
Proj4js.defs["EPSG:20074"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 15N (deprecated)
Proj4js.defs["EPSG:20075"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 16N (deprecated)
Proj4js.defs["EPSG:20076"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 17N (deprecated)
Proj4js.defs["EPSG:20077"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 18N (deprecated)
Proj4js.defs["EPSG:20078"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 19N (deprecated)
Proj4js.defs["EPSG:20079"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 20N (deprecated)
Proj4js.defs["EPSG:20080"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 21N (deprecated)
Proj4js.defs["EPSG:20081"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 22N (deprecated)
Proj4js.defs["EPSG:20082"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 23N (deprecated)
Proj4js.defs["EPSG:20083"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 24N (deprecated)
Proj4js.defs["EPSG:20084"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 25N (deprecated)
Proj4js.defs["EPSG:20085"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 26N (deprecated)
Proj4js.defs["EPSG:20086"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 27N (deprecated)
Proj4js.defs["EPSG:20087"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 28N (deprecated)
Proj4js.defs["EPSG:20088"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 29N (deprecated)
Proj4js.defs["EPSG:20089"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 30N (deprecated)
Proj4js.defs["EPSG:20090"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 31N (deprecated)
Proj4js.defs["EPSG:20091"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Pulkovo 1995 / Gauss-Kruger 32N (deprecated)
Proj4js.defs["EPSG:20092"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=24.47,-130.89,-81.56,-0,-0,0.13,-0.22 +units=m +no_defs  ";
// Adindan / UTM zone 35N
Proj4js.defs["EPSG:20135"]="+proj=utm +zone=35 +ellps=clrk80 +towgs84=-166,-15,204,0,0,0,0 +units=m +no_defs  ";
// Adindan / UTM zone 36N
Proj4js.defs["EPSG:20136"]="+proj=utm +zone=36 +ellps=clrk80 +towgs84=-166,-15,204,0,0,0,0 +units=m +no_defs  ";
// Adindan / UTM zone 37N
Proj4js.defs["EPSG:20137"]="+proj=utm +zone=37 +ellps=clrk80 +towgs84=-166,-15,204,0,0,0,0 +units=m +no_defs  ";
// Adindan / UTM zone 38N
Proj4js.defs["EPSG:20138"]="+proj=utm +zone=38 +ellps=clrk80 +towgs84=-166,-15,204,0,0,0,0 +units=m +no_defs  ";
// AGD66 / AMG zone 48
Proj4js.defs["EPSG:20248"]="+proj=utm +zone=48 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 49
Proj4js.defs["EPSG:20249"]="+proj=utm +zone=49 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 50
Proj4js.defs["EPSG:20250"]="+proj=utm +zone=50 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 51
Proj4js.defs["EPSG:20251"]="+proj=utm +zone=51 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 52
Proj4js.defs["EPSG:20252"]="+proj=utm +zone=52 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 53
Proj4js.defs["EPSG:20253"]="+proj=utm +zone=53 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 54
Proj4js.defs["EPSG:20254"]="+proj=utm +zone=54 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 55
Proj4js.defs["EPSG:20255"]="+proj=utm +zone=55 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 56
Proj4js.defs["EPSG:20256"]="+proj=utm +zone=56 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 57
Proj4js.defs["EPSG:20257"]="+proj=utm +zone=57 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD66 / AMG zone 58
Proj4js.defs["EPSG:20258"]="+proj=utm +zone=58 +south +ellps=aust_SA +towgs84=-117.808,-51.536,137.784,0.303,0.446,0.234,-0.29 +units=m +no_defs  ";
// AGD84 / AMG zone 48
Proj4js.defs["EPSG:20348"]="+proj=utm +zone=48 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 49
Proj4js.defs["EPSG:20349"]="+proj=utm +zone=49 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 50
Proj4js.defs["EPSG:20350"]="+proj=utm +zone=50 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 51
Proj4js.defs["EPSG:20351"]="+proj=utm +zone=51 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 52
Proj4js.defs["EPSG:20352"]="+proj=utm +zone=52 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 53
Proj4js.defs["EPSG:20353"]="+proj=utm +zone=53 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 54
Proj4js.defs["EPSG:20354"]="+proj=utm +zone=54 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 55
Proj4js.defs["EPSG:20355"]="+proj=utm +zone=55 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 56
Proj4js.defs["EPSG:20356"]="+proj=utm +zone=56 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 57
Proj4js.defs["EPSG:20357"]="+proj=utm +zone=57 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// AGD84 / AMG zone 58
Proj4js.defs["EPSG:20358"]="+proj=utm +zone=58 +south +ellps=aust_SA +towgs84=-134,-48,149,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / UTM zone 36N
Proj4js.defs["EPSG:20436"]="+proj=utm +zone=36 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / UTM zone 37N
Proj4js.defs["EPSG:20437"]="+proj=utm +zone=37 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / UTM zone 38N
Proj4js.defs["EPSG:20438"]="+proj=utm +zone=38 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / UTM zone 39N
Proj4js.defs["EPSG:20439"]="+proj=utm +zone=39 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / UTM zone 40N
Proj4js.defs["EPSG:20440"]="+proj=utm +zone=40 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// Ain el Abd / Bahrain Grid
Proj4js.defs["EPSG:20499"]="+proj=utm +zone=39 +ellps=intl +towgs84=-143,-236,7,0,0,0,0 +units=m +no_defs  ";
// Afgooye / UTM zone 38N
Proj4js.defs["EPSG:20538"]="+proj=utm +zone=38 +ellps=krass +towgs84=-43,-163,45,0,0,0,0 +units=m +no_defs  ";
// Afgooye / UTM zone 39N
Proj4js.defs["EPSG:20539"]="+proj=utm +zone=39 +ellps=krass +towgs84=-43,-163,45,0,0,0,0 +units=m +no_defs  ";
// Lisbon (Lisbon) / Portuguese National Grid
Proj4js.defs["EPSG:20790"]="+proj=tmerc +lat_0=39.66666666666666 +lon_0=1 +k=1 +x_0=200000 +y_0=300000 +ellps=intl +towgs84=-304.046,-60.576,103.64,0,0,0,0 +pm=lisbon +units=m +no_defs  ";
// Lisbon (Lisbon) / Portuguese Grid
Proj4js.defs["EPSG:20791"]="+proj=tmerc +lat_0=39.66666666666666 +lon_0=1 +k=1 +x_0=0 +y_0=0 +ellps=intl +towgs84=-304.046,-60.576,103.64,0,0,0,0 +pm=lisbon +units=m +no_defs  ";
// Aratu / UTM zone 22S
Proj4js.defs["EPSG:20822"]="+proj=utm +zone=22 +south +ellps=intl +towgs84=-151.99,287.04,-147.45,0,0,0,0 +units=m +no_defs  ";
// Aratu / UTM zone 23S
Proj4js.defs["EPSG:20823"]="+proj=utm +zone=23 +south +ellps=intl +towgs84=-151.99,287.04,-147.45,0,0,0,0 +units=m +no_defs  ";
// Aratu / UTM zone 24S
Proj4js.defs["EPSG:20824"]="+proj=utm +zone=24 +south +ellps=intl +towgs84=-151.99,287.04,-147.45,0,0,0,0 +units=m +no_defs  ";
// Arc 1950 / UTM zone 34S
Proj4js.defs["EPSG:20934"]="+proj=utm +zone=34 +south +a=6378249.145 +b=6356514.966398753 +towgs84=-143,-90,-294,0,0,0,0 +units=m +no_defs  ";
// Arc 1950 / UTM zone 35S
Proj4js.defs["EPSG:20935"]="+proj=utm +zone=35 +south +a=6378249.145 +b=6356514.966398753 +towgs84=-143,-90,-294,0,0,0,0 +units=m +no_defs  ";
// Arc 1950 / UTM zone 36S
Proj4js.defs["EPSG:20936"]="+proj=utm +zone=36 +south +a=6378249.145 +b=6356514.966398753 +towgs84=-143,-90,-294,0,0,0,0 +units=m +no_defs  ";
// Arc 1960 / UTM zone 35S
Proj4js.defs["EPSG:21035"]="+proj=utm +zone=35 +south +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs  ";
// Arc 1960 / UTM zone 36S
Proj4js.defs["EPSG:21036"]="+proj=utm +zone=36 +south +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs  ";
// Arc 1960 / UTM zone 37S
Proj4js.defs["EPSG:21037"]="+proj=utm +zone=37 +south +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs  ";
// Arc 1960 / UTM zone 35N
Proj4js.defs["EPSG:21095"]="+proj=utm +zone=35 +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs  ";
// Arc 1960 / UTM zone 36N
Proj4js.defs["EPSG:21096"]="+proj=utm +zone=36 +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs  ";
// Arc 1960 / UTM zone 37N
Proj4js.defs["EPSG:21097"]="+proj=utm +zone=37 +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs  ";
// Batavia (Jakarta) / NEIEZ (deprecated)
Proj4js.defs["EPSG:21100"]="+proj=merc +lon_0=110 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +pm=jakarta +units=m +no_defs  ";
// Batavia / UTM zone 48S
Proj4js.defs["EPSG:21148"]="+proj=utm +zone=48 +south +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +units=m +no_defs  ";
// Batavia / UTM zone 49S
Proj4js.defs["EPSG:21149"]="+proj=utm +zone=49 +south +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +units=m +no_defs  ";
// Batavia / UTM zone 50S
Proj4js.defs["EPSG:21150"]="+proj=utm +zone=50 +south +ellps=bessel +towgs84=-377,681,-50,0,0,0,0 +units=m +no_defs  ";
// Barbados 1938 / British West Indies Grid
Proj4js.defs["EPSG:21291"]="+proj=tmerc +lat_0=0 +lon_0=-62 +k=0.9995000000000001 +x_0=400000 +y_0=0 +ellps=clrk80 +towgs84=31.95,300.99,419.19,0,0,0,0 +units=m +no_defs  ";
// Barbados 1938 / Barbados National Grid
Proj4js.defs["EPSG:21292"]="+proj=tmerc +lat_0=13.17638888888889 +lon_0=-59.55972222222222 +k=0.9999986 +x_0=30000 +y_0=75000 +ellps=clrk80 +towgs84=31.95,300.99,419.19,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 13
Proj4js.defs["EPSG:21413"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=13500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 14
Proj4js.defs["EPSG:21414"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=14500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 15
Proj4js.defs["EPSG:21415"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=15500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 16
Proj4js.defs["EPSG:21416"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=16500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 17
Proj4js.defs["EPSG:21417"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=17500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 18
Proj4js.defs["EPSG:21418"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 19
Proj4js.defs["EPSG:21419"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 20
Proj4js.defs["EPSG:21420"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 21
Proj4js.defs["EPSG:21421"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=21500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 22
Proj4js.defs["EPSG:21422"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=22500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger zone 23
Proj4js.defs["EPSG:21423"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=23500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 75E
Proj4js.defs["EPSG:21453"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 81E
Proj4js.defs["EPSG:21454"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 87E
Proj4js.defs["EPSG:21455"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 93E
Proj4js.defs["EPSG:21456"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 99E
Proj4js.defs["EPSG:21457"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 105E
Proj4js.defs["EPSG:21458"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 111E
Proj4js.defs["EPSG:21459"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 117E
Proj4js.defs["EPSG:21460"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 123E
Proj4js.defs["EPSG:21461"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 129E
Proj4js.defs["EPSG:21462"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger CM 135E
Proj4js.defs["EPSG:21463"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 13N (deprecated)
Proj4js.defs["EPSG:21473"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 14N (deprecated)
Proj4js.defs["EPSG:21474"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 15N (deprecated)
Proj4js.defs["EPSG:21475"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 16N (deprecated)
Proj4js.defs["EPSG:21476"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 17N (deprecated)
Proj4js.defs["EPSG:21477"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 18N (deprecated)
Proj4js.defs["EPSG:21478"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 19N (deprecated)
Proj4js.defs["EPSG:21479"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 20N (deprecated)
Proj4js.defs["EPSG:21480"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 21N (deprecated)
Proj4js.defs["EPSG:21481"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 22N (deprecated)
Proj4js.defs["EPSG:21482"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Beijing 1954 / Gauss-Kruger 23N (deprecated)
Proj4js.defs["EPSG:21483"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +units=m +no_defs  ";
// Belge 1950 (Brussels) / Belge Lambert 50
Proj4js.defs["EPSG:21500"]="+proj=lcc +lat_1=49.83333333333334 +lat_2=51.16666666666666 +lat_0=90 +lon_0=0 +x_0=150000 +y_0=5400000 +ellps=intl +pm=brussels +units=m +no_defs  ";
// Bern 1898 (Bern) / LV03C
Proj4js.defs["EPSG:21780"]="+proj=somerc +lat_0=46.95240555555556 +lon_0=0 +k_0=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=674.4,15.1,405.3,0,0,0,0 +pm=bern +units=m +no_defs  ";
// CH1903 / LV03
Proj4js.defs["EPSG:21781"]="+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.4,15.1,405.3,0,0,0,0 +units=m +no_defs  ";
// CH1903 / LV03C-G
Proj4js.defs["EPSG:21782"]="+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=674.4,15.1,405.3,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / UTM zone 17N (deprecated)
Proj4js.defs["EPSG:21817"]="+proj=utm +zone=17 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / UTM zone 18N
Proj4js.defs["EPSG:21818"]="+proj=utm +zone=18 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia West zone (deprecated)
Proj4js.defs["EPSG:21891"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-77.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia Bogota zone (deprecated)
Proj4js.defs["EPSG:21892"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-74.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia East Central zone (deprecated)
Proj4js.defs["EPSG:21893"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-71.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia East (deprecated)
Proj4js.defs["EPSG:21894"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-68.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia West zone
Proj4js.defs["EPSG:21896"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-77.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia Bogota zone
Proj4js.defs["EPSG:21897"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-74.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia East Central zone
Proj4js.defs["EPSG:21898"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-71.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Bogota 1975 / Colombia East
Proj4js.defs["EPSG:21899"]="+proj=tmerc +lat_0=4.599047222222222 +lon_0=-68.08091666666667 +k=1 +x_0=1000000 +y_0=1000000 +ellps=intl +towgs84=307,304,-318,0,0,0,0 +units=m +no_defs  ";
// Camacupa / UTM zone 32S
Proj4js.defs["EPSG:22032"]="+proj=utm +zone=32 +south +ellps=clrk80 +towgs84=-50.9,-347.6,-231,0,0,0,0 +units=m +no_defs  ";
// Camacupa / UTM zone 33S
Proj4js.defs["EPSG:22033"]="+proj=utm +zone=33 +south +ellps=clrk80 +towgs84=-50.9,-347.6,-231,0,0,0,0 +units=m +no_defs  ";
// Camacupa / TM 11.30 SE
Proj4js.defs["EPSG:22091"]="+proj=tmerc +lat_0=0 +lon_0=11.5 +k=0.9996 +x_0=500000 +y_0=10000000 +ellps=clrk80 +towgs84=-50.9,-347.6,-231,0,0,0,0 +units=m +no_defs  ";
// Camacupa / TM 12 SE
Proj4js.defs["EPSG:22092"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=0.9996 +x_0=500000 +y_0=10000000 +ellps=clrk80 +towgs84=-50.9,-347.6,-231,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 1
Proj4js.defs["EPSG:22171"]="+proj=tmerc +lat_0=-90 +lon_0=-72 +k=1 +x_0=1500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 2
Proj4js.defs["EPSG:22172"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 3
Proj4js.defs["EPSG:22173"]="+proj=tmerc +lat_0=-90 +lon_0=-66 +k=1 +x_0=3500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 4
Proj4js.defs["EPSG:22174"]="+proj=tmerc +lat_0=-90 +lon_0=-63 +k=1 +x_0=4500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 5
Proj4js.defs["EPSG:22175"]="+proj=tmerc +lat_0=-90 +lon_0=-60 +k=1 +x_0=5500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 6
Proj4js.defs["EPSG:22176"]="+proj=tmerc +lat_0=-90 +lon_0=-57 +k=1 +x_0=6500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 98 / Argentina 7
Proj4js.defs["EPSG:22177"]="+proj=tmerc +lat_0=-90 +lon_0=-54 +k=1 +x_0=7500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 1
Proj4js.defs["EPSG:22181"]="+proj=tmerc +lat_0=-90 +lon_0=-72 +k=1 +x_0=1500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 2
Proj4js.defs["EPSG:22182"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 3
Proj4js.defs["EPSG:22183"]="+proj=tmerc +lat_0=-90 +lon_0=-66 +k=1 +x_0=3500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 4
Proj4js.defs["EPSG:22184"]="+proj=tmerc +lat_0=-90 +lon_0=-63 +k=1 +x_0=4500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 5
Proj4js.defs["EPSG:22185"]="+proj=tmerc +lat_0=-90 +lon_0=-60 +k=1 +x_0=5500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 6
Proj4js.defs["EPSG:22186"]="+proj=tmerc +lat_0=-90 +lon_0=-57 +k=1 +x_0=6500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// POSGAR 94 / Argentina 7
Proj4js.defs["EPSG:22187"]="+proj=tmerc +lat_0=-90 +lon_0=-54 +k=1 +x_0=7500000 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 1
Proj4js.defs["EPSG:22191"]="+proj=tmerc +lat_0=-90 +lon_0=-72 +k=1 +x_0=1500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 2
Proj4js.defs["EPSG:22192"]="+proj=tmerc +lat_0=-90 +lon_0=-69 +k=1 +x_0=2500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 3
Proj4js.defs["EPSG:22193"]="+proj=tmerc +lat_0=-90 +lon_0=-66 +k=1 +x_0=3500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 4
Proj4js.defs["EPSG:22194"]="+proj=tmerc +lat_0=-90 +lon_0=-63 +k=1 +x_0=4500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 5
Proj4js.defs["EPSG:22195"]="+proj=tmerc +lat_0=-90 +lon_0=-60 +k=1 +x_0=5500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 6
Proj4js.defs["EPSG:22196"]="+proj=tmerc +lat_0=-90 +lon_0=-57 +k=1 +x_0=6500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Campo Inchauspe / Argentina 7
Proj4js.defs["EPSG:22197"]="+proj=tmerc +lat_0=-90 +lon_0=-54 +k=1 +x_0=7500000 +y_0=0 +ellps=intl +towgs84=-148,136,90,0,0,0,0 +units=m +no_defs  ";
// Cape / UTM zone 34S
Proj4js.defs["EPSG:22234"]="+proj=utm +zone=34 +south +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / UTM zone 35S
Proj4js.defs["EPSG:22235"]="+proj=utm +zone=35 +south +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / UTM zone 36S (deprecated)
Proj4js.defs["EPSG:22236"]="+proj=utm +zone=36 +south +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo15
Proj4js.defs["EPSG:22275"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo17
Proj4js.defs["EPSG:22277"]="+proj=tmerc +lat_0=0 +lon_0=17 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo19
Proj4js.defs["EPSG:22279"]="+proj=tmerc +lat_0=0 +lon_0=19 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo21
Proj4js.defs["EPSG:22281"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo23
Proj4js.defs["EPSG:22283"]="+proj=tmerc +lat_0=0 +lon_0=23 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo25
Proj4js.defs["EPSG:22285"]="+proj=tmerc +lat_0=0 +lon_0=25 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo27
Proj4js.defs["EPSG:22287"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo29
Proj4js.defs["EPSG:22289"]="+proj=tmerc +lat_0=0 +lon_0=29 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo31
Proj4js.defs["EPSG:22291"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Cape / Lo33
Proj4js.defs["EPSG:22293"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=0 +y_0=0 +axis=wsu +a=6378249.145 +b=6356514.966398753 +towgs84=-136,-108,-292,0,0,0,0 +units=m +no_defs  ";
// Carthage (Paris) / Tunisia Mining Grid
// Unable to translate coordinate system EPSG:22300 into PROJ.4 format.
//
// Carthage / UTM zone 32N
Proj4js.defs["EPSG:22332"]="+proj=utm +zone=32 +a=6378249.2 +b=6356515 +towgs84=-263,6,431,0,0,0,0 +units=m +no_defs  ";
// Carthage / Nord Tunisie
Proj4js.defs["EPSG:22391"]="+proj=lcc +lat_1=36 +lat_0=36 +lon_0=9.9 +k_0=0.999625544 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=-263,6,431,0,0,0,0 +units=m +no_defs  ";
// Carthage / Sud Tunisie
Proj4js.defs["EPSG:22392"]="+proj=lcc +lat_1=33.3 +lat_0=33.3 +lon_0=9.9 +k_0=0.999625769 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=-263,6,431,0,0,0,0 +units=m +no_defs  ";
// Corrego Alegre 1970-72 / UTM zone 21S
Proj4js.defs["EPSG:22521"]="+proj=utm +zone=21 +south +ellps=intl +towgs84=-206,172,-6,0,0,0,0 +units=m +no_defs  ";
// Corrego Alegre 1970-72 / UTM zone 22S
Proj4js.defs["EPSG:22522"]="+proj=utm +zone=22 +south +ellps=intl +towgs84=-206,172,-6,0,0,0,0 +units=m +no_defs  ";
// Corrego Alegre 1970-72 / UTM zone 23S
Proj4js.defs["EPSG:22523"]="+proj=utm +zone=23 +south +ellps=intl +towgs84=-206,172,-6,0,0,0,0 +units=m +no_defs  ";
// Corrego Alegre 1970-72 / UTM zone 24S
Proj4js.defs["EPSG:22524"]="+proj=utm +zone=24 +south +ellps=intl +towgs84=-206,172,-6,0,0,0,0 +units=m +no_defs  ";
// Corrego Alegre 1970-72 / UTM zone 25S
Proj4js.defs["EPSG:22525"]="+proj=utm +zone=25 +south +ellps=intl +towgs84=-206,172,-6,0,0,0,0 +units=m +no_defs  ";
// Deir ez Zor / Levant Zone
Proj4js.defs["EPSG:22700"]="+proj=lcc +lat_1=34.65 +lat_0=34.65 +lon_0=37.35 +k_0=0.9996256 +x_0=300000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=-190.421,8.532,238.69,0,0,0,0 +units=m +no_defs  ";
// Deir ez Zor / Syria Lambert
Proj4js.defs["EPSG:22770"]="+proj=lcc +lat_1=34.65 +lat_0=34.65 +lon_0=37.35 +k_0=0.9996256 +x_0=300000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=-190.421,8.532,238.69,0,0,0,0 +units=m +no_defs  ";
// Deir ez Zor / Levant Stereographic
Proj4js.defs["EPSG:22780"]="+proj=sterea +lat_0=34.2 +lon_0=39.15 +k=0.9995341 +x_0=0 +y_0=0 +a=6378249.2 +b=6356515 +towgs84=-190.421,8.532,238.69,0,0,0,0 +units=m +no_defs  ";
// Douala / UTM zone 32N (deprecated)
Proj4js.defs["EPSG:22832"]="+proj=utm +zone=32 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Egypt 1907 / Blue Belt
Proj4js.defs["EPSG:22991"]="+proj=tmerc +lat_0=30 +lon_0=35 +k=1 +x_0=300000 +y_0=1100000 +ellps=helmert +towgs84=-130,110,-13,0,0,0,0 +units=m +no_defs  ";
// Egypt 1907 / Red Belt
Proj4js.defs["EPSG:22992"]="+proj=tmerc +lat_0=30 +lon_0=31 +k=1 +x_0=615000 +y_0=810000 +ellps=helmert +towgs84=-130,110,-13,0,0,0,0 +units=m +no_defs  ";
// Egypt 1907 / Purple Belt
Proj4js.defs["EPSG:22993"]="+proj=tmerc +lat_0=30 +lon_0=27 +k=1 +x_0=700000 +y_0=200000 +ellps=helmert +towgs84=-130,110,-13,0,0,0,0 +units=m +no_defs  ";
// Egypt 1907 / Extended Purple Belt
Proj4js.defs["EPSG:22994"]="+proj=tmerc +lat_0=30 +lon_0=27 +k=1 +x_0=700000 +y_0=1200000 +ellps=helmert +towgs84=-130,110,-13,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 28N
Proj4js.defs["EPSG:23028"]="+proj=utm +zone=28 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 29N
Proj4js.defs["EPSG:23029"]="+proj=utm +zone=29 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 30N
Proj4js.defs["EPSG:23030"]="+proj=utm +zone=30 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 31N
Proj4js.defs["EPSG:23031"]="+proj=utm +zone=31 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 32N
Proj4js.defs["EPSG:23032"]="+proj=utm +zone=32 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 33N
Proj4js.defs["EPSG:23033"]="+proj=utm +zone=33 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 34N
Proj4js.defs["EPSG:23034"]="+proj=utm +zone=34 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 35N
Proj4js.defs["EPSG:23035"]="+proj=utm +zone=35 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 36N
Proj4js.defs["EPSG:23036"]="+proj=utm +zone=36 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 37N
Proj4js.defs["EPSG:23037"]="+proj=utm +zone=37 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / UTM zone 38N
Proj4js.defs["EPSG:23038"]="+proj=utm +zone=38 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM 0 N
Proj4js.defs["EPSG:23090"]="+proj=tmerc +lat_0=0 +lon_0=0 +k=0.9996 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// ED50 / TM 5 NE
Proj4js.defs["EPSG:23095"]="+proj=tmerc +lat_0=0 +lon_0=5 +k=0.9996 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs  ";
// Fahud / UTM zone 39N
Proj4js.defs["EPSG:23239"]="+proj=utm +zone=39 +ellps=clrk80 +towgs84=-346,-1,224,0,0,0,0 +units=m +no_defs  ";
// Fahud / UTM zone 40N
Proj4js.defs["EPSG:23240"]="+proj=utm +zone=40 +ellps=clrk80 +towgs84=-346,-1,224,0,0,0,0 +units=m +no_defs  ";
// Garoua / UTM zone 33N (deprecated)
Proj4js.defs["EPSG:23433"]="+proj=utm +zone=33 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// HD72 / EOV
Proj4js.defs["EPSG:23700"]="+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 46.2
Proj4js.defs["EPSG:23830"]="+proj=tmerc +lat_0=0 +lon_0=94.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 47.1
Proj4js.defs["EPSG:23831"]="+proj=tmerc +lat_0=0 +lon_0=97.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 47.2
Proj4js.defs["EPSG:23832"]="+proj=tmerc +lat_0=0 +lon_0=100.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 48.1
Proj4js.defs["EPSG:23833"]="+proj=tmerc +lat_0=0 +lon_0=103.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 48.2
Proj4js.defs["EPSG:23834"]="+proj=tmerc +lat_0=0 +lon_0=106.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 49.1
Proj4js.defs["EPSG:23835"]="+proj=tmerc +lat_0=0 +lon_0=109.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 49.2
Proj4js.defs["EPSG:23836"]="+proj=tmerc +lat_0=0 +lon_0=112.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 50.1
Proj4js.defs["EPSG:23837"]="+proj=tmerc +lat_0=0 +lon_0=115.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 50.2
Proj4js.defs["EPSG:23838"]="+proj=tmerc +lat_0=0 +lon_0=118.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 51.1
Proj4js.defs["EPSG:23839"]="+proj=tmerc +lat_0=0 +lon_0=121.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 51.2
Proj4js.defs["EPSG:23840"]="+proj=tmerc +lat_0=0 +lon_0=124.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 52.1
Proj4js.defs["EPSG:23841"]="+proj=tmerc +lat_0=0 +lon_0=127.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 52.2
Proj4js.defs["EPSG:23842"]="+proj=tmerc +lat_0=0 +lon_0=130.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 53.1
Proj4js.defs["EPSG:23843"]="+proj=tmerc +lat_0=0 +lon_0=133.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 53.2
Proj4js.defs["EPSG:23844"]="+proj=tmerc +lat_0=0 +lon_0=136.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / Indonesia TM-3 zone 54.1
Proj4js.defs["EPSG:23845"]="+proj=tmerc +lat_0=0 +lon_0=139.5 +k=0.9999 +x_0=200000 +y_0=1500000 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 46N
Proj4js.defs["EPSG:23846"]="+proj=utm +zone=46 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 47N
Proj4js.defs["EPSG:23847"]="+proj=utm +zone=47 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 48N
Proj4js.defs["EPSG:23848"]="+proj=utm +zone=48 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 49N
Proj4js.defs["EPSG:23849"]="+proj=utm +zone=49 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 50N
Proj4js.defs["EPSG:23850"]="+proj=utm +zone=50 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 51N
Proj4js.defs["EPSG:23851"]="+proj=utm +zone=51 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 52N
Proj4js.defs["EPSG:23852"]="+proj=utm +zone=52 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 53N (deprecated)
Proj4js.defs["EPSG:23853"]="+proj=utm +zone=53 +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 46N
Proj4js.defs["EPSG:23866"]="+proj=utm +zone=46 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 47N
Proj4js.defs["EPSG:23867"]="+proj=utm +zone=47 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 48N
Proj4js.defs["EPSG:23868"]="+proj=utm +zone=48 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 49N
Proj4js.defs["EPSG:23869"]="+proj=utm +zone=49 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 50N
Proj4js.defs["EPSG:23870"]="+proj=utm +zone=50 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 51N
Proj4js.defs["EPSG:23871"]="+proj=utm +zone=51 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 52N
Proj4js.defs["EPSG:23872"]="+proj=utm +zone=52 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 47S
Proj4js.defs["EPSG:23877"]="+proj=utm +zone=47 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 48S
Proj4js.defs["EPSG:23878"]="+proj=utm +zone=48 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 49S
Proj4js.defs["EPSG:23879"]="+proj=utm +zone=49 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 50S
Proj4js.defs["EPSG:23880"]="+proj=utm +zone=50 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 51S
Proj4js.defs["EPSG:23881"]="+proj=utm +zone=51 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 52S
Proj4js.defs["EPSG:23882"]="+proj=utm +zone=52 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 53S
Proj4js.defs["EPSG:23883"]="+proj=utm +zone=53 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// DGN95 / UTM zone 54S
Proj4js.defs["EPSG:23884"]="+proj=utm +zone=54 +south +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 46S (deprecated)
Proj4js.defs["EPSG:23886"]="+proj=utm +zone=46 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 47S
Proj4js.defs["EPSG:23887"]="+proj=utm +zone=47 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 48S
Proj4js.defs["EPSG:23888"]="+proj=utm +zone=48 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 49S
Proj4js.defs["EPSG:23889"]="+proj=utm +zone=49 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 50S
Proj4js.defs["EPSG:23890"]="+proj=utm +zone=50 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 51S
Proj4js.defs["EPSG:23891"]="+proj=utm +zone=51 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 52S
Proj4js.defs["EPSG:23892"]="+proj=utm +zone=52 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 53S
Proj4js.defs["EPSG:23893"]="+proj=utm +zone=53 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// ID74 / UTM zone 54S
Proj4js.defs["EPSG:23894"]="+proj=utm +zone=54 +south +a=6378160 +b=6356774.50408554 +towgs84=-24,-15,5,0,0,0,0 +units=m +no_defs  ";
// Indian 1954 / UTM zone 46N
Proj4js.defs["EPSG:23946"]="+proj=utm +zone=46 +a=6377276.345 +b=6356075.41314024 +towgs84=217,823,299,0,0,0,0 +units=m +no_defs  ";
// Indian 1954 / UTM zone 47N
Proj4js.defs["EPSG:23947"]="+proj=utm +zone=47 +a=6377276.345 +b=6356075.41314024 +towgs84=217,823,299,0,0,0,0 +units=m +no_defs  ";
// Indian 1954 / UTM zone 48N
Proj4js.defs["EPSG:23948"]="+proj=utm +zone=48 +a=6377276.345 +b=6356075.41314024 +towgs84=217,823,299,0,0,0,0 +units=m +no_defs  ";
// Indian 1975 / UTM zone 47N
Proj4js.defs["EPSG:24047"]="+proj=utm +zone=47 +a=6377276.345 +b=6356075.41314024 +towgs84=210,814,289,0,0,0,0 +units=m +no_defs  ";
// Indian 1975 / UTM zone 48N
Proj4js.defs["EPSG:24048"]="+proj=utm +zone=48 +a=6377276.345 +b=6356075.41314024 +towgs84=210,814,289,0,0,0,0 +units=m +no_defs  ";
// Jamaica 1875 / Jamaica (Old Grid)
Proj4js.defs["EPSG:24100"]="+proj=lcc +lat_1=18 +lat_0=18 +lon_0=-77 +k_0=1 +x_0=167638.49597 +y_0=121918.90616 +a=6378249.144808011 +b=6356514.966204134 +to_meter=0.3047972654 +no_defs  ";
// JAD69 / Jamaica National Grid
Proj4js.defs["EPSG:24200"]="+proj=lcc +lat_1=18 +lat_0=18 +lon_0=-77 +k_0=1 +x_0=250000 +y_0=150000 +ellps=clrk66 +towgs84=70,207,389.5,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1937 / UTM zone 45N
Proj4js.defs["EPSG:24305"]="+proj=utm +zone=45 +a=6377276.345 +b=6356075.41314024 +towgs84=282,726,254,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1937 / UTM zone 46N
Proj4js.defs["EPSG:24306"]="+proj=utm +zone=46 +a=6377276.345 +b=6356075.41314024 +towgs84=282,726,254,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1962 / UTM zone 41N
Proj4js.defs["EPSG:24311"]="+proj=utm +zone=41 +a=6377301.243 +b=6356100.230165384 +towgs84=283,682,231,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1962 / UTM zone 42N
Proj4js.defs["EPSG:24312"]="+proj=utm +zone=42 +a=6377301.243 +b=6356100.230165384 +towgs84=283,682,231,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1962 / UTM zone 43N
Proj4js.defs["EPSG:24313"]="+proj=utm +zone=43 +a=6377301.243 +b=6356100.230165384 +towgs84=283,682,231,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / UTM zone 42N
Proj4js.defs["EPSG:24342"]="+proj=utm +zone=42 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / UTM zone 43N
Proj4js.defs["EPSG:24343"]="+proj=utm +zone=43 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / UTM zone 44N
Proj4js.defs["EPSG:24344"]="+proj=utm +zone=44 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / UTM zone 45N
Proj4js.defs["EPSG:24345"]="+proj=utm +zone=45 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / UTM zone 46N
Proj4js.defs["EPSG:24346"]="+proj=utm +zone=46 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / UTM zone 47N
Proj4js.defs["EPSG:24347"]="+proj=utm +zone=47 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1880 / India zone 0
Proj4js.defs["EPSG:24370"]="+proj=lcc +lat_1=39.5 +lat_0=39.5 +lon_0=68 +k_0=0.99846154 +x_0=2153865.73916853 +y_0=2368292.194628102 +a=6377299.36559538 +b=6356098.359005156 +to_meter=0.9143985307444408 +no_defs  ";
// Kalianpur 1880 / India zone I
Proj4js.defs["EPSG:24371"]="+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=68 +k_0=0.99878641 +x_0=2743195.592233322 +y_0=914398.5307444407 +a=6377299.36559538 +b=6356098.359005156 +to_meter=0.9143985307444408 +no_defs  ";
// Kalianpur 1880 / India zone IIa
Proj4js.defs["EPSG:24372"]="+proj=lcc +lat_1=26 +lat_0=26 +lon_0=74 +k_0=0.99878641 +x_0=2743195.592233322 +y_0=914398.5307444407 +a=6377299.36559538 +b=6356098.359005156 +to_meter=0.9143985307444408 +no_defs  ";
// Kalianpur 1880 / India zone III
Proj4js.defs["EPSG:24373"]="+proj=lcc +lat_1=19 +lat_0=19 +lon_0=80 +k_0=0.99878641 +x_0=2743195.592233322 +y_0=914398.5307444407 +a=6377299.36559538 +b=6356098.359005156 +to_meter=0.9143985307444408 +no_defs  ";
// Kalianpur 1880 / India zone IV
Proj4js.defs["EPSG:24374"]="+proj=lcc +lat_1=12 +lat_0=12 +lon_0=80 +k_0=0.99878641 +x_0=2743195.592233322 +y_0=914398.5307444407 +a=6377299.36559538 +b=6356098.359005156 +to_meter=0.9143985307444408 +no_defs  ";
// Kalianpur 1937 / India zone IIb
Proj4js.defs["EPSG:24375"]="+proj=lcc +lat_1=26 +lat_0=26 +lon_0=90 +k_0=0.99878641 +x_0=2743185.69 +y_0=914395.23 +a=6377276.345 +b=6356075.41314024 +towgs84=282,726,254,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1962 / India zone I
Proj4js.defs["EPSG:24376"]="+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=68 +k_0=0.99878641 +x_0=2743196.4 +y_0=914398.8 +a=6377301.243 +b=6356100.230165384 +towgs84=283,682,231,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1962 / India zone IIa
Proj4js.defs["EPSG:24377"]="+proj=lcc +lat_1=26 +lat_0=26 +lon_0=74 +k_0=0.99878641 +x_0=2743196.4 +y_0=914398.8 +a=6377301.243 +b=6356100.230165384 +towgs84=283,682,231,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / India zone I
Proj4js.defs["EPSG:24378"]="+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=68 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / India zone IIa
Proj4js.defs["EPSG:24379"]="+proj=lcc +lat_1=26 +lat_0=26 +lon_0=74 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / India zone IIb
Proj4js.defs["EPSG:24380"]="+proj=lcc +lat_1=26 +lat_0=26 +lon_0=90 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1975 / India zone III
Proj4js.defs["EPSG:24381"]="+proj=lcc +lat_1=19 +lat_0=19 +lon_0=80 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kalianpur 1880 / India zone IIb
Proj4js.defs["EPSG:24382"]="+proj=lcc +lat_1=26 +lat_0=26 +lon_0=90 +k_0=0.99878641 +x_0=2743195.592233322 +y_0=914398.5307444407 +a=6377299.36559538 +b=6356098.359005156 +to_meter=0.9143985307444408 +no_defs  ";
// Kalianpur 1975 / India zone IV
Proj4js.defs["EPSG:24383"]="+proj=lcc +lat_1=12 +lat_0=12 +lon_0=80 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs  ";
// Kertau 1968 / Singapore Grid
Proj4js.defs["EPSG:24500"]="+proj=cass +lat_0=1.287646666666667 +lon_0=103.8530022222222 +x_0=30000 +y_0=30000 +a=6377304.063 +b=6356103.038993155 +towgs84=-11,851,5,0,0,0,0 +units=m +no_defs  ";
// Kertau 1968 / UTM zone 47N
Proj4js.defs["EPSG:24547"]="+proj=utm +zone=47 +a=6377304.063 +b=6356103.038993155 +towgs84=-11,851,5,0,0,0,0 +units=m +no_defs  ";
// Kertau 1968 / UTM zone 48N
Proj4js.defs["EPSG:24548"]="+proj=utm +zone=48 +a=6377304.063 +b=6356103.038993155 +towgs84=-11,851,5,0,0,0,0 +units=m +no_defs  ";
// Kertau / R.S.O. Malaya (ch) (deprecated)
Proj4js.defs["EPSG:24571"]="+proj=omerc +lat_0=4 +lonc=102.25 +alpha=323.0257905 +k=0.99984 +x_0=804671.2997750348 +y_0=0 +gamma=323.1301023611111 +a=6377304.063 +b=6356103.038993155 +towgs84=-11,851,5,0,0,0,0 +to_meter=20.11678249437587 +no_defs  ";
// KOC Lambert
Proj4js.defs["EPSG:24600"]="+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=45 +k_0=0.9987864078000001 +x_0=1500000 +y_0=1166200 +ellps=clrk80 +towgs84=-294.7,-200.1,525.5,0,0,0,0 +units=m +no_defs  ";
// La Canoa / UTM zone 18N
Proj4js.defs["EPSG:24718"]="+proj=utm +zone=18 +ellps=intl +towgs84=-273.5,110.6,-357.9,0,0,0,0 +units=m +no_defs  ";
// La Canoa / UTM zone 19N
Proj4js.defs["EPSG:24719"]="+proj=utm +zone=19 +ellps=intl +towgs84=-273.5,110.6,-357.9,0,0,0,0 +units=m +no_defs  ";
// La Canoa / UTM zone 20N
Proj4js.defs["EPSG:24720"]="+proj=utm +zone=20 +ellps=intl +towgs84=-273.5,110.6,-357.9,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 17N
Proj4js.defs["EPSG:24817"]="+proj=utm +zone=17 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 18N
Proj4js.defs["EPSG:24818"]="+proj=utm +zone=18 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 19N
Proj4js.defs["EPSG:24819"]="+proj=utm +zone=19 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 20N
Proj4js.defs["EPSG:24820"]="+proj=utm +zone=20 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 21N
Proj4js.defs["EPSG:24821"]="+proj=utm +zone=21 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 17S
Proj4js.defs["EPSG:24877"]="+proj=utm +zone=17 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 18S
Proj4js.defs["EPSG:24878"]="+proj=utm +zone=18 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 19S
Proj4js.defs["EPSG:24879"]="+proj=utm +zone=19 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 20S
Proj4js.defs["EPSG:24880"]="+proj=utm +zone=20 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 21S
Proj4js.defs["EPSG:24881"]="+proj=utm +zone=21 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / UTM zone 22S
Proj4js.defs["EPSG:24882"]="+proj=utm +zone=22 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / Peru west zone
Proj4js.defs["EPSG:24891"]="+proj=tmerc +lat_0=-6 +lon_0=-80.5 +k=0.99983008 +x_0=222000 +y_0=1426834.743 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / Peru central zone
Proj4js.defs["EPSG:24892"]="+proj=tmerc +lat_0=-9.5 +lon_0=-76 +k=0.99932994 +x_0=720000 +y_0=1039979.159 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// PSAD56 / Peru east zone
Proj4js.defs["EPSG:24893"]="+proj=tmerc +lat_0=-9.5 +lon_0=-70.5 +k=0.99952992 +x_0=1324000 +y_0=1040084.558 +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs  ";
// Leigon / Ghana Metre Grid
Proj4js.defs["EPSG:25000"]="+proj=tmerc +lat_0=4.666666666666667 +lon_0=-1 +k=0.99975 +x_0=274319.51 +y_0=0 +ellps=clrk80 +towgs84=-130,29,364,0,0,0,0 +units=m +no_defs  ";
// Lome / UTM zone 31N
Proj4js.defs["EPSG:25231"]="+proj=utm +zone=31 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Luzon 1911 / Philippines zone I
Proj4js.defs["EPSG:25391"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-133,-77,-51,0,0,0,0 +units=m +no_defs  ";
// Luzon 1911 / Philippines zone II
Proj4js.defs["EPSG:25392"]="+proj=tmerc +lat_0=0 +lon_0=119 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-133,-77,-51,0,0,0,0 +units=m +no_defs  ";
// Luzon 1911 / Philippines zone III
Proj4js.defs["EPSG:25393"]="+proj=tmerc +lat_0=0 +lon_0=121 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-133,-77,-51,0,0,0,0 +units=m +no_defs  ";
// Luzon 1911 / Philippines zone IV
Proj4js.defs["EPSG:25394"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-133,-77,-51,0,0,0,0 +units=m +no_defs  ";
// Luzon 1911 / Philippines zone V
Proj4js.defs["EPSG:25395"]="+proj=tmerc +lat_0=0 +lon_0=125 +k=0.99995 +x_0=500000 +y_0=0 +ellps=clrk66 +towgs84=-133,-77,-51,0,0,0,0 +units=m +no_defs  ";
// Makassar (Jakarta) / NEIEZ (deprecated)
Proj4js.defs["EPSG:25700"]="+proj=merc +lon_0=110 +k=0.997 +x_0=3900000 +y_0=900000 +ellps=bessel +towgs84=-587.8,519.75,145.76,0,0,0,0 +pm=jakarta +units=m +no_defs  ";
// ETRS89 / UTM zone 28N
Proj4js.defs["EPSG:25828"]="+proj=utm +zone=28 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 29N
Proj4js.defs["EPSG:25829"]="+proj=utm +zone=29 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 30N
Proj4js.defs["EPSG:25830"]="+proj=utm +zone=30 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 31N
Proj4js.defs["EPSG:25831"]="+proj=utm +zone=31 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 32N
Proj4js.defs["EPSG:25832"]="+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 33N
Proj4js.defs["EPSG:25833"]="+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 34N
Proj4js.defs["EPSG:25834"]="+proj=utm +zone=34 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 35N
Proj4js.defs["EPSG:25835"]="+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 36N
Proj4js.defs["EPSG:25836"]="+proj=utm +zone=36 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 37N
Proj4js.defs["EPSG:25837"]="+proj=utm +zone=37 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / UTM zone 38N
Proj4js.defs["EPSG:25838"]="+proj=utm +zone=38 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// ETRS89 / TM Baltic93
Proj4js.defs["EPSG:25884"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9996 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Malongo 1987 / UTM zone 32S
Proj4js.defs["EPSG:25932"]="+proj=utm +zone=32 +south +ellps=intl +towgs84=-254.1,-5.36,-100.29,0,0,0,0 +units=m +no_defs  ";
// Merchich / Nord Maroc
Proj4js.defs["EPSG:26191"]="+proj=lcc +lat_1=33.3 +lat_0=33.3 +lon_0=-5.4 +k_0=0.999625769 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=31,146,47,0,0,0,0 +units=m +no_defs  ";
// Merchich / Sud Maroc
Proj4js.defs["EPSG:26192"]="+proj=lcc +lat_1=29.7 +lat_0=29.7 +lon_0=-5.4 +k_0=0.9996155960000001 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=31,146,47,0,0,0,0 +units=m +no_defs  ";
// Merchich / Sahara (deprecated)
Proj4js.defs["EPSG:26193"]="+proj=lcc +lat_1=26.1 +lat_0=26.1 +lon_0=-5.4 +k_0=0.9996 +x_0=1200000 +y_0=400000 +a=6378249.2 +b=6356515 +towgs84=31,146,47,0,0,0,0 +units=m +no_defs  ";
// Merchich / Sahara Nord
Proj4js.defs["EPSG:26194"]="+proj=lcc +lat_1=26.1 +lat_0=26.1 +lon_0=-5.4 +k_0=0.999616304 +x_0=1200000 +y_0=400000 +a=6378249.2 +b=6356515 +towgs84=31,146,47,0,0,0,0 +units=m +no_defs  ";
// Merchich / Sahara Sud
Proj4js.defs["EPSG:26195"]="+proj=lcc +lat_1=22.5 +lat_0=22.5 +lon_0=-5.4 +k_0=0.999616437 +x_0=1500000 +y_0=400000 +a=6378249.2 +b=6356515 +towgs84=31,146,47,0,0,0,0 +units=m +no_defs  ";
// Massawa / UTM zone 37N
Proj4js.defs["EPSG:26237"]="+proj=utm +zone=37 +ellps=bessel +towgs84=639,405,60,0,0,0,0 +units=m +no_defs  ";
// Minna / UTM zone 31N
Proj4js.defs["EPSG:26331"]="+proj=utm +zone=31 +ellps=clrk80 +towgs84=-92,-93,122,0,0,0,0 +units=m +no_defs  ";
// Minna / UTM zone 32N
Proj4js.defs["EPSG:26332"]="+proj=utm +zone=32 +ellps=clrk80 +towgs84=-92,-93,122,0,0,0,0 +units=m +no_defs  ";
// Minna / Nigeria West Belt
Proj4js.defs["EPSG:26391"]="+proj=tmerc +lat_0=4 +lon_0=4.5 +k=0.99975 +x_0=230738.26 +y_0=0 +ellps=clrk80 +towgs84=-92,-93,122,0,0,0,0 +units=m +no_defs  ";
// Minna / Nigeria Mid Belt
Proj4js.defs["EPSG:26392"]="+proj=tmerc +lat_0=4 +lon_0=8.5 +k=0.99975 +x_0=670553.98 +y_0=0 +ellps=clrk80 +towgs84=-92,-93,122,0,0,0,0 +units=m +no_defs  ";
// Minna / Nigeria East Belt
Proj4js.defs["EPSG:26393"]="+proj=tmerc +lat_0=4 +lon_0=12.5 +k=0.99975 +x_0=1110369.7 +y_0=0 +ellps=clrk80 +towgs84=-92,-93,122,0,0,0,0 +units=m +no_defs  ";
// Mhast / UTM zone 32S (deprecated)
Proj4js.defs["EPSG:26432"]="+proj=utm +zone=32 +south +ellps=intl +towgs84=-252.95,-4.11,-96.38,0,0,0,0 +units=m +no_defs  ";
// Monte Mario (Rome) / Italy zone 1 (deprecated)
Proj4js.defs["EPSG:26591"]="+proj=tmerc +lat_0=0 +lon_0=-3.45233333333333 +k=0.9996 +x_0=1500000 +y_0=0 +ellps=intl +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.68 +pm=rome +units=m +no_defs  ";
// Monte Mario (Rome) / Italy zone 2 (deprecated)
Proj4js.defs["EPSG:26592"]="+proj=tmerc +lat_0=0 +lon_0=2.54766666666666 +k=0.9996 +x_0=2520000 +y_0=0 +ellps=intl +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.68 +pm=rome +units=m +no_defs  ";
// M'poraloko / UTM zone 32N
Proj4js.defs["EPSG:26632"]="+proj=utm +zone=32 +a=6378249.2 +b=6356515 +towgs84=-74,-130,42,0,0,0,0 +units=m +no_defs  ";
// M'poraloko / UTM zone 32S
Proj4js.defs["EPSG:26692"]="+proj=utm +zone=32 +south +a=6378249.2 +b=6356515 +towgs84=-74,-130,42,0,0,0,0 +units=m +no_defs  ";
// NAD27 / UTM zone 1N
Proj4js.defs["EPSG:26701"]="+proj=utm +zone=1 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 2N
Proj4js.defs["EPSG:26702"]="+proj=utm +zone=2 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 3N
Proj4js.defs["EPSG:26703"]="+proj=utm +zone=3 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 4N
Proj4js.defs["EPSG:26704"]="+proj=utm +zone=4 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 5N
Proj4js.defs["EPSG:26705"]="+proj=utm +zone=5 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 6N
Proj4js.defs["EPSG:26706"]="+proj=utm +zone=6 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 7N
Proj4js.defs["EPSG:26707"]="+proj=utm +zone=7 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 8N
Proj4js.defs["EPSG:26708"]="+proj=utm +zone=8 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 9N
Proj4js.defs["EPSG:26709"]="+proj=utm +zone=9 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 10N
Proj4js.defs["EPSG:26710"]="+proj=utm +zone=10 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 11N
Proj4js.defs["EPSG:26711"]="+proj=utm +zone=11 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 12N
Proj4js.defs["EPSG:26712"]="+proj=utm +zone=12 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 13N
Proj4js.defs["EPSG:26713"]="+proj=utm +zone=13 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 14N
Proj4js.defs["EPSG:26714"]="+proj=utm +zone=14 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 15N
Proj4js.defs["EPSG:26715"]="+proj=utm +zone=15 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 16N
Proj4js.defs["EPSG:26716"]="+proj=utm +zone=16 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 17N
Proj4js.defs["EPSG:26717"]="+proj=utm +zone=17 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 18N
Proj4js.defs["EPSG:26718"]="+proj=utm +zone=18 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 19N
Proj4js.defs["EPSG:26719"]="+proj=utm +zone=19 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 20N
Proj4js.defs["EPSG:26720"]="+proj=utm +zone=20 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 21N
Proj4js.defs["EPSG:26721"]="+proj=utm +zone=21 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / UTM zone 22N
Proj4js.defs["EPSG:26722"]="+proj=utm +zone=22 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Alabama East
Proj4js.defs["EPSG:26729"]="+proj=tmerc +lat_0=30.5 +lon_0=-85.83333333333333 +k=0.99996 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alabama West
Proj4js.defs["EPSG:26730"]="+proj=tmerc +lat_0=30 +lon_0=-87.5 +k=0.999933333 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 1
Proj4js.defs["EPSG:26731"]="+proj=omerc +lat_0=57 +lonc=-133.6666666666667 +alpha=323.1301023611111 +k=0.9999 +x_0=5000000.001016002 +y_0=-5000000.001016002 +gamma=323.1301023611111 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 2
Proj4js.defs["EPSG:26732"]="+proj=tmerc +lat_0=54 +lon_0=-142 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 3
Proj4js.defs["EPSG:26733"]="+proj=tmerc +lat_0=54 +lon_0=-146 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 4
Proj4js.defs["EPSG:26734"]="+proj=tmerc +lat_0=54 +lon_0=-150 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 5
Proj4js.defs["EPSG:26735"]="+proj=tmerc +lat_0=54 +lon_0=-154 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 6
Proj4js.defs["EPSG:26736"]="+proj=tmerc +lat_0=54 +lon_0=-158 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 7
Proj4js.defs["EPSG:26737"]="+proj=tmerc +lat_0=54 +lon_0=-162 +k=0.9999 +x_0=213360.4267208534 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 8
Proj4js.defs["EPSG:26738"]="+proj=tmerc +lat_0=54 +lon_0=-166 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 9
Proj4js.defs["EPSG:26739"]="+proj=tmerc +lat_0=54 +lon_0=-170 +k=0.9999 +x_0=182880.3657607315 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Alaska zone 10
Proj4js.defs["EPSG:26740"]="+proj=lcc +lat_1=53.83333333333334 +lat_2=51.83333333333334 +lat_0=51 +lon_0=-176 +x_0=914401.8288036576 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone I
Proj4js.defs["EPSG:26741"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone II
Proj4js.defs["EPSG:26742"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone III
Proj4js.defs["EPSG:26743"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone IV
Proj4js.defs["EPSG:26744"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone V
Proj4js.defs["EPSG:26745"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone VI
Proj4js.defs["EPSG:26746"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone VII (deprecated)
Proj4js.defs["EPSG:26747"]="+proj=lcc +lat_1=34.41666666666666 +lat_2=33.86666666666667 +lat_0=34.13333333333333 +lon_0=-118.3333333333333 +x_0=1276106.450596901 +y_0=127079.524511049 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Arizona East
Proj4js.defs["EPSG:26748"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Arizona Central
Proj4js.defs["EPSG:26749"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Arizona West
Proj4js.defs["EPSG:26750"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Arkansas North
Proj4js.defs["EPSG:26751"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Arkansas South
Proj4js.defs["EPSG:26752"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Colorado North
Proj4js.defs["EPSG:26753"]="+proj=lcc +lat_1=39.71666666666667 +lat_2=40.78333333333333 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Colorado Central
Proj4js.defs["EPSG:26754"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Colorado South
Proj4js.defs["EPSG:26755"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Connecticut
Proj4js.defs["EPSG:26756"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=182880.3657607315 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Delaware
Proj4js.defs["EPSG:26757"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Florida East
Proj4js.defs["EPSG:26758"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Florida West
Proj4js.defs["EPSG:26759"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Florida North
Proj4js.defs["EPSG:26760"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Georgia East
Proj4js.defs["EPSG:26766"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Georgia West
Proj4js.defs["EPSG:26767"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Idaho East
Proj4js.defs["EPSG:26768"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Idaho Central
Proj4js.defs["EPSG:26769"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Idaho West
Proj4js.defs["EPSG:26770"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Illinois East
Proj4js.defs["EPSG:26771"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Illinois West
Proj4js.defs["EPSG:26772"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Indiana East
Proj4js.defs["EPSG:26773"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Indiana West
Proj4js.defs["EPSG:26774"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Iowa North
Proj4js.defs["EPSG:26775"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Iowa South
Proj4js.defs["EPSG:26776"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Kansas North
Proj4js.defs["EPSG:26777"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Kansas South
Proj4js.defs["EPSG:26778"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Kentucky North
Proj4js.defs["EPSG:26779"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=38.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Kentucky South
Proj4js.defs["EPSG:26780"]="+proj=lcc +lat_1=36.73333333333333 +lat_2=37.93333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Louisiana North
Proj4js.defs["EPSG:26781"]="+proj=lcc +lat_1=31.16666666666667 +lat_2=32.66666666666666 +lat_0=30.66666666666667 +lon_0=-92.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Louisiana South
Proj4js.defs["EPSG:26782"]="+proj=lcc +lat_1=29.3 +lat_2=30.7 +lat_0=28.66666666666667 +lon_0=-91.33333333333333 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Maine East
Proj4js.defs["EPSG:26783"]="+proj=tmerc +lat_0=43.83333333333334 +lon_0=-68.5 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Maine West
Proj4js.defs["EPSG:26784"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Maryland
Proj4js.defs["EPSG:26785"]="+proj=lcc +lat_1=38.3 +lat_2=39.45 +lat_0=37.83333333333334 +lon_0=-77 +x_0=243840.4876809754 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Massachusetts Mainland
Proj4js.defs["EPSG:26786"]="+proj=lcc +lat_1=41.71666666666667 +lat_2=42.68333333333333 +lat_0=41 +lon_0=-71.5 +x_0=182880.3657607315 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Massachusetts Island
Proj4js.defs["EPSG:26787"]="+proj=lcc +lat_1=41.28333333333333 +lat_2=41.48333333333333 +lat_0=41 +lon_0=-70.5 +x_0=60960.12192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Minnesota North
Proj4js.defs["EPSG:26791"]="+proj=lcc +lat_1=47.03333333333333 +lat_2=48.63333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Minnesota Central
Proj4js.defs["EPSG:26792"]="+proj=lcc +lat_1=45.61666666666667 +lat_2=47.05 +lat_0=45 +lon_0=-94.25 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Minnesota South
Proj4js.defs["EPSG:26793"]="+proj=lcc +lat_1=43.78333333333333 +lat_2=45.21666666666667 +lat_0=43 +lon_0=-94 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Mississippi East
Proj4js.defs["EPSG:26794"]="+proj=tmerc +lat_0=29.66666666666667 +lon_0=-88.83333333333333 +k=0.99996 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Mississippi West
Proj4js.defs["EPSG:26795"]="+proj=tmerc +lat_0=30.5 +lon_0=-90.33333333333333 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Missouri East
Proj4js.defs["EPSG:26796"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-90.5 +k=0.999933333 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Missouri Central
Proj4js.defs["EPSG:26797"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-92.5 +k=0.999933333 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Missouri West
Proj4js.defs["EPSG:26798"]="+proj=tmerc +lat_0=36.16666666666666 +lon_0=-94.5 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / California zone VII
Proj4js.defs["EPSG:26799"]="+proj=lcc +lat_1=34.41666666666666 +lat_2=33.86666666666667 +lat_0=34.13333333333333 +lon_0=-118.3333333333333 +x_0=1276106.450596901 +y_0=1268253.006858014 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD Michigan / Michigan East
Proj4js.defs["EPSG:26801"]="+proj=tmerc +lat_0=41.5 +lon_0=-83.66666666666667 +k=0.999942857 +x_0=152400.3048006096 +y_0=0 +a=6378450.047548896 +b=6356826.621488444 +units=us-ft +no_defs  ";
// NAD Michigan / Michigan Old Central
Proj4js.defs["EPSG:26802"]="+proj=tmerc +lat_0=41.5 +lon_0=-85.75 +k=0.999909091 +x_0=152400.3048006096 +y_0=0 +a=6378450.047548896 +b=6356826.621488444 +units=us-ft +no_defs  ";
// NAD Michigan / Michigan West
Proj4js.defs["EPSG:26803"]="+proj=tmerc +lat_0=41.5 +lon_0=-88.75 +k=0.999909091 +x_0=152400.3048006096 +y_0=0 +a=6378450.047548896 +b=6356826.621488444 +units=us-ft +no_defs  ";
// NAD Michigan / Michigan North
Proj4js.defs["EPSG:26811"]="+proj=lcc +lat_1=45.48333333333333 +lat_2=47.08333333333334 +lat_0=44.78333333333333 +lon_0=-87 +x_0=609601.2192024384 +y_0=0 +a=6378450.047548896 +b=6356826.621488444 +units=us-ft +no_defs  ";
// NAD Michigan / Michigan Central
Proj4js.defs["EPSG:26812"]="+proj=lcc +lat_1=44.18333333333333 +lat_2=45.7 +lat_0=43.31666666666667 +lon_0=-84.33333333333333 +x_0=609601.2192024384 +y_0=0 +a=6378450.047548896 +b=6356826.621488444 +units=us-ft +no_defs  ";
// NAD Michigan / Michigan South
Proj4js.defs["EPSG:26813"]="+proj=lcc +lat_1=42.1 +lat_2=43.66666666666666 +lat_0=41.5 +lon_0=-84.33333333333333 +x_0=609601.2192024384 +y_0=0 +a=6378450.047548896 +b=6356826.621488444 +units=us-ft +no_defs  ";
// NAD83 / Maine East (ftUS) (deprecated)
Proj4js.defs["EPSG:26814"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine West (ftUS) (deprecated)
Proj4js.defs["EPSG:26815"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Minnesota North (ftUS) (deprecated)
Proj4js.defs["EPSG:26819"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Minnesota Central (ftUS) (deprecated)
Proj4js.defs["EPSG:26820"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Minnesota South (ftUS) (deprecated)
Proj4js.defs["EPSG:26821"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Nebraska (ftUS) (deprecated)
Proj4js.defs["EPSG:26822"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.0000101601 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / West Virginia North (ftUS) (deprecated)
Proj4js.defs["EPSG:26823"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=1968500 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / West Virginia South (ftUS) (deprecated)
Proj4js.defs["EPSG:26824"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=1968500 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine East (ftUS) (deprecated)
Proj4js.defs["EPSG:26825"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Maine West (ftUS) (deprecated)
Proj4js.defs["EPSG:26826"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Minnesota North (ftUS) (deprecated)
Proj4js.defs["EPSG:26830"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Minnesota Central (ftUS) (deprecated)
Proj4js.defs["EPSG:26831"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Minnesota South (ftUS) (deprecated)
Proj4js.defs["EPSG:26832"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / Nebraska (ftUS) (deprecated)
Proj4js.defs["EPSG:26833"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.0000101601 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / West Virginia North (ftUS) (deprecated)
Proj4js.defs["EPSG:26834"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=1968500 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(HARN) / West Virginia South (ftUS) (deprecated)
Proj4js.defs["EPSG:26835"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=1968500 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maine East (ftUS) (deprecated)
Proj4js.defs["EPSG:26836"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Maine West (ftUS) (deprecated)
Proj4js.defs["EPSG:26837"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Minnesota North (ftUS) (deprecated)
Proj4js.defs["EPSG:26841"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Minnesota Central (ftUS) (deprecated)
Proj4js.defs["EPSG:26842"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Minnesota South (ftUS) (deprecated)
Proj4js.defs["EPSG:26843"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000.0000101601 +y_0=99999.99998984 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / Nebraska (ftUS) (deprecated)
Proj4js.defs["EPSG:26844"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.0000101601 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / West Virginia North (ftUS) (deprecated)
Proj4js.defs["EPSG:26845"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=1968500 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(NSRS2007) / West Virginia South (ftUS) (deprecated)
Proj4js.defs["EPSG:26846"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=1968500 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine East (ftUS)
Proj4js.defs["EPSG:26847"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Maine West (ftUS)
Proj4js.defs["EPSG:26848"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Minnesota North (ftUS)
Proj4js.defs["EPSG:26849"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Minnesota Central (ftUS)
Proj4js.defs["EPSG:26850"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Minnesota South (ftUS)
Proj4js.defs["EPSG:26851"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / Nebraska (ftUS)
Proj4js.defs["EPSG:26852"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / West Virginia North (ftUS)
Proj4js.defs["EPSG:26853"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / West Virginia South (ftUS)
Proj4js.defs["EPSG:26854"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Maine East (ftUS)
Proj4js.defs["EPSG:26855"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Maine West (ftUS)
Proj4js.defs["EPSG:26856"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Minnesota North (ftUS)
Proj4js.defs["EPSG:26857"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Minnesota Central (ftUS)
Proj4js.defs["EPSG:26858"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Minnesota South (ftUS)
Proj4js.defs["EPSG:26859"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / Nebraska (ftUS)
Proj4js.defs["EPSG:26860"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / West Virginia North (ftUS)
Proj4js.defs["EPSG:26861"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(HARN) / West Virginia South (ftUS)
Proj4js.defs["EPSG:26862"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Maine East (ftUS)
Proj4js.defs["EPSG:26863"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000.0000000001 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Maine West (ftUS)
Proj4js.defs["EPSG:26864"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Minnesota North (ftUS)
Proj4js.defs["EPSG:26865"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Minnesota Central (ftUS)
Proj4js.defs["EPSG:26866"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Minnesota South (ftUS)
Proj4js.defs["EPSG:26867"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000.0000101599 +y_0=99999.99998983997 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / Nebraska (ftUS)
Proj4js.defs["EPSG:26868"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.00001016 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / West Virginia North (ftUS)
Proj4js.defs["EPSG:26869"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(NSRS2007) / West Virginia South (ftUS)
Proj4js.defs["EPSG:26870"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83(CSRS) / MTM zone 11
Proj4js.defs["EPSG:26891"]="+proj=tmerc +lat_0=0 +lon_0=-82.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 12
Proj4js.defs["EPSG:26892"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 13
Proj4js.defs["EPSG:26893"]="+proj=tmerc +lat_0=0 +lon_0=-84 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 14
Proj4js.defs["EPSG:26894"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 15
Proj4js.defs["EPSG:26895"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 16
Proj4js.defs["EPSG:26896"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 17
Proj4js.defs["EPSG:26897"]="+proj=tmerc +lat_0=0 +lon_0=-96 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 1
Proj4js.defs["EPSG:26898"]="+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83(CSRS) / MTM zone 2
Proj4js.defs["EPSG:26899"]="+proj=tmerc +lat_0=0 +lon_0=-56 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 1N
Proj4js.defs["EPSG:26901"]="+proj=utm +zone=1 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 2N
Proj4js.defs["EPSG:26902"]="+proj=utm +zone=2 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 3N
Proj4js.defs["EPSG:26903"]="+proj=utm +zone=3 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 4N
Proj4js.defs["EPSG:26904"]="+proj=utm +zone=4 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 5N
Proj4js.defs["EPSG:26905"]="+proj=utm +zone=5 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 6N
Proj4js.defs["EPSG:26906"]="+proj=utm +zone=6 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 7N
Proj4js.defs["EPSG:26907"]="+proj=utm +zone=7 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 8N
Proj4js.defs["EPSG:26908"]="+proj=utm +zone=8 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 9N
Proj4js.defs["EPSG:26909"]="+proj=utm +zone=9 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 10N
Proj4js.defs["EPSG:26910"]="+proj=utm +zone=10 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 11N
Proj4js.defs["EPSG:26911"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 12N
Proj4js.defs["EPSG:26912"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 13N
Proj4js.defs["EPSG:26913"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 14N
Proj4js.defs["EPSG:26914"]="+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 15N
Proj4js.defs["EPSG:26915"]="+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 16N
Proj4js.defs["EPSG:26916"]="+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 17N
Proj4js.defs["EPSG:26917"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 18N
Proj4js.defs["EPSG:26918"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 19N
Proj4js.defs["EPSG:26919"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 20N
Proj4js.defs["EPSG:26920"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 21N
Proj4js.defs["EPSG:26921"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 22N
Proj4js.defs["EPSG:26922"]="+proj=utm +zone=22 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / UTM zone 23N
Proj4js.defs["EPSG:26923"]="+proj=utm +zone=23 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alabama East
Proj4js.defs["EPSG:26929"]="+proj=tmerc +lat_0=30.5 +lon_0=-85.83333333333333 +k=0.99996 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alabama West
Proj4js.defs["EPSG:26930"]="+proj=tmerc +lat_0=30 +lon_0=-87.5 +k=0.999933333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 1
Proj4js.defs["EPSG:26931"]="+proj=omerc +lat_0=57 +lonc=-133.6666666666667 +alpha=323.1301023611111 +k=0.9999 +x_0=5000000 +y_0=-5000000 +gamma=323.1301023611111 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 2
Proj4js.defs["EPSG:26932"]="+proj=tmerc +lat_0=54 +lon_0=-142 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 3
Proj4js.defs["EPSG:26933"]="+proj=tmerc +lat_0=54 +lon_0=-146 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 4
Proj4js.defs["EPSG:26934"]="+proj=tmerc +lat_0=54 +lon_0=-150 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 5
Proj4js.defs["EPSG:26935"]="+proj=tmerc +lat_0=54 +lon_0=-154 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 6
Proj4js.defs["EPSG:26936"]="+proj=tmerc +lat_0=54 +lon_0=-158 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 7
Proj4js.defs["EPSG:26937"]="+proj=tmerc +lat_0=54 +lon_0=-162 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 8
Proj4js.defs["EPSG:26938"]="+proj=tmerc +lat_0=54 +lon_0=-166 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 9
Proj4js.defs["EPSG:26939"]="+proj=tmerc +lat_0=54 +lon_0=-170 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Alaska zone 10
Proj4js.defs["EPSG:26940"]="+proj=lcc +lat_1=53.83333333333334 +lat_2=51.83333333333334 +lat_0=51 +lon_0=-176 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / California zone 1
Proj4js.defs["EPSG:26941"]="+proj=lcc +lat_1=41.66666666666666 +lat_2=40 +lat_0=39.33333333333334 +lon_0=-122 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / California zone 2
Proj4js.defs["EPSG:26942"]="+proj=lcc +lat_1=39.83333333333334 +lat_2=38.33333333333334 +lat_0=37.66666666666666 +lon_0=-122 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / California zone 3
Proj4js.defs["EPSG:26943"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.06666666666667 +lat_0=36.5 +lon_0=-120.5 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / California zone 4
Proj4js.defs["EPSG:26944"]="+proj=lcc +lat_1=37.25 +lat_2=36 +lat_0=35.33333333333334 +lon_0=-119 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / California zone 5
Proj4js.defs["EPSG:26945"]="+proj=lcc +lat_1=35.46666666666667 +lat_2=34.03333333333333 +lat_0=33.5 +lon_0=-118 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / California zone 6
Proj4js.defs["EPSG:26946"]="+proj=lcc +lat_1=33.88333333333333 +lat_2=32.78333333333333 +lat_0=32.16666666666666 +lon_0=-116.25 +x_0=2000000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Arizona East
Proj4js.defs["EPSG:26948"]="+proj=tmerc +lat_0=31 +lon_0=-110.1666666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Arizona Central
Proj4js.defs["EPSG:26949"]="+proj=tmerc +lat_0=31 +lon_0=-111.9166666666667 +k=0.9999 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Arizona West
Proj4js.defs["EPSG:26950"]="+proj=tmerc +lat_0=31 +lon_0=-113.75 +k=0.999933333 +x_0=213360 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Arkansas North
Proj4js.defs["EPSG:26951"]="+proj=lcc +lat_1=36.23333333333333 +lat_2=34.93333333333333 +lat_0=34.33333333333334 +lon_0=-92 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Arkansas South
Proj4js.defs["EPSG:26952"]="+proj=lcc +lat_1=34.76666666666667 +lat_2=33.3 +lat_0=32.66666666666666 +lon_0=-92 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Colorado North
Proj4js.defs["EPSG:26953"]="+proj=lcc +lat_1=40.78333333333333 +lat_2=39.71666666666667 +lat_0=39.33333333333334 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Colorado Central
Proj4js.defs["EPSG:26954"]="+proj=lcc +lat_1=39.75 +lat_2=38.45 +lat_0=37.83333333333334 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Colorado South
Proj4js.defs["EPSG:26955"]="+proj=lcc +lat_1=38.43333333333333 +lat_2=37.23333333333333 +lat_0=36.66666666666666 +lon_0=-105.5 +x_0=914401.8289 +y_0=304800.6096 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Connecticut
Proj4js.defs["EPSG:26956"]="+proj=lcc +lat_1=41.86666666666667 +lat_2=41.2 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096 +y_0=152400.3048 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Delaware
Proj4js.defs["EPSG:26957"]="+proj=tmerc +lat_0=38 +lon_0=-75.41666666666667 +k=0.999995 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Florida East
Proj4js.defs["EPSG:26958"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-81 +k=0.999941177 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Florida West
Proj4js.defs["EPSG:26959"]="+proj=tmerc +lat_0=24.33333333333333 +lon_0=-82 +k=0.999941177 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Florida North
Proj4js.defs["EPSG:26960"]="+proj=lcc +lat_1=30.75 +lat_2=29.58333333333333 +lat_0=29 +lon_0=-84.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Hawaii zone 1
Proj4js.defs["EPSG:26961"]="+proj=tmerc +lat_0=18.83333333333333 +lon_0=-155.5 +k=0.999966667 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Hawaii zone 2
Proj4js.defs["EPSG:26962"]="+proj=tmerc +lat_0=20.33333333333333 +lon_0=-156.6666666666667 +k=0.999966667 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Hawaii zone 3
Proj4js.defs["EPSG:26963"]="+proj=tmerc +lat_0=21.16666666666667 +lon_0=-158 +k=0.99999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Hawaii zone 4
Proj4js.defs["EPSG:26964"]="+proj=tmerc +lat_0=21.83333333333333 +lon_0=-159.5 +k=0.99999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Hawaii zone 5
Proj4js.defs["EPSG:26965"]="+proj=tmerc +lat_0=21.66666666666667 +lon_0=-160.1666666666667 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Georgia East
Proj4js.defs["EPSG:26966"]="+proj=tmerc +lat_0=30 +lon_0=-82.16666666666667 +k=0.9999 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Georgia West
Proj4js.defs["EPSG:26967"]="+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Idaho East
Proj4js.defs["EPSG:26968"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-112.1666666666667 +k=0.9999473679999999 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Idaho Central
Proj4js.defs["EPSG:26969"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-114 +k=0.9999473679999999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Idaho West
Proj4js.defs["EPSG:26970"]="+proj=tmerc +lat_0=41.66666666666666 +lon_0=-115.75 +k=0.999933333 +x_0=800000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Illinois East
Proj4js.defs["EPSG:26971"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-88.33333333333333 +k=0.9999749999999999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Illinois West
Proj4js.defs["EPSG:26972"]="+proj=tmerc +lat_0=36.66666666666666 +lon_0=-90.16666666666667 +k=0.999941177 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Indiana East
Proj4js.defs["EPSG:26973"]="+proj=tmerc +lat_0=37.5 +lon_0=-85.66666666666667 +k=0.999966667 +x_0=100000 +y_0=250000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Indiana West
Proj4js.defs["EPSG:26974"]="+proj=tmerc +lat_0=37.5 +lon_0=-87.08333333333333 +k=0.999966667 +x_0=900000 +y_0=250000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Iowa North
Proj4js.defs["EPSG:26975"]="+proj=lcc +lat_1=43.26666666666667 +lat_2=42.06666666666667 +lat_0=41.5 +lon_0=-93.5 +x_0=1500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Iowa South
Proj4js.defs["EPSG:26976"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.61666666666667 +lat_0=40 +lon_0=-93.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Kansas North
Proj4js.defs["EPSG:26977"]="+proj=lcc +lat_1=39.78333333333333 +lat_2=38.71666666666667 +lat_0=38.33333333333334 +lon_0=-98 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Kansas South
Proj4js.defs["EPSG:26978"]="+proj=lcc +lat_1=38.56666666666667 +lat_2=37.26666666666667 +lat_0=36.66666666666666 +lon_0=-98.5 +x_0=400000 +y_0=400000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Kentucky North (deprecated)
Proj4js.defs["EPSG:26979"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=37.96666666666667 +lat_0=37.5 +lon_0=-84.25 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Kentucky South
Proj4js.defs["EPSG:26980"]="+proj=lcc +lat_1=37.93333333333333 +lat_2=36.73333333333333 +lat_0=36.33333333333334 +lon_0=-85.75 +x_0=500000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Louisiana North
Proj4js.defs["EPSG:26981"]="+proj=lcc +lat_1=32.66666666666666 +lat_2=31.16666666666667 +lat_0=30.5 +lon_0=-92.5 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Louisiana South
Proj4js.defs["EPSG:26982"]="+proj=lcc +lat_1=30.7 +lat_2=29.3 +lat_0=28.5 +lon_0=-91.33333333333333 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine East
Proj4js.defs["EPSG:26983"]="+proj=tmerc +lat_0=43.66666666666666 +lon_0=-68.5 +k=0.9999 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maine West
Proj4js.defs["EPSG:26984"]="+proj=tmerc +lat_0=42.83333333333334 +lon_0=-70.16666666666667 +k=0.999966667 +x_0=900000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Maryland
Proj4js.defs["EPSG:26985"]="+proj=lcc +lat_1=39.45 +lat_2=38.3 +lat_0=37.66666666666666 +lon_0=-77 +x_0=400000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Massachusetts Mainland
Proj4js.defs["EPSG:26986"]="+proj=lcc +lat_1=42.68333333333333 +lat_2=41.71666666666667 +lat_0=41 +lon_0=-71.5 +x_0=200000 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Massachusetts Island
Proj4js.defs["EPSG:26987"]="+proj=lcc +lat_1=41.48333333333333 +lat_2=41.28333333333333 +lat_0=41 +lon_0=-70.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Michigan North
Proj4js.defs["EPSG:26988"]="+proj=lcc +lat_1=47.08333333333334 +lat_2=45.48333333333333 +lat_0=44.78333333333333 +lon_0=-87 +x_0=8000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Michigan Central
Proj4js.defs["EPSG:26989"]="+proj=lcc +lat_1=45.7 +lat_2=44.18333333333333 +lat_0=43.31666666666667 +lon_0=-84.36666666666666 +x_0=6000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Michigan South
Proj4js.defs["EPSG:26990"]="+proj=lcc +lat_1=43.66666666666666 +lat_2=42.1 +lat_0=41.5 +lon_0=-84.36666666666666 +x_0=4000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Minnesota North
Proj4js.defs["EPSG:26991"]="+proj=lcc +lat_1=48.63333333333333 +lat_2=47.03333333333333 +lat_0=46.5 +lon_0=-93.09999999999999 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Minnesota Central
Proj4js.defs["EPSG:26992"]="+proj=lcc +lat_1=47.05 +lat_2=45.61666666666667 +lat_0=45 +lon_0=-94.25 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Minnesota South
Proj4js.defs["EPSG:26993"]="+proj=lcc +lat_1=45.21666666666667 +lat_2=43.78333333333333 +lat_0=43 +lon_0=-94 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Mississippi East
Proj4js.defs["EPSG:26994"]="+proj=tmerc +lat_0=29.5 +lon_0=-88.83333333333333 +k=0.99995 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Mississippi West
Proj4js.defs["EPSG:26995"]="+proj=tmerc +lat_0=29.5 +lon_0=-90.33333333333333 +k=0.99995 +x_0=700000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Missouri East
Proj4js.defs["EPSG:26996"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-90.5 +k=0.999933333 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Missouri Central
Proj4js.defs["EPSG:26997"]="+proj=tmerc +lat_0=35.83333333333334 +lon_0=-92.5 +k=0.999933333 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Missouri West
Proj4js.defs["EPSG:26998"]="+proj=tmerc +lat_0=36.16666666666666 +lon_0=-94.5 +k=0.999941177 +x_0=850000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Nahrwan 1967 / UTM zone 37N
Proj4js.defs["EPSG:27037"]="+proj=utm +zone=37 +ellps=clrk80 +towgs84=-243,-192,477,0,0,0,0 +units=m +no_defs  ";
// Nahrwan 1967 / UTM zone 38N
Proj4js.defs["EPSG:27038"]="+proj=utm +zone=38 +ellps=clrk80 +towgs84=-243,-192,477,0,0,0,0 +units=m +no_defs  ";
// Nahrwan 1967 / UTM zone 39N
Proj4js.defs["EPSG:27039"]="+proj=utm +zone=39 +ellps=clrk80 +towgs84=-243,-192,477,0,0,0,0 +units=m +no_defs  ";
// Nahrwan 1967 / UTM zone 40N
Proj4js.defs["EPSG:27040"]="+proj=utm +zone=40 +ellps=clrk80 +towgs84=-243,-192,477,0,0,0,0 +units=m +no_defs  ";
// Naparima 1972 / UTM zone 20N
Proj4js.defs["EPSG:27120"]="+proj=utm +zone=20 +ellps=intl +towgs84=-10,375,165,0,0,0,0 +units=m +no_defs  ";
// NZGD49 / New Zealand Map Grid
Proj4js.defs["EPSG:27200"]="+proj=nzmg +lat_0=-41 +lon_0=173 +x_0=2510000 +y_0=6023150 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Mount Eden Circuit
Proj4js.defs["EPSG:27205"]="+proj=tmerc +lat_0=-36.87986527777778 +lon_0=174.7643393611111 +k=0.9999 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Bay of Plenty Circuit
Proj4js.defs["EPSG:27206"]="+proj=tmerc +lat_0=-37.76124980555556 +lon_0=176.46619725 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Poverty Bay Circuit
Proj4js.defs["EPSG:27207"]="+proj=tmerc +lat_0=-38.62470277777778 +lon_0=177.8856362777778 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Hawkes Bay Circuit
Proj4js.defs["EPSG:27208"]="+proj=tmerc +lat_0=-39.65092930555556 +lon_0=176.6736805277778 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Taranaki Circuit
Proj4js.defs["EPSG:27209"]="+proj=tmerc +lat_0=-39.13575830555556 +lon_0=174.22801175 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Tuhirangi Circuit
Proj4js.defs["EPSG:27210"]="+proj=tmerc +lat_0=-39.51247038888889 +lon_0=175.6400368055556 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Wanganui Circuit
Proj4js.defs["EPSG:27211"]="+proj=tmerc +lat_0=-40.24194713888889 +lon_0=175.4880996111111 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Wairarapa Circuit
Proj4js.defs["EPSG:27212"]="+proj=tmerc +lat_0=-40.92553263888889 +lon_0=175.6473496666667 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Wellington Circuit
Proj4js.defs["EPSG:27213"]="+proj=tmerc +lat_0=-41.30131963888888 +lon_0=174.7766231111111 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Collingwood Circuit
Proj4js.defs["EPSG:27214"]="+proj=tmerc +lat_0=-40.71475905555556 +lon_0=172.6720465 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Nelson Circuit
Proj4js.defs["EPSG:27215"]="+proj=tmerc +lat_0=-41.27454472222222 +lon_0=173.2993168055555 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Karamea Circuit
Proj4js.defs["EPSG:27216"]="+proj=tmerc +lat_0=-41.28991152777778 +lon_0=172.1090281944444 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Buller Circuit
Proj4js.defs["EPSG:27217"]="+proj=tmerc +lat_0=-41.81080286111111 +lon_0=171.5812600555556 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Grey Circuit
Proj4js.defs["EPSG:27218"]="+proj=tmerc +lat_0=-42.33369427777778 +lon_0=171.5497713055556 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Amuri Circuit
Proj4js.defs["EPSG:27219"]="+proj=tmerc +lat_0=-42.68911658333333 +lon_0=173.0101333888889 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Marlborough Circuit
Proj4js.defs["EPSG:27220"]="+proj=tmerc +lat_0=-41.54448666666666 +lon_0=173.8020741111111 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Hokitika Circuit
Proj4js.defs["EPSG:27221"]="+proj=tmerc +lat_0=-42.88632236111111 +lon_0=170.9799935 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Okarito Circuit
Proj4js.defs["EPSG:27222"]="+proj=tmerc +lat_0=-43.11012813888889 +lon_0=170.2609258333333 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Jacksons Bay Circuit
Proj4js.defs["EPSG:27223"]="+proj=tmerc +lat_0=-43.97780288888889 +lon_0=168.606267 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Mount Pleasant Circuit
Proj4js.defs["EPSG:27224"]="+proj=tmerc +lat_0=-43.59063758333333 +lon_0=172.7271935833333 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Gawler Circuit
Proj4js.defs["EPSG:27225"]="+proj=tmerc +lat_0=-43.74871155555556 +lon_0=171.3607484722222 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Timaru Circuit
Proj4js.defs["EPSG:27226"]="+proj=tmerc +lat_0=-44.40222036111111 +lon_0=171.0572508333333 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Lindis Peak Circuit
Proj4js.defs["EPSG:27227"]="+proj=tmerc +lat_0=-44.73526797222222 +lon_0=169.4677550833333 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Mount Nicholas Circuit
Proj4js.defs["EPSG:27228"]="+proj=tmerc +lat_0=-45.13290258333333 +lon_0=168.3986411944444 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Mount York Circuit
Proj4js.defs["EPSG:27229"]="+proj=tmerc +lat_0=-45.56372616666666 +lon_0=167.7388617777778 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Observation Point Circuit
Proj4js.defs["EPSG:27230"]="+proj=tmerc +lat_0=-45.81619661111111 +lon_0=170.6285951666667 +k=1 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / North Taieri Circuit
Proj4js.defs["EPSG:27231"]="+proj=tmerc +lat_0=-45.86151336111111 +lon_0=170.2825891111111 +k=0.99996 +x_0=300000 +y_0=700000 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / Bluff Circuit
Proj4js.defs["EPSG:27232"]="+proj=tmerc +lat_0=-46.60000961111111 +lon_0=168.342872 +k=1 +x_0=300002.66 +y_0=699999.58 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / UTM zone 58S
Proj4js.defs["EPSG:27258"]="+proj=utm +zone=58 +south +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / UTM zone 59S
Proj4js.defs["EPSG:27259"]="+proj=utm +zone=59 +south +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / UTM zone 60S
Proj4js.defs["EPSG:27260"]="+proj=utm +zone=60 +south +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +units=m +no_defs  ";
// NZGD49 / North Island Grid
Proj4js.defs["EPSG:27291"]="+proj=tmerc +lat_0=-39 +lon_0=175.5 +k=1 +x_0=274319.5243848086 +y_0=365759.3658464114 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +to_meter=0.9143984146160287 +no_defs  ";
// NZGD49 / South Island Grid
Proj4js.defs["EPSG:27292"]="+proj=tmerc +lat_0=-44 +lon_0=171.5 +k=1 +x_0=457199.2073080143 +y_0=457199.2073080143 +ellps=intl +towgs84=59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993 +to_meter=0.9143984146160287 +no_defs  ";
// NGO 1948 (Oslo) / NGO zone I
Proj4js.defs["EPSG:27391"]="+proj=tmerc +lat_0=58 +lon_0=-4.666666666666667 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone II
Proj4js.defs["EPSG:27392"]="+proj=tmerc +lat_0=58 +lon_0=-2.333333333333333 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone III
Proj4js.defs["EPSG:27393"]="+proj=tmerc +lat_0=58 +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone IV
Proj4js.defs["EPSG:27394"]="+proj=tmerc +lat_0=58 +lon_0=2.5 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone V
Proj4js.defs["EPSG:27395"]="+proj=tmerc +lat_0=58 +lon_0=6.166666666666667 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone VI
Proj4js.defs["EPSG:27396"]="+proj=tmerc +lat_0=58 +lon_0=10.16666666666667 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone VII
Proj4js.defs["EPSG:27397"]="+proj=tmerc +lat_0=58 +lon_0=14.16666666666667 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// NGO 1948 (Oslo) / NGO zone VIII
Proj4js.defs["EPSG:27398"]="+proj=tmerc +lat_0=58 +lon_0=18.33333333333333 +k=1 +x_0=0 +y_0=0 +a=6377492.018 +b=6356173.508712696 +towgs84=278.3,93,474.5,7.889,0.05,-6.61,6.21 +pm=oslo +units=m +no_defs  ";
// Datum 73 / UTM zone 29N
Proj4js.defs["EPSG:27429"]="+proj=utm +zone=29 +ellps=intl +towgs84=-223.237,110.193,36.649,0,0,0,0 +units=m +no_defs  ";
// Datum 73 / Modified Portuguese Grid (deprecated)
Proj4js.defs["EPSG:27492"]="+proj=tmerc +lat_0=39.66666666666666 +lon_0=-8.131906111111112 +k=1 +x_0=180.598 +y_0=-86.98999999999999 +ellps=intl +towgs84=-223.237,110.193,36.649,0,0,0,0 +units=m +no_defs  ";
// Datum 73 / Modified Portuguese Grid
Proj4js.defs["EPSG:27493"]="+proj=tmerc +lat_0=39.66666666666666 +lon_0=-8.131906111111112 +k=1 +x_0=180.598 +y_0=-86.98999999999999 +ellps=intl +towgs84=-223.237,110.193,36.649,0,0,0,0 +units=m +no_defs  ";
// ATF (Paris) / Nord de Guerre
Proj4js.defs["EPSG:27500"]="+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=5.399999999999999 +k_0=0.99950908 +x_0=500000 +y_0=300000 +a=6376523 +b=6355862.933255573 +pm=2.337208333333333 +units=m +no_defs  ";
// NTF (Paris) / Lambert Nord France
Proj4js.defs["EPSG:27561"]="+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert Centre France
Proj4js.defs["EPSG:27562"]="+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert Sud France
Proj4js.defs["EPSG:27563"]="+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert Corse
Proj4js.defs["EPSG:27564"]="+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert zone I
Proj4js.defs["EPSG:27571"]="+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=1200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert zone II
Proj4js.defs["EPSG:27572"]="+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert zone III
Proj4js.defs["EPSG:27573"]="+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=3200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Lambert zone IV
Proj4js.defs["EPSG:27574"]="+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=4185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / France I (deprecated)
Proj4js.defs["EPSG:27581"]="+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=1200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / France II (deprecated)
Proj4js.defs["EPSG:27582"]="+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / France III (deprecated)
Proj4js.defs["EPSG:27583"]="+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=3200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / France IV (deprecated)
Proj4js.defs["EPSG:27584"]="+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=4185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Nord France (deprecated)
Proj4js.defs["EPSG:27591"]="+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Centre France (deprecated)
Proj4js.defs["EPSG:27592"]="+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Sud France (deprecated)
Proj4js.defs["EPSG:27593"]="+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// NTF (Paris) / Corse (deprecated)
Proj4js.defs["EPSG:27594"]="+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs  ";
// OSGB 1936 / British National Grid
Proj4js.defs["EPSG:27700"]="+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +towgs84=375,-111,431,0,0,0,0 +units=m +no_defs  ";
// Palestine 1923 / Palestine Grid
Proj4js.defs["EPSG:28191"]="+proj=cass +lat_0=31.73409694444445 +lon_0=35.21208055555556 +x_0=170251.555 +y_0=126867.909 +a=6378300.789 +b=6356566.435 +towgs84=-275.722,94.7824,340.894,-8.001,-4.42,-11.821,1 +units=m +no_defs  ";
// Palestine 1923 / Palestine Belt
Proj4js.defs["EPSG:28192"]="+proj=tmerc +lat_0=31.73409694444445 +lon_0=35.21208055555556 +k=1 +x_0=170251.555 +y_0=1126867.909 +a=6378300.789 +b=6356566.435 +towgs84=-275.722,94.7824,340.894,-8.001,-4.42,-11.821,1 +units=m +no_defs  ";
// Palestine 1923 / Israeli CS Grid
Proj4js.defs["EPSG:28193"]="+proj=cass +lat_0=31.73409694444445 +lon_0=35.21208055555556 +x_0=170251.555 +y_0=1126867.909 +a=6378300.789 +b=6356566.435 +towgs84=-275.722,94.7824,340.894,-8.001,-4.42,-11.821,1 +units=m +no_defs  ";
// Pointe Noire / UTM zone 32S
Proj4js.defs["EPSG:28232"]="+proj=utm +zone=32 +south +a=6378249.2 +b=6356515 +towgs84=-148,51,-291,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 48
Proj4js.defs["EPSG:28348"]="+proj=utm +zone=48 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 49
Proj4js.defs["EPSG:28349"]="+proj=utm +zone=49 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 50
Proj4js.defs["EPSG:28350"]="+proj=utm +zone=50 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 51
Proj4js.defs["EPSG:28351"]="+proj=utm +zone=51 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 52
Proj4js.defs["EPSG:28352"]="+proj=utm +zone=52 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 53
Proj4js.defs["EPSG:28353"]="+proj=utm +zone=53 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 54
Proj4js.defs["EPSG:28354"]="+proj=utm +zone=54 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 55
Proj4js.defs["EPSG:28355"]="+proj=utm +zone=55 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 56
Proj4js.defs["EPSG:28356"]="+proj=utm +zone=56 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 57
Proj4js.defs["EPSG:28357"]="+proj=utm +zone=57 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// GDA94 / MGA zone 58
Proj4js.defs["EPSG:28358"]="+proj=utm +zone=58 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 2 (deprecated)
Proj4js.defs["EPSG:28402"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=2500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 3 (deprecated)
Proj4js.defs["EPSG:28403"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=3500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 4
Proj4js.defs["EPSG:28404"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=4500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 5
Proj4js.defs["EPSG:28405"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=5500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 6
Proj4js.defs["EPSG:28406"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=6500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 7
Proj4js.defs["EPSG:28407"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 8
Proj4js.defs["EPSG:28408"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=8500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 9
Proj4js.defs["EPSG:28409"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=9500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 10
Proj4js.defs["EPSG:28410"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=10500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 11
Proj4js.defs["EPSG:28411"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=11500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 12
Proj4js.defs["EPSG:28412"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=12500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 13
Proj4js.defs["EPSG:28413"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=13500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 14
Proj4js.defs["EPSG:28414"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=14500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 15
Proj4js.defs["EPSG:28415"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=15500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 16
Proj4js.defs["EPSG:28416"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=16500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 17
Proj4js.defs["EPSG:28417"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=17500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 18
Proj4js.defs["EPSG:28418"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=18500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 19
Proj4js.defs["EPSG:28419"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=19500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 20
Proj4js.defs["EPSG:28420"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 21
Proj4js.defs["EPSG:28421"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=21500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 22
Proj4js.defs["EPSG:28422"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=22500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 23
Proj4js.defs["EPSG:28423"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=23500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 24
Proj4js.defs["EPSG:28424"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=24500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 25
Proj4js.defs["EPSG:28425"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=25500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 26
Proj4js.defs["EPSG:28426"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=26500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 27
Proj4js.defs["EPSG:28427"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=27500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 28
Proj4js.defs["EPSG:28428"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=28500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 29
Proj4js.defs["EPSG:28429"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=29500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 30
Proj4js.defs["EPSG:28430"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=30500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 31
Proj4js.defs["EPSG:28431"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=31500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger zone 32
Proj4js.defs["EPSG:28432"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=32500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 2N (deprecated)
Proj4js.defs["EPSG:28462"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 3N (deprecated)
Proj4js.defs["EPSG:28463"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 4N (deprecated)
Proj4js.defs["EPSG:28464"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 5N (deprecated)
Proj4js.defs["EPSG:28465"]="+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 6N (deprecated)
Proj4js.defs["EPSG:28466"]="+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 7N (deprecated)
Proj4js.defs["EPSG:28467"]="+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 8N (deprecated)
Proj4js.defs["EPSG:28468"]="+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 9N (deprecated)
Proj4js.defs["EPSG:28469"]="+proj=tmerc +lat_0=0 +lon_0=51 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 10N (deprecated)
Proj4js.defs["EPSG:28470"]="+proj=tmerc +lat_0=0 +lon_0=57 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 11N (deprecated)
Proj4js.defs["EPSG:28471"]="+proj=tmerc +lat_0=0 +lon_0=63 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 12N (deprecated)
Proj4js.defs["EPSG:28472"]="+proj=tmerc +lat_0=0 +lon_0=69 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 13N (deprecated)
Proj4js.defs["EPSG:28473"]="+proj=tmerc +lat_0=0 +lon_0=75 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 14N (deprecated)
Proj4js.defs["EPSG:28474"]="+proj=tmerc +lat_0=0 +lon_0=81 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 15N (deprecated)
Proj4js.defs["EPSG:28475"]="+proj=tmerc +lat_0=0 +lon_0=87 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 16N (deprecated)
Proj4js.defs["EPSG:28476"]="+proj=tmerc +lat_0=0 +lon_0=93 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 17N (deprecated)
Proj4js.defs["EPSG:28477"]="+proj=tmerc +lat_0=0 +lon_0=99 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 18N (deprecated)
Proj4js.defs["EPSG:28478"]="+proj=tmerc +lat_0=0 +lon_0=105 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 19N (deprecated)
Proj4js.defs["EPSG:28479"]="+proj=tmerc +lat_0=0 +lon_0=111 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 20N (deprecated)
Proj4js.defs["EPSG:28480"]="+proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 21N (deprecated)
Proj4js.defs["EPSG:28481"]="+proj=tmerc +lat_0=0 +lon_0=123 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 22N (deprecated)
Proj4js.defs["EPSG:28482"]="+proj=tmerc +lat_0=0 +lon_0=129 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 23N (deprecated)
Proj4js.defs["EPSG:28483"]="+proj=tmerc +lat_0=0 +lon_0=135 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 24N (deprecated)
Proj4js.defs["EPSG:28484"]="+proj=tmerc +lat_0=0 +lon_0=141 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 25N (deprecated)
Proj4js.defs["EPSG:28485"]="+proj=tmerc +lat_0=0 +lon_0=147 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 26N (deprecated)
Proj4js.defs["EPSG:28486"]="+proj=tmerc +lat_0=0 +lon_0=153 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 27N (deprecated)
Proj4js.defs["EPSG:28487"]="+proj=tmerc +lat_0=0 +lon_0=159 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 28N (deprecated)
Proj4js.defs["EPSG:28488"]="+proj=tmerc +lat_0=0 +lon_0=165 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 29N (deprecated)
Proj4js.defs["EPSG:28489"]="+proj=tmerc +lat_0=0 +lon_0=171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 30N (deprecated)
Proj4js.defs["EPSG:28490"]="+proj=tmerc +lat_0=0 +lon_0=177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 31N (deprecated)
Proj4js.defs["EPSG:28491"]="+proj=tmerc +lat_0=0 +lon_0=-177 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Pulkovo 1942 / Gauss-Kruger 32N (deprecated)
Proj4js.defs["EPSG:28492"]="+proj=tmerc +lat_0=0 +lon_0=-171 +k=1 +x_0=500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs  ";
// Qatar 1974 / Qatar National Grid
Proj4js.defs["EPSG:28600"]="+proj=tmerc +lat_0=24.45 +lon_0=51.21666666666667 +k=0.99999 +x_0=200000 +y_0=300000 +ellps=intl +towgs84=-128.16,-282.42,21.93,0,0,0,0 +units=m +no_defs  ";
// Amersfoort / RD Old
Proj4js.defs["EPSG:28991"]="+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=0 +y_0=0 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs  ";
// Amersfoort / RD New
Proj4js.defs["EPSG:28992"]="+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs  ";
// SAD69 / Brazil Polyconic (deprecated)
Proj4js.defs["EPSG:29100"]="+proj=poly +lat_0=0 +lon_0=-54 +x_0=5000000 +y_0=10000000 +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / Brazil Polyconic
Proj4js.defs["EPSG:29101"]="+proj=poly +lat_0=0 +lon_0=-54 +x_0=5000000 +y_0=10000000 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 18N (deprecated)
Proj4js.defs["EPSG:29118"]="+proj=utm +zone=18 +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 19N (deprecated)
Proj4js.defs["EPSG:29119"]="+proj=utm +zone=19 +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 20N (deprecated)
Proj4js.defs["EPSG:29120"]="+proj=utm +zone=20 +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 21N (deprecated)
Proj4js.defs["EPSG:29121"]="+proj=utm +zone=21 +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 22N (deprecated)
Proj4js.defs["EPSG:29122"]="+proj=utm +zone=22 +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 18N
Proj4js.defs["EPSG:29168"]="+proj=utm +zone=18 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 19N
Proj4js.defs["EPSG:29169"]="+proj=utm +zone=19 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 20N
Proj4js.defs["EPSG:29170"]="+proj=utm +zone=20 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 21N
Proj4js.defs["EPSG:29171"]="+proj=utm +zone=21 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 22N
Proj4js.defs["EPSG:29172"]="+proj=utm +zone=22 +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 17S (deprecated)
Proj4js.defs["EPSG:29177"]="+proj=utm +zone=17 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 18S (deprecated)
Proj4js.defs["EPSG:29178"]="+proj=utm +zone=18 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 19S (deprecated)
Proj4js.defs["EPSG:29179"]="+proj=utm +zone=19 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 20S (deprecated)
Proj4js.defs["EPSG:29180"]="+proj=utm +zone=20 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 21S (deprecated)
Proj4js.defs["EPSG:29181"]="+proj=utm +zone=21 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 22S (deprecated)
Proj4js.defs["EPSG:29182"]="+proj=utm +zone=22 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 23S (deprecated)
Proj4js.defs["EPSG:29183"]="+proj=utm +zone=23 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 24S (deprecated)
Proj4js.defs["EPSG:29184"]="+proj=utm +zone=24 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 25S (deprecated)
Proj4js.defs["EPSG:29185"]="+proj=utm +zone=25 +south +ellps=GRS67 +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 17S
Proj4js.defs["EPSG:29187"]="+proj=utm +zone=17 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 18S
Proj4js.defs["EPSG:29188"]="+proj=utm +zone=18 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 19S
Proj4js.defs["EPSG:29189"]="+proj=utm +zone=19 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 20S
Proj4js.defs["EPSG:29190"]="+proj=utm +zone=20 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 21S
Proj4js.defs["EPSG:29191"]="+proj=utm +zone=21 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 22S
Proj4js.defs["EPSG:29192"]="+proj=utm +zone=22 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 23S
Proj4js.defs["EPSG:29193"]="+proj=utm +zone=23 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 24S
Proj4js.defs["EPSG:29194"]="+proj=utm +zone=24 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// SAD69 / UTM zone 25S
Proj4js.defs["EPSG:29195"]="+proj=utm +zone=25 +south +ellps=aust_SA +towgs84=-57,1,-41,0,0,0,0 +units=m +no_defs  ";
// Sapper Hill 1943 / UTM zone 20S
Proj4js.defs["EPSG:29220"]="+proj=utm +zone=20 +south +ellps=intl +towgs84=-355,21,72,0,0,0,0 +units=m +no_defs  ";
// Sapper Hill 1943 / UTM zone 21S
Proj4js.defs["EPSG:29221"]="+proj=utm +zone=21 +south +ellps=intl +towgs84=-355,21,72,0,0,0,0 +units=m +no_defs  ";
// Schwarzeck / UTM zone 33S
Proj4js.defs["EPSG:29333"]="+proj=utm +zone=33 +south +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +units=m +no_defs  ";
// Schwarzeck / Lo22/11
Proj4js.defs["EPSG:29371"]="+proj=tmerc +lat_0=-22 +lon_0=11 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/13
Proj4js.defs["EPSG:29373"]="+proj=tmerc +lat_0=-22 +lon_0=13 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/15
Proj4js.defs["EPSG:29375"]="+proj=tmerc +lat_0=-22 +lon_0=15 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/17
Proj4js.defs["EPSG:29377"]="+proj=tmerc +lat_0=-22 +lon_0=17 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/19
Proj4js.defs["EPSG:29379"]="+proj=tmerc +lat_0=-22 +lon_0=19 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/21
Proj4js.defs["EPSG:29381"]="+proj=tmerc +lat_0=-22 +lon_0=21 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/23
Proj4js.defs["EPSG:29383"]="+proj=tmerc +lat_0=-22 +lon_0=23 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Schwarzeck / Lo22/25
Proj4js.defs["EPSG:29385"]="+proj=tmerc +lat_0=-22 +lon_0=25 +k=1 +x_0=0 +y_0=0 +axis=wsu +ellps=bess_nam +towgs84=616,97,-251,0,0,0,0 +to_meter=1.0000135965 +no_defs  ";
// Sudan / UTM zone 35N (deprecated)
Proj4js.defs["EPSG:29635"]="+proj=utm +zone=35 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Sudan / UTM zone 36N (deprecated)
Proj4js.defs["EPSG:29636"]="+proj=utm +zone=36 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Tananarive (Paris) / Laborde Grid (deprecated)
Proj4js.defs["EPSG:29700"]="+proj=omerc +lat_0=-18.9 +lonc=44.10000000000001 +alpha=18.9 +k=0.9995000000000001 +x_0=400000 +y_0=800000 +gamma=18.9 +ellps=intl +towgs84=-189,-242,-91,0,0,0,0 +pm=paris +units=m +no_defs  ";
// Tananarive (Paris) / Laborde Grid
// Unable to translate coordinate system EPSG:29701 into PROJ.4 format.
//
// Tananarive (Paris) / Laborde Grid approximation
Proj4js.defs["EPSG:29702"]="+proj=omerc +lat_0=-18.9 +lonc=44.10000000000001 +alpha=18.9 +k=0.9995000000000001 +x_0=400000 +y_0=800000 +gamma=18.9 +ellps=intl +towgs84=-189,-242,-91,0,0,0,0 +pm=paris +units=m +no_defs  ";
// Tananarive / UTM zone 38S
Proj4js.defs["EPSG:29738"]="+proj=utm +zone=38 +south +ellps=intl +towgs84=-189,-242,-91,0,0,0,0 +units=m +no_defs  ";
// Tananarive / UTM zone 39S
Proj4js.defs["EPSG:29739"]="+proj=utm +zone=39 +south +ellps=intl +towgs84=-189,-242,-91,0,0,0,0 +units=m +no_defs  ";
// Timbalai 1948 / UTM zone 49N
Proj4js.defs["EPSG:29849"]="+proj=utm +zone=49 +ellps=evrstSS +towgs84=-533.4,669.2,-52.5,0,0,4.28,9.4 +units=m +no_defs  ";
// Timbalai 1948 / UTM zone 50N
Proj4js.defs["EPSG:29850"]="+proj=utm +zone=50 +ellps=evrstSS +towgs84=-533.4,669.2,-52.5,0,0,4.28,9.4 +units=m +no_defs  ";
// Timbalai 1948 / RSO Borneo (ch)
Proj4js.defs["EPSG:29871"]="+proj=omerc +lat_0=4 +lonc=115 +alpha=53.31582047222222 +k=0.99984 +x_0=590476.8714630401 +y_0=442857.653094361 +gamma=53.13010236111111 +ellps=evrstSS +towgs84=-533.4,669.2,-52.5,0,0,4.28,9.4 +to_meter=20.11676512155263 +no_defs  ";
// Timbalai 1948 / RSO Borneo (ft)
Proj4js.defs["EPSG:29872"]="+proj=omerc +lat_0=4 +lonc=115 +alpha=53.31582047222222 +k=0.99984 +x_0=590476.8727431979 +y_0=442857.6545573985 +gamma=53.13010236111111 +ellps=evrstSS +towgs84=-533.4,669.2,-52.5,0,0,4.28,9.4 +to_meter=0.3047994715386762 +no_defs  ";
// Timbalai 1948 / RSO Borneo (m)
Proj4js.defs["EPSG:29873"]="+proj=omerc +lat_0=4 +lonc=115 +alpha=53.31582047222222 +k=0.99984 +x_0=590476.87 +y_0=442857.65 +gamma=53.13010236111111 +ellps=evrstSS +towgs84=-533.4,669.2,-52.5,0,0,4.28,9.4 +units=m +no_defs  ";
// TM65 / Irish National Grid (deprecated)
Proj4js.defs["EPSG:29900"]="+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1.000035 +x_0=200000 +y_0=250000 +ellps=mod_airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +units=m +no_defs  ";
// OSNI 1952 / Irish National Grid
Proj4js.defs["EPSG:29901"]="+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1 +x_0=200000 +y_0=250000 +ellps=airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +units=m +no_defs  ";
// TM65 / Irish Grid
Proj4js.defs["EPSG:29902"]="+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1.000035 +x_0=200000 +y_0=250000 +ellps=mod_airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +units=m +no_defs  ";
// TM75 / Irish Grid
Proj4js.defs["EPSG:29903"]="+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1.000035 +x_0=200000 +y_0=250000 +ellps=mod_airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS I
Proj4js.defs["EPSG:30161"]="+proj=tmerc +lat_0=33 +lon_0=129.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS II
Proj4js.defs["EPSG:30162"]="+proj=tmerc +lat_0=33 +lon_0=131 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS III
Proj4js.defs["EPSG:30163"]="+proj=tmerc +lat_0=36 +lon_0=132.1666666666667 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS IV
Proj4js.defs["EPSG:30164"]="+proj=tmerc +lat_0=33 +lon_0=133.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS V
Proj4js.defs["EPSG:30165"]="+proj=tmerc +lat_0=36 +lon_0=134.3333333333333 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS VI
Proj4js.defs["EPSG:30166"]="+proj=tmerc +lat_0=36 +lon_0=136 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS VII
Proj4js.defs["EPSG:30167"]="+proj=tmerc +lat_0=36 +lon_0=137.1666666666667 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS VIII
Proj4js.defs["EPSG:30168"]="+proj=tmerc +lat_0=36 +lon_0=138.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS IX
Proj4js.defs["EPSG:30169"]="+proj=tmerc +lat_0=36 +lon_0=139.8333333333333 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS X
Proj4js.defs["EPSG:30170"]="+proj=tmerc +lat_0=40 +lon_0=140.8333333333333 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XI
Proj4js.defs["EPSG:30171"]="+proj=tmerc +lat_0=44 +lon_0=140.25 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XII
Proj4js.defs["EPSG:30172"]="+proj=tmerc +lat_0=44 +lon_0=142.25 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XIII
Proj4js.defs["EPSG:30173"]="+proj=tmerc +lat_0=44 +lon_0=144.25 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XIV
Proj4js.defs["EPSG:30174"]="+proj=tmerc +lat_0=26 +lon_0=142 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XV
Proj4js.defs["EPSG:30175"]="+proj=tmerc +lat_0=26 +lon_0=127.5 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XVI
Proj4js.defs["EPSG:30176"]="+proj=tmerc +lat_0=26 +lon_0=124 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XVII
Proj4js.defs["EPSG:30177"]="+proj=tmerc +lat_0=26 +lon_0=131 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XVIII
Proj4js.defs["EPSG:30178"]="+proj=tmerc +lat_0=20 +lon_0=136 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Tokyo / Japan Plane Rectangular CS XIX
Proj4js.defs["EPSG:30179"]="+proj=tmerc +lat_0=26 +lon_0=154 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=-146.414,507.337,680.507,0,0,0,0 +units=m +no_defs  ";
// Trinidad 1903 / Trinidad Grid
Proj4js.defs["EPSG:30200"]="+proj=cass +lat_0=10.44166666666667 +lon_0=-61.33333333333334 +x_0=86501.46392051999 +y_0=65379.0134283 +a=6378293.645208759 +b=6356617.987679838 +towgs84=-61.702,284.488,472.052,0,0,0,0 +to_meter=0.201166195164 +no_defs  ";
// TC(1948) / UTM zone 39N
Proj4js.defs["EPSG:30339"]="+proj=utm +zone=39 +ellps=helmert +units=m +no_defs  ";
// TC(1948) / UTM zone 40N
Proj4js.defs["EPSG:30340"]="+proj=utm +zone=40 +ellps=helmert +units=m +no_defs  ";
// Voirol 1875 / Nord Algerie (ancienne)
Proj4js.defs["EPSG:30491"]="+proj=lcc +lat_1=36 +lat_0=36 +lon_0=2.7 +k_0=0.999625544 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=-73,-247,227,0,0,0,0 +units=m +no_defs  ";
// Voirol 1875 / Sud Algerie (ancienne)
Proj4js.defs["EPSG:30492"]="+proj=lcc +lat_1=33.3 +lat_0=33.3 +lon_0=2.7 +k_0=0.999625769 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +towgs84=-73,-247,227,0,0,0,0 +units=m +no_defs  ";
// Voirol 1879 / Nord Algerie (ancienne)
Proj4js.defs["EPSG:30493"]="+proj=lcc +lat_1=36 +lat_0=36 +lon_0=2.7 +k_0=0.999625544 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Voirol 1879 / Sud Algerie (ancienne)
Proj4js.defs["EPSG:30494"]="+proj=lcc +lat_1=33.3 +lat_0=33.3 +lon_0=2.7 +k_0=0.999625769 +x_0=500000 +y_0=300000 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Nord Sahara 1959 / UTM zone 29N
Proj4js.defs["EPSG:30729"]="+proj=utm +zone=29 +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +units=m +no_defs  ";
// Nord Sahara 1959 / UTM zone 30N
Proj4js.defs["EPSG:30730"]="+proj=utm +zone=30 +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +units=m +no_defs  ";
// Nord Sahara 1959 / UTM zone 31N
Proj4js.defs["EPSG:30731"]="+proj=utm +zone=31 +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +units=m +no_defs  ";
// Nord Sahara 1959 / UTM zone 32N
Proj4js.defs["EPSG:30732"]="+proj=utm +zone=32 +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +units=m +no_defs  ";
// Nord Sahara 1959 / Voirol Unifie Nord
Proj4js.defs["EPSG:30791"]="+proj=lcc +lat_1=36 +lat_0=36 +lon_0=2.7 +k_0=0.999625544 +x_0=500135 +y_0=300090 +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +units=m +no_defs  ";
// Nord Sahara 1959 / Voirol Unifie Sud
Proj4js.defs["EPSG:30792"]="+proj=lcc +lat_1=33.3 +lat_0=33.3 +lon_0=2.7 +k_0=0.999625769 +x_0=500135 +y_0=300090 +ellps=clrk80 +towgs84=-186,-93,310,0,0,0,0 +units=m +no_defs  ";
// RT38 2.5 gon W (deprecated)
Proj4js.defs["EPSG:30800"]="+proj=tmerc +lat_0=0 +lon_0=15.80827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +units=m +no_defs  ";
// Yoff / UTM zone 28N
Proj4js.defs["EPSG:31028"]="+proj=utm +zone=28 +a=6378249.2 +b=6356515 +units=m +no_defs  ";
// Zanderij / UTM zone 21N
Proj4js.defs["EPSG:31121"]="+proj=utm +zone=21 +ellps=intl +towgs84=-265,120,-358,0,0,0,0 +units=m +no_defs  ";
// Zanderij / TM 54 NW
Proj4js.defs["EPSG:31154"]="+proj=tmerc +lat_0=0 +lon_0=-54 +k=0.9996 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-265,120,-358,0,0,0,0 +units=m +no_defs  ";
// Zanderij / Suriname Old TM
Proj4js.defs["EPSG:31170"]="+proj=tmerc +lat_0=0 +lon_0=-55.68333333333333 +k=0.9996 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-265,120,-358,0,0,0,0 +units=m +no_defs  ";
// Zanderij / Suriname TM
Proj4js.defs["EPSG:31171"]="+proj=tmerc +lat_0=0 +lon_0=-55.68333333333333 +k=0.9999 +x_0=500000 +y_0=0 +ellps=intl +towgs84=-265,120,-358,0,0,0,0 +units=m +no_defs  ";
// MGI (Ferro) / Austria GK West Zone
Proj4js.defs["EPSG:31251"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria GK Central Zone
Proj4js.defs["EPSG:31252"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria GK East Zone
Proj4js.defs["EPSG:31253"]="+proj=tmerc +lat_0=0 +lon_0=34 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI / Austria GK West
Proj4js.defs["EPSG:31254"]="+proj=tmerc +lat_0=0 +lon_0=10.33333333333333 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria GK Central
Proj4js.defs["EPSG:31255"]="+proj=tmerc +lat_0=0 +lon_0=13.33333333333333 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria GK East
Proj4js.defs["EPSG:31256"]="+proj=tmerc +lat_0=0 +lon_0=16.33333333333333 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria GK M28
Proj4js.defs["EPSG:31257"]="+proj=tmerc +lat_0=0 +lon_0=10.33333333333333 +k=1 +x_0=150000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria GK M31
Proj4js.defs["EPSG:31258"]="+proj=tmerc +lat_0=0 +lon_0=13.33333333333333 +k=1 +x_0=450000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria GK M34
Proj4js.defs["EPSG:31259"]="+proj=tmerc +lat_0=0 +lon_0=16.33333333333333 +k=1 +x_0=750000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / 3-degree Gauss zone 5 (deprecated)
Proj4js.defs["EPSG:31265"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / 3-degree Gauss zone 6 (deprecated)
Proj4js.defs["EPSG:31266"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / 3-degree Gauss zone 7 (deprecated)
Proj4js.defs["EPSG:31267"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=1 +x_0=7500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / 3-degree Gauss zone 8 (deprecated)
Proj4js.defs["EPSG:31268"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=1 +x_0=8500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Balkans zone 5 (deprecated)
Proj4js.defs["EPSG:31275"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Balkans zone 6 (deprecated)
Proj4js.defs["EPSG:31276"]="+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Balkans zone 7 (deprecated)
Proj4js.defs["EPSG:31277"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=0.9999 +x_0=7500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Balkans zone 8 (deprecated)
Proj4js.defs["EPSG:31278"]="+proj=tmerc +lat_0=0 +lon_0=21 +k=0.9999 +x_0=7500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Balkans zone 8 (deprecated)
Proj4js.defs["EPSG:31279"]="+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9999 +x_0=8500000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI (Ferro) / Austria West Zone
Proj4js.defs["EPSG:31281"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria Central Zone
Proj4js.defs["EPSG:31282"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria East Zone
Proj4js.defs["EPSG:31283"]="+proj=tmerc +lat_0=0 +lon_0=34 +k=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI / Austria M28
Proj4js.defs["EPSG:31284"]="+proj=tmerc +lat_0=0 +lon_0=10.33333333333333 +k=1 +x_0=150000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria M31
Proj4js.defs["EPSG:31285"]="+proj=tmerc +lat_0=0 +lon_0=13.33333333333333 +k=1 +x_0=450000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria M34
Proj4js.defs["EPSG:31286"]="+proj=tmerc +lat_0=0 +lon_0=16.33333333333333 +k=1 +x_0=750000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria Lambert
Proj4js.defs["EPSG:31287"]="+proj=lcc +lat_1=49 +lat_2=46 +lat_0=47.5 +lon_0=13.33333333333333 +x_0=400000 +y_0=400000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI (Ferro) / M28
Proj4js.defs["EPSG:31288"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=1 +x_0=150000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / M31
Proj4js.defs["EPSG:31289"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=450000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / M34
Proj4js.defs["EPSG:31290"]="+proj=tmerc +lat_0=0 +lon_0=34 +k=1 +x_0=750000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria West Zone (deprecated)
Proj4js.defs["EPSG:31291"]="+proj=tmerc +lat_0=0 +lon_0=28 +k=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria Central Zone (deprecated)
Proj4js.defs["EPSG:31292"]="+proj=tmerc +lat_0=0 +lon_0=31 +k=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI (Ferro) / Austria East Zone (deprecated)
Proj4js.defs["EPSG:31293"]="+proj=tmerc +lat_0=0 +lon_0=34 +k=1 +x_0=0 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +pm=ferro +units=m +no_defs  ";
// MGI / M28 (deprecated)
Proj4js.defs["EPSG:31294"]="+proj=tmerc +lat_0=0 +lon_0=10.33333333333333 +k=1 +x_0=150000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / M31 (deprecated)
Proj4js.defs["EPSG:31295"]="+proj=tmerc +lat_0=0 +lon_0=13.33333333333333 +k=1 +x_0=450000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / M34 (deprecated)
Proj4js.defs["EPSG:31296"]="+proj=tmerc +lat_0=0 +lon_0=16.33333333333333 +k=1 +x_0=750000 +y_0=0 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// MGI / Austria Lambert (deprecated)
Proj4js.defs["EPSG:31297"]="+proj=lcc +lat_1=49 +lat_2=46 +lat_0=47.5 +lon_0=13.33333333333333 +x_0=400000 +y_0=400000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs  ";
// Belge 1972 / Belge Lambert 72
Proj4js.defs["EPSG:31300"]="+proj=lcc +lat_1=49.83333333333334 +lat_2=51.16666666666666 +lat_0=90 +lon_0=4.356939722222222 +x_0=150000.01256 +y_0=5400088.4378 +ellps=intl +towgs84=-106.869,52.2978,-103.724,0.3366,-0.457,1.8422,-1.2747 +units=m +no_defs  ";
// Belge 1972 / Belgian Lambert 72
Proj4js.defs["EPSG:31370"]="+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-106.869,52.2978,-103.724,0.3366,-0.457,1.8422,-1.2747 +units=m +no_defs  ";
// DHDN / 3-degree Gauss zone 1 (deprecated)
Proj4js.defs["EPSG:31461"]="+proj=tmerc +lat_0=0 +lon_0=3 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss zone 2 (deprecated)
Proj4js.defs["EPSG:31462"]="+proj=tmerc +lat_0=0 +lon_0=6 +k=1 +x_0=2500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss zone 3 (deprecated)
Proj4js.defs["EPSG:31463"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss zone 4 (deprecated)
Proj4js.defs["EPSG:31464"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss zone 5 (deprecated)
Proj4js.defs["EPSG:31465"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss-Kruger zone 2
Proj4js.defs["EPSG:31466"]="+proj=tmerc +lat_0=0 +lon_0=6 +k=1 +x_0=2500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss-Kruger zone 3
Proj4js.defs["EPSG:31467"]="+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss-Kruger zone 4
Proj4js.defs["EPSG:31468"]="+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// DHDN / 3-degree Gauss-Kruger zone 5
Proj4js.defs["EPSG:31469"]="+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +no_defs  ";
// Conakry 1905 / UTM zone 28N
Proj4js.defs["EPSG:31528"]="+proj=utm +zone=28 +a=6378249.2 +b=6356515 +towgs84=-23,259,-9,0,0,0,0 +units=m +no_defs  ";
// Conakry 1905 / UTM zone 29N
Proj4js.defs["EPSG:31529"]="+proj=utm +zone=29 +a=6378249.2 +b=6356515 +towgs84=-23,259,-9,0,0,0,0 +units=m +no_defs  ";
// Dealul Piscului 1930 / Stereo 33
Proj4js.defs["EPSG:31600"]="+proj=sterea +lat_0=45.9 +lon_0=25.39246588888889 +k=0.9996667 +x_0=500000 +y_0=500000 +ellps=intl +towgs84=103.25,-100.4,-307.19,0,0,0,0 +units=m +no_defs  ";
// Dealul Piscului 1970/ Stereo 70 (deprecated)
Proj4js.defs["EPSG:31700"]="+proj=sterea +lat_0=46 +lon_0=25 +k=0.99975 +x_0=500000 +y_0=500000 +ellps=krass +towgs84=28,-121,-77,0,0,0,0 +units=m +no_defs  ";
// NGN / UTM zone 38N
Proj4js.defs["EPSG:31838"]="+proj=utm +zone=38 +ellps=WGS84 +towgs84=-3.2,-5.7,2.8,0,0,0,0 +units=m +no_defs  ";
// NGN / UTM zone 39N
Proj4js.defs["EPSG:31839"]="+proj=utm +zone=39 +ellps=WGS84 +towgs84=-3.2,-5.7,2.8,0,0,0,0 +units=m +no_defs  ";
// KUDAMS / KTM (deprecated)
Proj4js.defs["EPSG:31900"]="+proj=tmerc +lat_0=0 +lon_0=48 +k=0.9996 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=-20.8,11.3,2.4,0,0,0,0 +units=m +no_defs  ";
// KUDAMS / KTM
Proj4js.defs["EPSG:31901"]="+proj=tmerc +lat_0=0 +lon_0=48 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=-20.8,11.3,2.4,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 11N
Proj4js.defs["EPSG:31965"]="+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 12N
Proj4js.defs["EPSG:31966"]="+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 13N
Proj4js.defs["EPSG:31967"]="+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 14N
Proj4js.defs["EPSG:31968"]="+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 15N
Proj4js.defs["EPSG:31969"]="+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 16N
Proj4js.defs["EPSG:31970"]="+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 17N
Proj4js.defs["EPSG:31971"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 18N
Proj4js.defs["EPSG:31972"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 19N
Proj4js.defs["EPSG:31973"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 20N
Proj4js.defs["EPSG:31974"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 21N
Proj4js.defs["EPSG:31975"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 22N
Proj4js.defs["EPSG:31976"]="+proj=utm +zone=22 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 17S
Proj4js.defs["EPSG:31977"]="+proj=utm +zone=17 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 18S
Proj4js.defs["EPSG:31978"]="+proj=utm +zone=18 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 19S
Proj4js.defs["EPSG:31979"]="+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 20S
Proj4js.defs["EPSG:31980"]="+proj=utm +zone=20 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 21S
Proj4js.defs["EPSG:31981"]="+proj=utm +zone=21 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 22S
Proj4js.defs["EPSG:31982"]="+proj=utm +zone=22 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 23S
Proj4js.defs["EPSG:31983"]="+proj=utm +zone=23 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 24S
Proj4js.defs["EPSG:31984"]="+proj=utm +zone=24 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 2000 / UTM zone 25S
Proj4js.defs["EPSG:31985"]="+proj=utm +zone=25 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 17N
Proj4js.defs["EPSG:31986"]="+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 18N
Proj4js.defs["EPSG:31987"]="+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 19N
Proj4js.defs["EPSG:31988"]="+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 20N
Proj4js.defs["EPSG:31989"]="+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 21N
Proj4js.defs["EPSG:31990"]="+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 22N
Proj4js.defs["EPSG:31991"]="+proj=utm +zone=22 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 17S
Proj4js.defs["EPSG:31992"]="+proj=utm +zone=17 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 18S
Proj4js.defs["EPSG:31993"]="+proj=utm +zone=18 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 19S
Proj4js.defs["EPSG:31994"]="+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 20S
Proj4js.defs["EPSG:31995"]="+proj=utm +zone=20 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 21S
Proj4js.defs["EPSG:31996"]="+proj=utm +zone=21 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 22S
Proj4js.defs["EPSG:31997"]="+proj=utm +zone=22 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 23S
Proj4js.defs["EPSG:31998"]="+proj=utm +zone=23 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 24S
Proj4js.defs["EPSG:31999"]="+proj=utm +zone=24 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// SIRGAS 1995 / UTM zone 25S
Proj4js.defs["EPSG:32000"]="+proj=utm +zone=25 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD27 / Montana North
Proj4js.defs["EPSG:32001"]="+proj=lcc +lat_1=48.71666666666667 +lat_2=47.85 +lat_0=47 +lon_0=-109.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Montana Central
Proj4js.defs["EPSG:32002"]="+proj=lcc +lat_1=47.88333333333333 +lat_2=46.45 +lat_0=45.83333333333334 +lon_0=-109.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Montana South
Proj4js.defs["EPSG:32003"]="+proj=lcc +lat_1=46.4 +lat_2=44.86666666666667 +lat_0=44 +lon_0=-109.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Nebraska North
Proj4js.defs["EPSG:32005"]="+proj=lcc +lat_1=41.85 +lat_2=42.81666666666667 +lat_0=41.33333333333334 +lon_0=-100 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Nebraska South
Proj4js.defs["EPSG:32006"]="+proj=lcc +lat_1=40.28333333333333 +lat_2=41.71666666666667 +lat_0=39.66666666666666 +lon_0=-99.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Nevada East
Proj4js.defs["EPSG:32007"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Nevada Central
Proj4js.defs["EPSG:32008"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Nevada West
Proj4js.defs["EPSG:32009"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New Hampshire
Proj4js.defs["EPSG:32010"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New Jersey
Proj4js.defs["EPSG:32011"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.66666666666667 +k=0.9999749999999999 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New Mexico East
Proj4js.defs["EPSG:32012"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New Mexico Central
Proj4js.defs["EPSG:32013"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New Mexico West
Proj4js.defs["EPSG:32014"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New York East
Proj4js.defs["EPSG:32015"]="+proj=tmerc +lat_0=40 +lon_0=-74.33333333333333 +k=0.999966667 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New York Central
Proj4js.defs["EPSG:32016"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New York West
Proj4js.defs["EPSG:32017"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / New York Long Island (deprecated)
Proj4js.defs["EPSG:32018"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.5 +lon_0=-74 +x_0=304800.6096012192 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / North Carolina
Proj4js.defs["EPSG:32019"]="+proj=lcc +lat_1=34.33333333333334 +lat_2=36.16666666666666 +lat_0=33.75 +lon_0=-79 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / North Dakota North
Proj4js.defs["EPSG:32020"]="+proj=lcc +lat_1=47.43333333333333 +lat_2=48.73333333333333 +lat_0=47 +lon_0=-100.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / North Dakota South
Proj4js.defs["EPSG:32021"]="+proj=lcc +lat_1=46.18333333333333 +lat_2=47.48333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Ohio North
Proj4js.defs["EPSG:32022"]="+proj=lcc +lat_1=40.43333333333333 +lat_2=41.7 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Ohio South
Proj4js.defs["EPSG:32023"]="+proj=lcc +lat_1=38.73333333333333 +lat_2=40.03333333333333 +lat_0=38 +lon_0=-82.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Oklahoma North
Proj4js.defs["EPSG:32024"]="+proj=lcc +lat_1=35.56666666666667 +lat_2=36.76666666666667 +lat_0=35 +lon_0=-98 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Oklahoma South
Proj4js.defs["EPSG:32025"]="+proj=lcc +lat_1=33.93333333333333 +lat_2=35.23333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Oregon North
Proj4js.defs["EPSG:32026"]="+proj=lcc +lat_1=44.33333333333334 +lat_2=46 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Oregon South
Proj4js.defs["EPSG:32027"]="+proj=lcc +lat_1=42.33333333333334 +lat_2=44 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Pennsylvania North
Proj4js.defs["EPSG:32028"]="+proj=lcc +lat_1=40.88333333333333 +lat_2=41.95 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Pennsylvania South (deprecated)
Proj4js.defs["EPSG:32029"]="+proj=lcc +lat_1=39.93333333333333 +lat_2=40.8 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Rhode Island
Proj4js.defs["EPSG:32030"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.9999938 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / South Carolina North
Proj4js.defs["EPSG:32031"]="+proj=lcc +lat_1=33.76666666666667 +lat_2=34.96666666666667 +lat_0=33 +lon_0=-81 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / South Carolina South
Proj4js.defs["EPSG:32033"]="+proj=lcc +lat_1=32.33333333333334 +lat_2=33.66666666666666 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / South Dakota North
Proj4js.defs["EPSG:32034"]="+proj=lcc +lat_1=44.41666666666666 +lat_2=45.68333333333333 +lat_0=43.83333333333334 +lon_0=-100 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / South Dakota South
Proj4js.defs["EPSG:32035"]="+proj=lcc +lat_1=42.83333333333334 +lat_2=44.4 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Tennessee (deprecated)
Proj4js.defs["EPSG:32036"]="+proj=lcc +lat_1=35.25 +lat_2=36.41666666666666 +lat_0=34.66666666666666 +lon_0=-86 +x_0=30480.06096012192 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Texas North
Proj4js.defs["EPSG:32037"]="+proj=lcc +lat_1=34.65 +lat_2=36.18333333333333 +lat_0=34 +lon_0=-101.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Texas North Central
Proj4js.defs["EPSG:32038"]="+proj=lcc +lat_1=32.13333333333333 +lat_2=33.96666666666667 +lat_0=31.66666666666667 +lon_0=-97.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Texas Central
Proj4js.defs["EPSG:32039"]="+proj=lcc +lat_1=30.11666666666667 +lat_2=31.88333333333333 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Texas South Central
Proj4js.defs["EPSG:32040"]="+proj=lcc +lat_1=28.38333333333333 +lat_2=30.28333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Texas South
Proj4js.defs["EPSG:32041"]="+proj=lcc +lat_1=26.16666666666667 +lat_2=27.83333333333333 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Utah North
Proj4js.defs["EPSG:32042"]="+proj=lcc +lat_1=40.71666666666667 +lat_2=41.78333333333333 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Utah Central
Proj4js.defs["EPSG:32043"]="+proj=lcc +lat_1=39.01666666666667 +lat_2=40.65 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Utah South
Proj4js.defs["EPSG:32044"]="+proj=lcc +lat_1=37.21666666666667 +lat_2=38.35 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Vermont
Proj4js.defs["EPSG:32045"]="+proj=tmerc +lat_0=42.5 +lon_0=-72.5 +k=0.999964286 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Virginia North
Proj4js.defs["EPSG:32046"]="+proj=lcc +lat_1=38.03333333333333 +lat_2=39.2 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Virginia South
Proj4js.defs["EPSG:32047"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=37.96666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Washington North
Proj4js.defs["EPSG:32048"]="+proj=lcc +lat_1=47.5 +lat_2=48.73333333333333 +lat_0=47 +lon_0=-120.8333333333333 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Washington South
Proj4js.defs["EPSG:32049"]="+proj=lcc +lat_1=45.83333333333334 +lat_2=47.33333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / West Virginia North
Proj4js.defs["EPSG:32050"]="+proj=lcc +lat_1=39 +lat_2=40.25 +lat_0=38.5 +lon_0=-79.5 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / West Virginia South
Proj4js.defs["EPSG:32051"]="+proj=lcc +lat_1=37.48333333333333 +lat_2=38.88333333333333 +lat_0=37 +lon_0=-81 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wisconsin North
Proj4js.defs["EPSG:32052"]="+proj=lcc +lat_1=45.56666666666667 +lat_2=46.76666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wisconsin Central
Proj4js.defs["EPSG:32053"]="+proj=lcc +lat_1=44.25 +lat_2=45.5 +lat_0=43.83333333333334 +lon_0=-90 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wisconsin South
Proj4js.defs["EPSG:32054"]="+proj=lcc +lat_1=42.73333333333333 +lat_2=44.06666666666667 +lat_0=42 +lon_0=-90 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wyoming East
Proj4js.defs["EPSG:32055"]="+proj=tmerc +lat_0=40.66666666666666 +lon_0=-105.1666666666667 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wyoming East Central
Proj4js.defs["EPSG:32056"]="+proj=tmerc +lat_0=40.66666666666666 +lon_0=-107.3333333333333 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wyoming West Central
Proj4js.defs["EPSG:32057"]="+proj=tmerc +lat_0=40.66666666666666 +lon_0=-108.75 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Wyoming West
Proj4js.defs["EPSG:32058"]="+proj=tmerc +lat_0=40.66666666666666 +lon_0=-110.0833333333333 +k=0.999941177 +x_0=152400.3048006096 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / Guatemala Norte (deprecated)
Proj4js.defs["EPSG:32061"]="+proj=lcc +lat_1=16.81666666666667 +lat_0=16.81666666666667 +lon_0=-90.33333333333333 +k_0=0.99992226 +x_0=500000 +y_0=292209.579 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Guatemala Sur (deprecated)
Proj4js.defs["EPSG:32062"]="+proj=lcc +lat_1=14.9 +lat_0=14.9 +lon_0=-90.33333333333333 +k_0=0.99989906 +x_0=500000 +y_0=325992.681 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / BLM 14N (ftUS)
Proj4js.defs["EPSG:32064"]="+proj=tmerc +lat_0=0 +lon_0=-99 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 15N (ftUS)
Proj4js.defs["EPSG:32065"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 16N (ftUS)
Proj4js.defs["EPSG:32066"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 17N (ftUS)
Proj4js.defs["EPSG:32067"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 14N (feet) (deprecated)
Proj4js.defs["EPSG:32074"]="+proj=tmerc +lat_0=0 +lon_0=-99 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 15N (feet) (deprecated)
Proj4js.defs["EPSG:32075"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 16N (feet) (deprecated)
Proj4js.defs["EPSG:32076"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / BLM 17N (feet) (deprecated)
Proj4js.defs["EPSG:32077"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD27 / MTM zone 1
Proj4js.defs["EPSG:32081"]="+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / MTM zone 2
Proj4js.defs["EPSG:32082"]="+proj=tmerc +lat_0=0 +lon_0=-56 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / MTM zone 3
Proj4js.defs["EPSG:32083"]="+proj=tmerc +lat_0=0 +lon_0=-58.5 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / MTM zone 4
Proj4js.defs["EPSG:32084"]="+proj=tmerc +lat_0=0 +lon_0=-61.5 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / MTM zone 5
Proj4js.defs["EPSG:32085"]="+proj=tmerc +lat_0=0 +lon_0=-64.5 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / MTM zone 6
Proj4js.defs["EPSG:32086"]="+proj=tmerc +lat_0=0 +lon_0=-67.5 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Quebec Lambert
Proj4js.defs["EPSG:32098"]="+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +datum=NAD27 +units=m +no_defs  ";
// NAD27 / Louisiana Offshore
Proj4js.defs["EPSG:32099"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-91.33333333333333 +x_0=609601.2192024384 +y_0=0 +datum=NAD27 +units=us-ft +no_defs  ";
// NAD83 / Montana
Proj4js.defs["EPSG:32100"]="+proj=lcc +lat_1=49 +lat_2=45 +lat_0=44.25 +lon_0=-109.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Nebraska
Proj4js.defs["EPSG:32104"]="+proj=lcc +lat_1=43 +lat_2=40 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Nevada East
Proj4js.defs["EPSG:32107"]="+proj=tmerc +lat_0=34.75 +lon_0=-115.5833333333333 +k=0.9999 +x_0=200000 +y_0=8000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Nevada Central
Proj4js.defs["EPSG:32108"]="+proj=tmerc +lat_0=34.75 +lon_0=-116.6666666666667 +k=0.9999 +x_0=500000 +y_0=6000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Nevada West
Proj4js.defs["EPSG:32109"]="+proj=tmerc +lat_0=34.75 +lon_0=-118.5833333333333 +k=0.9999 +x_0=800000 +y_0=4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New Hampshire
Proj4js.defs["EPSG:32110"]="+proj=tmerc +lat_0=42.5 +lon_0=-71.66666666666667 +k=0.999966667 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New Jersey
Proj4js.defs["EPSG:32111"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New Mexico East
Proj4js.defs["EPSG:32112"]="+proj=tmerc +lat_0=31 +lon_0=-104.3333333333333 +k=0.999909091 +x_0=165000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New Mexico Central
Proj4js.defs["EPSG:32113"]="+proj=tmerc +lat_0=31 +lon_0=-106.25 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New Mexico West
Proj4js.defs["EPSG:32114"]="+proj=tmerc +lat_0=31 +lon_0=-107.8333333333333 +k=0.999916667 +x_0=830000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New York East
Proj4js.defs["EPSG:32115"]="+proj=tmerc +lat_0=38.83333333333334 +lon_0=-74.5 +k=0.9999 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New York Central
Proj4js.defs["EPSG:32116"]="+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New York West
Proj4js.defs["EPSG:32117"]="+proj=tmerc +lat_0=40 +lon_0=-78.58333333333333 +k=0.9999375 +x_0=350000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / New York Long Island
Proj4js.defs["EPSG:32118"]="+proj=lcc +lat_1=41.03333333333333 +lat_2=40.66666666666666 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / North Carolina
Proj4js.defs["EPSG:32119"]="+proj=lcc +lat_1=36.16666666666666 +lat_2=34.33333333333334 +lat_0=33.75 +lon_0=-79 +x_0=609601.22 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / North Dakota North
Proj4js.defs["EPSG:32120"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.43333333333333 +lat_0=47 +lon_0=-100.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / North Dakota South
Proj4js.defs["EPSG:32121"]="+proj=lcc +lat_1=47.48333333333333 +lat_2=46.18333333333333 +lat_0=45.66666666666666 +lon_0=-100.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Ohio North
Proj4js.defs["EPSG:32122"]="+proj=lcc +lat_1=41.7 +lat_2=40.43333333333333 +lat_0=39.66666666666666 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Ohio South
Proj4js.defs["EPSG:32123"]="+proj=lcc +lat_1=40.03333333333333 +lat_2=38.73333333333333 +lat_0=38 +lon_0=-82.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Oklahoma North
Proj4js.defs["EPSG:32124"]="+proj=lcc +lat_1=36.76666666666667 +lat_2=35.56666666666667 +lat_0=35 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Oklahoma South
Proj4js.defs["EPSG:32125"]="+proj=lcc +lat_1=35.23333333333333 +lat_2=33.93333333333333 +lat_0=33.33333333333334 +lon_0=-98 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Oregon North
Proj4js.defs["EPSG:32126"]="+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Oregon South
Proj4js.defs["EPSG:32127"]="+proj=lcc +lat_1=44 +lat_2=42.33333333333334 +lat_0=41.66666666666666 +lon_0=-120.5 +x_0=1500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Pennsylvania North
Proj4js.defs["EPSG:32128"]="+proj=lcc +lat_1=41.95 +lat_2=40.88333333333333 +lat_0=40.16666666666666 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Pennsylvania South
Proj4js.defs["EPSG:32129"]="+proj=lcc +lat_1=40.96666666666667 +lat_2=39.93333333333333 +lat_0=39.33333333333334 +lon_0=-77.75 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Rhode Island
Proj4js.defs["EPSG:32130"]="+proj=tmerc +lat_0=41.08333333333334 +lon_0=-71.5 +k=0.99999375 +x_0=100000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / South Carolina
Proj4js.defs["EPSG:32133"]="+proj=lcc +lat_1=34.83333333333334 +lat_2=32.5 +lat_0=31.83333333333333 +lon_0=-81 +x_0=609600 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / South Dakota North
Proj4js.defs["EPSG:32134"]="+proj=lcc +lat_1=45.68333333333333 +lat_2=44.41666666666666 +lat_0=43.83333333333334 +lon_0=-100 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / South Dakota South
Proj4js.defs["EPSG:32135"]="+proj=lcc +lat_1=44.4 +lat_2=42.83333333333334 +lat_0=42.33333333333334 +lon_0=-100.3333333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Tennessee
Proj4js.defs["EPSG:32136"]="+proj=lcc +lat_1=36.41666666666666 +lat_2=35.25 +lat_0=34.33333333333334 +lon_0=-86 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas North
Proj4js.defs["EPSG:32137"]="+proj=lcc +lat_1=36.18333333333333 +lat_2=34.65 +lat_0=34 +lon_0=-101.5 +x_0=200000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas North Central
Proj4js.defs["EPSG:32138"]="+proj=lcc +lat_1=33.96666666666667 +lat_2=32.13333333333333 +lat_0=31.66666666666667 +lon_0=-98.5 +x_0=600000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas Central
Proj4js.defs["EPSG:32139"]="+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=700000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas South Central
Proj4js.defs["EPSG:32140"]="+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=4000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Texas South
Proj4js.defs["EPSG:32141"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.66666666666667 +lon_0=-98.5 +x_0=300000 +y_0=5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Utah North
Proj4js.defs["EPSG:32142"]="+proj=lcc +lat_1=41.78333333333333 +lat_2=40.71666666666667 +lat_0=40.33333333333334 +lon_0=-111.5 +x_0=500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Utah Central
Proj4js.defs["EPSG:32143"]="+proj=lcc +lat_1=40.65 +lat_2=39.01666666666667 +lat_0=38.33333333333334 +lon_0=-111.5 +x_0=500000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Utah South
Proj4js.defs["EPSG:32144"]="+proj=lcc +lat_1=38.35 +lat_2=37.21666666666667 +lat_0=36.66666666666666 +lon_0=-111.5 +x_0=500000 +y_0=3000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Vermont
Proj4js.defs["EPSG:32145"]="+proj=tmerc +lat_0=42.5 +lon_0=-72.5 +k=0.999964286 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Virginia North
Proj4js.defs["EPSG:32146"]="+proj=lcc +lat_1=39.2 +lat_2=38.03333333333333 +lat_0=37.66666666666666 +lon_0=-78.5 +x_0=3500000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Virginia South
Proj4js.defs["EPSG:32147"]="+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Washington North
Proj4js.defs["EPSG:32148"]="+proj=lcc +lat_1=48.73333333333333 +lat_2=47.5 +lat_0=47 +lon_0=-120.8333333333333 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Washington South
Proj4js.defs["EPSG:32149"]="+proj=lcc +lat_1=47.33333333333334 +lat_2=45.83333333333334 +lat_0=45.33333333333334 +lon_0=-120.5 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / West Virginia North
Proj4js.defs["EPSG:32150"]="+proj=lcc +lat_1=40.25 +lat_2=39 +lat_0=38.5 +lon_0=-79.5 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / West Virginia South
Proj4js.defs["EPSG:32151"]="+proj=lcc +lat_1=38.88333333333333 +lat_2=37.48333333333333 +lat_0=37 +lon_0=-81 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wisconsin North
Proj4js.defs["EPSG:32152"]="+proj=lcc +lat_1=46.76666666666667 +lat_2=45.56666666666667 +lat_0=45.16666666666666 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wisconsin Central
Proj4js.defs["EPSG:32153"]="+proj=lcc +lat_1=45.5 +lat_2=44.25 +lat_0=43.83333333333334 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wisconsin South
Proj4js.defs["EPSG:32154"]="+proj=lcc +lat_1=44.06666666666667 +lat_2=42.73333333333333 +lat_0=42 +lon_0=-90 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wyoming East
Proj4js.defs["EPSG:32155"]="+proj=tmerc +lat_0=40.5 +lon_0=-105.1666666666667 +k=0.9999375 +x_0=200000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wyoming East Central
Proj4js.defs["EPSG:32156"]="+proj=tmerc +lat_0=40.5 +lon_0=-107.3333333333333 +k=0.9999375 +x_0=400000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wyoming West Central
Proj4js.defs["EPSG:32157"]="+proj=tmerc +lat_0=40.5 +lon_0=-108.75 +k=0.9999375 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Wyoming West
Proj4js.defs["EPSG:32158"]="+proj=tmerc +lat_0=40.5 +lon_0=-110.0833333333333 +k=0.9999375 +x_0=800000 +y_0=100000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Puerto Rico & Virgin Is.
Proj4js.defs["EPSG:32161"]="+proj=lcc +lat_1=18.43333333333333 +lat_2=18.03333333333333 +lat_0=17.83333333333333 +lon_0=-66.43333333333334 +x_0=200000 +y_0=200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / BLM 14N (ftUS)
Proj4js.defs["EPSG:32164"]="+proj=tmerc +lat_0=0 +lon_0=-99 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 15N (ftUS)
Proj4js.defs["EPSG:32165"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 16N (ftUS)
Proj4js.defs["EPSG:32166"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / BLM 17N (ftUS)
Proj4js.defs["EPSG:32167"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9996 +x_0=500000.001016002 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs  ";
// NAD83 / SCoPQ zone 2 (deprecated)
Proj4js.defs["EPSG:32180"]="+proj=tmerc +lat_0=0 +lon_0=-55.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 1
Proj4js.defs["EPSG:32181"]="+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 2
Proj4js.defs["EPSG:32182"]="+proj=tmerc +lat_0=0 +lon_0=-56 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 3
Proj4js.defs["EPSG:32183"]="+proj=tmerc +lat_0=0 +lon_0=-58.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 4
Proj4js.defs["EPSG:32184"]="+proj=tmerc +lat_0=0 +lon_0=-61.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 5
Proj4js.defs["EPSG:32185"]="+proj=tmerc +lat_0=0 +lon_0=-64.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 6
Proj4js.defs["EPSG:32186"]="+proj=tmerc +lat_0=0 +lon_0=-67.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 7
Proj4js.defs["EPSG:32187"]="+proj=tmerc +lat_0=0 +lon_0=-70.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 8
Proj4js.defs["EPSG:32188"]="+proj=tmerc +lat_0=0 +lon_0=-73.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 9
Proj4js.defs["EPSG:32189"]="+proj=tmerc +lat_0=0 +lon_0=-76.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 10
Proj4js.defs["EPSG:32190"]="+proj=tmerc +lat_0=0 +lon_0=-79.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 11
Proj4js.defs["EPSG:32191"]="+proj=tmerc +lat_0=0 +lon_0=-82.5 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 12
Proj4js.defs["EPSG:32192"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 13
Proj4js.defs["EPSG:32193"]="+proj=tmerc +lat_0=0 +lon_0=-84 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 14
Proj4js.defs["EPSG:32194"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 15
Proj4js.defs["EPSG:32195"]="+proj=tmerc +lat_0=0 +lon_0=-90 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 16
Proj4js.defs["EPSG:32196"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / MTM zone 17
Proj4js.defs["EPSG:32197"]="+proj=tmerc +lat_0=0 +lon_0=-96 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Quebec Lambert
Proj4js.defs["EPSG:32198"]="+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// NAD83 / Louisiana Offshore
Proj4js.defs["EPSG:32199"]="+proj=lcc +lat_1=27.83333333333333 +lat_2=26.16666666666667 +lat_0=25.5 +lon_0=-91.33333333333333 +x_0=1000000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs  ";
// WGS 72 / UTM zone 1N
Proj4js.defs["EPSG:32201"]="+proj=utm +zone=1 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 2N
Proj4js.defs["EPSG:32202"]="+proj=utm +zone=2 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 3N
Proj4js.defs["EPSG:32203"]="+proj=utm +zone=3 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 4N
Proj4js.defs["EPSG:32204"]="+proj=utm +zone=4 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 5N
Proj4js.defs["EPSG:32205"]="+proj=utm +zone=5 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 6N
Proj4js.defs["EPSG:32206"]="+proj=utm +zone=6 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 7N
Proj4js.defs["EPSG:32207"]="+proj=utm +zone=7 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 8N
Proj4js.defs["EPSG:32208"]="+proj=utm +zone=8 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 9N
Proj4js.defs["EPSG:32209"]="+proj=utm +zone=9 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 10N
Proj4js.defs["EPSG:32210"]="+proj=utm +zone=10 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 11N
Proj4js.defs["EPSG:32211"]="+proj=utm +zone=11 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 12N
Proj4js.defs["EPSG:32212"]="+proj=utm +zone=12 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 13N
Proj4js.defs["EPSG:32213"]="+proj=utm +zone=13 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 14N
Proj4js.defs["EPSG:32214"]="+proj=utm +zone=14 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 15N
Proj4js.defs["EPSG:32215"]="+proj=utm +zone=15 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 16N
Proj4js.defs["EPSG:32216"]="+proj=utm +zone=16 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 17N
Proj4js.defs["EPSG:32217"]="+proj=utm +zone=17 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 18N
Proj4js.defs["EPSG:32218"]="+proj=utm +zone=18 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 19N
Proj4js.defs["EPSG:32219"]="+proj=utm +zone=19 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 20N
Proj4js.defs["EPSG:32220"]="+proj=utm +zone=20 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 21N
Proj4js.defs["EPSG:32221"]="+proj=utm +zone=21 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 22N
Proj4js.defs["EPSG:32222"]="+proj=utm +zone=22 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 23N
Proj4js.defs["EPSG:32223"]="+proj=utm +zone=23 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 24N
Proj4js.defs["EPSG:32224"]="+proj=utm +zone=24 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 25N
Proj4js.defs["EPSG:32225"]="+proj=utm +zone=25 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 26N
Proj4js.defs["EPSG:32226"]="+proj=utm +zone=26 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 27N
Proj4js.defs["EPSG:32227"]="+proj=utm +zone=27 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 28N
Proj4js.defs["EPSG:32228"]="+proj=utm +zone=28 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 29N
Proj4js.defs["EPSG:32229"]="+proj=utm +zone=29 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 30N
Proj4js.defs["EPSG:32230"]="+proj=utm +zone=30 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 31N
Proj4js.defs["EPSG:32231"]="+proj=utm +zone=31 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 32N
Proj4js.defs["EPSG:32232"]="+proj=utm +zone=32 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 33N
Proj4js.defs["EPSG:32233"]="+proj=utm +zone=33 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 34N
Proj4js.defs["EPSG:32234"]="+proj=utm +zone=34 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 35N
Proj4js.defs["EPSG:32235"]="+proj=utm +zone=35 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 36N
Proj4js.defs["EPSG:32236"]="+proj=utm +zone=36 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 37N
Proj4js.defs["EPSG:32237"]="+proj=utm +zone=37 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 38N
Proj4js.defs["EPSG:32238"]="+proj=utm +zone=38 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 39N
Proj4js.defs["EPSG:32239"]="+proj=utm +zone=39 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 40N
Proj4js.defs["EPSG:32240"]="+proj=utm +zone=40 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 41N
Proj4js.defs["EPSG:32241"]="+proj=utm +zone=41 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 42N
Proj4js.defs["EPSG:32242"]="+proj=utm +zone=42 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 43N
Proj4js.defs["EPSG:32243"]="+proj=utm +zone=43 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 44N
Proj4js.defs["EPSG:32244"]="+proj=utm +zone=44 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 45N
Proj4js.defs["EPSG:32245"]="+proj=utm +zone=45 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 46N
Proj4js.defs["EPSG:32246"]="+proj=utm +zone=46 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 47N
Proj4js.defs["EPSG:32247"]="+proj=utm +zone=47 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 48N
Proj4js.defs["EPSG:32248"]="+proj=utm +zone=48 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 49N
Proj4js.defs["EPSG:32249"]="+proj=utm +zone=49 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 50N
Proj4js.defs["EPSG:32250"]="+proj=utm +zone=50 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 51N
Proj4js.defs["EPSG:32251"]="+proj=utm +zone=51 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 52N
Proj4js.defs["EPSG:32252"]="+proj=utm +zone=52 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 53N
Proj4js.defs["EPSG:32253"]="+proj=utm +zone=53 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 54N
Proj4js.defs["EPSG:32254"]="+proj=utm +zone=54 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 55N
Proj4js.defs["EPSG:32255"]="+proj=utm +zone=55 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 56N
Proj4js.defs["EPSG:32256"]="+proj=utm +zone=56 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 57N
Proj4js.defs["EPSG:32257"]="+proj=utm +zone=57 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 58N
Proj4js.defs["EPSG:32258"]="+proj=utm +zone=58 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 59N
Proj4js.defs["EPSG:32259"]="+proj=utm +zone=59 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 60N
Proj4js.defs["EPSG:32260"]="+proj=utm +zone=60 +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 1S
Proj4js.defs["EPSG:32301"]="+proj=utm +zone=1 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 2S
Proj4js.defs["EPSG:32302"]="+proj=utm +zone=2 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 3S
Proj4js.defs["EPSG:32303"]="+proj=utm +zone=3 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 4S
Proj4js.defs["EPSG:32304"]="+proj=utm +zone=4 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 5S
Proj4js.defs["EPSG:32305"]="+proj=utm +zone=5 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 6S
Proj4js.defs["EPSG:32306"]="+proj=utm +zone=6 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 7S
Proj4js.defs["EPSG:32307"]="+proj=utm +zone=7 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 8S
Proj4js.defs["EPSG:32308"]="+proj=utm +zone=8 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 9S
Proj4js.defs["EPSG:32309"]="+proj=utm +zone=9 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 10S
Proj4js.defs["EPSG:32310"]="+proj=utm +zone=10 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 11S
Proj4js.defs["EPSG:32311"]="+proj=utm +zone=11 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 12S
Proj4js.defs["EPSG:32312"]="+proj=utm +zone=12 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 13S
Proj4js.defs["EPSG:32313"]="+proj=utm +zone=13 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 14S
Proj4js.defs["EPSG:32314"]="+proj=utm +zone=14 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 15S
Proj4js.defs["EPSG:32315"]="+proj=utm +zone=15 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 16S
Proj4js.defs["EPSG:32316"]="+proj=utm +zone=16 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 17S
Proj4js.defs["EPSG:32317"]="+proj=utm +zone=17 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 18S
Proj4js.defs["EPSG:32318"]="+proj=utm +zone=18 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 19S
Proj4js.defs["EPSG:32319"]="+proj=utm +zone=19 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 20S
Proj4js.defs["EPSG:32320"]="+proj=utm +zone=20 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 21S
Proj4js.defs["EPSG:32321"]="+proj=utm +zone=21 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 22S
Proj4js.defs["EPSG:32322"]="+proj=utm +zone=22 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 23S
Proj4js.defs["EPSG:32323"]="+proj=utm +zone=23 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 24S
Proj4js.defs["EPSG:32324"]="+proj=utm +zone=24 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 25S
Proj4js.defs["EPSG:32325"]="+proj=utm +zone=25 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 26S
Proj4js.defs["EPSG:32326"]="+proj=utm +zone=26 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 27S
Proj4js.defs["EPSG:32327"]="+proj=utm +zone=27 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 28S
Proj4js.defs["EPSG:32328"]="+proj=utm +zone=28 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 29S
Proj4js.defs["EPSG:32329"]="+proj=utm +zone=29 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 30S
Proj4js.defs["EPSG:32330"]="+proj=utm +zone=30 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 31S
Proj4js.defs["EPSG:32331"]="+proj=utm +zone=31 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 32S
Proj4js.defs["EPSG:32332"]="+proj=utm +zone=32 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 33S
Proj4js.defs["EPSG:32333"]="+proj=utm +zone=33 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 34S
Proj4js.defs["EPSG:32334"]="+proj=utm +zone=34 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 35S
Proj4js.defs["EPSG:32335"]="+proj=utm +zone=35 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 36S
Proj4js.defs["EPSG:32336"]="+proj=utm +zone=36 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 37S
Proj4js.defs["EPSG:32337"]="+proj=utm +zone=37 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 38S
Proj4js.defs["EPSG:32338"]="+proj=utm +zone=38 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 39S
Proj4js.defs["EPSG:32339"]="+proj=utm +zone=39 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 40S
Proj4js.defs["EPSG:32340"]="+proj=utm +zone=40 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 41S
Proj4js.defs["EPSG:32341"]="+proj=utm +zone=41 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 42S
Proj4js.defs["EPSG:32342"]="+proj=utm +zone=42 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 43S
Proj4js.defs["EPSG:32343"]="+proj=utm +zone=43 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 44S
Proj4js.defs["EPSG:32344"]="+proj=utm +zone=44 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 45S
Proj4js.defs["EPSG:32345"]="+proj=utm +zone=45 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 46S
Proj4js.defs["EPSG:32346"]="+proj=utm +zone=46 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 47S
Proj4js.defs["EPSG:32347"]="+proj=utm +zone=47 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 48S
Proj4js.defs["EPSG:32348"]="+proj=utm +zone=48 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 49S
Proj4js.defs["EPSG:32349"]="+proj=utm +zone=49 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 50S
Proj4js.defs["EPSG:32350"]="+proj=utm +zone=50 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 51S
Proj4js.defs["EPSG:32351"]="+proj=utm +zone=51 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 52S
Proj4js.defs["EPSG:32352"]="+proj=utm +zone=52 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 53S
Proj4js.defs["EPSG:32353"]="+proj=utm +zone=53 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 54S
Proj4js.defs["EPSG:32354"]="+proj=utm +zone=54 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 55S
Proj4js.defs["EPSG:32355"]="+proj=utm +zone=55 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 56S
Proj4js.defs["EPSG:32356"]="+proj=utm +zone=56 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 57S
Proj4js.defs["EPSG:32357"]="+proj=utm +zone=57 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 58S
Proj4js.defs["EPSG:32358"]="+proj=utm +zone=58 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 59S
Proj4js.defs["EPSG:32359"]="+proj=utm +zone=59 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72 / UTM zone 60S
Proj4js.defs["EPSG:32360"]="+proj=utm +zone=60 +south +ellps=WGS72 +towgs84=0,0,4.5,0,0,0.554,0.2263 +units=m +no_defs  ";
// WGS 72BE / UTM zone 1N
Proj4js.defs["EPSG:32401"]="+proj=utm +zone=1 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 2N
Proj4js.defs["EPSG:32402"]="+proj=utm +zone=2 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 3N
Proj4js.defs["EPSG:32403"]="+proj=utm +zone=3 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 4N
Proj4js.defs["EPSG:32404"]="+proj=utm +zone=4 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 5N
Proj4js.defs["EPSG:32405"]="+proj=utm +zone=5 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 6N
Proj4js.defs["EPSG:32406"]="+proj=utm +zone=6 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 7N
Proj4js.defs["EPSG:32407"]="+proj=utm +zone=7 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 8N
Proj4js.defs["EPSG:32408"]="+proj=utm +zone=8 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 9N
Proj4js.defs["EPSG:32409"]="+proj=utm +zone=9 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 10N
Proj4js.defs["EPSG:32410"]="+proj=utm +zone=10 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 11N
Proj4js.defs["EPSG:32411"]="+proj=utm +zone=11 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 12N
Proj4js.defs["EPSG:32412"]="+proj=utm +zone=12 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 13N
Proj4js.defs["EPSG:32413"]="+proj=utm +zone=13 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 14N
Proj4js.defs["EPSG:32414"]="+proj=utm +zone=14 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 15N
Proj4js.defs["EPSG:32415"]="+proj=utm +zone=15 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 16N
Proj4js.defs["EPSG:32416"]="+proj=utm +zone=16 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 17N
Proj4js.defs["EPSG:32417"]="+proj=utm +zone=17 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 18N
Proj4js.defs["EPSG:32418"]="+proj=utm +zone=18 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 19N
Proj4js.defs["EPSG:32419"]="+proj=utm +zone=19 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 20N
Proj4js.defs["EPSG:32420"]="+proj=utm +zone=20 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 21N
Proj4js.defs["EPSG:32421"]="+proj=utm +zone=21 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 22N
Proj4js.defs["EPSG:32422"]="+proj=utm +zone=22 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 23N
Proj4js.defs["EPSG:32423"]="+proj=utm +zone=23 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 24N
Proj4js.defs["EPSG:32424"]="+proj=utm +zone=24 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 25N
Proj4js.defs["EPSG:32425"]="+proj=utm +zone=25 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 26N
Proj4js.defs["EPSG:32426"]="+proj=utm +zone=26 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 27N
Proj4js.defs["EPSG:32427"]="+proj=utm +zone=27 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 28N
Proj4js.defs["EPSG:32428"]="+proj=utm +zone=28 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 29N
Proj4js.defs["EPSG:32429"]="+proj=utm +zone=29 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 30N
Proj4js.defs["EPSG:32430"]="+proj=utm +zone=30 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 31N
Proj4js.defs["EPSG:32431"]="+proj=utm +zone=31 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 32N
Proj4js.defs["EPSG:32432"]="+proj=utm +zone=32 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 33N
Proj4js.defs["EPSG:32433"]="+proj=utm +zone=33 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 34N
Proj4js.defs["EPSG:32434"]="+proj=utm +zone=34 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 35N
Proj4js.defs["EPSG:32435"]="+proj=utm +zone=35 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 36N
Proj4js.defs["EPSG:32436"]="+proj=utm +zone=36 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 37N
Proj4js.defs["EPSG:32437"]="+proj=utm +zone=37 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 38N
Proj4js.defs["EPSG:32438"]="+proj=utm +zone=38 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 39N
Proj4js.defs["EPSG:32439"]="+proj=utm +zone=39 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 40N
Proj4js.defs["EPSG:32440"]="+proj=utm +zone=40 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 41N
Proj4js.defs["EPSG:32441"]="+proj=utm +zone=41 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 42N
Proj4js.defs["EPSG:32442"]="+proj=utm +zone=42 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 43N
Proj4js.defs["EPSG:32443"]="+proj=utm +zone=43 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 44N
Proj4js.defs["EPSG:32444"]="+proj=utm +zone=44 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 45N
Proj4js.defs["EPSG:32445"]="+proj=utm +zone=45 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 46N
Proj4js.defs["EPSG:32446"]="+proj=utm +zone=46 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 47N
Proj4js.defs["EPSG:32447"]="+proj=utm +zone=47 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 48N
Proj4js.defs["EPSG:32448"]="+proj=utm +zone=48 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 49N
Proj4js.defs["EPSG:32449"]="+proj=utm +zone=49 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 50N
Proj4js.defs["EPSG:32450"]="+proj=utm +zone=50 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 51N
Proj4js.defs["EPSG:32451"]="+proj=utm +zone=51 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 52N
Proj4js.defs["EPSG:32452"]="+proj=utm +zone=52 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 53N
Proj4js.defs["EPSG:32453"]="+proj=utm +zone=53 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 54N
Proj4js.defs["EPSG:32454"]="+proj=utm +zone=54 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 55N
Proj4js.defs["EPSG:32455"]="+proj=utm +zone=55 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 56N
Proj4js.defs["EPSG:32456"]="+proj=utm +zone=56 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 57N
Proj4js.defs["EPSG:32457"]="+proj=utm +zone=57 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 58N
Proj4js.defs["EPSG:32458"]="+proj=utm +zone=58 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 59N
Proj4js.defs["EPSG:32459"]="+proj=utm +zone=59 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 60N
Proj4js.defs["EPSG:32460"]="+proj=utm +zone=60 +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 1S
Proj4js.defs["EPSG:32501"]="+proj=utm +zone=1 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 2S
Proj4js.defs["EPSG:32502"]="+proj=utm +zone=2 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 3S
Proj4js.defs["EPSG:32503"]="+proj=utm +zone=3 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 4S
Proj4js.defs["EPSG:32504"]="+proj=utm +zone=4 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 5S
Proj4js.defs["EPSG:32505"]="+proj=utm +zone=5 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 6S
Proj4js.defs["EPSG:32506"]="+proj=utm +zone=6 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 7S
Proj4js.defs["EPSG:32507"]="+proj=utm +zone=7 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 8S
Proj4js.defs["EPSG:32508"]="+proj=utm +zone=8 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 9S
Proj4js.defs["EPSG:32509"]="+proj=utm +zone=9 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 10S
Proj4js.defs["EPSG:32510"]="+proj=utm +zone=10 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 11S
Proj4js.defs["EPSG:32511"]="+proj=utm +zone=11 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 12S
Proj4js.defs["EPSG:32512"]="+proj=utm +zone=12 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 13S
Proj4js.defs["EPSG:32513"]="+proj=utm +zone=13 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 14S
Proj4js.defs["EPSG:32514"]="+proj=utm +zone=14 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 15S
Proj4js.defs["EPSG:32515"]="+proj=utm +zone=15 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 16S
Proj4js.defs["EPSG:32516"]="+proj=utm +zone=16 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 17S
Proj4js.defs["EPSG:32517"]="+proj=utm +zone=17 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 18S
Proj4js.defs["EPSG:32518"]="+proj=utm +zone=18 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 19S
Proj4js.defs["EPSG:32519"]="+proj=utm +zone=19 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 20S
Proj4js.defs["EPSG:32520"]="+proj=utm +zone=20 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 21S
Proj4js.defs["EPSG:32521"]="+proj=utm +zone=21 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 22S
Proj4js.defs["EPSG:32522"]="+proj=utm +zone=22 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 23S
Proj4js.defs["EPSG:32523"]="+proj=utm +zone=23 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 24S
Proj4js.defs["EPSG:32524"]="+proj=utm +zone=24 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 25S
Proj4js.defs["EPSG:32525"]="+proj=utm +zone=25 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 26S
Proj4js.defs["EPSG:32526"]="+proj=utm +zone=26 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 27S
Proj4js.defs["EPSG:32527"]="+proj=utm +zone=27 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 28S
Proj4js.defs["EPSG:32528"]="+proj=utm +zone=28 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 29S
Proj4js.defs["EPSG:32529"]="+proj=utm +zone=29 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 30S
Proj4js.defs["EPSG:32530"]="+proj=utm +zone=30 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 31S
Proj4js.defs["EPSG:32531"]="+proj=utm +zone=31 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 32S
Proj4js.defs["EPSG:32532"]="+proj=utm +zone=32 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 33S
Proj4js.defs["EPSG:32533"]="+proj=utm +zone=33 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 34S
Proj4js.defs["EPSG:32534"]="+proj=utm +zone=34 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 35S
Proj4js.defs["EPSG:32535"]="+proj=utm +zone=35 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 36S
Proj4js.defs["EPSG:32536"]="+proj=utm +zone=36 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 37S
Proj4js.defs["EPSG:32537"]="+proj=utm +zone=37 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 38S
Proj4js.defs["EPSG:32538"]="+proj=utm +zone=38 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 39S
Proj4js.defs["EPSG:32539"]="+proj=utm +zone=39 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 40S
Proj4js.defs["EPSG:32540"]="+proj=utm +zone=40 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 41S
Proj4js.defs["EPSG:32541"]="+proj=utm +zone=41 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 42S
Proj4js.defs["EPSG:32542"]="+proj=utm +zone=42 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 43S
Proj4js.defs["EPSG:32543"]="+proj=utm +zone=43 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 44S
Proj4js.defs["EPSG:32544"]="+proj=utm +zone=44 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 45S
Proj4js.defs["EPSG:32545"]="+proj=utm +zone=45 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 46S
Proj4js.defs["EPSG:32546"]="+proj=utm +zone=46 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 47S
Proj4js.defs["EPSG:32547"]="+proj=utm +zone=47 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 48S
Proj4js.defs["EPSG:32548"]="+proj=utm +zone=48 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 49S
Proj4js.defs["EPSG:32549"]="+proj=utm +zone=49 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 50S
Proj4js.defs["EPSG:32550"]="+proj=utm +zone=50 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 51S
Proj4js.defs["EPSG:32551"]="+proj=utm +zone=51 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 52S
Proj4js.defs["EPSG:32552"]="+proj=utm +zone=52 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 53S
Proj4js.defs["EPSG:32553"]="+proj=utm +zone=53 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 54S
Proj4js.defs["EPSG:32554"]="+proj=utm +zone=54 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 55S
Proj4js.defs["EPSG:32555"]="+proj=utm +zone=55 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 56S
Proj4js.defs["EPSG:32556"]="+proj=utm +zone=56 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 57S
Proj4js.defs["EPSG:32557"]="+proj=utm +zone=57 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 58S
Proj4js.defs["EPSG:32558"]="+proj=utm +zone=58 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 59S
Proj4js.defs["EPSG:32559"]="+proj=utm +zone=59 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 72BE / UTM zone 60S
Proj4js.defs["EPSG:32560"]="+proj=utm +zone=60 +south +ellps=WGS72 +towgs84=0,0,1.9,0,0,0.814,-0.38 +units=m +no_defs  ";
// WGS 84 / UTM grid system (northern hemisphere)
// Unable to translate coordinate system EPSG:32600 into PROJ.4 format.
//
// WGS 84 / UTM zone 1N
Proj4js.defs["EPSG:32601"]="+proj=utm +zone=1 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 2N
Proj4js.defs["EPSG:32602"]="+proj=utm +zone=2 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 3N
Proj4js.defs["EPSG:32603"]="+proj=utm +zone=3 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 4N
Proj4js.defs["EPSG:32604"]="+proj=utm +zone=4 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 5N
Proj4js.defs["EPSG:32605"]="+proj=utm +zone=5 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 6N
Proj4js.defs["EPSG:32606"]="+proj=utm +zone=6 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 7N
Proj4js.defs["EPSG:32607"]="+proj=utm +zone=7 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 8N
Proj4js.defs["EPSG:32608"]="+proj=utm +zone=8 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 9N
Proj4js.defs["EPSG:32609"]="+proj=utm +zone=9 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 10N
Proj4js.defs["EPSG:32610"]="+proj=utm +zone=10 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 11N
Proj4js.defs["EPSG:32611"]="+proj=utm +zone=11 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 12N
Proj4js.defs["EPSG:32612"]="+proj=utm +zone=12 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 13N
Proj4js.defs["EPSG:32613"]="+proj=utm +zone=13 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 14N
Proj4js.defs["EPSG:32614"]="+proj=utm +zone=14 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 15N
Proj4js.defs["EPSG:32615"]="+proj=utm +zone=15 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 16N
Proj4js.defs["EPSG:32616"]="+proj=utm +zone=16 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 17N
Proj4js.defs["EPSG:32617"]="+proj=utm +zone=17 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 18N
Proj4js.defs["EPSG:32618"]="+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 19N
Proj4js.defs["EPSG:32619"]="+proj=utm +zone=19 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 20N
Proj4js.defs["EPSG:32620"]="+proj=utm +zone=20 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 21N
Proj4js.defs["EPSG:32621"]="+proj=utm +zone=21 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 22N
Proj4js.defs["EPSG:32622"]="+proj=utm +zone=22 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 23N
Proj4js.defs["EPSG:32623"]="+proj=utm +zone=23 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 24N
Proj4js.defs["EPSG:32624"]="+proj=utm +zone=24 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 25N
Proj4js.defs["EPSG:32625"]="+proj=utm +zone=25 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 26N
Proj4js.defs["EPSG:32626"]="+proj=utm +zone=26 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 27N
Proj4js.defs["EPSG:32627"]="+proj=utm +zone=27 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 28N
Proj4js.defs["EPSG:32628"]="+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 29N
Proj4js.defs["EPSG:32629"]="+proj=utm +zone=29 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 30N
Proj4js.defs["EPSG:32630"]="+proj=utm +zone=30 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 31N
Proj4js.defs["EPSG:32631"]="+proj=utm +zone=31 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 32N
Proj4js.defs["EPSG:32632"]="+proj=utm +zone=32 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 33N
Proj4js.defs["EPSG:32633"]="+proj=utm +zone=33 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 34N
Proj4js.defs["EPSG:32634"]="+proj=utm +zone=34 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 35N
Proj4js.defs["EPSG:32635"]="+proj=utm +zone=35 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 36N
Proj4js.defs["EPSG:32636"]="+proj=utm +zone=36 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 37N
Proj4js.defs["EPSG:32637"]="+proj=utm +zone=37 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 38N
Proj4js.defs["EPSG:32638"]="+proj=utm +zone=38 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 39N
Proj4js.defs["EPSG:32639"]="+proj=utm +zone=39 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 40N
Proj4js.defs["EPSG:32640"]="+proj=utm +zone=40 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 41N
Proj4js.defs["EPSG:32641"]="+proj=utm +zone=41 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 42N
Proj4js.defs["EPSG:32642"]="+proj=utm +zone=42 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 43N
Proj4js.defs["EPSG:32643"]="+proj=utm +zone=43 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 44N
Proj4js.defs["EPSG:32644"]="+proj=utm +zone=44 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 45N
Proj4js.defs["EPSG:32645"]="+proj=utm +zone=45 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 46N
Proj4js.defs["EPSG:32646"]="+proj=utm +zone=46 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 47N
Proj4js.defs["EPSG:32647"]="+proj=utm +zone=47 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 48N
Proj4js.defs["EPSG:32648"]="+proj=utm +zone=48 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 49N
Proj4js.defs["EPSG:32649"]="+proj=utm +zone=49 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 50N
Proj4js.defs["EPSG:32650"]="+proj=utm +zone=50 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 51N
Proj4js.defs["EPSG:32651"]="+proj=utm +zone=51 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 52N
Proj4js.defs["EPSG:32652"]="+proj=utm +zone=52 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 53N
Proj4js.defs["EPSG:32653"]="+proj=utm +zone=53 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 54N
Proj4js.defs["EPSG:32654"]="+proj=utm +zone=54 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 55N
Proj4js.defs["EPSG:32655"]="+proj=utm +zone=55 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 56N
Proj4js.defs["EPSG:32656"]="+proj=utm +zone=56 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 57N
Proj4js.defs["EPSG:32657"]="+proj=utm +zone=57 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 58N
Proj4js.defs["EPSG:32658"]="+proj=utm +zone=58 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 59N
Proj4js.defs["EPSG:32659"]="+proj=utm +zone=59 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 60N
Proj4js.defs["EPSG:32660"]="+proj=utm +zone=60 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UPS North (N,E)
Proj4js.defs["EPSG:32661"]="+proj=stere +lat_0=90 +lat_ts=90 +lon_0=0 +k=0.994 +x_0=2000000 +y_0=2000000 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / Plate Carree (deprecated)
Proj4js.defs["EPSG:32662"]="+proj=eqc +lat_ts=0 +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / World Equidistant Cylindrical (deprecated)
// Unable to translate coordinate system EPSG:32663 into PROJ.4 format.
//
// WGS 84 / BLM 14N (ftUS)
Proj4js.defs["EPSG:32664"]="+proj=tmerc +lat_0=0 +lon_0=-99 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=WGS84 +units=us-ft +no_defs  ";
// WGS 84 / BLM 15N (ftUS)
Proj4js.defs["EPSG:32665"]="+proj=tmerc +lat_0=0 +lon_0=-93 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=WGS84 +units=us-ft +no_defs  ";
// WGS 84 / BLM 16N (ftUS)
Proj4js.defs["EPSG:32666"]="+proj=tmerc +lat_0=0 +lon_0=-87 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=WGS84 +units=us-ft +no_defs  ";
// WGS 84 / BLM 17N (ftUS)
Proj4js.defs["EPSG:32667"]="+proj=tmerc +lat_0=0 +lon_0=-81 +k=0.9996 +x_0=500000.001016002 +y_0=0 +datum=WGS84 +units=us-ft +no_defs  ";
// WGS 84 / UTM grid system (southern hemisphere)
// Unable to translate coordinate system EPSG:32700 into PROJ.4 format.
//
// WGS 84 / UTM zone 1S
Proj4js.defs["EPSG:32701"]="+proj=utm +zone=1 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 2S
Proj4js.defs["EPSG:32702"]="+proj=utm +zone=2 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 3S
Proj4js.defs["EPSG:32703"]="+proj=utm +zone=3 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 4S
Proj4js.defs["EPSG:32704"]="+proj=utm +zone=4 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 5S
Proj4js.defs["EPSG:32705"]="+proj=utm +zone=5 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 6S
Proj4js.defs["EPSG:32706"]="+proj=utm +zone=6 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 7S
Proj4js.defs["EPSG:32707"]="+proj=utm +zone=7 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 8S
Proj4js.defs["EPSG:32708"]="+proj=utm +zone=8 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 9S
Proj4js.defs["EPSG:32709"]="+proj=utm +zone=9 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 10S
Proj4js.defs["EPSG:32710"]="+proj=utm +zone=10 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 11S
Proj4js.defs["EPSG:32711"]="+proj=utm +zone=11 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 12S
Proj4js.defs["EPSG:32712"]="+proj=utm +zone=12 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 13S
Proj4js.defs["EPSG:32713"]="+proj=utm +zone=13 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 14S
Proj4js.defs["EPSG:32714"]="+proj=utm +zone=14 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 15S
Proj4js.defs["EPSG:32715"]="+proj=utm +zone=15 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 16S
Proj4js.defs["EPSG:32716"]="+proj=utm +zone=16 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 17S
Proj4js.defs["EPSG:32717"]="+proj=utm +zone=17 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 18S
Proj4js.defs["EPSG:32718"]="+proj=utm +zone=18 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 19S
Proj4js.defs["EPSG:32719"]="+proj=utm +zone=19 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 20S
Proj4js.defs["EPSG:32720"]="+proj=utm +zone=20 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 21S
Proj4js.defs["EPSG:32721"]="+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 22S
Proj4js.defs["EPSG:32722"]="+proj=utm +zone=22 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 23S
Proj4js.defs["EPSG:32723"]="+proj=utm +zone=23 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 24S
Proj4js.defs["EPSG:32724"]="+proj=utm +zone=24 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 25S
Proj4js.defs["EPSG:32725"]="+proj=utm +zone=25 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 26S
Proj4js.defs["EPSG:32726"]="+proj=utm +zone=26 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 27S
Proj4js.defs["EPSG:32727"]="+proj=utm +zone=27 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 28S
Proj4js.defs["EPSG:32728"]="+proj=utm +zone=28 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 29S
Proj4js.defs["EPSG:32729"]="+proj=utm +zone=29 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 30S
Proj4js.defs["EPSG:32730"]="+proj=utm +zone=30 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 31S
Proj4js.defs["EPSG:32731"]="+proj=utm +zone=31 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 32S
Proj4js.defs["EPSG:32732"]="+proj=utm +zone=32 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 33S
Proj4js.defs["EPSG:32733"]="+proj=utm +zone=33 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 34S
Proj4js.defs["EPSG:32734"]="+proj=utm +zone=34 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 35S
Proj4js.defs["EPSG:32735"]="+proj=utm +zone=35 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 36S
Proj4js.defs["EPSG:32736"]="+proj=utm +zone=36 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 37S
Proj4js.defs["EPSG:32737"]="+proj=utm +zone=37 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 38S
Proj4js.defs["EPSG:32738"]="+proj=utm +zone=38 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 39S
Proj4js.defs["EPSG:32739"]="+proj=utm +zone=39 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 40S
Proj4js.defs["EPSG:32740"]="+proj=utm +zone=40 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 41S
Proj4js.defs["EPSG:32741"]="+proj=utm +zone=41 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 42S
Proj4js.defs["EPSG:32742"]="+proj=utm +zone=42 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 43S
Proj4js.defs["EPSG:32743"]="+proj=utm +zone=43 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 44S
Proj4js.defs["EPSG:32744"]="+proj=utm +zone=44 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 45S
Proj4js.defs["EPSG:32745"]="+proj=utm +zone=45 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 46S
Proj4js.defs["EPSG:32746"]="+proj=utm +zone=46 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 47S
Proj4js.defs["EPSG:32747"]="+proj=utm +zone=47 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 48S
Proj4js.defs["EPSG:32748"]="+proj=utm +zone=48 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 49S
Proj4js.defs["EPSG:32749"]="+proj=utm +zone=49 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 50S
Proj4js.defs["EPSG:32750"]="+proj=utm +zone=50 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 51S
Proj4js.defs["EPSG:32751"]="+proj=utm +zone=51 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 52S
Proj4js.defs["EPSG:32752"]="+proj=utm +zone=52 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 53S
Proj4js.defs["EPSG:32753"]="+proj=utm +zone=53 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 54S
Proj4js.defs["EPSG:32754"]="+proj=utm +zone=54 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 55S
Proj4js.defs["EPSG:32755"]="+proj=utm +zone=55 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 56S
Proj4js.defs["EPSG:32756"]="+proj=utm +zone=56 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 57S
Proj4js.defs["EPSG:32757"]="+proj=utm +zone=57 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 58S
Proj4js.defs["EPSG:32758"]="+proj=utm +zone=58 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 59S
Proj4js.defs["EPSG:32759"]="+proj=utm +zone=59 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UTM zone 60S
Proj4js.defs["EPSG:32760"]="+proj=utm +zone=60 +south +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / UPS South (N,E)
Proj4js.defs["EPSG:32761"]="+proj=stere +lat_0=-90 +lat_ts=-90 +lon_0=0 +k=0.994 +x_0=2000000 +y_0=2000000 +datum=WGS84 +units=m +no_defs  ";
// WGS 84 / TM 36 SE
Proj4js.defs["EPSG:32766"]="+proj=tmerc +lat_0=0 +lon_0=36 +k=0.9996 +x_0=500000 +y_0=10000000 +datum=WGS84 +units=m +no_defs  ";