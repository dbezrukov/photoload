$(document).ready(function () {
    var orientation = 1;

    $('.video-rotate').click(function(event) {
        if (orientation === 4) {
            orientation = 0;
        }

        $('.video-image').attr('orientation', ++orientation);

        console.log('rotating!');
    });
});