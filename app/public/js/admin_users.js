require([
    'jquery', 
    'bootstrap',
    'bootstrap-multiselect', 
	'css!stylesheets/bootstrap-multiselect'
], function($) {

    var ui = {};

    $(function() {

    	populateUserTable();

	    // user management
	    $('#btnAddUser').on('click', addUser);
	    
	    ui.dialog = $('#modalUserEdit');
	    ui.dialog.btnSubmit = ui.dialog.find('#btnSubmit');

	    ui.dialog.form = ui.dialog.find('form');
	    ui.dialog.form.userPropertiesGroup = ui.dialog.find('.userPropertiesGroup');
	    ui.dialog.form.userPasswordGroup = ui.dialog.find('.userPasswordGroup');
	    ui.dialog.form.name = ui.dialog.form.find('#userName');
	    ui.dialog.form.password1 = ui.dialog.form.find('#userPassword1');
	    ui.dialog.form.password2 = ui.dialog.form.find('#userPassword2');
	    ui.dialog.form.groups = ui.dialog.form.find('#userGroups');
	    ui.dialog.form.groupsAdmin = ui.dialog.form.find('#userGroupsAdmin');

	    ui.dialog.btnSubmit.on('click', userSubmit);
	    
	    ui.dialog.on('keypress', function(e) {
	        if (e.keyCode == 13) {
	            userSubmit();
	        }
	    });

	    ui.dialog.form.groups.removeClass('hidden');
	    ui.dialog.form.groups.multiselect({
	        nonSelectedText: 'Не выбрано',
	        nSelectedText: 'выбрано',
	        numberDisplayed: 4
	    });

	    ui.dialog.form.groupsAdmin.removeClass('hidden');
	    ui.dialog.form.groupsAdmin.multiselect({
	        nonSelectedText: 'Не выбрано',
	        nSelectedText: 'выбрано',
	        numberDisplayed: 4
	    });

	    // $('#modalUserEdit #userRectP1Lat, #modalUserEdit #userRectP2Lat').inputmask({
	    //     mask: "99°99'99" + '"',
	    //     placeholder: "0"
	    // });
	    // $('#modalUserEdit #userRectP1Lon, #modalUserEdit #userRectP2Lon').inputmask({
	    //     mask: "999°99'99" + '"',
	    //     placeholder: "0"
	    // });

	    $('#userList table tbody').on('click', 'td a.linkediteuser', editUser);
	    $('#userList table tbody').on('click', 'td a.linksetpassword', setPassword);
	    $('#userList table tbody').on('click', 'td a.linkdeleteuser', deleteUser);
    });

	function sign(number) {
	    return number ? number < 0 ? -1 : 1 : 1;
	}

	function pad(n, width, z) {
	    z = z || '0';
	    n = n + '';
	    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	}

	function fromDMS(dms, sign) {
	    var dmsParts = dms.match(/(\d+)/g);

	    var dd = 0;
	    dd += parseInt(dmsParts[0]);        // deg
	    dd += parseInt(dmsParts[1]) / 60;   // min
	    dd += parseInt(dmsParts[2]) / 3600; // sec
	    dd *= sign;

	    return dd.toFixed(6);
	}

	function toDMS(coordinate, isLatitude) {
	    var dd = Math.abs(coordinate);

	    var deg = dd | 0;              // truncate dd to get degrees
	    var frac = Math.abs(dd - deg); // get fractional part
	    var min = (frac * 60) | 0;     // multiply fraction by 60 and truncate
	    var sec = (frac * 3600 - min * 60) | 0;
	    
	    var dms = '';
	    dms += pad(deg, isLatitude === true ? 2 : 3) + '°';
	    dms += pad(min, 2) + "'";
	    dms += pad(sec, 2) + '"';

	    return dms;
	}

	function populateUserTable() {
	    var tableContent = '';

	    $.getJSON( '/users', function( data ) {
	        
	        $.each(data, function(index){
	            tableContent += '<tr>';
	            tableContent += '<td>' + this.id + '</td>';
	            tableContent += '<td>' + this.name + '</td>';
	            tableContent += '<td>' + (this.is_admin === 'true' ? 'Да' : '-') + '</td>';
	            tableContent += '<td>' + (this.is_online === true ? 'Да' : '-') + '</td>';

	            tableContent += '<td>';
	            tableContent += '<a href="#" class="linkediteuser" style="text-decoration: none" rel="' + this.id + '">Редактировать</a> &middot ';
	            tableContent += '<a href="#" class="linksetpassword" style="text-decoration: none" rel="' + this.id + '">Задать пароль</a> &middot ';
	            tableContent += '<a href="#" class="linkdeleteuser" style="text-decoration: none" rel="' + this.id + '">Удалить</a>';
	            tableContent += '</td>';
	            tableContent += '</tr>';
	        });

	        $('#userList table tbody').html(tableContent);
	    });
	};

	function addUser(event) {
	    event.preventDefault();

	    ui.dialog.form.attr('userId', '');
	    ui.dialog.form.name.val('');

	    // get available groups
	    $.getJSON( '/groups', function( groups ) {
	        ui.dialog.form.groups.multiselect('dataprovider', multiselectData(groups));
	        ui.dialog.form.groupsAdmin.multiselect('dataprovider', multiselectData(groups));

	        ui.dialog.form.userPropertiesGroup.show();
	        ui.dialog.form.userPasswordGroup.show();

	        ui.dialog.btnSubmit.text('Создать');
	        ui.dialog.modal('show');
	    });

	    // form.find('#userRectP1Lat').val("00°00'00" + '"');
	    // form.find('#userRectP1Lon').val("000°00'00" + '"');

	    // form.find('#userRectP2Lat').val("00°00'00" + '"');
	    // form.find('#userRectP2Lon').val("000°00'00" + '"');

	    return false;    
	};

	function editUser(event) {
	    event.preventDefault();

	    var id = $(this).attr('rel');

	    ui.dialog.form.attr('userId', id);

	    // get available groups (where you're a member)
	    $.getJSON( '/groups', function( groups ) {
	        ui.dialog.form.groups.multiselect('dataprovider', multiselectData(groups));
	        ui.dialog.form.groupsAdmin.multiselect('dataprovider', multiselectData(groups));

	        // get user properties
	        $.getJSON( '/users/' + id, function( data ) {
	            ui.dialog.form.name.val(data.name);

	            data.groups.forEach(function(groupId) {
	                ui.dialog.form.groups.multiselect('select', groupId, true);
	            });

	            data.groups_admin.forEach(function(groupId) {
	                ui.dialog.form.groupsAdmin.multiselect('select', groupId, true);
	            });

	            ui.dialog.form.userPropertiesGroup.show();
	            ui.dialog.form.userPasswordGroup.hide();
	            ui.dialog.btnSubmit.text('Сохранить');
	            ui.dialog.modal('show');

	            // form.find('#userNpuList').val(data.npu_list);

	            // form.find('#userRectP1Lat').val(toDMS(data.region.p1_lat, true));
	            // form.find('#userRectP1LatSign').val(sign(data.region.p1_lat));

	            // form.find('#userRectP1Lon').val(toDMS(data.region.p1_lon, false)); 
	            // form.find('#userRectP1LonSign').val(sign(data.region.p1_lon));
	        
	            // form.find('#userRectP2Lat').val(toDMS(data.region.p2_lat, true)); 
	            // form.find('#userRectP2LatSign').val(sign(data.region.p2_lat));
	            
	            // form.find('#userRectP2Lon').val(toDMS(data.region.p2_lon, false)); 
	            // form.find('#userRectP2LonSign').val(sign(data.region.p2_lon));
	        });
	    });

	    return false;
	};

	function setPassword(event) {
	    event.preventDefault();

	    $('#modalUserEdit').modal('show');
	    $('#modalUserEdit #btnSubmit').text('Сохранить');

	    var id = $(this).attr('rel');

	    var form = $('#modalUserEdit form');
	    
	    form.attr('userId', id);

	    ui.dialog.form.userPropertiesGroup.hide();
	    ui.dialog.form.userPasswordGroup.show();

	    return false;
	};

	function deleteUser(event) {
	    event.preventDefault();

	    var confirmation = confirm('Удалить этого пользователя?');

	    if (confirmation !== true) {
	        return false;
	    }

	    $.ajax({
	        type: 'DELETE',
	        url: '/users/' + $(this).attr('rel')
	    })
	    .done(function( response ) {
	        populateUserTable();
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	};

	function userSubmit(event) {
	    var name = ui.dialog.form.name.val();
	    var groups = ui.dialog.form.groups.val();
	    var groups_admin = ui.dialog.form.groupsAdmin.val();
	    var password1 = ui.dialog.form.password1.val();
	    var password2 = ui.dialog.form.password2.val();
	    //var npu_list = form.find('#userNpuList').val();

	    // show whether we're editing a user or creating one
	    var userId = ui.dialog.form.attr('userId');

	    var userData = {};

	    var hasPropertiesData = (ui.dialog.form.userPropertiesGroup.is(":visible") === true);
	    var hasPasswordData = (ui.dialog.form.userPasswordGroup.is(":visible") === true);

	    if (hasPropertiesData === true) {
	        if (jQuery.inArray("", [ name ]) !== -1) {
	            alert('Пожалуйста, введите имя');
	            return false;
	        }

	        if (!groups) {
	            alert('Пожалуйста, задайте минимум одну группу');
	            return false;
	        }

	        userData.name = name;
	        userData.npu_list = []; //npu_list;
	        userData.groups = groups;
	        userData.groups_admin = groups_admin;
	    }

	    if (hasPasswordData === true) {
	        if (jQuery.inArray("", [ password1, password2 ]) !== -1) {
	            alert('Пожалуйста, введите пароль');
	            return false;
	        }
	        if (password1 !== password2 ) {
	            alert('Пароли не совпадают');
	            return false;
	        }

	        userData.password = password1;
	    }

	    ui.dialog.modal('hide');

	    var url; 
	    
	    if (userId === '') {
	        url = '/users';
	    } else {
	        url = (hasPropertiesData === true ? '/users/' + userId : '/users/' + userId + '/set-password');
	    }


	    $.ajax({
	        type: 'POST',
	        data: userData,
	        url: url,
	        dataType: 'JSON'
	    })
	    .done(function( response ) {
	        populateUserTable();
	    })
	    .fail(function(jqXHR, textStatus, err) {
	        alert(JSON.parse(jqXHR.responseText).error);
	    });
	}
	
});

