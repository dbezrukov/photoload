define([
    'marionette',
    'tpl!templates/modal_incomming_call.tmpl',
    'css!stylesheets/modal_incomming_call_style'
], function(Marionette, template) {

    navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "modal-dialog",
        id: 'modalIncommingCall',

        ui: {
            'modalLabel': '#modalLabel'
        },

        events: {
            'click .incommingCallAnswer': '_onContactAnswerCall',
            'click .incommingCallReject': '_onContactRejectCall'
        },

        initialize: function(){
            var self = this;
            self.model.on('change:callState', self._onCallStateChanged, self);
        },

        onRender: function() {
            var name = this.model.get('name');

            this.ui.modalLabel.text('Звонит ' + name);
        },

        _onContactAnswerCall: function(event){
            var self = this;

            event.preventDefault();

            var canVideo = true;

            navigator.getUserMedia({ audio: true, video: canVideo }, 
                function(localStream) {
                    app.vent.trigger('call:localStream', URL.createObjectURL(localStream));
                    self.model.answerCall(localStream, canVideo);
                },
                function(error) { 
                    switch(error.name) {
                        case "PermissionDeniedError": 
                            alert('Вы запретили доступ к микрофону.\nДля функционирования звонков разрешите доступ в настройках браузера.');
                        break;
                        default:
                            alert('Не удается получить доступ к микрофону.\nКод ошибки браузера: ' + error.name);
                    }
                    console.log(error.name) 
                }
            );
        },

        _onContactRejectCall: function(event){
            event.preventDefault();
            this.model.rejectCall();
        },

        _onCallStateChanged: function(model, callState) {
            this.close();
        }
    });
});




        
