define([
    'underscore',
    'underscore.string',
    'marionette',
    'tpl!templates/file_uploader.tmpl'
], function(_, _str, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "modal-dialog",
        id: 'fileUploader',

        ui: {
        	'fileUpload': '#fileUpload',
        	'labelInfo': '#labelInfo'
        },

        events: {
        	'click #btnSelect': 'selectFolder',
        	'change #fileUpload': 'onUploadFolderSelected',
        },

        initialize: function(){
            var self = this;
        },

        onRender: function() {
            var self = this;
        },

        onShow: function(){
        },

		selectFolder: function() {
        	var self = this;

			self.ui.fileUpload.trigger('click');
        },

        onUploadFolderSelected: function(event) {
    		var self = this;

    		self.ui.labelInfo.text('Файл выбран, загрузка..');

			var selectedFile = event.target.files[0];
			var fileName = selectedFile.name.split('.')[0];
    		var reader = new FileReader();
			
			reader.onload = function(event) {
				self.uploadFile(fileName, event.target.result);
			};

			reader.readAsArrayBuffer(selectedFile);
		},

		uploadFile: function(fileName, data) {
			var self = this;

			function uploadCallback(data, status, xhr) {
				var tileId = data.id;

				var currentTimestamp = new Date();
				var metaData = {
					name: fileName,
					utc: (currentTimestamp.getTime() - window.timedelta) / 1000
				}

				$.ajax({
					url: self.options.collection.url + '/' + tileId + '/meta',
      				type: 'POST',
      				data: JSON.stringify(metaData),
      				dataType: 'json',
      				processData: true,
    				contentType: 'application/json',
      				success: function(data, status, xhr) {
      					self.ui.labelInfo.text('Файл загружен');
      			
      					self.options.collection.fetch(self.options.searchControl.getQueryObject());

		    			setTimeout(function() {
		    				self.ui.labelInfo.text('');
						}, 10000);
      				},
      				error: function(e, status) {
      					console.log('Ошибка записи метаданных на сервер');
      				}
				});
	      	}

			$.ajax({url: self.options.collection.url,
      			type: 'POST',
      			data: data,
      			processData: false,
    			contentType: 'image/tiff',
      			success: uploadCallback
			});
		}

    });
});
