define([
	'underscore',
    'marionette',
    'tpl!templates/photo_search_control.tmpl',
    'css!stylesheets/photo_search_control_style',
	'bootstrap-multiselect',
    'bootstrap-datepicker'
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "photoSearchControl",

        events: {
            'click #btnFetchPhotos' : 'fetchPhotos',
            'change #regSWLat' : 'changedSW',
            'change #regSWLon' : 'changedSW',
            'change #regNELat' : 'changedNE',
            'change #regNELon' : 'changedNE',
            'click #dateDropDownCancel' : 'dateCancel',
            'click #dateDropDownOK' : 'dateOK',
            'click #regionDropDownCancel' : 'regionCancel',
            'click #regionDropDownOK' : 'regionOK',
            'click #regionFromMap' : 'regionFromMap',
        },

        ui: {
        	datepicker: '.datepicker',
            dateDropDown: '.dateDropDown',
            regionDropDown: '.regionDropDown',
            dateDropDownControl: '#dateDropDownControl',
            regionDropDownControl: '#regionDropDownControl',
        	searchUsers: '#searchUsers',
        	searchBoards: '#searchBoards',
            regSWLat: '#regSWLat',
        	regSWLon: '#regSWLon',
        	regNELat: '#regNELat',
        	regNELon: '#regNELon',
            dateValue: '#dateValue',
            regionValue: '#regionValue',
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});

            self.self_id = options.self_id;

            $.fn.datepicker.dates['ru'] = {
                days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
                daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
                daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
                months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                today: "Сегодня",
                weekStart: 1
        	};

            app.vent.on('map:region-point', self.setRegionPoint, self);

            self.isaboutToSetFirstRegionPoint = true;
        },

        onRender: function() {
			var self = this;

        	var dateEnd = new Date();
        	var dateStart = new Date();
        	dateStart.setDate(dateEnd.getDate() - 365);

        	function format(n){
    			return n > 9 ? "" + n: "0" + n;
			}

        	var valStart = format(dateStart.getDate()) + "/" + format(dateStart.getMonth() + 1) + "/" + dateStart.getFullYear();
        	var valEnd   = format(dateEnd.getDate()) + "/" + format(dateEnd.getMonth() + 1) + "/" + dateEnd.getFullYear();

        	$(self.ui.datepicker[0]).find('input').val(valStart);
        	$(self.ui.datepicker[1]).find('input').val(valEnd);

        	$(self.ui.datepicker[0]).datepicker({
            	language: "ru",
            	orientation: "right top",
            	autoclose: true,
            	startDate: '-10y',
            	endDate: new Date(),
            	todayHighlight: true
        	});

        	$(self.ui.datepicker[1]).datepicker({
            	language: "ru",
            	orientation: "right top",
            	autoclose: true,
            	startDate: '-10y',
            	endDate: new Date(),
            	todayHighlight: true
        	});

            self.ui.regionDropDown.find('input:radio').on('change', function(e) {
                if (e.target.id === 'regionCoordsWGS') {
                    self.updateUsingWGS(true);
                } else if (e.target.id === 'regionCoordsXY') {
                    self.updateUsingWGS(false);
                }
            });

            // default search region
            self.region = {
                sw: { lat: 10, lon: 10 },
                ne: { lat: 70, lon: 170 }
            }

            // draw values in WGS
            self.updateUsingWGS(true);

            self.ui.dateDropDownControl.on({
                "shown.bs.dropdown": function() { this.closable = false; },
                "hide.bs.dropdown":  function() { return this.closable; }
            });

            self.ui.dateDropDownControl.on("click", function(e) { 
                this.closable = $(e.target).is('.dropdown-toggle, #dateValue, #dateDropDownOK, #dateDropDownCancel');
            });

            self.ui.regionDropDownControl.on({
                "shown.bs.dropdown": function() { this.closable = false; },
                "hide.bs.dropdown":  function() { return this.closable; }
            });

            self.ui.regionDropDownControl.on("click", function(e) { 
                this.closable = $(e.target).is('.dropdown-toggle, #regionValue, #regionDropDownOK, #regionDropDownCancel'); 
            });

            self.ui.searchUsers.removeClass('hidden');
    		self.ui.searchUsers.multiselect({
        		nonSelectedText: 'Не выбрано',
        		nSelectedText: 'выбрано',
        		allSelectedText: 'Все',
        		numberDisplayed: 3,
            	includeSelectAllOption: true,
            	selectAllText: 'Выбрать все',
            	maxHeight: 400
    		});

			self.ui.searchBoards.removeClass('hidden');
    		self.ui.searchBoards.multiselect({
        		nonSelectedText: 'Не выбран',
        		nSelectedText: 'выбрано',
        		allSelectedText: 'Все',
        		numberDisplayed: 3,
        		includeSelectAllOption: true,
            	selectAllText: 'Выбрать все',
            	maxHeight: 400
    		});

    		self.fetchUsers();
    		self.fetchBoards();
        },

        changedSW: function(){
            var self = this;

            var lat = parseFloat(self.ui.regSWLat.val());
            var lon = parseFloat(self.ui.regSWLon.val());

            if (self.usingWGS) {
                setRegionSWWGS(lat, lon);
            } else {
                setRegionSWXY(lat, lon);
            }
        },

        changedNE: function(){
			var self = this;

            var lat = parseFloat(self.ui.regNELat.val());
            var lon = parseFloat(self.ui.regNELon.val());

            if (self.usingWGS) {
                setRegionNEWGS(lat, lon);
            } else {
                setRegionNEXY(lat, lon);
            }
        },

        fetchUsers: function(){
			var self = this;

			$.getJSON( '/users', function( users ) {

    			var userData = [];
    			userData.push({ label: 'Синхронизация', value: 0 });
    			userData.push({ label: 'Я', value: self.self_id });

    			users.forEach(function(entity) {
    				if (entity.id != self.self_id) {
    					userData.push({ label: entity.name, value: entity.id });
    				}
    			});

        		self.ui.searchUsers.multiselect('dataprovider', userData);
        	});
        },

        fetchBoards: function(){
			var self = this;

			$.getJSON( '/photo/bla', function( boards ) {

    			self.boardData = [];

    			boards.forEach(function(entity) {
        			self.boardData.push({ label: entity, value: entity });
    			});

        		self.ui.searchBoards.multiselect('dataprovider', self.boardData);
        	});
        },

        checkBoardExist: function(boardId) {
        	var self = this;

        	var existed = self.boardData.filter(function (entity) { 
        		return entity.value == boardId 
        	});

        	var curSelection = self.ui.searchBoards.val();
        	var newSelection = _.union( curSelection === null ? [] : curSelection, [ boardId ]);

        	if (existed.length === 0) {
        		self.boardData.push({ label: boardId, value: boardId });
        		self.ui.searchBoards.multiselect('dataprovider', self.boardData);
        	}
        	
        	self.ui.searchBoards.multiselect('select', newSelection, false);
        },

        fetchPhotos: function(){
			var self = this;
            self.collection.fetch(self.getQueryObject());
        },

        getQueryObject: function() {
        	// constructing query string for /photo/:src/query method
        	var self = this;

            var checkDate = self.ui.dateDropDown.is('.checked');
            var checkRegion = self.ui.regionDropDown.is('.checked');

            var query = {}

        	if (checkDate) {
                query.dateStart = $(self.ui.datepicker[0]).find('input').val().replace(/\//g, '-');
                query.dateEnd = $(self.ui.datepicker[1]).find('input').val().replace(/\//g, '-');
            }

            if (checkRegion) {

                query.region = {

                    sw: self.usingWGS ? { lat: self.ui.regSWLat.val(), lon: self.ui.regSWLon.val() }
                                      : {   x: self.ui.regSWLat.val(),   y: self.ui.regSWLon.val() },

                    ne: self.usingWGS ? { lat: self.ui.regNELat.val(), lon: self.ui.regNELon.val() }
                                      : {   x: self.ui.regNELat.val(),   y: self.ui.regNELon.val() }
                
                }
            }

            if (self.ui.searchUsers.val()) {
        		query.users = self.ui.searchUsers.val();
        	}

        	if (self.ui.searchBoards.val()) {
        		query.albums = self.ui.searchBoards.val();
        	}

        	return query;
        },

        dateCancel: function() {
            this.ui.dateDropDown.removeClass('checked');
            this.ui.dateValue.text('Не задана');
        },

        dateOK: function() {
            var self = this;

            self.ui.dateDropDown.addClass('checked');

            var dateStart = $(self.ui.datepicker[0]).find('input').val();
            var dateEnd = $(self.ui.datepicker[1]).find('input').val();

            self.ui.dateValue.text(dateStart + ' - ' + dateEnd);
        },

        regionCancel: function() {
            this.ui.regionDropDown.removeClass('checked');
            this.ui.regionValue.text('Не задан');

            app.vent.trigger("search:cancel-region");
        },

        regionOK: function() {
            var self = this;

            self.ui.regionDropDown.addClass('checked');
            
            self.ui.regionValue.text(
                '{' + self.region.sw.lat.toFixed(5) + ',' + self.region.sw.lon.toFixed(5) + '}, {' + 
                      self.region.ne.lat.toFixed(5) + ',' + self.region.ne.lon.toFixed(5) + '}'
            );

            app.vent.trigger("search:cancel-region");
            app.vent.trigger("search:set-region", self.region);
        },

        regionFromMap: function(e) {
            var self = this;

            e.stopPropagation();
            e.stopImmediatePropagation();

            app.vent.trigger("search:open-map");
            app.vent.trigger("search:select-region");
        },

        setRegionPoint: function(point) {
            var self = this;

            // always in WGS
            if (self.isaboutToSetFirstRegionPoint) {
                self.setRegionSWWGS(point.lat(), point.lng());
            } else {
                self.setRegionNEWGS(point.lat(), point.lng());
		        app.vent.trigger("search:set-region", self.region);
            }

            self.updateUsingWGS(self.usingWGS);

            self.isaboutToSetFirstRegionPoint = !self.isaboutToSetFirstRegionPoint;
        },

        setRegionSWWGS: function(lat, lon)
        {
            var self = this;

            self.region.sw.lat = lat;
            self.region.sw.lon = lon;
        },

        setRegionSWXY: function(x, y)
        {
            var self = this;

            app.geo.XYtoWGS(x, y, function(wgs) {
                self.region.ne = wgs;
            });
        },

        setRegionNEWGS: function(lat, lon)
        {
            var self = this;

            self.region.ne.lat = lat;
            self.region.ne.lon = lon;
        },

        setRegionNEXY: function(x, y)
        {
            var self = this;

            app.geo.XYtoWGS(x, y, function(wgs) {
                self.region.ne = wgs;
            });
        },

        updateUsingWGS: function(value) {
            var self = this;

            self.usingWGS = value;

            // отобразить данные в нужном формате

            if (self.usingWGS) {
                self.ui.regSWLat.val(self.region.sw.lat.toFixed(5));
                self.ui.regSWLon.val(self.region.sw.lon.toFixed(5));
                self.ui.regNELat.val(self.region.ne.lat.toFixed(5));
                self.ui.regNELon.val(self.region.ne.lon.toFixed(5));

            } else {
                app.geo.WGStoXY(self.region.sw.lat, self.region.sw.lon, function(sw) {
                    self.ui.regSWLat.val(sw.x.toFixed(0));
                    self.ui.regSWLon.val(sw.y.toFixed(0));
                });

                app.geo.WGStoXY(self.region.ne.lat, self.region.ne.lon, function(ne) {
                    self.ui.regNELat.val(ne.x.toFixed(0));
                    self.ui.regNELon.val(ne.y.toFixed(0));
                });
            }
        }
    });
});
