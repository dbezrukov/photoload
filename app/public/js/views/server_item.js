define([
	'underscore',
    'marionette',
    'views/admin_server_editor',
    'tpl!templates/server_item.tmpl',
    'css!stylesheets/server_item_style',
], function(_, Marionette, ServerEditor, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',

        ui: {
        },

        events: {
            'click a.sync': 'syncServer',
            'click a.edit': 'editServer',
            'click a.delete': 'deleteServer'
        },

        modelEvents: {
        	'change': 'render'
        },

        templateHelpers: {
            dateString: function() {
            	function formatDate(date) {
					return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
				}
                return formatDate(new Date(this.utc * 1000));
            }
        },

        initialize: function(){
            var self = this;

            //self.model.on('change:online', self._onOnlineChanged, self);
        },

        syncServer: function() {
        	var self = this;

        	self.model.startSync();

        }, 

        editServer: function() {
        	var self = this;

			var serverEditor = new ServerEditor({
				model: self.model
			});
            
            app.getRegion('modal').show(serverEditor); 
    
        }, 

		deleteServer: function(){
            this.model.destroy();
        },

        onRender: function() {
        }
    });
});