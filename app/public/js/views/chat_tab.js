define([
    'marionette',
    'tpl!templates/chat_tab.tmpl',
    'css!stylesheets/chat_tab_style'
], function(Marionette, template) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: "li",

        initialize: function(){
            app.vent.on("chat:unread", this.onChatUnread, this);
            app.vent.on("chat:opened", this.onChatOpened, this);
        },
  
        onChatUnread: function(chatId) {
            var me = this;
            if (me.model.get('chatId') === chatId) {
                me.$('.tabUnread').css('display', 'inline-block')
            }
        }, 

        onChatOpened: function(chatId) {
            var me = this;
            if (me.model.get('chatId') === chatId) {
                me.$('.tabUnread').css('display', 'none')
            }
        }
    });
});
