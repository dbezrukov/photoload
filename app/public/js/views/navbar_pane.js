define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/navbar_pane.tmpl',
    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!stylesheets/navbar_pane_style',
    'bootstrap',
    'css!stylesheets/bootstrap-common',
    'bootstrap-multiselect',
    'css!stylesheets/bootstrap-multiselect',
    'css!stylesheets/main'

], function($, _, Marionette, templatePanel) {

    return Marionette.ItemView.extend({
        template: templatePanel,
        tagName: 'container',
        
        ui: {
        	dropdownMenu: '.dropdown-menu'
        },

        events: {
        },       

        onRender: function() {
        	var self = this;

        	var adminMenu = '<li><a href="/admin"><span class="glyphicon glyphicon-cog"></span> Администрирование</a></li>';

        	if (app.account.get('is_admin')) {
        		$(self.ui.dropdownMenu).prepend(adminMenu);
        	}

        	// Google maps inside tabs requires resizing on tab change
        	$(this.el).find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  				var target = $(e.target).attr("href") // activated tab
  				app.vent.trigger("tab:change", target);
			});
        },

    });
});