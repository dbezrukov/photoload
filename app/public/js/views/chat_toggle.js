define([
    'marionette',
    'tpl!templates/chat_toggle.tmpl',
    'css!stylesheets/chat_toggle_style'
], function(Marionette, template) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: "label",
        className: "btn btn-default active",

        ui: {
            'number': '.numberCircle'
        },

        events: {
        },
  
        initialize: function(){
            this.chatsUnread = [];

            app.vent.on("chat:unread", this.onChatUnread, this);
            app.vent.on("chat:opened", this.onChatOpened, this);
        },

        onChatUnread: function(chatId) {
            var me = this;

            var index = this.chatsUnread.indexOf(chatId);
            if (index === -1) {
                me.chatsUnread.push(chatId);
            }

            this.ui.number.text(this.chatsUnread.length);
            this.ui.number.show();
        }, 

        onChatOpened: function(chatId) {
            var me = this;

            var index = this.chatsUnread.indexOf(chatId);
            if (index > -1) {
                me.chatsUnread.splice(index, 1);
                me.ui.number.text(me.chatsUnread.length);
            }

            if (this.chatsUnread.length === 0) {
                me.ui.number.hide();
            }
        }
    });
});
