define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'views/gm_map_pane',
    'tpl!templates/photo_pane.tmpl',
    'css!stylesheets/photo_pane_style',
    'css!bower_components/Ionicons/css/ionicons.min.css'
], function($, _, _str, Marionette, MapPane, templatePanel) {

    return Marionette.CompositeView.extend({
        template: templatePanel,
        itemView: '',
        itemViewContainer: '.photos',
        tagName: "container-fluid",
        className: "photoPane",

        ui: {
        	'photos': ".photos",
        	'search_wrapper': '.search-wrapper',
        	'fileDelete': '#files-delete',
        	'map_pane': '#view-map'
        },

        events: {
            'click #files-upload': 'onUploadPhoto',
            'click #files-download': 'onDownloadPhoto',
			'click #map-toggle': 'onToggleMap',
			'click #files-select-all': 'onSelectAll',
    		'click #files-delete': 'onDelete'
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

            self.itemView = self.options.itemView;
   
            app.vent.on(self.options.itemViewClassName + ':open', self.onOpenPhoto, self);
            app.vent.on(self.options.itemViewClassName + ':highlight', self.onHighlightPhoto, self);
            app.vent.on('search:open-map', self.onShowMap, self);
        },

        onRender: function() {
        	var self = this;
            
            self.addSearchControl();
            self.addMapPane();
            
            self.ui.photos.scroll(self.loadMorePhotos.bind(this));
		}, 

		addSearchControl: function() {
        	var self = this;

            self.collection.on('reset', function() {
            	self.checkCanDelete();
			});

			self.collection.on('nomore', function() {
			});

			self.collection.on('hasmore', function() {
			});

			self.collection.on('add', function() {
			});

			self.collection.on('remove', function() {
				self.checkCanDelete();
			});

			self.collection.on('change:selected', function() {
				self.checkCanDelete();
			});

            self.searchControl = new self.options.searchControl({
                collection: self.collection,
                self_id: self.model.get('id')
            });

            self.ui.search_wrapper.append(self.searchControl.render().el);
        },

        addMapPane: function() {
        	var self = this;

            self.mapPane = new MapPane({
            	collection: self.collection
            });

            this.ui.map_pane.append(self.mapPane.render().el);

            self.onShowMap();
        },

		onOpenPhoto: function(photo) {
			var self = this;

			if (!self.options.fileViewer) {
				console.log('no file viewer');
				return;
			}

			var photoViewer = new self.options.fileViewer({
				model: photo
			});
            
            app.getRegion('modal').show(photoViewer);
		},

		onHighlightPhoto: function(photo) {
			var self = this;

			self.onShowMap();
		},

		onUploadPhoto: function() {
			var self = this;

			if (!self.options.fileUploader) {
				console.log('no file uploader');
				return;
			}

			var photoUploader = new self.options.fileUploader({
				collection: self.collection,
				searchControl: self.searchControl
			});
            
            app.getRegion('modal').show(photoUploader); 
		},

		onDownloadPhoto: function() {
			var self = this;
			
			if (!self.options.fileDownloader) {
				console.log('no file downloader');
				return;
			}

			var fileDownloader = new self.options.fileDownloader({
				collection: self.collection,
				searchControl: self.searchControl,
				url: self.options.fileDownloaderUrl,
				filename: self.options.getFileDownloaderFilename()
			});
            
            app.getRegion('modal').show(fileDownloader); 
		},

		loadMorePhotos: function() {
			var self = this;
			var scrollHeight = $(document).height();
			var scrollPosition = $(window).height() + $(window).scrollTop();
			
			if ((scrollHeight - scrollPosition) / scrollHeight === 0) {

				self.fetchMore();
			}
		},

		onShowMap: function() {
			var self = this; 
			var display = $('.view-wrapper #view-map').css("display");

			if (display === 'none') {

				$('.view-wrapper #view-map').css('display','inline-block');
				$('.view-wrapper #view-list').addClass('compact');

				self.mapPane.redrawMap();
			}
		},

		onToggleMap: function() {
			var self = this; 
			var display = $('.view-wrapper #view-map').css("display");

			if (display === 'none') {

				$('.view-wrapper #view-map').css('display','inline-block');
				$('.view-wrapper #view-list').addClass('compact');

				self.mapPane.redrawMap();

			} else {

				$('.view-wrapper #view-map').css('display','none');
				$('.view-wrapper #view-list').removeClass('compact');
			}
		},

		checkCanDelete: function() {
			var self = this;
			var firstSelectedPhoto = self.collection.findWhere({ selected: true });

			if (firstSelectedPhoto) {
				self.ui.fileDelete.removeClass('disabled');
			} else {
				self.ui.fileDelete.addClass('disabled');
			}
		},

		onSelectAll: function() {
			var self = this;
			var firstSelectedPhoto = self.collection.findWhere({ selected: true });

			if (firstSelectedPhoto) {
				app.vent.trigger(self.options.itemViewClassName + ':deselect-all');
			} else {
				app.vent.trigger(self.options.itemViewClassName + ':select-all');
			}
		},
		
		onDelete: function() {
			var self = this;
			var selectedPhotos = this.collection.where({ selected: true });
			var message = "Выбрано фотографий: " + selectedPhotos.length + ". Удалить?";
			
			if (confirm(message)) {
				app.vent.trigger(self.options.itemViewClassName + ':delete-selected');
			}
		},

		fetchMore: function() {
			var self = this; 
			self.collection.fetchMore.call(self.collection);
		},
    });
});
