define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'modules/gm_maptype_builder',
    'tpl!templates/map_pane.tmpl',
    'css!stylesheets/gm_map_style',
    'bootstrap-multiselect'
], function($, _, _str, Marionette, MapTypeBulder, templatePanel) {

	USGSOverlay.prototype = new google.maps.OverlayView();

	/** @constructor */
	function USGSOverlay(bounds, image, map) {

	  // Initialize all properties.
	  this.bounds_ = bounds;
	  this.image_ = image;
	  this.map_ = map;

	  // Define a property to hold the image's div. We'll
	  // actually create this div upon receipt of the onAdd()
	  // method so we'll leave it null for now.
	  this.div_ = null;

	  // Explicitly call setMap on this overlay.
	  this.setMap(map);
	}

	/**
	 * onAdd is called when the map's panes are ready and the overlay has been
	 * added to the map.
	 */
	USGSOverlay.prototype.onAdd = function() {

	  var div = document.createElement('div');
	  div.style.borderStyle = 'none';
	  div.style.borderWidth = '0px';
	  div.style.position = 'absolute';

	  // Create the img element and attach it to the div.
	  var img = document.createElement('img');
	  img.src = this.image_;
	  img.style.width = '100%';
	  img.style.height = '100%';
	  img.style.position = 'absolute';
	  img.style.opacity = '0.5';
	  div.appendChild(img);

	  this.div_ = div;

	  // Add the element to the "overlayLayer" pane.
	  var panes = this.getPanes();
	  panes.overlayLayer.appendChild(div);
	};

	USGSOverlay.prototype.draw = function() {

	  // We use the south-west and north-east
	  // coordinates of the overlay to peg it to the correct position and size.
	  // To do this, we need to retrieve the projection from the overlay.
	  var overlayProjection = this.getProjection();

	  // Retrieve the south-west and north-east coordinates of this overlay
	  // in LatLngs and convert them to pixel coordinates.
	  // We'll use these coordinates to resize the div.
	  var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
	  var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

	  // Resize the image's div to fit the indicated dimensions.
	  var div = this.div_;
	  div.style.left = sw.x + 'px';
	  div.style.top = ne.y + 'px';
	  div.style.width = (ne.x - sw.x) + 'px';
	  div.style.height = (sw.y - ne.y) + 'px';
	};

	// The onRemove() method will be called automatically from the API if
	// we ever set the overlay's map property to 'null'.
	USGSOverlay.prototype.onRemove = function() {
	  this.div_.parentNode.removeChild(this.div_);
	  this.div_ = null;
	};

    var map, select, mapTypeBuilder;
    var markers = []; /// replace with iterating colleciton models

    return Marionette.ItemView.extend({
        template: templatePanel,
        tagName: "container",
        ui: {
            "map": "#map",
            "coords": "#coords #text",
        },
        
        initialize: function(options) {
        	var self = this;

            options || (options = {});
            this.options = options;

            app.vent.on('tile:open', self.onOpenTile, self);
            app.vent.on('search:select-region', self.onSelectRegion, self);
            app.vent.on('search:set-region',    self.onSetRegion, self);
            app.vent.on('search:cancel-region', self.onCancelRegion, self);
            app.vent.on('tab:change', self.onTabChanged, self);
            
            app.vent.on('photo-item:highlight', self.onHighlightPhoto, self);
            app.vent.on('photo-item:selected', self.onShowOverlay, self);
            app.vent.on('photo-item:deselected', self.onHideOverlay, self);

            self.collection.on('reset', function() {
                var self = this;

                for (var i = 0, marker; marker = markers[i]; i++) {
                    
					if (marker.overlay) {
						marker.overlay.setMap(null);
					}

                    marker.rectangle.setMap(null);
                    marker.setMap(null);
                }
            });

            self.collection.on('add', function(photo) {
                if (!self.options.map || !photo.getBBox) {
                	return;
                }

                photo.getBBox(function(err, bbox) {
					if (err) {
						return;
					}

					var pos = new google.maps.LatLng(photo.get('lat'), photo.get('lon'));
					//var pos = new google.maps.LatLng(59.6320, 29.2144);

					var bounds = new google.maps.LatLngBounds(
                  		new google.maps.LatLng(bbox.sw.lat, bbox.sw.lon), 
                  		new google.maps.LatLng(bbox.ne.lat, bbox.ne.lon));

					var rectangle = new google.maps.Rectangle({
		                strokeColor: '#E26161',
		                strokeOpacity: 0.9,
		                strokeWeight: 1,
		                clickable: false,
		                fillColor: 'transparent',
		                fillOpacity: 0.35,
		                map: self.options.map,
		                bounds: bounds
        			});

					photo.marker = new google.maps.Marker({
                    	//icon: getIcon(),
                    	map: self.options.map,
                    	title: photo.get('title') ? photo.get('title') : '',
                    	position: pos,
                    	model: photo,
                    	rectangle: rectangle
                	});

                	google.maps.event.addListener(photo.marker, "click", function () {
                		var self = this;

                    	app.vent.trigger('photo-item' + (self.model.get('selected') ? ':deselect' : ':select'), self.model);
                	});

                	google.maps.event.addListener(photo.marker, "dblclick", function () {
                    	app.vent.trigger("photo-item:open", this.model);
                	});

                	markers.push(photo.marker);
				})

				function getIcon() {
                    /*var svg = 'M609.208,298.575L313.306,3.815c-3.987-3.988-10.445-3.988-14.433,0L2.961,298.575c-3.988,3.988-3.907,10.353,0,14.393 \
                        c1.999,2.06,4.61,3.11,7.201,3.131v0.041h71.441v284.865c0,2.815,1.142,5.335,2.988,7.171c1.846,1.856,4.396,3.009,7.211,3.009 \
                        h428.401c5.63,0,10.2-4.569,10.2-10.18V316.14h71.899c2.673,0.051,5.191-1,6.905-3.162 \
                        C612.676,308.541,613.197,302.563,609.208,298.575z M408.003,417.926h-91.8v91.524c0,5.6-4.569,10.18-10.201,10.18 \
                        c-5.63,0-10.2-4.57-10.2-10.18v-91.524h-91.8c-5.63,0-10.2-4.56-10.2-10.18c0-5.601,4.57-10.159,10.2-10.159h91.8v-91.505 \
                        c0-5.6,4.57-10.159,10.2-10.159c5.631,0,10.201,4.56,10.201,10.159v91.505h91.8c5.631,0,10.2,4.549,10.2,10.159 \
                        C418.204,413.376,413.634,417.926,408.003,417.926z';*/

                    var svg = 'M37,23c0-1.104-0.896-2-2-2h-6c-1.104,0-2,0.896-2,2v1h10V23z M44,26H20c-1.104,0-2,0.896-2,2v12c0,1.104,0.896,2,2,2 \
						h24c1.104,0,2-0.896,2-2V28C46,26.896,45.104,26,44,26z M32,40c-3.313,0-6-2.688-6-6c0-3.313,2.687-6,6-6c3.312,0,6,2.687,6,6 \
						C38,37.312,35.312,40,32,40z M43,30c-0.552,0-1-0.447-1-1c0-0.552,0.448-1,1-1c0.553,0,1,0.448,1,1C44,29.553,43.553,30,43,30z \
					 	M32,30c-2.209,0-4,1.791-4,4s1.791,4,4,4s4-1.791,4-4S34.209,30,32,30z M32,0C14.327,0,0,14.327,0,32c0,17.674,14.327,32,32,32 \
						s32-14.326,32-32C64,14.327,49.673,0,32,0z M48,40c0,2.209-1.791,4-4,4H20c-2.209,0-4-1.791-4-4V28c0-2.209,1.791-4,4-4h5v-1 \
						c0-2.209,1.791-4,4-4h6c2.209,0,4,1.791,4,4v1h5c2.209,0,4,1.791,4,4V40z';

                    return {
                        path: svg,
                        fillColor: '#E26161',
                        fillOpacity: 0.1,
                        strokeColor: 'red',
                        strokeWeight: 1,
                        strokeOpacity: 1,
                        rotation: 0,
                        scale: 0.5
                    }
                }
            });
        },

        onRender: function() {
            var self = this;

            var markers = [];

            var defaultMapType = window.is_offline ? "TMSOSM" : google.maps.MapTypeId.SATELLITE;
            
            var availableMapTypes = window.is_offline 

                ? [ "TMSOSM"  ] 

                : [ google.maps.MapTypeId.SATELLITE, 
                    google.maps.MapTypeId.HYBRID,
                    "OSM",
                    "Yandex" ];

            var mapOptions = {
                mapTypeId: defaultMapType,
                mapTypeControlOptions: {
                    mapTypeIds: availableMapTypes,
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
                }
            }

            map = new google.maps.Map(self.ui.map[0], mapOptions);
            
            // passing map to Layer Builder
            self.options.map = map;

            mapTypeBuilder = new MapTypeBulder(self.options);

            map.mapTypes.set("TMSOSM", mapTypeBuilder.constructTMSOSMMap());
            //map.mapTypes.set("TMSWorld", mapTypeBuilder.constructTMSWorldMap());

		    var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(61.0, 29.0),
                new google.maps.LatLng(59.0, 31.0)
            );

       	    map.fitBounds(defaultBounds);

            google.maps.event.addListener(map, 'mousemove', function(event) {
                var lat = parseFloat(event.latLng.lat());
                var lng = parseFloat(event.latLng.lng());

                app.geo.WGStoXY(lat, lng, function(xy) {
                    var coordWGS = _str.vsprintf("Ш: %s, Д: %s", [lat.toFixed(5), lng.toFixed(5)]);
                    //var coordXY = _str.vsprintf("X: %s, Y: %s", [xy.x.toFixed(0), xy.y.toFixed(0)]);
                    
                    self.ui.coords.html(coordWGS/* + '<br>' + coordXY*/);
                })

            });

            google.maps.event.addListener(map, "click", function(event) {
                
                if (self.selectingRegion) {
                    app.vent.trigger("map:region-point", event.latLng);
                }
            });
        }, 

        redrawMap: function() {
            var self = this;

            google.maps.event.trigger(self.options.map, 'resize');

            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(61.0, 29.0),
                new google.maps.LatLng(59.0, 31.0));

            self.options.map.fitBounds(defaultBounds);


            // tile clicked
            // map.layer...
        },         

        onOpenTile: function(tile) {
        	var self = this;

        	// tile clicked
            // map.layer...
        },

        onSelectRegion: function() {
            var self = this;

            self.options.map.setOptions({ draggableCursor: 'crosshair' });
            
            if (self.rectangle) {
                self.rectangle.setMap(null);
            }

            self.selectingRegion = true;
        },

        onSetRegion: function(region) {
            var self = this;

            console.log('Setting region');

            if (self.rectangle) {
                self.rectangle.setMap(null);
            }

            self.rectangle = new google.maps.Rectangle({
                strokeColor: '#E26161',
                strokeOpacity: 0.9,
                strokeWeight: 3,
                clickable: false,
                fillColor: 'transparent',
                fillOpacity: 0.35,
                map: self.options.map,
                bounds: new google.maps.LatLngBounds(
                  new google.maps.LatLng(region.sw.lat, region.sw.lon), 
                  new google.maps.LatLng(region.ne.lat, region.ne.lon))
            });
        },

        onCancelRegion: function() {
            var self = this;

            console.log('Cancelling region');
            self.options.map.setOptions({ draggableCursor: 'default' });

            if (self.rectangle) {
	        	self.rectangle.setMap(null);
	    	}

	    	self.selectingRegion = false;
        },

        onTabChanged: function() {
            var self = this;

            google.maps.event.trigger(map, 'resize');
        },

        onHighlightPhoto: function(photo) {
			var self = this;

    		self.options.map.panTo(photo.marker.getPosition());  
		},

		onShowOverlay: function(photo) {
			var self = this;

    		if (!photo.marker.overlay) {
    			var url = 'photo/bla/' + photo.get('album') + '/' + photo.get('id') + '/rotated';
    			photo.marker.overlay = new USGSOverlay(photo.marker.rectangle.bounds, url, self.options.map);
    		}
		},

		onHideOverlay: function(photo) {
			var self = this;

    		if (photo.marker.overlay) {
				photo.marker.overlay.setMap(null);
				delete photo.marker.overlay;
			}
		},
    });
});

