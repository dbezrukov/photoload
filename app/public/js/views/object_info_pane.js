define([
    'marionette',
    'tpl!templates/object_info.tmpl',
    'css!stylesheets/object_info_style'
], function(Marionette, template) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "object-info unselectable",

        ui: {
            'preview': '.video-preview',
            'objectVideo': '.object-video',
            'dataFields': '.object-data-fields'
        },
        events: {
            'click .object-video': '_onVideoClicked',
            'dblclick .video-preview': '_onPreviewDblClicked'
            // 'click .object-photo': '_onPhotoClicked',
        },

        modelEvents: {
            'change': 'updateDataFields'
        },
  
        initialize: function(){
        },

        onRender: function() {
            this.updateDataFields();
        },

        updateDataFields: function() {
            var self = this;
            $(self.ui.dataFields).html(self.model.get('content'));
        },

        onOpened: function() {
            var self = this;
            self.checkVideo();
            self.timer = setInterval(self.checkVideo.bind(self), 2000);
        },

        checkVideo: function() {
            var self = this;

            var url = '/video/board/' + self.model.get('id') + '/check-video';

            $.get(url, function(data) {
                if (data.hasVideo === true) {
                    self.ui.objectVideo.attr('hasVideo', 'true');
                } else {
                    self.ui.objectVideo.removeAttr('hasVideo');
                    
                    if (self.hasVideo === true) {
                        self._hideVideo();
                    }
                }
                self.hasVideo = data.hasVideo;
            }, 'json')
            .fail(function() {
                if (self.hasVideo === true) {
                    self._hideVideo();
                }
                self.hasVideo = false;
            });
        },

        onClosed: function() {
            var self = this;

            console.log('onClosed');
            clearInterval(this.timer);

            self.ui.objectVideo.removeAttr('hasVideo');
            self._hideVideo();
        },

        _onVideoClicked: function(event){
            var self = this;

            if (!self.ui.preview.attr('src') || self.ui.preview.attr('src') === '') {
                self.ui.preview.attr('src', '/video/board/' + self.model.get('id') + '/stream?fps=20');
                self.ui.preview.show();
                self.trigger('resizeRequest');
            } else {
                self._hideVideo();
            }
        },

        _onPhotoClicked: function(event){
            this.ui.preview.attr('src', '/video/board/' + this.model.get('id') + '/stream?fps=20');
        },

        _onPreviewDblClicked: function(event){
            window.open(window.location.href + 'video/board/' + this.model.get('id'), '_blank');
        },

        _hideVideo: function() {
            this.ui.preview.attr('src', '');
            this.ui.preview.hide();
            this.trigger('resizeRequest');
        }
    });
});
