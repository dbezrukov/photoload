define([
    'jquery',
    'underscore',
    'marionette',
    'models/server',
    'views/server_item',
    'views/admin_server_editor',
    'tpl!templates/admin_sync_pane.tmpl',
    'css!stylesheets/admin_sync_pane_style',
], function($, _, Marionette, Server, ServerItem, ServerEditor, templatePanel) {

    return Marionette.CompositeView.extend({
        template: templatePanel,
        itemView: ServerItem,
        itemViewContainer: '.servers',
        tagName: "div",
        className: "syncPane",

        ui: {
        	'syncInterval': '#syncInterval',
        	'localAddress': '#localAddress'
        },

        events: {
        	'click #btnNewServer': 'onNewServer',
        	'click #btnSubmitInterval': 'onSubmitInterval',
        },

        modelEvents: {
			"change": "render"
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

           	self.collection.fetch();
          	self.model.fetch();
        },

        onRender: function() {
        	var self = this;
		}, 

		onNewServer: function() {

			var self = this;

			var serverEditor = new ServerEditor({
				model: new Server,
				collection: self.collection
			});
            
            app.getRegion('modal').show(serverEditor); 
    
		    return false;    
		},

		onSubmitInterval: function() {

			var self = this;

			var val = self.ui.syncInterval.val();
			var address = self.ui.localAddress.val();

			self.model.save({
				interval: parseInt(val),
				localAddress: address
			});
    
		    return false;    
		}
    });
});