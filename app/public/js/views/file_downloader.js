define([
    'underscore',
    'underscore.string',
    'marionette',
    'tpl!templates/file_downloader.tmpl'
], function(_, _str, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "modal-dialog",
        id: 'fileDownloader',

        ui: {
        	'btnCreate': '#btnCreate',
        	'fileCount': '#fileCount',
        	'progress': '#progress',
    		'downloadLink': '#downloadLink'
        },

        events: {
        	'click #btnCreate': 'createArchive'
        },

        initialize: function(options){
            var self = this;

            options || (options = {});
            self.options = options;

            self.searchControl = self.options.searchControl;
        },

        onRender: function() {
            var self = this;

            self.ui.progress.attr('style', 'width:0%');
        	self.ui.downloadLink.text('');
        	self.ui.downloadLink.prop('download', self.options.filename);

            var selectedFiles = self.options.collection.where({ selected: true });
       		self.ui.fileCount.val(selectedFiles.length);

       		if (selectedFiles.length > 0) {
       			self.ui.btnCreate.removeClass('disabled');
       		} else {
      			self.ui.btnCreate.addClass('disabled');
       		}
        },

        onShow: function(){
        },

        createArchive: function() {
        	var self = this;

			var query = self.searchControl.getQueryObject();
			console.dir(query);

			query.offset = 0; 
			query.count = 100000; 

			var query_string = $.param(query);

			self.ui.downloadLink.text('Архив создается..');

			function createArchiveFromFiles(files) {
				$.ajax({
					type: "POST",
        			url: self.options.url,
    				data: JSON.stringify(files),
  					dataType: 'json',
  					processData: true,
					contentType: 'application/json',
    				success: function(data){
    					self.ui.downloadLink.attr('href', window.location + self.options.url.slice(1) + '/' + data.id);
    					self.ui.downloadLink.text(window.location + self.options.url.slice(1) + '/' + data.id);
    				},
    				error: function(xhr){
    				}
				});
			}

			var selectedFiles = self.options.collection.where({ selected: true });
			if (selectedFiles.length > 0) {

				console.log('archieving ' + selectedFiles.length + ' files');
				createArchiveFromFiles(selectedFiles);
			}
		}
    });
});
