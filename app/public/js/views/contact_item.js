define([
    'marionette',
    'tpl!templates/contact_item.tmpl',
    'css!stylesheets/contact_item_style'
], function(Marionette, template) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: "li",

        ui: {
        },

        events: {
            'click span': 'openChat'
        },
  
        initialize: function(){
            var self = this;

            app.vent.on("chat:unread", self.onChatUnread, self);
            app.vent.on("chat:opened", self.onChatOpened, self);

            self.model.on('change:online', self._onOnlineChanged, self);

            var isGroup = self.model.get('isGroup');
            if (isGroup) {
                self.chatId = 'group_' + self.model.get('id');
            } else {
                self.chatId = 'chat_' + Math.min(app.account.get('id'), self.model.get('id')) 
                              + '_' + Math.max(app.account.get('id'), self.model.get('id'));
            }
        },

        onRender: function() {
            $(this.el).find('.contactOnline').attr('online', this.model.get('online'));
        },
  
        _onOnlineChanged: function(model, online) {
            $(this.el).find('.contactOnline').attr('online', online);
        },

        openChat: function(){
            app.vent.trigger("contacts:chat", this.model);
        },

        onChatUnread: function(chatId) {
            var me = this;
            if (me.chatId === chatId) {
                me.$('.contactUnread').css('display', 'inline-block');
            }
        }, 

        onChatOpened: function(chatId) {
            var me = this;
            if (me.chatId === chatId) {
                me.$('.contactUnread').css('display', 'none');
            }
        }
    });
});
