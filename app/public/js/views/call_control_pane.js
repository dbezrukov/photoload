define([
    'marionette',
    'tpl!templates/call_control.tmpl',
    'css!stylesheets/call_control_style'
], function(Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "callControlPane",

        events: {
            'click .contactCall'      : '_onContactCall',
            'click .contactVideoCall' : '_onContactVideoCall',
            'click .contactStopCall'  : '_onContactStopCall',
            'click .contactAnswerCall': '_onContactAnswerCall',
            'click .contactRejectCall': '_onContactRejectCall'
        },

        initialize: function(){
            var self = this;

            self._onCallStateChanged(self.model, self.model.get('callState'));
            self._onOnlineChanged(self.model, self.model.get('online'));
            
            self.model.on('change:callState', self._onCallStateChanged, self);
            self.model.on('change:online', self._onOnlineChanged, self);

            app.vent.on('transport:connected', self._onConnect, self);
            app.vent.on('transport:disconnected', self._onDisconnect, self);
        },

        _onCallStateChanged: function(model, callState) {
            $(this.el).attr('callState', callState);
        },

        _onOnlineChanged: function(model, online) {
            $(this.el).attr('callState', online === true ? 'drop' : 'offline' );
        },

        _onContactCall: function(event){
            event.preventDefault();
            this.model.call(false);
        },

        _onContactVideoCall: function(event){
            event.preventDefault();
            this.model.call(true);
        },

        _onContactStopCall: function(event){
            event.preventDefault();
            this.model.stopCall();
        },

        _onContactAnswerCall: function(event){
            event.preventDefault();
            this.model.answerCall();
        },

        _onContactRejectCall: function(event){
            event.preventDefault();
            this.model.rejectCall();
        },

        _onConnect: function() {
            this._onCallStateChanged(self.model, 'drop');
        },

        _onDisconnect: function() {
            this._onCallStateChanged(self.model, 'offline');
        },
    });
});




        
