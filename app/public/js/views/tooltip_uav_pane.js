define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'tpl!templates/tooltip_uav_pane.tmpl',
    'css!stylesheets/tooltip_uav_pane_style',
], function($, _, _str, Marionette, templatePanel) {

    var tooltipPane = Marionette.ItemView.extend({
        template: templatePanel,
        tagName: "container",
        ui: {
        	intervalSelectorTitle: '#intervalSelectorTitle',
        	intervalSelectorMenu: '.dropdown-menu'
        },
        events: {
        	'click #intervalSelector li a': 'trackIntervalChanged'
        },

		templateHelpers: {
            photoHelper: function() {
                return this.photo ? window.user.photostorage + '/w_100,h_100,c_scale' + this.photo : '';
            }
        },

        initialize: function(options) {
        	var self = this;

            options || (options = {});
            self.options = options;
        },

		onRender: function() {
			var self = this;

            var track = self.model.get('track') 
						? self.model.get('track')
						: { interval: 'no', epsilon: 0.0001 };

			var title = self.ui.intervalSelectorMenu.find('li a[value=' + track.interval + ']').text();
			self.ui.intervalSelectorTitle.html(title);
        },

		trackIntervalChanged: function(event) {
			if (this.model) {
				this.model.set('track', {
					interval: event.target.getAttribute('value'),
					epsilon: ''
				});
			}
		}
    });
    
    return tooltipPane;
});