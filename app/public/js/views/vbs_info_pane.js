define([
    'underscore',
    'marionette',
    'tpl!templates/vbs_info.tmpl',
    'tpl!templates/vbs_info_fields.tmpl',
    'css!stylesheets/vbs_info_style'
], function(_, Marionette, template, templateFields) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "vbs-info unselectable",

        ui: {
            'preview': '.photo-preview',
            'container': '.photo-container',
            'objectPhoto': '.object-photo',
            'dataFields': '.vbs-data-fields'
        },
        events: {
             'click .object-photo': '_onPhotoClicked',
             
             'click .photo-fulscreen': '_onFullscreenClicked',
             'click .photo-preview': '_onPreviewClicked'
        },

        modelEvents: {
            'change': 'updateDataFields'
        },

        initialize: function() {
            var self = this;

            self.currentPhoto = 0;
        },

        onRender: function() {
            this.updateDataFields();
        },

        updateDataFields: function() {
            function prettyMonth(date) {
                var months = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
                return date.getUTCDate() + ' ' + months[date.getUTCMonth()];
            }

            function checkTime(i) {
                if (i<10) {
                    i="0" + i;
                }
                return i;
            }

            function formatDate(date) {
                return prettyMonth(date)
                    + ' ' + checkTime(date.getHours())
                    + ':' + checkTime(date.getMinutes())
                    + ':' + checkTime(date.getSeconds())
            }

            var self = this;
            var dataExt = self.model.get('dataExt');

            var context = {
                data : self.model.get('data'),
                readableDate: formatDate(new Date(this.model.get('data').timestamp)),
                
                imsi: (dataExt && dataExt.imsi) ? dataExt.imsi : 'нет данных',
                imei: (dataExt && dataExt.imei) ? dataExt.imei : '-',
                fio:  (dataExt && dataExt.fio ) ? dataExt.fio  : '-',
                passport_no:   (dataExt && dataExt.passport_no)   ? dataExt.passport_no   : '-',
                passport_who:  (dataExt && dataExt.passport_who)  ? dataExt.passport_who  : '-',
                passport_when: (dataExt && dataExt.passport_when) ? dataExt.passport_when : '-'
            }

            $(self.ui.dataFields).html(templateFields(context));
        },

        onOpened: function() {
            var self = this;
            self.getExtData();
        },

        getExtData: function() {
            var self = this;

            var url = '/profiles/vbs/' + self.model.get('data').id;
            $.get(url, function(dataExt) {
                self.model.set('dataExt', dataExt);

                var hasPhoto = dataExt.photo ? dataExt.photo.length > 0 : false;

                if (hasPhoto === true) {
                    self.ui.objectPhoto.attr('hasPhoto', 'true');
                } else {
                    self.ui.objectPhoto.removeAttr('hasPhoto');
                    if (self.hasPhoto === true) {
                        self._hidePhoto();
                    }
                }
                self.hasPhoto = hasPhoto;
            })
            .fail(function() {
                if (self.hasPhoto === true) {
                    self._hidePhoto();
                }
                self.hasPhoto = false;
            });
        },

        onClosed: function() {
            var self = this;

            self.ui.objectPhoto.removeAttr('hasPhoto');
            self.hasPhoto = false;
            self._hidePhoto();
        },

        _onPhotoClicked: function(event){
            var self = this;

            if (!self.ui.preview.attr('src') || self.ui.preview.attr('src') === '') {
                var path = '/profiles/vbs/' + self.model.get('data').id + '/photo/' + self.model.get('dataExt').photo[self.currentPhoto]
                self.ui.preview.attr('src', path);
                self.ui.container.show();
                self.trigger('resizeRequest');
            } else {
                self._hidePhoto();
            }
        },

        _onPreviewClicked: function(event){
            var self = this;

            self.currentPhoto++;

            var photoCount = self.model.get('dataExt').photo.length;
            if (self.currentPhoto >= photoCount) {
                self.currentPhoto = 0;
            }

            var path = '/profiles/vbs/' + self.model.get('data').id + '/photo/' + self.model.get('dataExt').photo[self.currentPhoto];
            self.ui.preview.attr('src', path);
        },

        _onFullscreenClicked: function(event){
            var self = this;

            var path = 'profiles/vbs/' + self.model.get('data').id + '/photo/' + self.model.get('dataExt').photo[self.currentPhoto];
            window.open(window.location.href + path, '_blank');
        },

        _hidePhoto: function() {
            this.ui.preview.attr('src', '');
            this.ui.container.hide();
            this.trigger('resizeRequest');
        }
    });
});

