define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'views/chat_item',
    'views/call_control_pane',
    'tpl!templates/chat_pane.tmpl',
    'css!stylesheets/chat_pane_style',
    'jquery-dragscrollable'
], function($, _, _str, Marionette, ChatItem, CallControl, templatePanel) {

    var chatPane = Marionette.CompositeView.extend({
        template: templatePanel,
        itemView: ChatItem,
        itemViewContainer: '.messages',
        tagName: "div",
        className: "chatPane",

        ui: {
            'inputMessage': ".inputMessage",
            'messages': ".messages"
        },
        collectionEvents: {
            'add': 'scrollToBottom'
        },
        events: {
            'keydown .inputMessage': 'onMessageKeyDown',
        },
        
        initialize: function(options) {
            options || (options = {});
            
            this.chatId = options.chatId;
            this.selfId = options.selfId;

            app.vent.on('transport:connected', this.getHistory.bind(this));
        },

        onRender: function() {
            var self = this;

            self.getHistory();

            self.callControl = new CallControl({
                model: self.model
            });

            $(self.el).prepend(self.callControl.render().el);
        },

        getHistory: function() {
            var me = this;

            app.vent.trigger('transport:sendMessage', 'getHistory', { 
                chatId: me.chatId, 
                selfId: me.selfId 
            });
        },

        scrollToBottom: function() {
            var lastLi = $('.messages li:last');
            var me = this;
            if (lastLi[0])  {
                me.ui.messages.scrollTop(lastLi.offset().top);
            }
        },

        onMessage: function(message) {
            this.collection.push(message);
        }, 

        onMessageKeyDown: function(e) {
            var me = this;
            if(e.keyCode === 13) {
                me.sendChatMessage();
                me.ui.inputMessage.focus();
            }
        },

        onHistory: function(history) {
            this.collection.reset(history);
        }, 

        sendChatMessage: function() {
            var me = this;

            var message = me.ui.inputMessage.val();

            // Prevent markup from being injected into the message
            message = function(input) {
                return $('<div/>').text(input).text();
            }(message);
        
            if (window.connected && window.connected === true) {
                me.ui.inputMessage.val('');

                app.vent.trigger('transport:sendMessage', 'chatMessage', { 
                    chatId: me.chatId, 
                    message: {
                        authorId: me.selfId,
                        text: message
                    } 
                });
            }
        }
    });

    return chatPane;
});
