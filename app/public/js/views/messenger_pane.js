define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'views/contacts_pane',
    'views/chat_tab',
    'views/chat_pane',
    'models/contact',
    'collections/contacts',
    'tpl!templates/messenger_pane.tmpl',
    'css!stylesheets/messenger_style',
    'css!stylesheets/video_style',
    'jquery-dragscrollable'
], function($, _, _str, Marionette, ContactsPane,  
			ChatTab, ChatPane, Contact, Contacts, templatePanel) {
    /* Read/unread logic
       chat:opened
       chat:unread

       chatButton:
            chatUnread(chatId)
            chatClosed(chatId)
     */

    var messengerPane = Marionette.ItemView.extend({
        template: templatePanel,
        tagName: "container",
        ui: {
            'messengerTab': '#messengerTab',
            'tabContent': '.tab-content'
        },
        events: {
            'click .closeTab': 'closeClicked',
        },        
        initialize: function(options) {
            options || (options = {});
            this.options = options;

            app.vent.on('contacts:chat', this.addChat, this);

            app.vent.on('transport:connected', this.onConnect, this);

            app.vent.on('transport:disconnected', this.onDisconnect, this);

            app.vent.on('transport:loggedIn', this.onLoggedIn, this);

            app.vent.on('transport:chatMessage', this.onMessage, this);
            app.vent.on('transport:chatHistory', this.onHistory, this);

            app.vent.on('transport:userOnline', this.onUserOnline, this);
            app.vent.on('transport:userOffline', this.onUserOffline, this);

            this.chatPanes = {};
        },

        onConnect: function() {
            this.ui.messengerTab.find('.glyphicon').removeClass('offline');
            //this.ui.messengerTab.find('.glyphicon').attr('title','Контакты');

            this.ui.messengerTab.on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                var chatIdShown = $(e.target).attr('href').substr(1);
                app.vent.trigger("chat:opened", chatIdShown);
            });

            this.login();
        },

        onDisconnect: function() {
            this.ui.messengerTab.find('.glyphicon').addClass('offline');
            //this.ui.messengerTab.find('.glyphicon').attr('title','Контакты (нет сети)');
        },

        onRender: function() {
            this.addContactPane();
        },

        login: function() {
            app.vent.trigger('transport:sendMessage', 'login');
        },

        closeClicked: function(event) {
            var me = this;

            var chatId = $(event.target).parent().attr("href").substr(1);
            $(event.target).parent().parent().remove();

            me.closeChat(chatId);

            if (chatId in me.chatPanes) {
                delete me.chatPanes[chatId];
            }
        }, 

        openChat: function(chatId) {
            var query = _str.sprintf('a[href$=#%s]', chatId);
            this.ui.messengerTab.find(query).tab('show');
        },

        closeChat: function(chatId) {
            var lastChatId = this.ui.messengerTab.find('a:last').attr('href').substr(1);
            $('#' + chatId).remove();
            this.openChat(lastChatId);
        },

        isChatOpened: function(chatId) {
            var query = _str.sprintf('a[href$=#%s]', chatId);
            var chatTab = this.ui.messengerTab.find(query);
            if (chatTab.length > 0) {
                if (chatTab.parent().hasClass('active')) {
                    return true;
                }
            }
            return false;
        },

        addContactPane: function() {
            var contacts_tab = '<li class=""><a href="#contacts" role="tab" data-toggle="tab"' 
                + 'style="padding-right: 10px;"><span title="Контакты" class="offline glyphicon glyphicon-user"></a></li>';

            this.ui.messengerTab.append(contacts_tab);

            var contacts = new Contacts;

            this.contactPane = new ContactsPane({
                collection: contacts
            });

            app.vent.trigger('chat:start', contacts);

            this.ui.tabContent.append('<div class="tab-pane" id="contacts"><div class="contactsPane"></div></div>');
            this.ui.tabContent.find('.contactsPane').append(this.contactPane.render().el);
            this.ui.tabContent.find('.contactsPane').dragscrollable();
        },

        addChat: function(contactModel) {
            var me = this;

            var selfId = app.account.get('id');
            var contactId = contactModel.get('id');

            var chatId;

            var isGroup = contactModel.get('isGroup');
            if (isGroup) {
                chatId = 'group_' + contactId;
            } else {
                chatId = 'chat_' + Math.min(selfId, contactId) + '_' + Math.max(selfId, contactId);
            }

            if (chatId in me.chatPanes === true) {
                me.openChat(chatId);
            } else {
                var chatTab = new ChatTab({
                    model :  new Backbone.Model({
                        name: contactModel.get('name'),
                        chatId: chatId
                    })
                });
                
                var chatPane = new ChatPane({
                    model : contactModel, 
                    collection : new Backbone.Collection(),
                    selfId: selfId,
                    chatId: chatId
                });

                me.ui.messengerTab.append(chatTab.render().el);
                me.ui.tabContent.append('<div class="tab-pane" id="' + chatId + '"></div>');
                me.ui.tabContent.find('#' + chatId).append(chatPane.render().el);
                me.openChat(chatId);
                me.chatPanes[chatId] = chatPane;
            }
        },

        onLoggedIn: function(data) {
            // data: contacts, contacts_pending, groups

            data.groups.forEach(function(group) {
                group.isGroup = true;
            });

            this.contactPane.collection.reset(data.groups.concat(data.contacts));

            // show unread chats
            if (data.unread_chats) {
                data.unread_chats.forEach(function(chatId) {
                    app.vent.trigger('chat:unread', chatId);
                });
            }
        },

        onMessage: function(data) {
            // data: chatId, message

            var me = this;

            _.defer(function(){ 
                // add message to chat pane
                if (data.chatId in me.chatPanes === true) {
                    var chatPane = me.chatPanes[data.chatId];
                    _.bind(chatPane.onMessage, chatPane, data.message)();
                }

                // fire unread event in case the chat is invisible
                if (me.isChatOpened(data.chatId) === false) {
                    app.vent.trigger('chat:unread', data.chatId);
                }
            });

        },

        onUserOnline: function(data) {
            // data: userId

            var self = this;

            _.defer(function(){ 
                var contact = self.contactPane.collection.findWhere({ id : data.userId, isGroup: undefined });
                if (contact) {
                    contact.set({ online: true });
                }
            });
        },

        onUserOffline: function(data) {
            // data: userId

            var self = this;

            _.defer(function(){ 
                var contact = self.contactPane.collection.findWhere({ id : data.userId, isGroup: undefined });
                if (contact) {
                    contact.set({ online: false });
                }
            });
        },

        onHistory: function(data) {
            // data: chatId, history

            var me = this;

            _.defer(function(){ 
                if (data.chatId in me.chatPanes === true) {
                    var chatPane = me.chatPanes[data.chatId];
                    _.bind(chatPane.onHistory, chatPane, data.history)();
                }
            });

        },

        resetPhones: function(phones) {
        	this.phonesPane.collection.reset(phones);
        }
    });
    
    return messengerPane;
});
