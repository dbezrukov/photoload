define([
    'jquery',
    'underscore',
    'marionette',
    'views/contact_item',
    'css!stylesheets/contacts_pane_style'
], function($, _, Marionette, ContactItem) {
    return Marionette.CollectionView.extend({
        tagName: "ul",
        className: "contact_list",
        itemView: ContactItem,
        ui: {
        },
        initialize: function() {
            if (this.collection) {
                this.listenTo(this.collection, "reset", this.render, this);
            }
        }
    });
});
