define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'views/tile_item',
    'views/tiles_search_control',
    'views/gm_map_pane',
    'tpl!templates/tiles_pane.tmpl',
    'css!stylesheets/tiles_pane_style',
], function($, _, _str, Marionette, TileItem, TilesSearchControl, MapPane, templatePanel) {

    return Marionette.CompositeView.extend({
        template: templatePanel,
        itemView: TileItem,
        itemViewContainer: '.tiles',
        tagName: "container-fluid",
        className: "tilesPane",

        ui: {
        	'gallery' : '.pswp',
        	'photos': ".tiles",
        	'search_wrapper': '.search-wrapper',
        	'map_pane': '#main__region'
        },

        events: {
        	'click #openGallery': 'onOpenGallery',
            'click .tile-upload-wrapper #files-open-ref': 'onAddTile',
            'change #files-open': 'onTileSelected'
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

            // selected files
    		self.files = [];
        },

        onRender: function() {
        	var self = this;
            self.addSearchControl();
            //self.addMapPane();
		}, 

		addSearchControl: function() {
        	var self = this;

            this.searchControl = new TilesSearchControl({
                collection: self.collection,
                self_id: self.model.get('id')
            });

            self.ui.search_wrapper.append(this.searchControl.render().el);
        },

        addMapPane: function() {
        	var self = this;

            self.mapPane = new MapPane({
            	collection: self.collection
            });

            this.ui.map_pane.append(self.mapPane.render().el);
        },

		onAddTile: function() {
			$(".tile-upload-wrapper #files-open").trigger("click");
		},

		onTileSelected: function(event) {
			var self = this;

			var selectedFile = event.target.files[0];
			var fileName = selectedFile.name.split('.')[0];
    		var reader = new FileReader();
			
			reader.onload = function(event) {
				self.uploadFile(fileName, event.target.result);
			};

			reader.readAsArrayBuffer(selectedFile);
		},

		uploadFile: function(fileName, data) {
			var self = this;

			function uploadCallback(data, status, xhr) {
				var tileId = data.id;

				var metaData = {
					name: fileName
				}

				$.ajax({
					url: '/tiles/' + tileId + '/meta',
      				type: 'POST',
      				data: JSON.stringify(metaData),
      				dataType: 'json',
      				processData: true,
    				contentType: 'application/json',
      				success: function(data, status, xhr) {
      					$(".tile-upload-wrapper span").text('GeoTiff файл загружен');
      			
      					self.collection.fetchTiles();

		    			setTimeout(function() {
		    				$(".tile-upload-wrapper span").text('');
						}, 10000);
      				},
      				error: function(e, status) {
      					console.log('Ошибка записи метаданных на сервер');
      				}
				});
	      	}

			$.ajax({url: '/tiles',
      			type: 'POST',
      			data: data,
      			processData: false,
    			contentType: 'image/tiff',
      			success: uploadCallback
			});
		}
    });
});