define([
	"underscore",
	"underscore.string",
    'marionette',
    'tpl!templates/photo_item.tmpl',
    'css!stylesheets/photo_item_style'
], function(_, _str, Marionette, template) {

	// TODO: Trigger photo:select, photo:deselect

    return Marionette.ItemView.extend({
        template: template,
        tagName: "li",
        className: "photo-item",

        ui: {
        	'photoSelector': '.photo-selector'
        },

        events: {
            'click img': 'highlightPhoto',
            'dblclick img': 'openPhoto',
            'click .photo-selector': 'selectPhoto'
        },
  
        initialize: function(){
            var self = this;

            app.vent.on(self.className + ':select', self.onSelect, self);
            app.vent.on(self.className + ':deselect', self.onDeselect, self);
            app.vent.on(self.className + ':select-all', self.onSelectAll, self);
            app.vent.on(self.className + ':deselect-all', self.onDeselectAll, self);
            app.vent.on(self.className + ':delete-selected', self.onDeleteSelected, self);
        },

        onRender: function() {
        	var self = this;

            $(self.el).attr('title', self.model.get('title'));
            $(self.ui.photoSelector).attr('selected', !!self.model.get('selected'));
        },
  
        highlightPhoto: function(){
            app.vent.trigger(this.className + ':highlight', this.model);
        },

        openPhoto: function(){
            app.vent.trigger(this.className + ':open', this.model);
        },

        selectPhoto: function() {
        	var newSelection = !this.model.get('selected');
        	this.setSelected(newSelection);
        },

        onSelect: function(model) {
        	var self = this;

        	if (self.model === model) {
        		self.setSelected(true);
        	}
        },

        onDeselect: function(model) {
        	var self = this;

        	if (self.model === model) {
        		self.setSelected(false);
        	}
        },

        onSelectAll: function() {
        	this.setSelected(true);
        },

        onDeselectAll: function() {
        	this.setSelected(false);
        },

		onDeleteSelected: function() {
			var self = this;

			if (self.model.get('selected')) {

				app.vent.off(self.className + ':select-all', self.onSelectAll, self);
            	app.vent.off(self.className + ':deselect-all', self.onDeselectAll, self);
            	app.vent.off(self.className + ':delete-selected', self.onDeleteSelected, self);

				self.model.destroy();
			}
		},

		setSelected: function(selected) {
			console.log('setSelected for: ' + this.model.get('id'));

			this.model.set('selected', selected);
            $(this.ui.photoSelector).attr('selected', selected);

			app.vent.trigger(this.className + (selected ? ':selected' : ':deselected'), this.model);
		}
    });
});
