define([
    'underscore',
    'underscore.string',
    'marionette',
    'tpl!templates/admin_server_editor.tmpl'
], function(_, _str, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "modal-dialog",
        id: 'serverEditor',

        ui: {
        	'name': '#serverName',
        	'url': '#serverUrl',
        },

        events: {
        	'click #btnSubmit': 'submitChanges',
        	'keypress': 'keyPressed',
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        keyPressed: function(e) {
        	var self = this;

        	if (e.keyCode == 13) {
		        self.submitChanges.call(self);
		    }
        },

        onRender: function() {
            var self = this;
        },

        onShow: function(){
        },

		submitChanges: function() {
        	var self = this;

        	var serverData = {
        		name : self.ui.name.val(),
        		url: self.ui.url.val() 
        	}

        	var options = {
        		type: 'POST',
        		success: function() {
    				self.close();
    			},
    			error: function() { 
    				alert('Не удается сохранить данные');
    			}
        	}

        	if (self.options.collection) {
        		self.options.collection.create(serverData, options)
        	} else {
        		self.model.save(serverData, options);
			}
        }
    });
});
