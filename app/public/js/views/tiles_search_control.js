define([
	'underscore',
    'marionette',
    'tpl!templates/tiles_search_control.tmpl',
    'css!stylesheets/tiles_search_control_style',
	'bootstrap-multiselect',
    'bootstrap-datepicker'
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "tilesSearchControl",

        events: {
            'click #btnFetchTiles' : 'onFetchTiles'
        },

        ui: {
        	lat: '#tileLat',
        	lon: '#tileLon',
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});

            self.self_id = options.self_id;
        },

        onRender: function() {
			var self = this;

    		self.ui.lat.val('60');
    		self.ui.lon.val('30');

        	self.collection.fetchTiles();
        },

        onFetchTiles: function(event){
            event.preventDefault();
			var self = this;
            
            var query = {
            	lat: self.ui.lat.val(),
        		lon: self.ui.lon.val()
            }

            this.collection.fetchTiles(query);
        }
    });
});
