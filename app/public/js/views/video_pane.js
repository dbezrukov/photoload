define([
    'jquery',
    'underscore',
    'underscore.string',
    'marionette',
    'tpl!templates/video_pane.tmpl',
    'css!stylesheets/video_style'
], function($, _, _str, Marionette, templatePanel) {

    var videoPane = Marionette.ItemView.extend({
        template: templatePanel,
        tagName: "div",
        className: "videoPane",

        ui: {
            'localVideo': '.localVideo',
            'remoteVideo': '.remoteVideo'
        },
        events: {
            'click .stopCall'  : '_onContactStopCall'
        },        

        initialize: function(options) {
            var self = this;

            options || (options = {});

            self.localStream = options.localStream;
            self.remoteStream = options.remoteStream;

            app.vent.on('call:localStream', this.setLocalStream, this);
            app.vent.on('call:remoteStream', this.setRemoteStream, this);
        },

        onRender: function() {
            var self = this;

            self.setLocalStream(self.localStream);
            self.setRemoteStream(self.remoteStream);
        }, 

        setLocalStream: function(src) {
            var self = this;

            self.localStream = src;

            if (src) {
                self.ui.localVideo[0].src = src;
            } else if (self.ui.localVideo[0]) {
                self.ui.localVideo[0].pause();
                self.ui.localVideo[0].src = '';
            }
        },

        setRemoteStream: function(src) {
            var self = this;

            self.remoteStream = src;

            if (src) {
                self.ui.remoteVideo[0].src = src;
            } else if (self.ui.remoteVideo[0]) {
                self.ui.remoteVideo[0].pause();
                self.ui.remoteVideo[0].src = '';
            }
        }, 

        setPartner: function(partner) {
            var self = this;

            // unsubscribe from the prev partner
            if (self.partner) {
                self.partner.off('change:callState', self._onCallStateChanged);
            }

            self.partner = partner;

            self._onCallStateChanged(self.partner, self.partner.get('callState'));
            self.partner.on('change:callState', self._onCallStateChanged, self);
        }, 

        _onCallStateChanged: function(model, callState) {
            $(this.el).attr('callState', callState);
        },

        _onContactStopCall: function(event){
            event.preventDefault();
            this.partner.stopCall();
        }
    });
    
    return videoPane;
});
