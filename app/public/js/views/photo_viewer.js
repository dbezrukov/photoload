define([
    'underscore',
    'underscore.string',
    'marionette',
    'leaflet',
    'tpl!templates/photo_viewer.tmpl',
    'css!stylesheets/photo_viewer_style',
    'css!vendors/leaflet/leaflet'
], function(_, _str, Marionette, L, template) {

	// TODO: Trigger photo:delete

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "modal-dialog",
        id: 'photoViewer',

        ui: {
            'modalLabel': '#modalLabel',
            'photo': '#image-map',
            "coords": "#photoCoords #text",
            "specs": "#photoInfo #text"
        },

        events: {
        	'change #bla-files-open': 'onUploadFolderSelected',
        },

        initialize: function(){
            var self = this;
        },

        onRender: function() {
            var self = this;

            var album = self.model.get('album');
            var id = self.model.get('id');

            self.ui.modalLabel.text('Борт ' + self.model.get('album'));
            self.ui.specs.html(self.model.get('info'))

			setupPlainOverlay();

            function setupPlainOverlay() {
            	self.map = L.map(self.ui.photo[0], {
				  	minZoom: 1,
				  	maxZoom: 4,
				  	center: [0, 0],
				  	zoom: 1,
				  	crs: L.CRS.Simple
				});

				// dimensions of the image
				var w = self.model.get('w'),
				    h = self.model.get('h'),
				    url = 'photo/bla/' + album + '/' + id;

				// calculate the edges of the image, in coordinate space
				var southWest = self.map.unproject([0, h], self.map.getMaxZoom()-1);
				var northEast = self.map.unproject([w, 0], self.map.getMaxZoom()-1);
				var bounds = new L.LatLngBounds(southWest, northEast);

				// add the image overlay, 
				// so that it covers the entire map
				L.imageOverlay(url, bounds).addTo(self.map);

				// tell leaflet that the map is exactly as big as the image
				self.map.setMaxBounds(bounds);
            }

            function setupXYOverlay() {
            	self.map = L.map(self.ui.photo[0], {});

				self.model.getBBox(function(err, bbox) {
					if (err) {
						return;
					}

					var path = 'photo/bla/' + self.model.get('album') + '/' + self.model.get('id') + '/rotated';

					var sw = L.latLng(bbox.sw.lat, bbox.sw.lon),
	                    ne = L.latLng(bbox.ne.lat, bbox.ne.lon),
	                    bounds = new L.LatLngBounds(sw, ne);

					L.imageOverlay(path, bounds).addTo(self.map);

	                self.map.fitBounds(bounds);

	                self.map.on('mousemove', function(mouseEvent) {
	                    var lat = mouseEvent.latlng.lat;
	                    var lng = mouseEvent.latlng.lng;

	                    app.geo.WGStoXY(lat, lng, function(xy) {
	                        var coordWGS = _str.vsprintf("Ш: %s, Д: %s", [lat.toFixed(5), lng.toFixed(5)]);
	                        var coordXY = _str.vsprintf("X: %s, Y: %s", [xy.x.toFixed(0), xy.y.toFixed(0)]);
	                        
	                        $(self.ui.coords).html(coordWGS + '<br>' + coordXY);
	                    })
	                });
				})
            }
        },

        onShow: function(){
            var self = this;
            self.map.invalidateSize();
        }
    });
});
