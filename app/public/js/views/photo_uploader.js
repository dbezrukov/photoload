define([
    'underscore',
    'underscore.string',
    'marionette',
    'tpl!templates/photo_uploader.tmpl',
    'exif-js'
], function(_, _str, Marionette, template) {

    function parseINIString(data) {

	    var regex = {
	        section: /^\s*\[\s*([^\]]*)\s*\]\s*$/,
	        param: /^\s*([\w\.\-\_]+)\s*=\s*(.*?)\s*$/,
	        comment: /^\s*;.*$/
	    };

	    var value = {};
	    var lines = data.split(/\r\n|\r|\n/);
	    var section = null;

	    lines.forEach(function(line) {
	        
	        if(regex.comment.test(line)) {
	            return;

	        } else if(regex.param.test(line)) {
	            var match = line.match(regex.param);
	            if (section) {
	                value[section][match[1]] = match[2];
	            } else {
	                value[match[1]] = match[2];
	            }

	        } else if(regex.section.test(line)) {
	            var match = line.match(regex.section);
	            value[match[1]] = {};
	            section = match[1];

	        } else if(line.length == 0 && section) {
	            section = null;

	        };
	    });
    	return value;
	};

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "modal-dialog",
        id: 'photoUploader',

        ui: {
        	'fileUpload': '#fileUpload',
        	'boardId': '#boardId',
        	'readFilesCount': '#readFilesCount',
        	'progress': '#progress',
        	'btnSubmit': '#btnSubmit'
        },

        events: {
        	'click #btnSelect': 'selectFolder',
        	'click #btnSubmit': 'submitBoardId',
        	'change #fileUpload': 'onUploadFolderSelected',
        },

        initialize: function(){
            var self = this;

            self.searchControl = self.options.searchControl;
            self.selectedFiles = [];
        	self.imageFiles = [];
        },

        onRender: function() {
            var self = this;

            self.ui.fileUpload.val('');

            self.ui.boardId.val('');
        	self.ui.readFilesCount.text('');

        	self.ui.progress.attr('style', 'width:0%');

        	self.ui.btnSubmit.text('Загрузить');
        	self.ui.btnSubmit.removeClass('btn-success');
        	self.ui.btnSubmit.removeClass('btn-danger');
        	self.ui.btnSubmit.addClass('btn-primary');
        	self.ui.btnSubmit.prop('disabled', true);
        },

        onShow: function(){
        },

        selectFolder: function() {
        	var self = this;

			self.ui.fileUpload.trigger('click');
        },

        submitBoardId: function() {
        	var self = this;

			// disable in quest mode
			if (app.account.get('name') === 'guest') {
				alert('Недоступно в режиме гостевого доступа.\nЗайдите под своим пользователем.');
				return;
			}

			var text = self.ui.btnSubmit.text();

			if (text === 'Загрузить')  {
				if (self.ui.boardId.val() === '') {
					alert('Необходимо ввести номер борта');
					return;
				}
				self.ui.btnSubmit.prop('disabled', true);
				self.uploadFiles.call(self);
			} else {
				self.close();
			}
        },

        onUploadFolderSelected: function(event) {
			var self = this;

			self.selectedFiles = event.target.files;

        	self.imageFiles = _.filter(self.selectedFiles, function(item) {
         		return item.type.match('image.*');
       		});
       		
       		self.ui.readFilesCount.text('Выбрано файлов: ' + self.imageFiles.length);

       		if (self.imageFiles.length === 0) {
       			return;
       		}

	    	self.ui.progress.attr('style', 'width:0%');
	    	self.ui.btnSubmit.removeClass('btn-success');
	    	self.ui.btnSubmit.removeClass('btn-danger');
	    	self.ui.btnSubmit.addClass('btn-default');
	    	self.ui.btnSubmit.text('Загрузить');
    		self.ui.btnSubmit.prop('disabled', false);
		},

		uploadFiles: function() {
			var self = this;

	    	var i = 0;

	    	testIniFile(i, function(err) {
				if (err) {
					self.ui.readFilesCount.text(err);
					return;
				}

				// tel test passed, start uploading
				var boardId = self.ui.boardId.val();
	    	
	    		i = 0;

	    		processImageFile(i, function(err) {
	    			if (err) {
	    				console.log('error during uploading: ' + err);
	    				self.ui.readFilesCount.text(err);
			 			self.ui.fileUpload.val('');
	    			}

	    			self.ui.btnSubmit.text('Закрыть');
		    		self.ui.btnSubmit.addClass(err ? 'btn-danger' : 'btn-success');
		    		self.ui.btnSubmit.removeClass('btn-primary');
		    		self.ui.btnSubmit.prop('disabled', false);

		    		self.options.searchControl.checkBoardExist.call(self.options.searchControl, boardId);
            		self.options.collection.fetch(self.options.searchControl.getQueryObject());
    			});
			});

	    	function testIniFile(index, callback) {
	    		readIniFile(self.imageFiles[index], function(err) {
					if (err) {
						callback(err);
						return;
					}

					i++;

					self.ui.readFilesCount.text('Проверка данных телеметрии.. ' + i + ' из ' + self.imageFiles.length);

					if (i < self.imageFiles.length) {
						testIniFile(i, callback);
					} else {
						callback();
					    return;
					}
				});
	    	}

    		function readIniFile(imagefile, callback) {
    			
				function errorReadingFile(reason) {
		 			callback(reason);
				}

				var metaData = {};

    			// read exif
    			var exifReader = new FileReader;
 
				exifReader.onerror = function() {
					errorReadingFile('Ошибка чтения EXIF из файла ' + imagefile.name);
					return;
				}

	            exifReader.onload = function () {
	                var exif = EXIF.readFromBinaryFile(exifReader.result);
	 
	                // telemetry
	                metaData.utc = (Date.now() / 1000).toFixed();
					metaData.hdg = '0';
					metaData.pch = '0';
					metaData.roll = '0';
					metaData.yaw = '0';

					// ext telemetry
					if (exif.GPSLatitude && exif.GPSLongitude && exif.GPSLatitudeRef && exif.GPSLongitudeRef) {
						var lat = exif.GPSLatitude;
	                	var lon = exif.GPSLongitude;
	 
	                	var latRef = exif.GPSLatitudeRef;
	                	var lonRef = exif.GPSLongitudeRef;

	                	lat = (lat[0] + lat[1]/60 + lat[2]/3600) * (latRef == "N" ?  1 : -1);
	                	lon = (lon[0] + lon[1]/60 + lon[2]/3600) * (lonRef == "W" ? -1 :  1);

	                	metaData.lat = lat.toFixed(4);
						metaData.lon = lon.toFixed(4);
					}

					if (exif.GPSSpeed && exif.GPSAltitude) {
						metaData.speed = exif.GPSSpeed.toString();
						metaData.hgt = exif.GPSAltitude.toString();
					}

					// camara specs
					metaData.cameraWidth = ((exif.PixelXDimension / exif.FocalPlaneXResolution) * 25.4).toString();
            		metaData.cameraHeight = ((exif.PixelYDimension / exif.FocalPlaneYResolution) * 25.4).toString();
            		metaData.cameraF = exif.FocalLength.toString();

					self.ui.readFilesCount.text('Загружено файлов: ' + (i + 1) + ' из ' + self.imageFiles.length);

					// try to find a corresponding tel file
					var telFileName = imagefile.name.replace('.jpg', '.tel');

					var telFile = _.findWhere(self.selectedFiles, { name: telFileName });

					// using exif only
					if (!telFile) {
						callback(null, metaData);
						return;
					}

					var telReader = new FileReader();

					telReader.onerror = function() {
						errorReadingFile('Файл ' + telFileName + ' не загружен.');
					}
					
					telReader.onload = function(iniLoadedEvent) {
						var telData = parseINIString(iniLoadedEvent.target.result);

						// remove empty fields
						for (var property in telData) {
							if (telData.hasOwnProperty(property)) {
    							if (telData[property].length === 0) {
    								delete telData[property];
    							}
							}
						}

						// check the must-have values
						var lat = parseFloat(telData.lat);
						if (isNaN(lat) || lat === 0 || lat < -89 || lat > 89) {
							console.log('photo lat is invalid');
							errorReadingFile('Файл ' + telFileName + ' содержит недопустимую широту');
							return;
						}

						var lon = parseFloat(telData.lon);
						if (isNaN(lon) || lon === 0 || lat < -180 || lat > 180) {
							console.log('photo lon is invalid');
							errorReadingFile('Файл ' + telFileName + ' содержит недопустимую долготу');
							return;
						}

						metaData = _.extend(metaData, telData);

						callback(null, metaData);
					}
					
					telReader.readAsText(telFile);
				}

				exifReader.readAsArrayBuffer(imagefile);
			};

			function uploadImageFile(boardId, imageData, imageName, metaData, callback) {
				function errorUploadingImageFile(reason) {
		 			callback(reason);
				}

				$.ajax({
					url: self.options.collection.url + '/' + boardId + '/',
	      			type: 'POST',
	      			data: imageData,
	      			processData: false,
	    			contentType: 'image/jpeg',
	      			success: function(data, status, xhr) {
						callback(null, data.id);
	      			},
	      			error: function(e, status) {
	      				errorUploadingImageFile('Ошибка записи файла ' + imageName + ' на сервер');
	      			}
				});
			}

			function uploadMeta(boardId, imageId, metaData, callback) {
				function errorUploadingMeta(reason) {
		 			callback(reason);
				}

				$.ajax({
					url: self.options.collection.url + '/' + boardId + '/' + imageId + '/meta',
      				type: 'POST',
      				data: JSON.stringify(metaData),
      				dataType: 'json',
      				processData: true,
    				contentType: 'application/json',
      				success: function(data, status, xhr) {
      					callback();
      				},
      				error: function(e, status) {
      					errorUploadingMeta('Ошибка записи метаданных на сервер');
      				}
				});
			}

    		function processImageFile(fileIndex, callback) {
			    function errorReadingImageFile(reason) {
		 			callback(reason);
				}

			    imagefile = self.imageFiles[fileIndex];

			    console.log("Processing file: ", imagefile.name);
			        
				var reader = new FileReader();
			                
				reader.onerror = function() {
					errorReadingImageFile('Файл ' + imagefile.name + ' не загружен.');
				};
				
				reader.onload = function(imageLoadedEvent) {
					
					readIniFile(imagefile, function(err, metaData) {
						if (err) {
							callback(err);
			        		return;
						}

						console.log('Image meta data:');
						console.log(metaData);

						var boardId = self.ui.boardId.val();

						uploadImageFile(boardId, imageLoadedEvent.target.result, imagefile.name, metaData, function(err, imageId) {
							if (err) {
								callback(err);
								return;
							}

							uploadMeta(boardId, imageId, metaData, function(err) {
								if (err) {
									callback(err);
									return;
								}

								var percents = ( (++i) * 100) / self.imageFiles.length;
	      						self.ui.progress.attr('style', 'width:' + percents + '%');

								if (i < self.imageFiles.length) {
					            	processImageFile(i, callback);
								} else {
									callback();
					        		return;
								}
							})
						})
					});
			    }

				reader.readAsArrayBuffer(imagefile);
  			}
		},

    });
});
