define([
    'marionette',
    'tpl!templates/chat_item.tmpl',
], function(Marionette, template) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: "li",

        events: {
        },
  
        initialize: function(){
        }
    });
});
