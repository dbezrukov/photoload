define([
    'marionette',
    'tpl!templates/photo_item.tmpl',
    'css!stylesheets/photo_item_style'
], function(Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "li",
        className: "tile-item",

        ui: {
        	'photoSelector': '.photo-selector'
        },

        events: {
            'click img': 'openPhoto',
            'click .photo-selector': 'selectPhoto'
        },
  
        initialize: function(){
            var self = this;

        },

        onRender: function() {
        	var self = this;

            var info = 'Сшивка: ' + self.model.get('name') + '\n' +
                       'Дата: ' + self.model.get('time_string');

            $(self.el).attr('title', info);
            $(self.ui.photoSelector).attr('selected', !!self.model.get('selected'));

            app.vent.on(self.className + ':select-all', self.onSelectAll, self);
            app.vent.on(self.className + ':deselect-all', self.onDeselectAll, self);
            app.vent.on(self.className + ':delete-selected', self.onDeleteSelected, self);
        },
  
        openPhoto: function(){
            app.vent.trigger(this.className + ':open', this.model);
        },

        selectPhoto: function() {
        	var newSelection = !this.model.get('selected');
        	this.setSelected(newSelection);
        },

        onSelectAll: function() {
        	this.setSelected(true);
        },

        onDeselectAll: function() {
        	this.setSelected(false);
        },

		onDeleteSelected: function() {
			var self = this;

			if (self.model.get('selected')) {

				app.vent.off(self.className + ':select-all', self.onSelectAll, self);
            	app.vent.off(self.className + ':deselect-all', self.onDeselectAll, self);
            	app.vent.off(self.className + ':delete-selected', self.onDeleteSelected, self);

				self.model.destroy();
			}
		},

		setSelected: function(selected) {
			console.log('setSelected for: ' + this.model.get('id'));

			this.model.set('selected', selected);
            $(this.ui.photoSelector).attr('selected', selected);
		}
    });
});

