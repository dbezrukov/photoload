var express = require('express');
var shortId = require('shortid');
var config = require('../config');
var fs = require('fs');
var _ = require('underscore');

var Zip = require('node-7z');
var ZipTask = new Zip();

var dir = require('node-dir');

var router = express.Router();

var Sits = global.db.collection("sits");

// application/x-compressed type sits

// выполнить поиск фото внутри заданного источника
router.get('/', global.restrictApi, function(req, res) {

	console.log('quering sits');
	console.dir(req.query);

	var timestampStart;
	var timestampEnd;

	if (req.query.timestampStart && req.query.timestampEnd) { // using timestamps

		timestampStart = parseInt(req.query.timestampStart);
		timestampEnd = parseInt(req.query.timestampEnd);

	} else { // using dates

		var dateStart = new Date(req.query.dateStart ? req.query.dateStart.split('-').reverse().join('-') : '02-01-1970');
		var dateEnd   = new Date(req.query.dateEnd   ? req.query.dateEnd.split('-').reverse().join('-')   : '01-01-2100');
    	
    	// including the entire da
		dateEnd.setDate(dateEnd.getDate() + 1);

		timestampStart = dateStart.getTime() / 1000;
		timestampEnd =  dateEnd.getTime() / 1000;
	}

	console.log('timestampStart: ', timestampStart);
    console.log('timestampEnd: ', timestampEnd);

	var query = { utc:  { $gte: timestampStart, $lte: timestampEnd } };

	// apply users filter
	if (req.query.users) {
		var usersIds = req.query.users.map(function (x) { 
			return parseInt(x, 10); 
		});

		query['userId'] = { $in: usersIds };
    }

    var fields = { _id: 0 };
    var skip = parseInt(req.query.offset ? req.query.offset : 0);
    var limit = parseInt(req.query.count ? req.query.count : 75);

    Sits.find(query, fields, { limit: limit, skip: skip }).sort({ utc: -1 }).toArray(function (err, items) {
	if (err) { res.status(500).send({ error: err }); return; }
		console.log('returning %d items', items.length);
    	res.json(items);
	});
});

// write the sit itself
router.post('/', global.restrictApi, function(req, res) {

    var sitId = shortId.generate();

    console.log('adding a sit, id: %s', sitId);

    var sitPath = config.sits.path + '/' + sitId + '.zip';

    console.log('sitPath: %s', sitPath);
        
    var file = fs.createWriteStream(sitPath);

    req.on('data', function(chunk) {
        file.write(chunk);
    });

    req.on('end', function() {
        file.end(function() {
        	console.log('writing complete');
			console.log('sitPath: %s', sitPath);
			
  			var sitData = {
	            id: sitId,
	            userId: req.session.userData.id,
	            utc: Date.now() / 1000
        	}

	        Sits.update({ id : sitId }, { $set: sitData }, { upsert: true }, function(err, result) {
	            if (err) {  
	                res.status(500).send({ error: 'can not write sit' });
	                return;
	            }

				var unzipPath = config.archive.path + '/' + sitId + '.sit';

    			ZipTask.extractFull(sitPath, unzipPath)
					.then(function () {
			  			console.log('Extracting done!');

			  			dir.files(unzipPath, function(err, files) {
							if (!err) {

								var sit7Path = config.sits.path + '/' + sitId + '.7z';

								ZipTask.add(sit7Path, files)
									.progress(function (files) {
  										console.log('Some files are extracted: %s', files);
									})
									.then(function () {
						  				console.log('Extracting done!');
						  				fs.unlink(sitPath);
						  				res.json({ id: sitId });
									})
									.catch(function (err) {
						  				console.error(err);
									})
							}
						})
					})
					.catch(function (err) {
			  			console.error(err);
					});
	            
	        });
        });
    });
});

// выполнить поиск фото внутри заданного источника
router.post('/archive', global.restrictApi, function(req, res) {

	console.log('archive request');
	
	var archiveId = shortId.generate();

	process.nextTick(function() {
		console.log('creating archive ' + archiveId);

    	var fileList = [];

    	req.body.forEach(function(file) {
    		var fileName = config.sits.path + '/' + file.id + '.7z'
    		fileList.push(fileName);
    	});

		var archivePath = config.archive.path + '/' + archiveId + '.7z';
		console.log('archivePath: ' + archivePath)

		console.log('files:');
		console.log(fileList);

		ZipTask.add(archivePath, fileList)
			.then(function () {
  				console.log('archive written: ' + archivePath);
  				res.json({ id: archiveId });
			})
 			// On error 
			.catch(function (err) {
  				console.error(err);
  				res.json({});
			})
	});
});

router.get('/archive/:id', global.restrictApi, function(req, res) {
	var archiveId = req.params.id;
    var archivePath = config.archive.path + '/' + req.params.id + '.7z';

    var options = {
    	dotfiles: 'deny',
    	headers: {
        	'x-timestamp': Date.now(),
        	'x-sent': true
    	}
  	};

    console.log('sending file ' + archivePath);
    res.sendfile(archivePath, options, function(err) {
    	if (err) {
      		console.log(err);
      		res.status(err.status).end();
    	}
    	else {
      		console.log('archive send, deleting');
      		fs.unlink(archivePath);
    	}
    });
});

// get sit by id
router.get('/:id', global.restrictApi, function(req, res) {
    var sitId = req.params.id;
    var sitPath = config.sits.path + '/' + req.params.id + '.7z';

    res.sendfile(sitPath);
});

router.delete('/:id', global.restrictApi, function(req, res) {
	var sitId = req.params.id;
    var sitPath = config.sits.path + '/' + req.params.id + '.7z';

    Sits.remove({ id: req.params.id}, function(err, sit) {
	    if (err) { 
	    	console.log('sits collection error');
	    	res.status(500).send({ error: 'can not be deleted from database' });
	    	return;
	    }

    	fs.unlink(sitPath, function() {
    		console.log('file deleted: ' + sitPath);
      		res.end();
    	});
    });
});

// write meta data
router.post('/:id/meta', function(req, res) {
    var sitId = req.params.id;

	if (!req.body) {
		console.log('no json data');
		res.status(500).send({ error: 'no json data' }); 
    	return; 
    }

    // skip this data
    if (req.body.utc) {
		delete req.body.utc;
	}

	// set meta data
	Sits.update({ id : sitId }, { $set: req.body }, { upsert: false }, function(err, result) {
        if (err || result !== 1) {
        	console.log('err: ', err);
            res.status(500).send({ error: 'can not set meta' });
            return;
        }

        res.json({ id: sitId });
	});
});

router.get('/:id/meta', function(req, res) {
    var sitId = req.params.id;

    Sits.findOne({ id: sitId }, { loc: 0 }, function(err, meta) {
        if (err || !meta) {
            res.json({});
            return;
        }

        res.json(meta);
    })
});

module.exports = router;
