var _ = require('express');
var express = require('express');
var pass = require('pass');
var router = express.Router();

var Users = global.db.collection("users");

// get user list =================================================
router.get('/', global.restrictUserUi, function(req, res) {
    // return members of those groups where the current user is admin
    var query = { groups: { $in: req.session.userData.groups } };

    // exclude superadmin for non superadmin users
    if (req.session.userData.id !== 1) {
        query.id = { $ne: 1 };
    }

    Users.find(query).sort({ id: 1 }).toArray(function (err, items) {
        if (err) {
            res.status(500).send({ error: 'can not get user list' });
            return;
        }

        items.forEach(function(item) {
            item.is_online = (item.id in global.loggedUsers === true);
        })
        
        res.json(items);
    });
});

// get editable users list =================================================
router.get('/editable', global.restrictAdminUi, function(req, res) {
    // return members of those groups where the current user is admin
    var query = { groups: { $in: req.session.userData.groups_admin } };

    // exclude superadmin for non superadmin users
    if (req.session.userData.id !== 1) {
        query.id = { $ne: 1 };
    }

    Users.find(query).sort({ id: 1 }).toArray(function (err, items) {
        if (err) {
            res.status(500).send({ error: 'can not get user list' });
            return;
        }

        items.forEach(function(item) {
            item.is_online = (item.id in global.loggedUsers === true);
        })
        
        res.json(items);
    });
});

// add a user  ===================================================
router.post('/', global.restrictAdminUi, function(req, res) {
    global.db.nextId("userId", function(id) {
        if (!id) {
            res.status(500).send({ error: 'can not create id' });
            return;
        }

        updateOrInsertUser(id, req.body, req.session, function(error) {
            if (error) {
                res.status(500).send({ error: error });
                return;
            }
            res.send({});
        });
    });
});

// get the user ==================================================
router.get('/:id', global.restrictUserUi, function(req, res) {
    var id = parseInt(req.params.id);

    Users.findOne({ id: id }, function(err, result) {
        if (err || !result) {
            res.status(500).send({ error: 'can not get user' });
        }
        res.json(result);
    });
});

// delete the user ===============================================
router.delete('/:id', global.restrictApi, function(req, res) {
    var id = parseInt(req.params.id);

    if (id === 1) {
        res.status(500).send({ error: "Запрещено удаление суперадмина" });
        return;
    }

    Users.remove({ id : id }, function(err, result) {
        if (err || result !== 1) {
            res.status(500).send({ error: 'can not delete user' });
            return;
        };
        res.send({});
    });
});

// update the user ===============================================
router.post('/:id', global.restrictAdminUi, function(req, res) {
    var id = parseInt(req.params.id);

    updateOrInsertUser(id, req.body, req.session, function(error) {
        if (error) {
            res.status(500).send({ error: error });
            return;
        }
        res.send({});
    });
});

// update the user ===============================================
router.post('/:id/set-password', global.restrictAdminUi, function(req, res) {
    var id = parseInt(req.params.id);

    // encrypt the password
    pass.generate(req.body.password, function(error, hash){
        if(error){
            res.status(500).send({ error: 'password hashing error' });
            return;
        }

        Users.update({ id : id }, { $set: { password: hash } }, function(err, result) {
            if (err || result !== 1) {
                res.status(500).send({ error: 'can not update password' });
                return;
            }
            res.send({});
        });
    });
});

module.exports = router;

function updateOrInsertUser(id, data, session, callback) 
{
    function toIntArray(entities) {
        var data = [];
        entities.forEach(function(entity) {
            data.push(parseInt(entity));
        });
        return data;
    }

    function updateUser(userData, callback) {
        Users.update({ id : id }, { $set: userData }, { upsert: true }, function(err, result) {
            if (err || result !== 1) {
                callback('can not update user');
                return;
            };
            
            if (session.userData.id === userData.id) {
                session.userData = userData;
            }

            callback();
        });
    }

    var userData = {
        id: id,
        name: data.name,
        is_admin: (data.groups_admin.length > 0),
        groups: toIntArray(data.groups),
        groups_admin: toIntArray(data.groups_admin ? data.groups_admin : []),
        npu_list: data.npu_list,
        region: data.region
    }

    if (data.password) {

        // encrypt the password
        pass.generate(data.password, function(error, hash){
            if(error){
                callback('password hashing error');
                return;
            }
            userData.password = hash;
            updateUser(userData, callback);
        });
    } else {
        updateUser(userData, callback);
    }
}