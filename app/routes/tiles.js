var express = require('express');
var shortId = require('shortid');
var config = require('../config');
var fs = require('fs');
var _ = require('underscore');
var _str = require('underscore.string');
var xml2object = require('xml2object');

var router = express.Router();

var Tiles = global.db.collection("tiles");

router.get('/', global.restrictApi, function(req, res) {
	var query = {};
    var fields = { _id: 0 };

    Tiles.find(query, fields).sort({ utc: -1 }).toArray(function (err, items) {
    	if (err) { res.status(500).send({ error: 'can not get tiles' }); return; }
		res.json(items);
    });
});

// write the kmz itself
router.post('/', global.restrictApi, function(req, res) {
    
    var tileId = shortId.generate();

    console.log('adding a tile with id ' + tileId);

    var tilePath = config.tiles.path + '/' + tileId + '.tif';
        
    var file = fs.createWriteStream(tilePath);

    req.on('data', function(chunk) {
        console.log('chunk received');
        file.write(chunk);
    });

    req.on('end', function() {
    	console.log('eo stream');

        file.end(function() {
			console.log('saving GeoTiff');

  			var tileData = {
            	id: tileId,
            	name: tileId,
            	box: { north: 80,
            		   west: -180,
            		   south: -80,
            		   east: 180 
				},
            	utc: Date.now() / 1000
			}

	        Tiles.update({ id : tileId }, { $set: tileData }, { upsert: true }, function(err, result) {
			    if (err) {  
			        res.status(500).send({ error: 'can not write tile' });
			    	return;
			    }

			    res.json({ id: tileId });
			});
        });
    });
});

// get tile by id
router.get('/:id', function(req, res) {
    
    var tileId = req.params.id;
    var tilePath = config.tiles.path + '/' + req.params.id + '.tif';

    res.setHeader("Content-Type", "image/tiff");
    res.sendfile(tilePath);
});

// write meta data
router.post('/:id/meta', function(req, res) {
    var tileId = req.params.id;

    console.log('attaching meta:');
    console.dir(req.body);

    Tiles.update({ id : tileId }, { $set: req.body }, { upsert: false }, function(err, result) {

        if (err || result !== 1) {
            res.status(500).send({ error: 'can not set meta' });
            return;
        };
        res.send({});
    });
});

router.delete('/:id', global.restrictApi, function(req, res) {
	var tileId = req.params.id;
    var tilePath = config.tiles.path + '/' + req.params.id + '.tif';

    Tiles.remove({ id: req.params.id}, function(err, tile) {
	    if (err) { 
	    	console.log('tiles collection error');
	    	res.status(500).send({ error: 'can not be deleted from database' });
	    	return;
	    }

    	fs.unlink(tilePath, function() {
    		console.log('file deleted: ' + tilePath);
      		res.end();
    	});
    });
});

router.get('/:id/meta', function(req, res) {
    var tileId = req.params.id;
    res.json({});
});

module.exports = router;
