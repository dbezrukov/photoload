var express = require('express');
var _ = require('underscore');
var _str = require('underscore.string');
var http = require('http');
var request = require('request');
var config = require('../config');
var replaceStream = require('replacestream');
var fs = require('fs')
var dir = require('node-dir');
var iconv = require('iconv-lite');

var sprintf = require("sprintf-js").sprintf;

//var unzip = require('unzip');
//var zip = require("node-native-zip");

var Zip = require('node-7z');
var ZipTask = new Zip();

var Server = require('../models/server');
var SyncTask = require('../models/sync_task');

var Sits = global.db.collection("sits");
var Photos = global.db.collection("photos");

var router = express.Router();

var cookieJar = request.jar();

var syncTimer;
var localAddress;

router.post('/', function(req, res) {

	if (!req.body) {
		console.log('no json data');
		res.send(500, { error: 'no json data' });
	}

	console.log('new sync settings received:');

	var settingsData = req.body;
	console.log(settingsData);

	SyncTask
        .findOne({})
        .exec(function(err, syncTask) {
        
        if (err) {
            res.send(500, { error: 'db error' });
            return;
        }

        if (syncTask) {

        	console.log('updating existing task');

        	syncTask.interval = settingsData.interval;
        	syncTask.localAddress = settingsData.localAddress;
        	localAddress = settingsData.localAddress;
        	
        	syncTask.save(function(err) {
        		if (err) {
        			res.send(500, { error: 'db error' });
            		return;
            	}

            	res.json(syncTask.toJSON());

            	restartAutosync();
        	});

        } else {

        	console.log('creating new task');

        	var newSyncTask = new SyncTask(settingsData);

        	newSyncTask.save(function(err, createdTask) {
        		if (err) {
        			res.send(500, { error: 'db error' });
            		return;
            	}

            	res.json(createdTask.toJSON());

            	restartAutosync();
        	});

        }
    });

});

router.get('/', function(req, res) {

    SyncTask
        .findOne({})
        .lean()
        .exec(function(err, syncTask) {

        	if (err) {
            	res.send(500, { error: 'db error' });
            	return;
        	}

        	res.json(syncTask);
    });
});

router.post('/servers', function(req, res) {

	console.log('New server:');
	console.log(req.body);

	var server = new Server(req.body);
	server.save(function(err) {
		if (err) {
            res.send(500, { error: 'db error' });
            return;
        }

        console.log('Server saved: ' + server.id);
        res.json({ _id: server.id });
	})
});

router.get('/servers', function(req, res) {

	Server
        .find({})
        .sort({ name: 'asc' })
    	.lean()
        .exec(function(err, servers) {

        	if (err) {
            	res.send(500, { error: 'db error' });
            	return;
        	}

        	res.json(servers);
    });
});

router.get('/servers/:id', function(req, res) {

    var serverId = req.params.id;

    Server
        .findById(serverId)
        .exec(function(err, server) {
        
        if (err) {
            res.send(500, { error: 'db error' });
            return;
        }

        res.json(server);
    });

});

router.post('/servers/:id', function(req, res) {

	console.log('/servers/:id');

    var serverId = req.params.id;

    console.log('Updated server:');
    console.log(req.body);

    Server
        .findByIdAndUpdate(serverId, req.body)
        .exec(function(err, server) {
        
        if (err) {
            res.send(500, { error: 'db error' });
            return;
        }

        res.json(server);
    });

});

router.post('/servers/:id/start', function(req, res) {

	var serverId = req.params.id;

	console.log('syncing server: ' + serverId);

	startSync(serverId, function(err) {
		if (err) {
			console.log('sync error');
			res.send(500, { error: err });
			return;
		}

		console.log('sync ok');
		res.end();
	});
});


router.delete('/servers/:id', function(req, res) {

    var serverId = req.params.id;

    Server
        .findByIdAndRemove(serverId)
        .exec(function(err) {
        
        if (err) {
            res.send(500, { error: 'db error' });
            return;
        }

        res.json({});
    });

});


function restartAutosync() {
	
	console.log('restartAutosync');

	SyncTask
        .findOne({})
        .lean()
        .exec(function(err, syncTask) {
        
        if (!err && syncTask) {

        	if (syncTimer) {
        		clearInterval(syncTimer);
        	}

        	console.log('Autosync interval is ' + syncTask.interval + ' minutes');

        	console.log('Sync local address is ' + syncTask.localAddress);

        	localAddress = syncTask.localAddress;

        	var interval = syncTask.interval * 60 * 1000;

        	syncTimer = setInterval(function() {

				console.log('Autostarting server sync..');
				
				Server
			        .find({})
			        .sort({ name: 'asc' })
			    	.lean()
			        .exec(function(err, servers) {

			        	if (err) {
			            	res.send(500, { error: 'db error' });
			            	return;
			        	}

			        	servers.forEach(function(server) {

			        		startSync(server._id, function(err) {
								if (err) {
									console.log('sync error');
								}
								console.log('sync ok');
							});

			        	});
			    });

			}, interval); // every n minutes
        }
    });
}

function startSync(serverId, callback) {
	
	console.log('Staring sync for server ' + serverId);
	
	Server
        .findById(serverId)
        .exec(function(err, server) {
        
        if (err) {
        	callback('db error');
            return;
        }

        server.status = 'Запуск синхронизации..';
		server.save();

		// сохранить время начала синхронизации
		server.utcSyncStarted = (new Date()).getTime() / 1000;

		console.log('last sync was ' + (server.utcSyncStarted - server.utc) + ' seconds ago');
		console.log('quering updates from ' + server.url);

		// получить список новых ситов на удаленном сервере
		getUpdatedFiles(server.url, server.utc, server.utcSyncStarted, function(err, files) {
			
			if (err) {
				server.status = 'Синхронизация не удалась';
				server.save();
				
				callback('cannot query files to sync');
            	return;
			}

			console.log('parsing file list');

			server.status = 'Чтение списка обновленных файлов';
			server.save();

			callback();

			if (files.length === 0) {
				server.status = 'Синхронизация завершена. Новых файлов нет';
				server.save();
				return;
			}

			// continue time-consuming operations
			var fileSynced = _.after(files.length, function() {
			
				if (files.length > 0) {
					server.utc = server.utcSyncStarted;
				}
				
				server.status = 'Синхронизация завершена. Количество файлов: ' + files.length;
				server.save();
    		});

    		for (var i = 0; i < files.length; i++) {
				
				var opts = {
					id: files[i].id,
					url: server.url + '/sits/' + files[i].id,
					path: config.sits.path,
					ext: '.7z',
					model: Sits
				}

				downloadFile(opts, function(err, metaData) {

					console.log('sit file downloaded, and contains ' + metaData.photos.length + ' photos');

					// ok, lets replace server.url with localAddress
					var zipPath = opts.path + '/' + opts.id + opts.ext;
					var unzipPath = config.archive.path + '/' + opts.id + '.sit';

					// unpack - change url - pack
					ZipTask.extractFull(zipPath, unzipPath)
						
						.then(function () {
							
							console.log('Extracting done!');

							dir.files(unzipPath, function(err, files) {
												    
								if (!err) {

									console.log('Unzip folder scanned');
									console.log(files);

									files.forEach(function(file) {

										if (_str.endsWith(file, '.sit')) {

											var fileName = file.replace(/^.*[\\\/]/, '').slice(0,-4);
											console.log('Sit file found, name: ' + fileName);

											// search and replace url and then pack it back
											var urlFile = unzipPath + "/" + fileName + "." + config.sits.urlFileExt;
											
											console.log('SSE file to fix: ' + urlFile);
											console.log('Updated SSE file: ' + urlFile + '2');
											console.log('Replacing: ' + server.url);
											console.log('With: ' + localAddress);

											var urlFileWrite = fs.createWriteStream(urlFile + '2');

											fs.createReadStream(urlFile)
													.pipe(iconv.decodeStream('win1251'))
												    .pipe(iconv.encodeStream('utf8'))
												    .pipe(replaceStream(server.url, localAddress))
												    .pipe(iconv.decodeStream('utf8'))
												    .pipe(iconv.encodeStream('win1251'))
													.pipe(urlFileWrite)
													
													.on('finish', function(){
														console.log('SSE file written');

														fs.unlink(urlFile, function() {

															fs.rename(urlFile + '2', urlFile, function (err) {
																if (!err) {
																	console.log('Renaming complete');
																	
																	var sit7Path = config.sits.path + '/' + opts.id + '.7z';
																	console.log('Archieving to ' + sit7Path);
																	
																	dir.files(unzipPath, function(err, files) {
																	    
																	    if (!err) {

																	    	console.log('Files:');
																	    	console.log(files);

																			ZipTask.add(sit7Path, files)
																				.then(function() {
																	  				console.log('Archieving complete');
																	  				console.log('Intermediate results in ' + unzipPath);
																	  				
																	  				// remove unzipPath folder

																	  				//  skip syncing if not photos
																					if (metaData.photos.length === 0) {
																						console.log('Skip syncing photos');
																						fileSynced();
																						return;
																					}

																					var photoSynced = _.after(metaData.photos.length, function() {
																						fileSynced();
																					});

																					metaData.photos.forEach(function(photoId) {

																						console.log('Sync photo ' + photoId);

																						var opts = {
																							id: photoId,
																							url: server.url + '/photo/bla/whatever/' + photoId,
																							path: config.photo.path,
																							ext: '.jpg',
																							model: Photos
																						}

																						downloadFile(opts, function(err) {
																							photoSynced();
																						})
																					})
																				})
																				.catch(function (err) {
																	  				console.error(err);
																				})
																		}
																	})
																}
															});

    													});
													})

										}

									})
								}
							});
						})
						.catch(function (err) {
								console.error(err);
						});
				})
			}
		})

		function getUpdatedFiles(url, utcStart, utcEnd, callback) {

			server.status = 'Авторизация не удаленном сервере';
			server.save();

			var cookie;

			request({
    			url: url + '/login2',
    			method: 'POST',
    			json: {
        			name: 'oko',
        			password: 'photoload'
    			}
			}, function(error, response, body){
			    if(error) {
			        console.log(error);
			        callback(error);
			    } else {

			    	console.log('login ok');

			    	var expressCookie = response.headers["set-cookie"];
			    	cookie = request.cookie(expressCookie['0']);
			    	cookieJar.setCookie(cookie, url);

			    	var qs = {
						timestampStart: utcStart, 
		    			timestampEnd: utcEnd,
		    			count: 1000
			    	}

					console.log('actual query:');
			    	console.log(qs);

			    	request({
		    			url: url + '/sits',
		    			jar: cookieJar,
		    			qs: qs,
		    			method: 'GET',
					}, function(error, response, body){
					    
					    if(error || !body) {
					        console.log(error);
					        callback(error);
					    } else {

					    	console.log('fetched file list');

					    	callback(null, JSON.parse(body));
						}
					});

				}
			});
		} // eo getUpdatedFiles

		/*  id
		    url
		    path          config.sits.path
		    ext           '.zip'
			model         Sits
		*/
		function downloadFile(opts, callback) {

            console.log('syncing file ' + opts.id + ' with url: ' + opts.url);

			server.status = 'Загрузка файла ' + opts.url;
			server.save();

			request(
				{
	    			url: opts.url,
	    			jar: cookieJar,
	    			method: 'GET',
	    			encoding: null
				}, 
				function(error, response, body) {

				    if (error || !body) {
						
						callback(error);
					} else {

						var filePath = opts.path + '/' + opts.id + opts.ext;

						console.log('saving to ' + filePath);

    					var file = fs.createWriteStream(filePath);

        				file.write(body);

        				file.end(function() {

        					console.log('saving ok');

        					request({
		    					url: opts.url + '/meta',
		    					jar: cookieJar,
		    					method: 'GET'
							}, function(error, response, body){
					    
					    		if(error || !body) {
					        		console.log(error);
					        		callback(error);
					    		} else {

					    			console.log('fetched meta');

					    			console.log('body: ');
									console.log(body);


					    			var metaData = JSON.parse(body);

					    			metaData.userId = 0; // replace by admin user

									opts.model.update({ id : opts.id }, { $set: metaData }, { upsert: true }, function(err, result) {

							            if (err) {
							            	console.log('error writing to db'); 
							            	callback(err);
							                return;
							            }

							            console.log('written to db'); 
							            console.log(metaData); 

							            // geo-indexing object position
							            // skip if there are no coords
							            if (!metaData.lat || !metaData.lon) {
							            	callback(null, metaData);
							            	return;
							            }

						            	opts.model.update({ id : opts.id }, 
						            		{ $set: { loc: { coordinates : [metaData.lon, metaData.lat], "type" : "Point" } } }, 
						            		{ upsert: false },
						            		function(err, result) {
									        	if (err || result !== 1) {
									        		console.log('error writing to db'); 
							            			callback(err);
							                		return;
										        }

									        	callback(null, metaData);
							        	});
							        });
								}
							});
        				});
					}
				}
			)
		} // eo downloadFile


	})
}

module.exports = router;
