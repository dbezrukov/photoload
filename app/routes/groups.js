var express = require('express');
var _ = require('underscore');

var router = express.Router();

var Groups = global.db.collection("groups");
var Users = global.db.collection("users");

// get group list==================================================
router.get('/', global.restrictUserUi, function(req, res) {
    var selfId = req.session.userData.id;

    // return groups where the current user is admin
    var query = { id: { $in: req.session.userData.groups_admin } };

    Groups.find(query).sort({ id: 1 }).toArray(function (err, items) {
        if (err) { res.status(500).send({ error: 'can not get group list' }); return; }
        res.json(items);
    });
});

// add a group ====================================================
router.post('/', global.restrictAdminUi, function(req, res) {
    global.db.nextId("groupId", function(id) {
        if (!id) { res.status(500).send({ error: 'can not create id' }); return; }

        updateOrInsertGroup(id, req.body, req.session, function(error) {
            if (error) { res.status(500).send({ error: error }); return; }
            res.send({});
        });
    });
});

// get the group ==================================================
router.get('/:id', global.restrictUserUi, function(req, res) {
    var id = parseInt(req.params.id);

    Groups.findOne({ id: id }, function(err, result) {
        if (err || !result) { res.status(500).send({ error: 'can not get group' }); return; }

        // db doesn't contain any info about group users so that we need to get it from Users collection
        var query = { groups: { $in: [id] } };

        Users.find(query).sort({ id: 1 }).toArray(function (err, items) {

            result.users = items;

            query = { groups_admin: { $in: [id] } };

            Users.find(query).sort({ id: 1 }).toArray(function (err, items) {

                result.admins = items;

                res.json(result);
            });
        });
    });
});

// delete the group ===============================================
router.delete('/:id', global.restrictAdminUi, function(req, res) {
    var id = parseInt(req.params.id);

    if (id === 1) { res.status(500).send({ error: "Запрещено удаление группы суперадминов" }); return; }

    Groups.findOne({ id: id }, function(err, result) {
        if (err || !result) { res.status(500).send({ error: 'can not get group' }); return; }

        var userIds = _.pluck(result.users, 'id');
        var adminIds = _.pluck(result.admins, 'id');

        excludeGroupFromUserRecords(id, userIds, false, function(error) {
            if (error) { res.status(500).send({ error: error }); return; }

            excludeGroupFromUserRecords(id, adminIds, true, function(error) {
                if (error) { res.status(500).send({ error: error }); return; }

                Groups.remove({ id : id }, function(err, result) {
                    if (err || result !== 1) { res.status(500).send({ error: 'can not delete group' }); return; }

                    // update current user data
                    Users.findOne({ id : req.session.userData.id }, function(err, result) {
                        if (err) { return false; }
                        
                        req.session.userData = result;
                        res.send({});
                    });
                });
            });
        });
    });
});

// update the group ===============================================
router.post('/:id', global.restrictAdminUi, function(req, res) {
    var id = parseInt(req.params.id);

    updateOrInsertGroup(id, req.body, req.session, function(error) {
        if (error) { res.status(500).send({ error: error }); return; }
        res.send({});
    });
});

module.exports = router;

function updateOrInsertGroup(groupId, data, session, callback) {

    function updateGroupUsers(newUsers, newAdmins, callback) {
        Users.find({ groups: { $in: [groupId] } }, { id: 1, _id: 0 }).toArray(function (err, result) {
            var origUsers = _.pluck(result, 'id');

            Users.find({ groups_admin: { $in: [groupId] } }, { id: 1, _id: 0 }).toArray(function (err, result) {
                var origAdmins = _.pluck(result, 'id');

                var usersExclude = _.filter(origUsers, function(el) { return newUsers.indexOf(el) < 0; });
                var adminsExclude = _.filter(origAdmins, function(el) { return newAdmins.indexOf(el) < 0; });
                var usersInclude = _.filter(newUsers, function(el) { return origUsers.indexOf(el) < 0; });
                var adminsInclude = _.filter(newAdmins, function(el) { return origAdmins.indexOf(el) < 0; });

                excludeGroupFromUserRecords(groupId, usersExclude, false, function(error) {
                    if (error) { callback(error); return; }

                    excludeGroupFromUserRecords(groupId, adminsExclude, true, function(error) {
                        if (error) { callback(error); return; }

                        includeGroupToUserRecords(groupId, usersInclude, false, function(error) {
                            if (error) { callback(error); return; }

                            includeGroupToUserRecords(groupId, adminsInclude, true, function(error) {
                                if (error) { callback(error); return; }

                                // update current user data
                                Users.findOne({ id : session.userData.id }, function(err, result) {
                                    if (err) { callback('can not update current user'); return; }

                                    session.userData = result;
                                    callback();
                                });
                            });
                        });
                    });
                });
            });
        });
    }

    function toIntArray(entities) {
        var data = [];
        entities.forEach(function(entity) {
            data.push(parseInt(entity));
        });
        return data;
    }

    // normalizing data - converting ids to integers
    var userIds = data.users ? toIntArray(data.users) : [];
    var adminIds = data.admins ? toIntArray(data.admins) : [];

    if (userIds.indexOf(session.userData.id) === -1) {
        callback('Создатель группы должен быть участником');
        return;
    }

    if (adminIds.indexOf(session.userData.id) === -1) {
        callback('Создатель группы должен быть администратором');
        return;
    }

    // write basic group data
    var groupData = {
        id: groupId,
        name: data.name,
        region: data.region
    };

    Groups.update({ id : groupId }, groupData, { upsert: true }, function(err, result){
        if (err) { callback('can not update group'); return; }

        updateGroupUsers(userIds, adminIds, function(error) {
            callback(error);
        });
    });
}

function excludeGroupFromUserRecords(groupId, userIds, isAdmin, callback) 
{
    var query = { id: { $in: userIds }, groups: { $in: [groupId] } };
    var fields = { id: 1, name: 1, groups: 1, _id: 0 };

    Users.find(query, fields).sort({ id: 1 }).toArray(function (err, result) {
        
        // detect homeless users
        if (isAdmin === false) {
            var blockingUsers = [];
        
            result.forEach(function(user) {
                if (user.groups.length === 1) {
                    blockingUsers.push(user.name);
                }
            });

            if (blockingUsers.length > 0) {
                callback('Удаление невозможно. Данная группа является единственной для пользователей: ' + blockingUsers);
                return;
            }
        }

        userIds = _.pluck(result, 'id');

        query = { id: { $in: userIds } };
        
        var update;

        if (isAdmin === true) {
            update = { $pullAll: { groups_admin : [groupId] } };
        } else {
            update = { $pullAll: { groups : [groupId] } }
        }

        Users.update(query, update, { upsert: false, multi: true }, function(err, result) {
            if (err) { callback('can not update group users'); return; }
            callback();
        });
    });
}

function includeGroupToUserRecords(groupId, userIds, isAdmin, callback) 
{
    var query = { id: { $in: userIds } };
    var field = (isAdmin === true ? 'groups_admin' : 'groups');
    
    var update;

    if (isAdmin === true) {
        update = { $addToSet: { groups_admin : groupId } };
    } else {
        update = { $addToSet: { groups : groupId } }
    }

    Users.update(query, update, { upsert: false, multi: true }, function(err, result) {
        if (err) { callback('can not update users'); return; }
        callback();
    });
}
