var config = require('./../config');
var express = require('express');
var pass = require("pass");
var _ = require('underscore');

var router = express.Router();

var Users = global.db.collection("users");
var Groups = global.db.collection("groups");

router.get('/', global.restrictUserUi, function(req, res) {
    
    res.render('main', { 
        title: 'Photoload',
        servertime: Date.now(),
        is_offline: (global.process.env.NODE_ENV === 'offline'),
        tms_url: config.tms.url,
    });
});

router.get('/login', function(req, res){

    var successRedirect = '/';

    if (req.session.successRedirect) {
    	successRedirect = req.session.successRedirect;
    	delete req.session.successRedirect;
    }

    res.render('login', { 
       	app_version: global.app_version, 
        product: 'Фотосервер',
        title: 'Авторизация',
       	is_offline: (global.process.env.NODE_ENV === 'offline'),
       	successRedirect: successRedirect
    }); 
});

router.get('/api', global.restrictUserUi, function(req, res){
    res.render('api', {}); 
});

router.get('/3d', function(req, res){
    res.render('3d', {}); 
});

router.get('/logout', function(req, res){
    req.session.destroy(function(){
        res.redirect('/login');
    });
});

router.post('/login', function(req, res) {
    
    if (!req.body.name)	{ 
       	res.status(403).send({ error: 'User not found' });
       	return;
    }

    var query = { name: req.body.name };

    Users.findOne(query, function(err, result) {
        if ( !result ) {
            res.status(403).send({ error: 'Пользователь не найден' });
            return;
        } else {
                // 1 Test passwords
                pass.validate(req.body.password, result.password, function(error, success){
                if(error){
                    res.status(403).send({ error: 'Ошибка валидации пароля' });
                    return;
                }

                if (!success) {
                    res.status(403).send({ error: 'Неверный пароль' });
                    return;
                }

                // 2 Test captcha if not offline
                if (global.process.env.NODE_ENV !== 'offline') {
                    if ( req.session.captcha !== req.body.captcha ) {
                        res.status(403).send({ error: 'Неверно введен код с картинки' });
                        return;
                    }
                }

                // 3 Test if connected already
                if ( result.id in global.loggedUsers === true ) {   
                   res.status(403).send({ error: 'Данный пользователь уже в системе' });
                   return;
                }

                // 4 Generate session
                req.session.regenerate(function() {
                    console.log('Generated session: ');
                    console.dir(req.session);
                     
                    req.session.userData = result;

                    getWorkingRegion(req.session.userData, function(region) {
                        req.session.region = region;
                        res.send({});
                    });
                });
            });
        }
    });
});

router.post('/login2', function(req, res) {
    
    if (!req.body.name) {
        res.status(403).send({ error: 'User not found'});
        return;
    }

    var query = { name: req.body.name };

    Users.findOne(query, function(err, result) {
        if ( !result ) {
            res.status(403).send({ error: 'User not found' });
            return;
        } else {
                // 1 Test passwords
                pass.validate(req.body.password, result.password, function(error, success){
                if(error){
                    res.status(403).send({ error: 'Password validation error' });
                    return;
                }

                if (!success) {
                    res.status(403).send({ error: 'Incorrect password' });
                    return;
                }

                // 2 Generate session
                req.session.regenerate(function() {
                    console.log('Generated session: ');
                    console.dir(req.session);
                     
                    req.session.userData = result;

                    res.send({ result: 'Login ok' });
                });
            });
        }
    });
});

// utulity functions
function getWorkingRegion(userData, callback) {
    
    // get working region
    var user_groups = _.union(userData.groups, userData.groups_admin);

    console.log('User groups:');
    console.log(user_groups);

    var query = { id: { $in: user_groups } };

    Groups.find(query, { region: 1, _id: 0 }).toArray(function (err, result) {
        if (err) { res.status(500).send({ error: 'can not get user region' }); return; }

        var regions = _.pluck(result, 'region');
        regions = regions.filter(function(n){ return n != undefined });

        var region = { p1Lat: -89, p1Lon: 180, p2Lat: 89, p2Lon: -180 };
    
        regions.forEach(function(el) {
        	console.log(el);
            if (el.p1Lat > region.p1Lat) { region.p1Lat = el.p1Lat; }
            if (el.p1Lon < region.p1Lon) { region.p1Lon = el.p1Lon; }
            if (el.p2Lat < region.p2Lat) { region.p2Lat = el.p2Lat; }
            if (el.p2Lon > region.p2Lon) { region.p2Lon = el.p2Lon; }
        });

        console.log('User region:');
        console.log(region);

        callback(region);
    });
}

module.exports = router;
