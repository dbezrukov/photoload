var express = require('express');
var router = express.Router();

router.get('/', global.restrictAdminUi, function(req, res) {
    var nchars = 20;
    var name = req.session.userData.name.substring(0,nchars).toLowerCase();
    if (req.session.userData.name.length > nchars) {
        name += '..';
    }

    res.render('admin', { 
        title: 'Photoload',
        user: {
            name: name,
            id: req.session.userData.id
        } 
    });
});

module.exports = router;