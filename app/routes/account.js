var express = require('express');
var config = require('../config');
var router = express.Router();

router.get('/profile', global.restrictUserUi, function(req, res) {
    var user = req.user;

    var nchars = 7;
    var name = req.session.userData.name.substring(0,nchars).toLowerCase();
    if (req.session.userData.name.length > nchars) {
        name += '..';
    }

    console.log(req.session.userData);

    var profile = {
		id: req.session.userData.id,
        name: name,
        fullname: req.session.userData.name,
		is_admin: (req.session.userData.groups_admin.length > 0),
		region: req.session.region,
		usingPlaneCoordinates: config.usingPlaneCoordinates
    };

    res.json(profile);
});

module.exports = router;
