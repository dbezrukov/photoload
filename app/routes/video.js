var express = require('express');
var router = express.Router();

var lastBoardFrames = {};

router.get('/board', function(req, res) {
    res.json({ 
        id1: addr1,
        id2: addr2
    });
});

// asking for MJPEG video stream directly
router.get('/board/:id/stream', function(req, res) {
    var id = parseInt(req.params.id);
    var fps = req.query.fps ? req.query.fps : 15;    

    console.log('Asked video for board id ' + id);
    console.log('Required FPS is ' + fps);

    if (id in lastBoardFrames === false) {
        console.log('no video for board id ' + id);
        return res.send(401, { error: 'no stream for the board' });
    }
    
    console.log('Subscribe to stream from board Id: ' + id);
    
    var boundary = 'wOOt';

    res.writeHead(200, {
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache',
        'Expires': 'Thu, 01 Dec 1994 16:00:00 GMT',
        'Connection': 'close',
        'Content-Type': 'multipart/x-mixed-replace; boundary="' + boundary + '"',
    });
    
    setInterval(function() {
        var frameData = lastBoardFrames[id].data;

        res.write('--' + boundary + '\r\n');
        res.write('Content-Type: image/jpeg\r\n');
        res.write('Content-Length: ' + frameData.length + '\r\n\r\n');
        res.write(frameData);
        res.write('\r\n');
    }, 1000 / fps);
});

// asking for MJPEG video stream directly
router.get('/board/:id/check-video', function(req, res) {
    var id = parseInt(req.params.id);

    var hasVideo = (id in lastBoardFrames);
    if (hasVideo === true) {
        var now = Date.now();
        var elapsed = now - lastBoardFrames[id].time;

        res.json({ hasVideo: (elapsed < 3000) });
    } else {
        res.json({ hasVideo: false });
    }
});

// asking video page for a board
router.get('/board/:id', function(req, res) {
    var id = parseInt(req.params.id);

    console.log('Asked video page for board id ' + id);

    var streamUrl = req.protocol + '://' + req.get('host') + req.originalUrl + '/stream';
    res.render('video', { 
        streamUrl: streamUrl
    });
});

router.post('/board/:id', function(req, res) {
    var id = parseInt(req.params.id);

    var frameData = new Buffer('');
        
    req.on('data', function(chunk) {
        frameData = Buffer.concat([frameData, chunk]);
    });

    req.on('end', function() {
        lastBoardFrames[id] = { 
            data: frameData,
            time: Date.now()
        };

        res.end();
    });
});

module.exports = router;
