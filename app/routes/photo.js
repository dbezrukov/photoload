var express = require('express');
var shortId = require('shortid');
var config = require('../config');
var fs = require('fs');
var _ = require('underscore');

var Zip = require('node-7z');
var ZipTask = new Zip();

var im = require('imagemagick');
im.taskCount = 0;
im.maxTaskCount = 1;

//var photometric = require('../addons/photometric/node/build/Release/photometric');

var router = express.Router();

//var Users = global.db.collection("users");

var Photos = global.db.collection("photos");

var sources = ['bla', 'potok'];

// получить список источников для данного типа
router.get('/', global.restrictApi, function(req, res) {
    res.json(sources);
});

// получить список альбомов для заданного источника
router.get('/:src', global.restrictApi, function(req, res) {
    if (sources.indexOf(req.params.src) === -1) {
		res.status(500).send({ error: 'no source with name ' + req.params.src }); 
    	return; 
    }

	var query = { src: req.params.src };
    var fields = { _id: 0, album: 1 };

    Photos.find(query, fields).sort({ utc: -1 }).toArray(function (err, items) {
	if (err) { res.status(500).send({ error: 'can not get albums' }); return; }
		var albums = _.uniq(_.pluck(items, 'album'));
    	res.json(albums);
	});
});

// выполнить поиск фото внутри заданного источника
router.get('/:src/query', global.restrictApi, function(req, res) {
    if (sources.indexOf(req.params.src) === -1) {
		res.status(500).send({ error: 'no source with name ' + req.params.src }); 
    	return; 
    }

	var timestampStart;
	var timestampEnd;

	if (req.query.timestampStart && req.query.timestampEnd) { // using timestamps

		timestampStart = parseInt(req.query.timestampStart);
		timestampEnd = parseInt(req.query.timestampEnd);

	} else { // using dates

		var dateStart = new Date(req.query.dateStart ? req.query.dateStart.split('-').reverse().join('-') : '02-01-1970');
		var dateEnd   = new Date(req.query.dateEnd   ? req.query.dateEnd.split('-').reverse().join('-')   : '01-01-2100');
    	
    	// including the entire da
		dateEnd.setDate(dateEnd.getDate() + 1);

		timestampStart = dateStart.getTime() / 1000;
		timestampEnd =  dateEnd.getTime() / 1000;
	}

	console.log('timestampStart: ', timestampStart);
    console.log('timestampEnd: ', timestampEnd);

	var neLat = parseFloat(req.query.region ? req.query.region.ne.lat : '89');
	var neLon = parseFloat(req.query.region ? req.query.region.ne.lon : '0');
	var swlat = parseFloat(req.query.region ? req.query.region.sw.lat : '-89');
	var swlon = parseFloat(req.query.region ? req.query.region.sw.lon : '180');

	var query = { src: req.params.src,
				  utc: { $gte: timestampStart, $lte: timestampEnd },
				  loc : { $geoWithin : 
                            { $geometry : 
				  	                { type : "Polygon", 
                                       coordinates : [[ [swlon, neLat], [neLon, neLat], [neLon, swlat], [swlon, swlat], [swlon, neLat] ]] 
                                   	} 
				            }
                        }
				};

	// apply users filter
	if (req.query.users) {
		var usersIds = req.query.users.map(function (x) { 
			return parseInt(x, 10);
		});

		query['userId'] = { $in: usersIds };
    }

    // apply boards filter
	if (req.query.albums) {
		query['album'] = { $in: req.query.albums };
    }

    //console.dir(query);

    var fields = { _id: 0 };
    var skip = parseInt(req.query.offset ? req.query.offset : 0);
    var limit = parseInt(req.query.count ? req.query.count : 75);

    Photos.find(query, fields, { limit: limit, skip: skip }).sort({ utc: -1 }).toArray(function (err, items) {
	if (err) { res.status(500).send({ error: err }); return; }
		console.log('returning %d items', items.length);

		if (config.usingPlaneCoordinates) {
			// add wgs
			for (var i = 0; i < items.length; i++) {
				//var xyStr = photometric.WGStoXY(JSON.stringify( { lat: items[i].lat, lon: items[i].lon } ));
				//var xy = JSON.parse(xyStr);
				//items[i] = _.extend(items[i], xy);
                items[i] = _.extend(items[i], xy);
			}
		}

    	res.json(items);
	});
});

// выполнить поиск фото внутри заданного источника
router.post('/:src/archive', global.restrictApi, function(req, res) {

	console.log('archive request');
	
	var archiveId = shortId.generate();

	process.nextTick(function() {
		console.log('creating archive ' + archiveId);

    	var fileList = [];

    	req.body.forEach(function(file) {
    		var fileName = config.photo.path + '/' + file.id + '.jpg';
    		fileList.push(fileName );
    	});

		var archivePath = config.archive.path + '/' + archiveId + '.7z';
		console.log('archivePath: ' + archivePath);

		console.log('files:');
		console.log(fileList);

		ZipTask.add(archivePath, fileList)
			.then(function () {
  				console.log('archive written: ' + archivePath);
  				res.json({ id: archiveId });
			})
 			// On error 
			.catch(function (err) {
  				console.error(err);
  				res.json({});
			})
	});
});

router.get('/:src/archive/:id', global.restrictApi, function(req, res) {
	var archiveId = req.params.id;
    var archivePath = config.archive.path + '/' + req.params.id + '.7z';

    var options = {
    	dotfiles: 'deny',
    	headers: {
        	'x-timestamp': Date.now(),
        	'x-sent': true
    	}
  	};

    console.log('sending file ' + archivePath);
    res.sendfile(archivePath, options, function(err) {
    	if (err) {
      		console.log(err);
      		res.status(err.status).end();
    	}
    	else {
      		console.log('archive send, deleting');
      		fs.unlink(archivePath);
    	}
    });
});

// получить список фотографий для заданного альбома
router.get('/:src/:album', global.restrictApi, function(req, res) {
    if (sources.indexOf(req.params.src) === -1) {
		res.status(500).send({ error: 'no source with name ' + req.params.src }); 
    	return; 
    }

	var query = { src: req.params.src, album: req.params.album };
    var fields = { _id: 0 };

    Photos.find(query, fields).sort({ utc: -1 }).toArray(function (err, items) {
	if (err) { res.status(500).send({ error: 'can not get photos' }); return; }
    	res.json(items);
	});
});

// write the photo itself
router.post('/:src/:album', global.restrictApi, function(req, res) {

    console.log('posting a photo');

	if (sources.indexOf(req.params.src) === -1) {
		res.status(500).send({ error: 'no source with name ' + req.params.src }); 
    	return; 
    }

    var photoId = shortId.generate();

    console.log('adding a photo, src: %s, album: %s, id: %s', req.params.src, req.params.album, photoId);

    var photoPath = config.photo.path + '/' + photoId + '.jpg';

    console.log('photoPath: %s', photoPath);
        
    var file = fs.createWriteStream(photoPath);

    req.on('data', function(chunk) {
        console.log('writing chunk');
        file.write(chunk);
    });

    req.on('end', function() {
        file.end(function() {
        	console.log('writing complete, reading photo parameters');
			console.log('photoPath: %s', photoPath);
            console.log('ImageMagick should be installed via: yum install ImageMagick ImageMagick-devel');

            im.identify(photoPath, function(err, features){
                if (err) {
                    console.log('sizeOf error');
                    res.status(500).send({ error: 'can not write photo' });
                    return;
                }
                
                console.log('saving image of size: %d x %d', features.width, features.height);

                var photoData = {
                    src: req.params.src,
                    album: req.params.album,
                    id: photoId,
                    userId: req.session.userData.id,
                    w: features.width, 
                    h: features.height,
                    utc: Date.now() / 1000
                }

                Photos.update({ id : photoId }, { $set: photoData }, { upsert: true }, function(err, result) {
                    if (err) {  
                        res.status(500).send({ error: 'can not write photo' });
                        return;
                    }
                    res.json({ id: photoId });
                });
            });
        });
    });
});

// get photo by id
router.get('/:src/:album/:id/preview', global.restrictApi, function(req, res) {
    var photoId = req.params.id;
    var photoPath = config.photo.path + '/' + req.params.id + '.jpg';
    var previewPath = config.photo.path + '/' + req.params.id + '_w256.jpg';

    fs.exists(previewPath, function(exists) {
  		
        if (exists) {
  		    res.sendfile(previewPath);
  		} else {
            function resizePhoto() {

                im.taskCount++;
                
                im.resize({
                    srcPath: photoPath,
                    dstPath: previewPath,
                    width: 256
                }, function(err, stdout, stderr){

                    im.taskCount--;

                    if (err) {
                        console.log('resize failed for: ' + photoPath);
                        res.sendfile(photoPath);
                        return;
                    } 

                    console.log('resize ok for: ' + photoPath);
                    res.sendfile(previewPath);
                });
            }

            if (im.taskCount <= im.maxTaskCount) {
                resizePhoto();
            } else { 
                var busyCheck = setInterval(function() {
                    if (im.taskCount <= im.maxTaskCount) {
                        clearInterval(busyCheck);
                        resizePhoto();
                    }
                }, 100);
            }
  		}
  	});
});

// get photo by id
router.get('/:src/:album/:id/rotated', global.restrictApi, function(req, res) {
    var photoId = req.params.id;
    var photoPath = config.photo.path + '/' + req.params.id + '.jpg';
    var rotatedPath = config.photo.path + '/' + req.params.id + '_rotated.png';

    fs.exists(rotatedPath, function(exists) {
  		
        if (exists) {
  		    res.sendfile(rotatedPath);
  		} else {
  			res.json({ err: 'no file'}); ;
  		}
  	});
});

// get photo by id
router.get('/:src/:album/:id', global.restrictApi, function(req, res) {
    var photoId = req.params.id;
    var photoPath = config.photo.path + '/' + req.params.id + '.jpg';

    res.sendfile(photoPath);
});

router.delete('/:id', global.restrictApi, function(req, res) {
	var photoId = req.params.id;
    var photoPath = config.photo.path + '/' + req.params.id + '.jpg';

    Photos.remove({ id: req.params.id}, function(err, photo) {
	    if (err) { 
	    	console.log('photos collection error');
	    	res.status(500).send({ error: 'can not be deleted from database' });
	    	return;
	    }

    	fs.unlink(photoPath, function() {
    		console.log('file deleted: ' + photoPath);
      		res.end();
    	});
    });
});

// write meta data
router.post('/:src/:album/:id/meta', function(req, res) {
    var photoId = req.params.id;

	function normalizeTypes(data) {
    	if (data.lat)   { data.lat   = parseFloat(data.lat); }
    	if (data.lon)   { data.lon   = parseFloat(data.lon); }
    	if (data.utc)   { data.utc   = parseInt(data.utc); }
    	if (data.hgt)   { data.hgt   = parseFloat(data.hgt); }
    	if (data.pch)   { data.pch   = parseFloat(data.pch); }
    	if (data.roll)  { data.roll  = parseFloat(data.roll); }
    	if (data.yaw)   { data.yaw   = parseFloat(data.yaw); }
    	if (data.hdg)   { data.hdg   = parseFloat(data.hdg); }
    	if (data.speed) { data.speed = parseFloat(data.speed); }

    	if (data.cameraF) { data.cameraF = parseFloat(data.cameraF); }
    	if (data.cameraHeight) { data.cameraHeight = parseFloat(data.cameraHeight); }
    	if (data.cameraWidth) { data.cameraWidth = parseFloat(data.cameraWidth); }

		return data;
	}

    req.body = normalizeTypes(req.body);

	// check the must-have values
	if (!req.body.lat || req.body.lat === 0 || isNaN(req.body.lat)) {
		console.log('photo lat is invalid');
		res.status(500).send({ error: 'lat is invalid' });
		return;
	}

	if (!req.body.lon || req.body.lon === 0 || isNaN(req.body.lon)) {
		console.log('photo lon is invalid');
		res.status(500).send({ error: 'lon is invalid' });
		return;
	}

	// if (req.body.utc === 0 || isNaN(req.body.utc)) {
	// 	delete req.body.utc;
	// }

    // skip utc data
    if (req.body.utc) {
		delete req.body.utc;
	}

	// set meta data
	Photos.update({ id : photoId }, { $set: req.body }, { upsert: false }, function(err, result) {
        if (err || result !== 1) {
        	console.log('err: ', err);
            res.status(500).send({ error: 'can not set meta' });
            return;
        }

        // set location
        Photos.update({ id : photoId }, { $set: { loc: { coordinates : [req.body.lon, req.body.lat], "type" : "Point" } } }, { upsert: false }, function(err, result) {
        	if (err || result !== 1) {
        		console.log('err: ', err);
            	res.status(500).send({ error: 'can not set location' });
            	return;
	        }

        	res.send({});
        });
	});
});

router.get('/:src/:album/:id/meta', function(req, res) {
    var photoId = req.params.id;

    Photos.findOne({ id: photoId }, { loc: 0 }, function(err, meta) {
        if (err || !meta) {
            res.json({});
            return;
        }

        res.json(meta);
    })
});

router.get('/:src/:album/:id/bbox', function(req, res) {
    var id = req.params.id;

    var bboxNull = { 
        sw: { lat: 0, lon: 0 },
        ne: { lat: 0, lon: 0 } 
    };

    Photos.findOne({ id: id }, function(err, photo) {
        if (err || !photo) {
            res.json(bboxNull);
            return;
        }
        
        var params = {

            photoPath: config.photo.path + '/' + req.params.id + '.jpg',
            photoWidth: photo.w,
            photoHeight: photo.h,
             
            cameraWidth: photo.cameraWidth ? photo.cameraWidth : 22.71,
            cameraHeight: photo.cameraHeight ? photo.cameraHeight : 15.16,
            cameraF: photo.cameraF ? photo.cameraF : 50.0,
             
            cameraCxOffset: 0.0,
            cameraCyOffset: 0.0,
            cameraDelta_f: 0.0,
            cameraK1: 0.0,
            cameraK2: 0.0,
            cameraP1: 0.0,
            cameraP2: 0.0,
            cameraK3: 0.0,
         
            telLon: photo.lon,
            telLat: photo.lat,
            telHeight: photo.hgt,
            telHeading: photo.yaw,
            telPitch: photo.pch,
            telRoll: photo.roll
        };
        
        if (photo.bbox) {
        	res.json(photo.bbox);
        	return;
        }

        //console.log('Calculating bounds with params');
        //console.dir(params);

        var bbox = { error: 'bbox not supported under linux server' }; // = JSON.parse( photometric.getBBox( JSON.stringify( params ) ) );

        if (bbox.error) {
            console.log('Error while calculating bbox: ', bbox.error);  
            res.json(bboxNull);
            return;
        }

        var newBbox = {
            sw: { lat: bbox.minLat, lon: bbox.minLon },
            ne: { lat: bbox.maxLat, lon: bbox.maxLon },
            w: photo.w, 
            h: photo.h
        }

        Photos.update({ id : id }, { $set: { bbox: newBbox } }, { upsert: false }, function(err, result) {
        	if (err || result !== 1) {
        		console.log('err: ', err);
            	res.json(bboxNull);
            	return;
	        }

        	res.json(newBbox);
        });

    });
});

module.exports = router;
