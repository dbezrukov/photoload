var express = require('express');
var config = require('../config');
var fs = require('fs');
var _ = require('underscore');
//var photometric = require('../addons/photometric/node/build/Release/photometric');

var router = express.Router();

/*
var params = {
    photoWidth: 5184,
    photoHeight: 3456,
     
    cameraWidth: 22.3,
    cameraHeight: 14.9,
    cameraF: 50.0,
     
    cameraCxOffset: 0.0,
    cameraCyOffset: 0.0,
    cameraDelta_f: 0.0,
    cameraK1: 0.0,
    cameraK2: 0.0,
    cameraP1: 0.0,
    cameraP2: 0.0,
    cameraK3: 0.0,
 
    telLon: 29.2144,
    telLat: 59.6320,
    telHeight: 624.0,
    telHeading: 196.6,
    telPitch: 0.4,
    telRoll: 17.5
};
*/

router.get('/wgstoxy', function(req, res) {
    
    /*
    console.log('query: ', req.query);

    var wgs = photometric.WGStoXY( JSON.stringify( req.query ) );

    //console.log('wgs: ', wgs);

    try {

        var xy = JSON.parse( wgs );
        res.json( xy );

    } catch (e){

        res.json({ x: 0, y: 0 });
    }
    */

    res.json({ x: 0, y: 0 });
});

router.get('/xytowgs', function(req, res) {
    
    /*
    var xy = photometric.XYtoWGS( JSON.stringify( req.query ) );

    try {

        var wgs = JSON.parse( xy );
        res.json( wgs );

    } catch (e){

        res.json({ lat: 0, lon: 0 });
    }
    */

    res.json({ lat: 0, lon: 0 });
});

module.exports = router;
