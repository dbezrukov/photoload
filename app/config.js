module.exports = { 
    session : {
        name: 'photoload.sid',
        secret: 'photoload'
    },
    server : {
        port : 3010
    },
    db : 'mongodb://localhost:27017/photoload',
    tms : '',
    photo : { 
        path: '/mnt/okocenter.ru/photoload/photo'
    },
    archive : {
        path: '/mnt/okocenter.ru/photoload/archive'
    },
    tiles : {
        path: '/mnt/okocenter.ru/photoload/tiles'
    },
    sits : {
        path: '/mnt/okocenter.ru/photoload/sits',
        urlFileExt: 'sse'
    },
    usingPlaneCoordinates : false
}
