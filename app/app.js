var config = require('./config');

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var session  = require('express-session');
var MongoStore = require('connect-mongostore')(session);
var MongoClient = require('mongodb').MongoClient;
var mongo = new MongoClient;
var mongoose = require('mongoose');

var path = require('path');
var async = require("async");
var require_tree = require('require-tree');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var _ = require('underscore');

var git = require('git-rev');

var Chat = require('./chat');

var chats = {};

// usernames which are currently connected to the chat
global.loggedUsers = {};

function initializeApplication(){

	var routers = require_tree('./routes');

    app.set('views', path.join(__dirname, 'views'));
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');

    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());
    app.use(cookieParser());

    var oneDay = 86400000;
    app.use(express.static(path.join(__dirname, '/public'), { maxAge: oneDay }));

    app.use(session({
        name: config.session.name,
        secret: config.session.secret,
        saveUninitialized: true,
        resave: true,
        store: new MongoStore({ db: global.db})
    }));

    app.use(favicon(__dirname + '/public/img/favicon.ico'));

    app.use('/',            routers.root);
    app.use('/admin',       routers.admin);
    app.use('/account',     routers.account);
    app.use('/users',       routers.users);
    app.use('/groups',      routers.groups);
    app.use('/photo',       routers.photo);
    app.use('/tiles',       routers.tiles);
    app.use('/sits',        routers.sits);
    app.use('/sync',        routers.sync);
    app.use('/video',       routers.video);
    app.use('/photometric', routers.photometric);

    console.log('Starting Express 4.0 on port ' + config.server.port);
    server.listen(config.server.port);
}

// Node exception handler
process.on('uncaughtException', function(err) {
    console.log('Exception handled: ');
    console.log(err);
});

async.parallel({
    restrictions: function(callback) {
        global.restrictUserUi = function (req, res, next) {
            if (req.session && req.session.userData) {
                next();
            } else {
                
                console.log('access denied, redirecting to the login page');

                req.session.error = 'access denied';
                req.session.successRedirect = req.originalUrl;

                console.log('success redirect is: ' + req.session.successRedirect);

                res.redirect('/login');
            }
        }
        global.restrictAdminUi = function (req, res, next) {
            if (req.session && req.session.userData && req.session.userData.is_admin === 'true') {
                next();
            } else {

                console.log('access denied, redirecting to the login page');

                req.session.error = 'access denied';
                req.session.successRedirect = req.originalUrl;

                console.log('success redirect is: ' + req.session.successRedirect);

                res.redirect('/login');
            }
        }
        global.restrictApi = function (req, res, next) {
            if (req.session && req.session.userData) {
                next();
            } else {
                req.session.error = 'access denied';
                res.status(403).send({ error: 'Authorization needed' });
            }
        }
        callback();
    },
    app_version: function(callback){
        console.log('Acquiring app version');
        git.tag(function (str) {
            global.app_version = str;
            callback();
        })
    },
    database_mongo: function(callback){
        mongo.connect(config.db, function(err, db){
            
            if(err) throw err;
            console.log('Mongo client connected');
            
            global.db = db;

            var Counters = db.collection("counters");
            
            // numerical id with atomic inrement
            global.db.nextId = function(name, callback) {
                var query = { name: name };
                var sort = [];
                var update = { $inc: { lastId: 1 } };
                var options = { new: true };

                // returns 'undefined' in case counters couldn't be found
                Counters.findAndModify(query, sort, update, options, function(err, object) {
                    var lastId;
                    if (!err && object) {
                        lastId = object.lastId;
                    }
                    callback(lastId);
                });
            }

            var Photos = db.collection("photos");
            Photos.ensureIndex( { loc : "2dsphere" }, function(error) {
  				if (error) {
   					console.error(error);
  				}
  				callback();
  			});
        });
    },
    database_mongoose: function(callback){
    	mongoose.connect(config.db);
		mongoose.connection.on("open", function(err, db){

			if(err) throw err;
            
            callback();
		});
    }
}, function(){
    initializeApplication();
});
