function Chat(chatId, chatCollection) 
{
    this.chatId = chatId;
    this.collection = chatCollection;
    this.history = [];

    me = this;

    // initial chats loading
    me.collection.findOne({ id: chatId }, function(err, result) {
        if (err) {
            console.error('Chat: can not read chat');
            return;
        }

        if (result) {
            me.history = result.history;
            return;

        } else {
            me.collection.insert({ id: chatId, history: [] }, function(err, result){
                if (err || !result) {
                    console.error('Chat: can not create chat');
                }
            });
        }
    });
}

Chat.prototype.getId = function()
{
    return this.chatId;
}

Chat.prototype.getHistory = function()
{
    return this.history;
}

Chat.prototype.pushMessage = function(message)
{
    /* message:
        authorId
        text
    */

    message.time = Date.now();

    this.history.push(message);

    this.collection.update({ id: this.chatId }, { $push: { history : message } }, 
        function(err, result){
            if (err || !result) {
                console.error('Chat: can not write a message');
            }
    });

    // exit immediately
    return this;
}

module.exports = Chat;